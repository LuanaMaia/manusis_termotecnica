<?php
/**
* Corpo do Manusis - Arquivo geral que administra o fluxo das informa��es
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package engine
*/

// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
    die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
    die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
    die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
    die ($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/delcascata.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/autent.php")) {
    die ($ling['autent01']);
}
elseif (!require("lib/forms.php")) {
    die ($ling['autent01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
    die ($ling['mod01']);
}
// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
    $manusis['tema']="padrao";
}



// Buscando dados
$ajax=(int)$_GET['ajax'];
$deleta=$_GET['deleta'];
$deleta_s=(int)$_GET['deleta_s'];
$deletaarq=$_GET['deletaarq'];
$oq=(int)$_GET['oq'];
$logar = ($_GET['logar'] == true)? true : false;

if ($ajax == 0) {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
     <meta http-equiv=\"content-type\" CONTENT=\"text/html; charset=iso-8859-1\" />
    <title>{$ling['deletando']}</title>
    <link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>
    <script>
    var idel = 0;
    function atualiza_tela() {
        var corpo = document.getElementById('corpo_deleta').innerHTML;
        if ((corpo != '') || (idel == 0)) {
            atualiza_area2('corpo_deleta', 'deleta.php?ajax=1&deleta=$deleta&deleta_s=$deleta_s&deletaarq=$deletaarq&oq=$oq&logar=$logar&confirma=1', 'atualiza_tela', false);
        }
        else {
            // Bloqueando a pagina
            bloqueiaPagina(false);
            parent.location.reload();
        }
        
        idel ++;
    }
    
    window.onload = function() {
        atualiza_tela ();
    }

    </script>
    </head>
    <body>
    <h3>{$ling['deletando']} " . $tdb[$deleta]['DESC'] . "...</h3>
    <div id=\"corpo_deleta\" style=\"text-align:left\">\n";
}

// Deletando o item desejado caso tenha confirmado a a��o
if (($deleta != "") and ($_GET['confirma'] == 1)) {
    
    // Deletando arquivos somente
    if ($deletaarq != "") {
        $deletaarq=str_replace("..","",$deletaarq);
        $deletaarq=str_replace("/","",$deletaarq);
        $deletaarq=str_replace("\\","",$deletaarq);

        if ($deleta == "graficos") @unlink("../../".$manusis['dir']["graficos"]."/$deletaarq");
        elseif(file_exists($manusis['dir'][$deleta]."/$oq/$deletaarq")) @unlink($manusis['dir'][$deleta]."/$oq/$deletaarq");
    }
    // Deletando dados
    else {
        $ok = DelCascata($deleta, $oq, 'MID', true, true);
        
        foreach ($_SESSION[ManuSess]['del'] as $tab => $def) {
            if ($def['cont'] > 0) {
                echo $tdb[$tab]['DESC'] . ": " . $def['cont'] . "<br />\n";
            }
        }
    }
}
if ($ajax == 0) {
    echo "</div>
    </body>
    </html>";
}
?>
