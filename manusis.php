<?
/**
* Corpo do Manusis - Arquivo geral que administra o fluxo das informaÃ§Ãµes
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package engine
*/

// FunÃ§Ãµes do Sistema
if (!require("lib/mfuncoes.php")) {
    die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// ConfiguraÃ§Ãµes
elseif (!require("conf/manusis.conf.php")) {
    die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
    die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstraÃ§Ã£o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
    die ($ling['bd01']);
}
// InformaÃ§Ãµes do banco de dados
elseif (!require("lib/bd.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/delcascata.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/autent.php")) {
    die ($ling['autent01']);
}
elseif (!require("lib/forms.php")) {
    die ($ling['autent01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
    die ($ling['mod01']);
}
// Caso nÃ£o exista um padrÃ£o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
    $manusis['tema']="padrao";
}

// Variaveis de direcionamento
$st=(int)$_GET['st'];
$id=(int)$_GET['id']; // Modulo
$op=(int)$_GET['op']; // OperaÃ§Ã£o do modulo
if ($op == "") $op=1;
$act=(int)$_GET['act'];
$foq=(int)$_GET['foq'];
$exe=(int)$_GET['exe'];
$f=$_GET['f'];
$oq = (int) $_GET['oq'];
if ($st == "") {
    // Montando XML do Arquivo
    // Header("Content-Type: application/xhtml+xml");
    $Navegador = array (
    "MSIE",
    "OPERA",
    "MOZILLA",
    "NETSCAPE",
    "FIREFOX",
    "SAFARI"
    );
    $info['browser'] = "OTHER";
    foreach ($Navegador as $parent) {
        $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
        $f = $s + strlen($parent);
        $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
        $version = preg_replace('/[^0-9,.]/','',$version);

        if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
            $tmp_navegador['browser'] = $parent;
            $tmp_navegador['version'] = $version;
        }
    }
    if ($tmp_navegador['browser'] == "MSIE"){
        $frame_scroll="yes";
    }
    else {
        $frame_scroll="auto";
    }
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
     <meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />
	 <meta http-equiv=\"pragma\" content=\"no-cache\" />
	 <meta name=\"robots\" content=\"none\" />
    <title>{$ling['manusis']}</title>
    <link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link rel=\"shortcut icon\" href=\"favicon.ico\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>
    <script type=\"text/javascript\" src=\"lib/jquery/jquery-1.4.2.min.js\"> </script>
    <script type=\"text/javascript\" src=\"lib/jquery/jquery.livequery.js\"> </script>\n
    <script type=\"text/javascript\" src=\"lib/jquery/jquery.manusis.js\"> </script>\n";
   
    
    if (($tmp_navegador['browser'] == "MSIE") and ($tmp_navegador['version'] == "6.0")) {
        echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    }
    
    echo "</head>
    <body>
    <div id=\"central\">
    <div id=\"cab\">
    <h1>{$ling['manusis']}</h1>
    <h2 id=\"titulo\">";

    // Mostrando a Empresa Matriz
    $emp_mostra = htmlentities(VoltaValor(EMPRESAS, "NOME", "EMP_MATRIZ", 1)) ;
   
    if ($emp_mostra == '' ) {
        $emp_mostra = $emp_mostra;      
    }
    
    echo "$emp_mostra <br/>" . $manusis['modulo'][$id]['nome'];

    if ($manusis['modulo'][$id]['menu'][$op] != "") {
        echo " - ".$manusis['modulo'][$id]['menu'][$op]."</h2>\n";
    }
    elseif ($manusis['modulo'][$id]['menu_admin'][$op] != "") {
        echo " - ".$manusis['modulo'][$id]['menu_admin'][$op]."</h2>\n";
    }
    $tema_imagens = "temas/".$manusis['tema']."/imagens";
    
    if ((int)$_SESSION[ManuSess]['user']['MID'] == 0) {
        $js_argumentos = "'USUARIO', '','0','1','0','1','2', " . $form['USUARIO'][0]['altura'] . ", " . $form['USUARIO'][0]['largura'] . "";
    } else {
        $js_argumentos = "'USUARIO','" . (int)$_SESSION[ManuSess]['user']['MID'] . "','0','2','1','0','1', " . $form['USUARIO'][0]['altura'] . ", " . $form['USUARIO'][0]['largura'] . ",'',''";
    }
    
    echo "</h2>
    <div style=\"font-size: 8px; position:absolute; display:block; width:100%; text-align:right; top:45px; right:10px;border:1px;\" />
    
    <a class=\"link\" href=\"javascript:abre_janela_form($js_argumentos)\"><img src=\"imagens/icones/22x22/usuarios.png\" border=\"0\" alt=\"\" /> {$ling['sol_usuario']}</a> &nbsp; &nbsp; &nbsp;
    <a class=\"link\" href=\"manusis.php\"><img src=\"$tema_imagens/home.gif\" border=\"0\" alt=\"\" /> {$ling['prin00']}</a> &nbsp; &nbsp; &nbsp;
    <a class=\"link\" href=\"index.php?sair=1\" onclick=\"return confirma(this, '{$ling['log_sair_manusis']}')\"><img src=\"$tema_imagens/logout.gif\" border=\"0\" alt=\"\" /> {$ling['prin_sair']}</a> &nbsp; &nbsp; &nbsp;
   
<input size=\"35\" id=\"ocr_busca\" onkeypress=\"return EnviaForm(this,event,'buscar_ocorrencias.php?texto=')\" name=\"texto\" value=\"{$ling['busca_histo_servi']}\" class=\"campo_text\" onclick=\"this.value=''\" />
<input type=\"hidden\" name=\"datai\" value=\"0\" />
<input type=\"hidden\" name=\"dataf\" value=\"0\" />
    <img src=\"imagens/icones/16x16/ir.png\" valign=\"bottom\"border=\"0\" alt=\"\" />
";
    echo "</div>\n";
    echo "<ul id=\"menu_principal\">\n\n";
    $si=0;
    foreach ($manusis['modulo'] as $i => $mod_info) {

            $si++;
            echo "<li onmouseover=\"menu_sobre($i)\" onmouseout=\"menu_fora($i)\"><a href=\"javascript:void(0);\" title=\"".$manusis['modulo'][$i]['nome']."\">".$manusis['modulo'][$i]['nome']."</a>
<ul id=\"m_$i\">\n";
            if($manusis['modulo'][$i]['menu']){
                foreach ($manusis['modulo'][$i]['menu'] as $ii => $i_info) { // com foreach os indexes nÃ£o precisam ser sequenciais. pode ter 1, 2, 7, 8
                    echo "<li><a href=\"manusis.php?id=$i&amp;op=$ii\" title=\"".$manusis['modulo'][$i]['menu'][$ii]."\">".$manusis['modulo'][$i]['menu'][$ii]."</a></li>\n";
                }
            }
            if (($_SESSION[ManuSess]['user']['MID'] == "ROOT") and ($i == 0)) {
                foreach ($manusis['modulo'][$i]['menu_admin'] as $ii => $mod_info) {
                    echo "<li><a href=\"manusis.php?id=$i&amp;op=$ii\" title=\"".$manusis['modulo'][$i]['menu_admin'][$ii]."\">".$manusis['modulo'][$i]['menu_admin'][$ii]."</a></li>\n";
                }
            }
            echo "</ul>\n</li>\n\n";
        
    }
    echo "</ul>
</div>
<div id=\"div_formulario\" class=\"moverobj\"></div>
<div id=\"corpo\">\n";
}
if ($st) {
    $Navegador = array (
    "MSIE",
    "OPERA",
    "MOZILLA",
    "NETSCAPE",
    "FIREFOX",
    "SAFARI"
    );
    $info[browser] = "OTHER";

    foreach ($Navegador as $parent) {
        $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
        $f = $s + strlen($parent);
        $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
        $version = preg_replace('/[^0-9,.]/','',$version);

        if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
            $tmp_navegador['browser'] = $parent;
            $tmp_navegador['version'] = $version;
        }
    }
    if ($tmp_navegador['browser'] == "MSIE"){
        //
        $frame_scroll="yes";
    }
    else {
        $frame_scroll="auto";
    }
}
if (($st == "") or ($st == 1)) {
    if (($si == 0) and ($st == "")) {
        if ($op == "") {
            $op=1;
        }
        $id=4;
        require("modulos/ordens/solicitacao.php");
    }
    else {
        // Incluindo o Modulo e suas funÃ§Ãµes
        Setar_Usuario_Offline();
        if ($id == "") {
            require("modulos/principal/modulo.php");
        }
        elseif ($manusis['modulo'][$id] != "") {
            require($manusis['modulo'][$id]['dir']."/modulo.php");
        }
    }
}

// sistema anti logout
if ($st == "") {
    echo "
    <br clear=\"all\" />
    <iframe src=\"parametros.php?id=anti_logout\" frameborder=\"0\" scrolling=\"no\" style=\"width:95%; height:20px; border:0px;\"></iframe>
    </div>
    <div id=\"rodape\"></div>";
}

if ($st == 2) {
    $f=$_GET['f'];
    $frame_altura=$form[$f][0]['altura'] - 16;
    echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"$frame_altura\" frameborder=\"0\" scrolling=\"$frame_scroll\" src=\"form.php?f=$f&act=$act&oq=$oq&id=$id&op=$op&exe=$exe&foq=$foq&form_recb_valor=".$_GET['form_recb_valor']."&atualiza=".$_GET['atualiza']."\"> </iframe>
</div>";
}

elseif ($st == 3) {
    $f=$_GET['f'];
    echo "<span id=\"div_formulario_cab\">".$ling['anexo_titulo']."
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"385\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"form.php?idf=arq&f=$f&act=$act&id=$id&oq=$oq\"> </iframe>
</div>";
}

if ($st == 4) {
    $osmid=$_GET['osmid'];
    echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"400\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"apontaos.php?osmid=$osmid\"> </iframe>
</div>";
}

if ($st == 5) {
    $osmid=$_GET['osmid'];
    $ap=(int)$_GET['ap'];
    $idModulo = (int) $_GET['idModulo'];
    $speedcheck = (int) $_GET['speedcheck'];
    $op = (int) $_GET['op'];
    $arq="apontaosplan.php";

    echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"400\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"$arq?osmid=$osmid&ap=$ap&idModulo={$idModulo}&op={$op}&speedcheck=$speedcheck\"> </iframe>
</div>";
}

if ($st == 6) {
    $foq=(int)$_GET['foq'];
    echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"400\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"progra.php?foq=$foq\"> </iframe>
</div>";
}

if ($st == 7) {
    $foq=(int)$_GET['foq'];
    echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"".$ling['fechar']."\" /></a>
</span>
<div id=\"div_formulario_corpo\">
<iframe width=\"100%\" height=\"400\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"sol.php?foq=$foq\"> </iframe>
</div>";
}

if ($st == 8) {
    // Buscando dados
    $deleta=$_GET['deleta'];
    $deleta_s=(int)$_GET['deleta_s'];
    $deletaarq=$_GET['deletaarq'];
    $oq=(int)$_GET['oq'];
    $logar = $_GET['logar'];
    
    echo "<div id=\"div_formulario_corpo\">
    <iframe width=\"100%\" height=\"100\" scrolling=\"$frame_scroll\" frameborder=\"0\" src=\"deleta.php?deleta=$deleta&deleta_s=$deleta_s&deletaarq=$deletaarq&oq=$oq&logar=$logar&confirma=1\"> </iframe>
    </div>";
}

if ($st == "") {
    echo"
</div>
</body>
</html>";
}

echo "<script type=\"text/javascript\">
var __lc = {};
__lc.license = 5995291;
 
(function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>";
?>
