<?
/**
* Link Materiais
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package Cadastro
* @subpackage  Estrutural
*/

// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
	die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
	die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
	die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
	die ($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
	die ($ling['bd01']);
}
// Autentifica��o
elseif (!require("lib/autent.php")) {
	die ($ling['autent01']);
}
// Formul�rios
elseif (!require("lib/forms.php")) {
	die ($ling['bd01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
	die ($ling['mod01']);
}

if ($mid_velho) {
	$conjunto_mid = (int)VoltaValor(MAQUINAS_CONJUNTO_MATERIAL,'MID_CONJUNTO','MID',$mid_velho,0);
	$mat_mid = (int)VoltaValor(MAQUINAS_CONJUNTO_MATERIAL,'MID_MATERIAL','MID',$mid_velho,0);
	$mat_pai_mid = (int)VoltaValor(MAQUINAS_CONJUNTO_MATERIAL,'MID_MATERIAL_PAI','MID',$mid_velho,0);
	$qtd = (int)VoltaValor(MAQUINAS_CONJUNTO_MATERIAL,'QUANTIDADE','MID',$mid_velho,0);
}


// Mensagens de erro
if ($_POST['env_form'] != "") {
	$erromsg = '';
	if (!$_POST['conjunto']) $erromsg .= "<li>{$ling['campo_obrigatorio']}: {$ling['sol_posicao']}</li>";
	if (!$_POST['mat']) $erromsg .= "<li>{$ling['campo_obrigatorio']}: {$ling['rel_desc_material']}</li>";
	if (!$_POST['qtd']) $erromsg .= "<li>{$ling['campo_obrigatorio']}: {$ling['qtd']}</li>";
	if ($erromsg) $erromsg = "<ul>$erromsg</ul>";
}

$ajax=$_GET['ajax'];
$mid_maq=(int)$_REQUEST['mid_maq'];
$mid_velho = (int)$_REQUEST['mid'];
//die("[$mid_velho]");

$conjunto_mid = (int)$_REQUEST['conjunto'];
$mat_mid = (int)$_REQUEST['mat'];
$mat_pai_mid = (int)$_REQUEST['mat_pai'];
$qtd = (int)$_REQUEST['qtd'];

// Vari�veis que s�o na verdade constantes:
$phpself = 'linka_mat.php';


// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$fpos = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $$fpos, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);

	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}



if ($ajax == 'conjunto') {
	echo "
	<label class=\"campo_label\" for=\"mat\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MATERIAL']."</label>";
	$acao = "atualiza_area2('dmat_pai','$phpself?ajax=mat&mid_maq=$mid_maq&conjunto=$conjunto_mid&mat=' + this.options[this.selectedIndex].value)";
	FormSelectD('COD','DESCRICAO',MATERIAIS,$mat_mid,'mat','mat','MID','','campo_select_ob',$acao,"",'A','COD');
	echo "<div id=\"dmat_pai\"></div>";
}

if ($ajax == 'mat') {
	
	$sql = "SELECT MID_MATERIAL FROM ".MAQUINAS_CONJUNTO_MATERIAL." WHERE MID_CONJUNTO = '$conjunto_mid' AND MID_MATERIAL_PAI = '0'";
	$tmp=$dba[0] ->Execute($sql);
	$sqlfil = '';
	while (!$tmp->EOF) {
		$campo = $tmp->fields;
		AddStr($sqlfil,' OR ',"MID = '{$campo['MID_MATERIAL']}'");
		$tmp->MoveNext();
	}
	if ($sqlfil) $sqlfil = "($sqlfil)";
	else $sqlfil = "MID = '-1'"; // for�a 0 resultados
	
	AddStr($sqlfil,' AND ',"MID != '$mat_mid'");
	if ($sqlfil) $sqlfil = "WHERE $sqlfil";
	echo "<br clear=\"all\" />
	<label class=\"campo_label\" for=\"mat_pai\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MATERIAL_PAI']."</label>";
	$acao = "";
	FormSelectD('COD','DESCRICAO',MATERIAIS,$mat_pai_mid,'mat_pai','mat_pai','MID','','campo_select',$acao,$sqlfil,'A','COD');
	echo "<br clear=\"all\" />
	<label class=\"campo_label\" for=\"mat\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['QUANTIDADE']."</label>
	<input type=text name=\"qtd\" value=\"{$qtd}\" class=\"campo_text_ob\">";
}


if ($ajax == "") {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body class=\"body_form\">
<div id=\"formularioos\">";

	if ($_POST['env_form'] != "")  {
		// ENVIANDO
		if ($erromsg) erromsg($erromsg);
		else {
			$mid_mat = (int)$_POST['mat'];
			$mid_mat_pai = (int)$_POST['mat_pai'];
			$mid_conj = (int)$_POST['conjunto'];
			$qtd = (float)$_POST['qtd'];
	
			if ($mid_velho) {
				$sql = "UPDATE ".MAQUINAS_CONJUNTO_MATERIAL." SET
				 `MID_MATERIAL` = '$mid_mat',
				 `QUANTIDADE` = '$qtd',
				 `MID_MAQUINA` = '$mid_maq',
				 `MID_CONJUNTO` = '$mid_conj',
				 `MID_MATERIAL_PAI` = '$mid_mat_pai'
				 WHERE `MID` = '$mid_velho'";
			}
			else {
				$mid=GeraMid(MAQUINAS_CONJUNTO_MATERIAL,"MID",0);
				$sql = "INSERT INTO ".MAQUINAS_CONJUNTO_MATERIAL." SET
				 `MID_MATERIAL` = '$mid_mat',
				 `QUANTIDADE` = '$qtd',
				 `MID_MAQUINA` = '$mid_maq',
				 `MID_CONJUNTO` = '$mid_conj',
				 `MID_MATERIAL_PAI` = '$mid_mat_pai',
				 `MID` = '$mid'";
			}

				//die($sql);
			$tmp=$dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']] -> Execute($sql);
			if (!$tmp) erromsg($dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']] -> ErrorMsg());

			die("<script>
			parent.location.href='manusis.php?id=1&op=1&exe=65&oq=$mid_maq';
			</script>");
			
		}
	}
		$cc=$_POST['cc'];
		echo "<form name=\"form\" method=\"POST\" action=\"\">
		<input type=\"hidden\" name=\"mid_maq\" value=\"{$_GET['mid_maq']}\">

	<fieldset><legend>{$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC']}</legend>
	<label for=\"conjunto\" class=\"campo_label\">{$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_CONJUNTO']} </label>";
	$acao = "atualiza_area2('dmat','$phpself?ajax=conjunto&mid_maq=$mid_maq&conjunto=' + this.options[this.selectedIndex].value)";
	$sqlfil = "WHERE MID_MAQUINA = '$mid_maq'";
	FormSelectD('TAG','DESCRICAO',MAQUINAS_CONJUNTO,$conjunto_mid,'conjunto','conjunto','MID','','campo_select_ob',$acao,$sqlfil,'A','TAG');
	echo "<br clear=\"all\" />
	<div id=\"dmat\">";
	
	if ($conjunto_mid) {
		echo "
		<label class=\"campo_label\" for=\"mat\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MATERIAL']."</label>";
		$acao = "atualiza_area2('dmat_pai','$phpself?ajax=mat&mid_maq=$mid_maq&conjunto={$conjunto_mid}&mat=' + this.options[this.selectedIndex].value)";
		FormSelectD('COD','DESCRICAO',MATERIAIS,$mat_mid,'mat','mat','MID','','campo_select_ob',$acao,"",'A','COD');
		echo "<div id=\"dmat_pai\">";

		if ($mat_mid) {
			$sql = "SELECT MID_MATERIAL FROM ".MAQUINAS_CONJUNTO_MATERIAL." WHERE MID_CONJUNTO = '{$conjunto_mid}' AND MID_MATERIAL_PAI = '0'";
			$tmp=$dba[0] ->Execute($sql);
			$sqlfil = '';
			while (!$tmp->EOF) {
				$campo = $tmp->fields;
				AddStr($sqlfil,' OR ',"MID = '{$campo['MID_MATERIAL']}'");
				$tmp->MoveNext();
			}
			if ($sqlfil) $sqlfil = "($sqlfil)";
			else $sqlfil = "MID = '-1'"; // for�a 0 resultados
			
			AddStr($sqlfil,' AND ',"MID != '{$mat_mid}'");
			if ($sqlfil) $sqlfil = "WHERE $sqlfil";
			echo "<br clear=\"all\" />
			<label class=\"campo_label\" for=\"mat_pai\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MATERIAL_PAI']."</label>";
			$acao = "";
			FormSelectD('COD','DESCRICAO',MATERIAIS,$mat_pai_mid,'mat_pai','mat_pai','MID','','campo_select',$acao,$sqlfil,'A','COD');
			echo "<br clear=\"all\" />
			<label class=\"campo_label\" for=\"mat\">".$tdb[MAQUINAS_CONJUNTO_MATERIAL]['QUANTIDADE']."</label>
			<input type=text name=\"qtd\" value=\"{$qtd}\" class=\"campo_text_ob\">";
		}
		echo "</div>"; // fim do div de quantidade
	}
	echo "</div>"; // fim do div de material
	echo "</fieldset>

	<br /><input type=\"submit\" class=\"botao\" name=\"env_form\" value=\"{$ling['gravar']}\" />";

		echo "
	</form></div>
	</body>
	</html>";
	//}
}

?>
