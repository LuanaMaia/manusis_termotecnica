<?
function tumb_jpg($imgname,$size){
	//Header("Content-Type: image/jpeg");
	$img_src=ImageCreateFromjpeg ($imgname);
	$true_width=imagesx($img_src);
	$true_height=imagesy($img_src);

	if ($true_width>=$true_height){
		$width=$size;
		$height=($width/$true_width)*$true_height;
	}
	else {
		$height=$size;
		$width = ($height/$true_height)*$true_width;
	}
	$img_des = ImageCreateTrueColor($width,$height);
	imagecopyresized ($img_des, $img_src, 0, 0, 0, 0, $width, $height, $true_width, $true_height);
	return $img_des;
}
function tumb_png($imgname,$size){
	//Header("Content-Type: image/png");
	$img_src=ImageCreateFrompng ($imgname);
	$true_width=imagesx($img_src);
	$true_height=imagesy($img_src);

	if ($true_width>=$true_height){
		$width=$size;
		$height=($width/$true_width)*$true_height;
	}
	else {
		$height=$size;
		$width = ($height/$true_height)*$true_width;
	}
	$img_des = ImageCreateTrueColor($width,$height);
	imagecopyresized ($img_des, $img_src, 0, 0, 0, 0, $width, $height, $true_width, $true_height);
	return $img_des;
}
if (($_GET['i'] != "") and ($_GET['s'] != "")) {
	$t=explode(".",$_GET['i']);
	if ($t[1] == "png") {
		$img=tumb_png($_GET['i'],$_GET['s']);
		imagepng($img);
	}
	if ($t[1] == "jpg") {
		$img=tumb_jpg($_GET['i'],$_GET['s']);
		imagejpeg($img);
	}
}
?>
