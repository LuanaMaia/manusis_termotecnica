<?
/**
 * Rela&ccedil;&atilde;o entre as tabelas dos bancos de dados
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package manusis
 * @subpackage  engine
 */
$i=0;
while($manusis['db'][$i]['driver'] != "") {
    $dba[$i] = ADONewConnection($manusis['db'][$i]['driver']);
    $dba[$i]->SetFetchMode(ADODB_FETCH_ASSOC);
    if (!$dba[$i]->Connect($manusis['db'][$i]['host'],$manusis['db'][$i]['user'],$manusis['db'][$i]['senha'],$manusis['db'][$i]['base'])) errofatal($ling['bd02']);
    if ($manusis['debug'] == 1) $dba[$i]->debug = true;
    $i++;
}
/**
 * Retorna um array com as informações de relação de um campo.
 *
 * @param string $tabela
 * @param string $campo
 * @return array
 */
function VoltaRelacao ($tabela, $campo) {
    global $tdb;
    $VoltaRelacao=$_SESSION[ManuSess]['VoltaValor'];
    if ($tabela == EMPRESAS) {
        switch ($campo) {
            case "EMP_MATRIZ" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    if ($tabela == AREAS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == CENTRO_DE_CUSTO) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "PAI" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == ESTOQUE_ENTRADA) {
        switch ($campo) {
            case "MID_ALMOXARIFADO" :
                $tmp['tb']=ALMOXARIFADO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    if ($tabela == ESTOQUE_SAIDA) {
        switch ($campo) {
            case "MID_ALMOXARIFADO" :
                $tmp['tb']=ALMOXARIFADO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == USUARIOS_PERMISSAO) {
        switch ($campo) {
            case "GRUPO" :
                $tmp['tb']=USUARIOS_GRUPOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MODULO" :
                $tmp['tb']=USUARIOS_MODULOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "PERMISSAO" :
                $tmp['tb']=USUARIOS_PERMISSAO_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    if ($tabela == USUARIOS) {
        switch ($campo) {
            case "GRUPO" :
                $tmp['tb']=USUARIOS_GRUPOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="USUARIO_GRUPO";
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="FUNCIONARIO";
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == USUARIOS_PERMISAO_SOLICITACAO) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="";
                return $tmp;
                break;
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == USUARIOS_PERMISAO_CENTRODECUSTO) {
        switch ($campo) {
            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    if ($tabela == LOGS) {
        switch ($campo) {
            case "TIPO" :
                $tmp['tb']=LOGS_TIPOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    if ($tabela == SETORES) {
        switch ($campo) {
            case "MID_AREA" :
                $tmp['tb']=AREAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="AREA";
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    
    if ($tabela == USUARIOS_GRUPOS) {
        switch ($campo) {
            case "GRUPO_MOLDES" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=0;
                return $tmp;
                break;
        }
    }

    if ($tabela == MAQUINAS) {
        switch ($campo) {
            case "FAMILIA" :
                $tmp['tb']=MAQUINAS_FAMILIA;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MAQUINAS_FAMILIA";
                return $tmp;
                break;
            case "CLASSE" :
                $tmp['tb']=MAQUINAS_CLASSE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "STATUS" :
                $tmp['tb']=MAQUINAS_STATUS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="CENTRO_DE_CUSTO";
                return $tmp;
                break;
            case "MID_SETOR" :
                $tmp['tb']=SETORES;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="SETOR";
                return $tmp;
                break;
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['campo']="NOME";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="";
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
            case "FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="FORNECEDOR";
                return $tmp;
                break;
            case "FABRICANTE" :
                $tmp['tb']=FABRICANTES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="FORNECEDOR2";
                return $tmp;
                break;
        }
    }
    elseif ($tabela == MAQUINAS_FAMILIA) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ESPECIALIDADES) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == NATUREZA_SERVICOS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == CAUSA) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == DEFEITO) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == SOLUCAO) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == MAQUINAS_CONJUNTO) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;

            case "FABRICANTE" :
                $tmp['tb']=FABRICANTES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="FORNECEDOR2";
                return $tmp;
                break;

            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == MAQUINAS_CONJUNTO_MATERIAL) {
        switch ($campo) {
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_CLASSE" :
                $tmp['tb']=MATERIAIS_CLASSE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == MAQUINAS_CONTADOR) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;

            case "TIPO" :
                $tmp['tb']=MAQUINAS_CONTADOR_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == MAQUINAS_FORNECEDOR) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;

            case "MID_FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="FORNECEDOR";
                return $tmp;
                break;
        }
    }

    elseif ($tabela == LANCA_CONTADOR) {
        switch ($campo) {
            case "MID_CONTADOR" :
                $tmp['tb']=MAQUINAS_CONTADOR;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == EQUIPAMENTOS_MATERIAL) {
        switch ($campo) {
            case "MID_EQUIPAMENTO" :
                $tmp['tb']=EQUIPAMENTOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="MATERIAIS";
                return $tmp;
                break;
            case "MID_CLASSE" :
                $tmp['tb']=MATERIAIS_CLASSE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="MATERIAIS_CLASSE";
                return $tmp;
                break;
        }
    }

    elseif ($tabela == EQUIPAMENTOS) {
        switch ($campo) {
            case "FAMILIA" :
                $tmp['tb']=EQUIPAMENTOS_FAMILIA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="EQUIPAMENTO_FAMILIA";
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
                
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['campo']="NOME";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=2;
                $tmp['del_cascata']=1;
                return $tmp;
                break;    
            
            case "FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="FORNECEDOR";
                return $tmp;
                break;
                
            case "FABRICANTE" :
                $tmp['tb']=FABRICANTES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="FORNECEDOR2";
                return $tmp;
                break;
                
            case "MID_STATUS" :
                $tmp['tb']=EQUIPAMENTOS_STATUS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == EQUIPAMENTOS_FAMILIA) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    
    elseif ($tabela == MATERIAIS_SUBFAMILIA) {
        switch ($campo) {
            case "MID_FAMILIA" :
                $tmp['tb']=MATERIAIS_FAMILIA;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="MATERIAL_FAMILIA";
                return $tmp;
                break;
        }
    }

    elseif ($tabela == MATERIAIS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
            case "FAMILIA" :
                $tmp['tb']=MATERIAIS_FAMILIA;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MATERIAL_FAMILIA";
                return $tmp;
                break;
            case "MID_SUBFAMILIA" :
                $tmp['tb']=MATERIAIS_SUBFAMILIA;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MATERIAL_SUBFAMILIA";
                return $tmp;
                break;
            case "UNIDADE" :
                $tmp['tb']=MATERIAIS_UNIDADE;
                $tmp['campo']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MATERIAL_UNIDADE";
                return $tmp;
                break;
            case "FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "FABRICANTE" :
                $tmp['tb']=FABRICANTES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_CRITICIDADE" :
                $tmp['tb']=MATERIAIS_CRITICIDADE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == ATIVIDADES) {
        switch ($campo) {
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
            case "MID_PLANO_PADRAO" :
                $tmp['tb']=PLANO_PADRAO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['form']="PLANOPADRAO";
                return $tmp;
                break;
            case "MAQUINA_PARADA" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "ESPECIALIDADE" :
                $tmp['tb']=ESPECIALIDADES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="ESPECIALIDADE";
                return $tmp;
                break;
            case "CONTADOR" :
                $tmp['tb']=MAQUINAS_CONTADOR;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "PERIODICIDADE" :
                $tmp['tb']=PROGRAMACAO_PERIODICIDADE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=0;
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MATERIAIS";
                return $tmp;
                break;
        }
    }

    elseif ($tabela == PLANO_PADRAO) {
        switch ($campo) {
            case "MAQUINA_FAMILIA" :
                $tmp['tb']=MAQUINAS_FAMILIA;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="MAQUINAS_FAMILIA";
                return $tmp;
                break;
            case "EQUIPAMENTO_FAMILIA" :
                $tmp['tb']=EQUIPAMENTOS_FAMILIA;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="EQUIPAMENTO_FAMILIA";
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['form']="MAQUINA";
                return $tmp;
                break;
            case "TIPO" :
                $tmp['tb']=TIPO_PLANOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;

            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == PLANO_ROTAS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == AFASTAMENTOS) {
        switch ($campo) {
            case "MID_FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == AUSENCIAS) {
        switch ($campo) {
            case "MID_FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == TURNOS_HORARIOS) {
        switch ($campo) {
            case "MID_TURNO" :
                $tmp['tb']=TURNOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == FUNCIONARIOS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "TURNO" :
                $tmp['tb']=TURNOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "ESPECIALIDADE" :
                $tmp['tb']=ESPECIALIDADES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="ESPECIALIDADE";
                return $tmp;
                break;
            case "EQUIPE" :
                $tmp['tb']=EQUIPES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="EQUIPE";
                return $tmp;
                break;
            case "FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['form']="FORNECEDOR";
                return $tmp;
                break;

            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['campo']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['form']="CENTRO_DE_CUSTO";
                $tmp['cadastra']=1;
                return $tmp;
                break;
            case "SITUACAO" :
                $tmp['tb']=FUNCIONARIOS_SITUACAO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    
    elseif ($tabela == EQUIPES) {
        switch ($campo) {
            case "FORNECEDOR" :
                $tmp['tb']=FORNECEDORES;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="FORNECEDOR";
                return $tmp;
                break;
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == PONTOS_LUBRIFICACAO) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }

    }
    elseif ($tabela == PONTOS_PREDITIVA) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_EQUIPAMENTO" :
                $tmp['tb']=EQUIPAMENTOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == LINK_ROTAS) {
        switch ($campo) {
            case "ESPECIALIDADE" :
                $tmp['tb']=ESPECIALIDADES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                return $tmp;
                break;
            case "PERIODICIDADE" :
                $tmp['tb']=PROGRAMACAO_PERIODICIDADE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="MATERIAL";
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_PONTO" :
                $tmp['tb']=PONTOS_LUBRIFICACAO;
                $tmp['campo']="PONTO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_PLANO" :
                $tmp['tb']=PLANO_ROTAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "CONTADOR" :
                $tmp['tb']=MAQUINAS_CONTADOR;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "PERIODICIDADE" :
                $tmp['tb']=PROGRAMACAO_PERIODICIDADE;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "TIPO" :
                $tmp['tb']=TIPO_ROTAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;

        }
    }

    elseif ($tabela == SOLICITACOES) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="USUARIO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "PRODUCAO" :
                $tmp['tb']=SOLICITACAO_SITUACAO_PRODUCAO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "SEGURANCA" :
                $tmp['tb']=SOLICITACAO_SITUACAO_SEGURANCA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == REQUISICAO) {
        switch ($campo) {
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="USUARIO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_ALMOXARIFADO" :
                $tmp['tb']=ALMOXARIFADO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == SOLICITACAO_RESPOSTA) {
        switch ($campo) {
            case "MID_SOLICITACAO" :
                $tmp['tb']=SOLICITACOES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="USUARIO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == REQUISICAO_RESPOSTA) {
        switch ($campo) {
            case "MID_REQUISICAO" :
                $tmp['tb']=REQUISICAO;
                $tmp['campo']="MOTIVO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "USUARIO" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="USUARIO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == PROGRAMACAO) {
        switch ($campo) {
            case "TIPO" :
                $tmp['tb']=PROGRAMACAO_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_PLANO" :
                if ($VoltaRelacao == 1) $tmp['tb']=PLANO_PADRAO;
                if ($VoltaRelacao == 2) $tmp['tb']=PLANO_ROTAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_CONTADOR" :
                $tmp['tb']=MAQUINAS_CONTADOR;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['cod']="TAG";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_EQUIPAMENTO" :
                $tmp['tb']=EQUIPAMENTOS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == PROGRAMACAO_MAODEOBRA) {
        switch ($campo) {
            case "ESPECIALIDADE" :
                $tmp['tb']=ESPECIALIDADES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == PROGRAMACAO_MATERIAIS) {
        switch ($campo) {
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_PLANEJADO_MATERIAL) {
        switch ($campo) {
            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_PLANEJADO_MAQ) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_PLANEJADO) {
        switch ($campo) {
            // COLUNA L&Oacute;GICAS PARA FILTRO NO ASSIST. DE GR&Aacute;FICOS
            case "TEM_PARADA" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
                
            case "TEM_PENDENCIA1" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
                
            case "TEM_PENDENCIA2" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
                    
            case "TEM_REPROG" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
             
            case "MID_EQUIPE" :
                $tmp['tb']=EQUIPES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            // FIM COLUNAS L&Oacute;GICAS
            
            case "RESPONSAVEL" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "TIPO_SERVICO" :
                $tmp['tb']=TIPOS_SERVICOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="TIPO_SERVICO";
                return $tmp;
                break;
            case "NATUREZA" :
                $tmp['tb']=NATUREZA_SERVICOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="NATUREZA";
                return $tmp;
                break;
            case "CAUSA" :
                $tmp['tb']=CAUSA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="CAUSA";
                return $tmp;
                break;
            case "DEFEITO" :
                $tmp['tb']=DEFEITO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="DEFEITO";
                return $tmp;
                break;
            case "SOLUCAO" :
                $tmp['tb']=SOLUCAO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['form']="SOLUCAO";
                return $tmp;
                break;
            case "TIPO" :
                $tmp['tb']=PROGRAMACAO_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_EQUIPAMENTO" :
                $tmp['tb']=EQUIPAMENTOS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['cod']="COD";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="MAQUINA";
                return $tmp;
                break;

            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['fil_empresa']=2;
                return $tmp;
                break;

            case "MID_AREA" :
                $tmp['tb']=AREAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                return $tmp;
                break;

            case "MID_SETOR" :
                $tmp['tb']=SETORES;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                return $tmp;
                break;

            case "STATUS" :
                $tmp['tb']=ORDEM_PLANEJADO_STATUS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
                
            case "CENTRO_DE_CUSTO" :
                $tmp['tb']=CENTRO_DE_CUSTO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            
            case "MID_MAQUINA_FAMILIA" :
                $tmp['tb']=MAQUINAS_FAMILIA;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;                
                
            case "MID_PROGRAMACAO" :
                $tmp['tb']=PROGRAMACAO;
                $tmp['campo']="MID_PLANO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_REMOVIDA) {
		switch ($campo) {
			case "MID_USUARIO_REMOVE" :
                $tmp['tb']=USUARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=0;
                return $tmp;
                break;
		}
		
	}
    elseif ($tabela == ORDEM_PLANEJADO_MADODEOBRA) {
        switch ($campo) {
            case "MID_FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "EQUIPE" :
                $tmp['tb']=EQUIPES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_PLANEJADO_MAQ_PARADA) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_PLANEJADO_PREV) {
        switch ($campo) {
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == ORDEM_LUB) {
        switch ($campo) {
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['cod']="COD";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=1;
                $tmp['delcascata']=1;
                $tmp['form']="MAQUINA";
                return $tmp;
                break;

            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['fil_empresa']=1;
                return $tmp;
                break;

            case "MID_AREA" :
                $tmp['tb']=AREAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                return $tmp;
                break;

            case "MID_SETOR" :
                $tmp['tb']=SETORES;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                return $tmp;
                break;
        }
    }

    elseif ($tabela == ORDEM_MO_PREVISTO) {
        switch ($campo) {
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_ESPECIALIDADE" :
                $tmp['tb']=ESPECIALIDADES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == ORDEM_MO_ALOC) {
        switch ($campo) {
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_FUNCIONARIO" :
                $tmp['tb']=FUNCIONARIOS;
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }

     elseif ($tabela == ORDEM_MAT_PREVISTO) {
        switch ($campo) {
            case "MID_ORDEM" :
                $tmp['tb']=ORDEM;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == PENDENCIAS) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['cod']="COD";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['cod']="TAG";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_ORDEM" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

            case "MID_ORDEM_EXC" :
                $tmp['tb']=ORDEM_PLANEJADO;
                $tmp['campo']="NUMERO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;

        }
    }
    elseif ($tabela == MOV_EQUIPAMENTO) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_CONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="TAG";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_DMAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_DCONJUNTO" :
                $tmp['tb']=MAQUINAS_CONJUNTO;
                $tmp['cod']="TAG";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
            case "MID_EQUIPAMENTO" :
                $tmp['tb']=EQUIPAMENTOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == MOV_MAQUINA) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['campo']="DESCRICAO";
                $tmp['cod']="COD";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_SETOR" :
                $tmp['tb']=SETORES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
            case "MID_DSETOR" :
                $tmp['tb']=SETORES;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == APR) {
        switch ($campo) {
            case "TIPO" :
                $tmp['tb']=APR_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == TIPOS_SERVICOS) {
        switch ($campo) {
            case "TIPO" :
                $tmp['tb']=PROGRAMACAO_TIPO;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
            case "SPEED_CHECK" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "TROCA" :
                $tmp['tb']=MAQUINAS_PARADA;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
            case "MID_PLANO" :
               	$tmp['tb']= SPEED_CHECK;
               	$tmp['campo']="DESCRICAO";
               	$tmp['mid']="MID";
               	$tmp['dba']=$tdb[$tmp['tb']]['dba'];
               	$tmp['cadastra']=0;
               	return $tmp;
               	break;
        }
    }
    elseif ($tabela == MAQUINAS_DISPONIBILIDADE) {
        switch ($campo) {
            case "MID_MAQUINA" :
                $tmp['tb']=MAQUINAS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == INTEGRADOR) {
        switch ($campo) {
            case "TIPO" :
                $tmp['tb']=INTEGRACAO_TIPOS;
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                return $tmp;
                break;
        }
    }

    elseif ($tabela == ALMOXARIFADO) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == TURNOS) {
        switch ($campo) {
            case "MID_EMPRESA" :
                $tmp['tb']=EMPRESAS;
                $tmp['cod']="COD";
                $tmp['campo']="NOME";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=2;
                return $tmp;
                break;
        }
    }
    
    elseif ($tabela == MATERIAIS_ALMOXARIFADO) {
        switch ($campo) {
            case "MID_ALMOXARIFADO" :
                $tmp['tb']=ALMOXARIFADO;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                $tmp['fil_empresa']=1;
                return $tmp;
                break;

            case "MID_MATERIAL" :
                $tmp['tb']=MATERIAIS;
                $tmp['cod']="COD";
                $tmp['campo']="DESCRICAO";
                $tmp['mid']="MID";
                $tmp['dba']=$tdb[$tmp['tb']]['dba'];
                $tmp['cadastra']=0;
                $tmp['delcascata']=1;
                return $tmp;
                break;
        }
    }
    elseif ($tabela == SPEED_CHECK_ATIVIDADES) {
    	switch ($campo) {
    		case "SPEED_CHECK_ID" :
    			$tmp['tb']=SPEED_CHECK;
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			return $tmp;
    			break;
    		case "ETAPA" :
    			$tmp['tb']=ETAPAS_PLANO;
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			return $tmp;
    			break;
    	}
    }
    elseif ($tabela == ORDEM_SPEED_CHECK) {
    	switch ($campo) {
    		case "MID_ORDEM" :
    			$tmp['tb'] = ORDEM;
    			$tmp['campo'] = "NUMERO";
    			$tmp['mid'] = "MID";
    			$tmp['dba'] = $tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra'] = 0;
    			return $tmp;
    			break;
    		case "MID_SPEED_ATV" :
    			$tmp['tb']=SPEED_CHECK_ATIVIDADES;
    			$tmp['cod']="NUMERO";
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			return $tmp;
    			break;
    		case "ETAPA" :
    			$tmp['tb']=ETAPAS_PLANO;
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			return $tmp;
    			break;
    	}
    }
    elseif ($tabela == SPEED_CHECK) {
    	switch ($campo) {
    		case "MID_EQUIPE" :
    			$tmp['tb'] = EQUIPES;
    			$tmp['campo'] = "DESCRICAO";
    			$tmp['mid'] = "MID";
    			$tmp['dba'] = $tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra'] = 0;
    			return $tmp;
    			$tmp['fil_empresa']=1;
    			break;
    		case "MID_FAMILIA" :
    			$tmp['tb']=MAQUINAS_FAMILIA;
    			$tmp['cod']="COD";
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			$tmp['fil_empresa']=1;
    			return $tmp;
    			break;
    		case "MID_EMPRESA" :
    			$tmp['tb']=EMPRESAS;
    			$tmp['cod']="COD";
    			$tmp['campo']="NOME";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			$tmp['fil_empresa']=1;
    			return $tmp;
    			break;
            case "MID_FAMILIA_COMPONENTE" :
    			$tmp['tb']=EQUIPAMENTOS_FAMILIA;
    			$tmp['cod']="COD";
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			$tmp['fil_empresa']=1;
    			return $tmp;
    			break;
            case "MID_TIPO_PLANO" :
    			$tmp['tb']=TIPO_PLANOS;
    			$tmp['campo']="DESCRICAO";
    			$tmp['mid']="MID";
    			$tmp['dba']=$tdb[$tmp['tb']]['dba'];
    			$tmp['cadastra']=0;
    			$tmp['fil_empresa']=1;
    			return $tmp;
    			break;
    	}
    } 
}
?>
