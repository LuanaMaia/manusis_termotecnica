<?
/**
 *
 * Deleta os itens da tabela que correspondem com o mid informado.
 * Busca recursivamente por tabelas relacionadas marcadas com
 * delcascata = 1 no bd.php
 *
 * @author Felipe Matos Malinoski <felipe@manusis.com.br>
 * @param string $tabela
 * @param int $mid
 * @param [string $campoMid]
 * @param [string $logar] Logar todo o processo? true / false
 * @return boolean TRUE para sucesso e FALSE em caso de erro
 *
 */
function DelCascata($tabela, $mid, $campoMid = "MID", $logar = true, $paginar = false, $pag_ini = 0) {
    global $dba,$tdb,$manusis;
    
    // Tamanho da p�gina
    $pag_tam = 100;
    
    // Valida o tabela
    if(($tabela == "") or (count($tdb[$tabela]) == 0)){
        erromsg("Arquivo:" . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Tabela invalida $tabela.");
        return false;
    }
    // Valida o mid
    if((int) $mid == 0) {
        erromsg("Arquivo:" . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Mid invalido $mid.");
        return false;
    }
    $mid = (int) $mid;
    // Valida o campoMid
    if($campoMid == ""){
        erromsg("Arquivo:" . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Nao informou o campo MID usado para o DELETE.");
        return false;
    }

    // Necess�rio para trabalhar com programa��o
    if($tabela == PLANO_PADRAO) {
        $_SESSION[ManuSess]['VoltaValor']=1;
    }
    if($tabela == PLANO_ROTAS) {
        $_SESSION[ManuSess]['VoltaValor']=2;
    }
    
    // CASO SEJA NECESS�RIO REALIZAR ALGUMA OPERA��O ANTES DE EXCLUIR COLOQUE A CHAMADA DA FUN��O AQUI!!!!
    // FA�A UMA FUN��O EXTERNA N�O SUJE O C�DIGO!!!
    switch ($tabela) {
        case ALMOXARIFADO:
            DeletaAlmox($campoMid, $mid, $logar);
            break;
            
        case MAQUINAS:
            DeletaMaq($campoMid, $mid, $logar);
            break;
        
        case MAQUINAS_CONJUNTO:
            DeletaConj($campoMid, $mid, $logar);
            break;
            
        case ATIVIDADES:
            DeletaAtividade($campoMid, $mid, $logar);
            break;
        
        case LINK_ROTAS:
            DeletaLinkRota($campoMid, $mid, $logar);
            break;
            
        case PROGRAMACAO:
            DeletaProg($campoMid, $mid, $logar);
            break;
            
        case USUARIOS_PERMISAO_SOLICITACAO:
            DeletaPermissao($campoMid, $mid, $logar);
            break;
            
        case ORDEM:
            DeletaOrdem($campoMid, $mid, $logar);
            break;
    }

    // Busca por possiveis cascatas
    $tabCascata = VoltaCascata($tabela);

    
    if($campoMid != "MID") {
        $sql = "SELECT MID FROM $tabela WHERE $campoMid = $mid";
        //echo $sql . "<br />";
        
        if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)) {
            erromsg("Arquivo:" . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[$tabela]['dba']] -> ErrorMsg() . "<br />" . $sql);
            return FALSE;
        }
        while (! $rs->EOF) {
            $campo = $rs->fields;

            // Encontrando algum passa deletando
            if($tabCascata != "") {
                foreach ($tabCascata as $tabDel => $campoDel) {
                    $tmp = DelCascata($tabDel, $campo['MID'], $campoDel, $logar, $paginar, $pag_ini);
                    if($tmp === FALSE){
                        return false;
                    }
                    
                    $pag_ini += $tmp;
                }
            }
            
            // Deleta Arquivos Anexados caso exista op��o
            if($manusis['dir'][$tabela]){
                DeletaDir($manusis['dir'][$tabela] . "/" . $campo['MID']);
            }
            
            if (($pag_ini <= $pag_tam) or (! $paginar)) {
                if ($paginar) {
                    $logar = false;
                }
                
                // Salvando itens deletados
                if ($tdb[$tabela]['DESC_CAMPO']) {
                    $_SESSION[ManuSess]['del'][$tabela]['ident'] .= ($_SESSION[ManuSess]['del'][$tabela]['ident'] == '')? '' : ', ';
                    $_SESSION[ManuSess]['del'][$tabela]['ident'] .= VoltaValor($tabela, $tdb[$tabela]['DESC_CAMPO'], 'MID', $campo['MID']);
                }
                
                $_SESSION[ManuSess]['del'][$tabela]['cont'] ++;
            
                
                // Deleta o item
                $tmp = DeletaItem($tabela, 'MID', $campo['MID'], $logar);
                $pag_ini += $tmp;
            }
            else {
                return FALSE;
            }
            
            $rs -> MoveNext();
        }
    }
    else {
        // Encontrando algum passa deletando
        if($tabCascata != "") {
            foreach ($tabCascata as $tabDel => $campoDel) {
                $tmp = DelCascata($tabDel, $mid, $campoDel, $logar, $paginar, $pag_ini);
                if($tmp === FALSE){
                    return false;
                }
                
                $pag_ini += $tmp;
            }
        }
        
        // Deleta Arquivos Anexados caso exista op��o
        if($manusis['dir'][$tabela]){
            DeletaDir($manusis['dir'][$tabela]."/$mid");
        }
        
        if (($pag_ini <= $pag_tam) or (! $paginar)) {
            // Deleta o item
            $tmp = DeletaItem($tabela, $campoMid, $mid, $logar);
            $pag_ini += $tmp;
            // Limpando
            $_SESSION[ManuSess]['del'] = array();
        }
        else {
            return FALSE;
        }
    }

    
    // Returnando o sucesso ou n�o
    return $pag_ini;
}



// Buscando dados
$deleta=$_GET['deleta'];
$deleta_s=(int)$_GET['deleta_s'];
$deletaarq=$_GET['deletaarq'];
$oq=(int)$_GET['oq'];

// Deletando o item desejado caso tenha confirmado a a��o
if (($deleta != "") and ($_GET['confirma'] == 1)) {
    // Deletando arquivos somente
    if ($deletaarq != "") {
        $deletaarq=str_replace("..","",$deletaarq);
        $deletaarq=str_replace("/","",$deletaarq);
        $deletaarq=str_replace("\\","",$deletaarq);

        if ($deleta == "graficos") @unlink("../../".$manusis['dir']["graficos"]."/$deletaarq");
        elseif(file_exists($manusis['dir'][$deleta]."/$oq/$deletaarq")) @unlink($manusis['dir'][$deleta]."/$oq/$deletaarq");
    }
}

?>
