<?
/**
* Formularios do sistema
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  engine
*/

$form['MAQUINA_DISPONIBILIDADE'][0]['nome']=MAQUINAS_DISPONIBILIDADE;
$form['MAQUINA_DISPONIBILIDADE'][0]['titulo']=$tdb[MAQUINAS_DISPONIBILIDADE]['DESC'];
$form['MAQUINA_DISPONIBILIDADE'][0]['obs']="";
$form['MAQUINA_DISPONIBILIDADE'][0]['largura']=490;
$form['MAQUINA_DISPONIBILIDADE'][0]['altura']=230;
$form['MAQUINA_DISPONIBILIDADE'][0]['chave']="MID";

$form['MAQUINA_DISPONIBILIDADE'][1]['abre_fieldset']=$tdb[MAQUINAS_DISPONIBILIDADE]['DESC'];

$form['MAQUINA_DISPONIBILIDADE'][1]['campo']="MID_MAQUINA";
$form['MAQUINA_DISPONIBILIDADE'][1]['tipo']="select_rela";
$form['MAQUINA_DISPONIBILIDADE'][1]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][1]['tam']=30;
$form['MAQUINA_DISPONIBILIDADE'][1]['max']=75;
$form['MAQUINA_DISPONIBILIDADE'][1]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][2]['campo']="MES";
$form['MAQUINA_DISPONIBILIDADE'][2]['tipo']="mes";
$form['MAQUINA_DISPONIBILIDADE'][2]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][2]['tam']=2;
$form['MAQUINA_DISPONIBILIDADE'][2]['max']=2;
$form['MAQUINA_DISPONIBILIDADE'][2]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][3]['campo']="ANO";
$form['MAQUINA_DISPONIBILIDADE'][3]['tipo']="texto";
$form['MAQUINA_DISPONIBILIDADE'][3]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][3]['tam']=4;
$form['MAQUINA_DISPONIBILIDADE'][3]['max']=4;
$form['MAQUINA_DISPONIBILIDADE'][3]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][4]['campo']="HORAS";
$form['MAQUINA_DISPONIBILIDADE'][4]['tipo']="texto_float";
$form['MAQUINA_DISPONIBILIDADE'][4]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][4]['tam']=11;
$form['MAQUINA_DISPONIBILIDADE'][4]['max']=11;
$form['MAQUINA_DISPONIBILIDADE'][4]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][4]['fecha_fieldset']=1;
/**
 *  FORMULARIOS REVISADOS
 */

$form['USUARIO_SOL'][0]['nome']=USUARIOS_PERMISAO_SOLICITACAO;
$form['USUARIO_SOL'][0]['titulo']=$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC'];
$form['USUARIO_SOL'][0]['obs']="";
$form['USUARIO_SOL'][0]['largura']=480;
$form['USUARIO_SOL'][0]['altura']=160;
$form['USUARIO_SOL'][0]['chave']="MID";

$form['USUARIO_SOL'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC'];

$form['USUARIO_SOL'][1]['campo']="USUARIO";
$form['USUARIO_SOL'][1]['tipo']="select_rela";
$form['USUARIO_SOL'][1]['null']=0;
$form['USUARIO_SOL'][1]['tam']=30;
$form['USUARIO_SOL'][1]['max']=75;
$form['USUARIO_SOL'][1]['unico']=0;

$form['USUARIO_SOL'][2]['campo']="MID_EMPRESA";
$form['USUARIO_SOL'][2]['tipo']="select_rela";
$form['USUARIO_SOL'][2]['null']=0;
$form['USUARIO_SOL'][2]['tam']=30;
$form['USUARIO_SOL'][2]['max']=75;
$form['USUARIO_SOL'][2]['unico']=0;

$form['USUARIO_SOL'][2]['fecha_fieldset']=1;

$form['USUARIO_CC'][0]['nome']=USUARIOS_PERMISAO_CENTRODECUSTO;
$form['USUARIO_CC'][0]['titulo']=$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC'];
$form['USUARIO_CC'][0]['obs']="";
$form['USUARIO_CC'][0]['largura']=480;
$form['USUARIO_CC'][0]['altura']=160;
$form['USUARIO_CC'][0]['chave']="MID";

$form['USUARIO_CC'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC'];

$form['USUARIO_CC'][1]['campo']="USUARIO";
$form['USUARIO_CC'][1]['tipo']="select_rela";
$form['USUARIO_CC'][1]['null']=0;
$form['USUARIO_CC'][1]['tam']=30;
$form['USUARIO_CC'][1]['max']=75;
$form['USUARIO_CC'][1]['unico']=0;
$form['USUARIO_CC'][1]['js']="atualiza_area2('select_cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]', 'parametros.php?id=filtra_cc&campo_id=cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]&campo_nome=cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]&mid_u=' + this.value);";

$form['USUARIO_CC'][2]['campo']="CENTRO_DE_CUSTO";
$form['USUARIO_CC'][2]['tipo']="select_rela";
$form['USUARIO_CC'][2]['null']=0;
$form['USUARIO_CC'][2]['tam']=30;
$form['USUARIO_CC'][2]['max']=75;
$form['USUARIO_CC'][2]['autofiltro']='N';
$form['USUARIO_CC'][2]['unico']=0;

if ($act==2) {
    $form['USUARIO_CC'][2]['valor']="WHERE MID_EMPRESA IN (SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '".(int)VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, "USUARIO", "MID", $_GET['foq'], 0)."')";
}
elseif ((int)$_POST['cc'][USUARIOS_PERMISAO_CENTRODECUSTO][1]) {
    $form['USUARIO_CC'][2]['valor']="WHERE MID_EMPRESA IN (SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '".(int)$_POST['cc'][USUARIOS_PERMISAO_CENTRODECUSTO][1]."')";
}

$form['USUARIO_CC'][2]['fecha_fieldset']=1;

$form['USUARIO_PERMISSAO'][0]['nome']=USUARIOS_PERMISSAO;
$form['USUARIO_PERMISSAO'][0]['titulo']=$tdb[USUARIOS_PERMISSAO]['DESC'];
$form['USUARIO_PERMISSAO'][0]['obs']="";
$form['USUARIO_PERMISSAO'][0]['largura']=410;
$form['USUARIO_PERMISSAO'][0]['altura']=180;
$form['USUARIO_PERMISSAO'][0]['chave']="MID";

$form['USUARIO_PERMISSAO'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISSAO]['DESC'];

$form['USUARIO_PERMISSAO'][1]['campo']="GRUPO";
$form['USUARIO_PERMISSAO'][1]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][1]['null']=0;
$form['USUARIO_PERMISSAO'][1]['tam']=30;
$form['USUARIO_PERMISSAO'][1]['max']=75;
$form['USUARIO_PERMISSAO'][1]['unico']=0;

$form['USUARIO_PERMISSAO'][3]['campo']="MODULO";
$form['USUARIO_PERMISSAO'][3]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][3]['null']=0;
$form['USUARIO_PERMISSAO'][3]['tam']=30;
$form['USUARIO_PERMISSAO'][3]['max']=75;
$form['USUARIO_PERMISSAO'][3]['unico']=0;

$form['USUARIO_PERMISSAO'][2]['campo']="PERMISSAO";
$form['USUARIO_PERMISSAO'][2]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][2]['null']=0;
$form['USUARIO_PERMISSAO'][2]['tam']=30;
$form['USUARIO_PERMISSAO'][2]['max']=75;
$form['USUARIO_PERMISSAO'][2]['unico']=0;

$form['USUARIO_PERMISSAO'][3]['fecha_fieldset']=1;

$form['USUARIO_GRUPO'][0]['nome']=USUARIOS_GRUPOS;
$form['USUARIO_GRUPO'][0]['titulo']=$tdb[USUARIOS_GRUPOS]['DESC'];
$form['USUARIO_GRUPO'][0]['obs']="";
$form['USUARIO_GRUPO'][0]['largura']=420;
$form['USUARIO_GRUPO'][0]['altura']=150;
$form['USUARIO_GRUPO'][0]['chave']="MID";

$form['USUARIO_GRUPO'][1]['abre_fieldset']=$tdb[USUARIOS_GRUPOS]['DESC'];

$form['USUARIO_GRUPO'][1]['campo']="DESCRICAO";
$form['USUARIO_GRUPO'][1]['tipo']="texto";
$form['USUARIO_GRUPO'][1]['null']=0;
$form['USUARIO_GRUPO'][1]['tam']=30;
$form['USUARIO_GRUPO'][1]['max']=75;
$form['USUARIO_GRUPO'][1]['unico']=1;

$form['USUARIO_GRUPO'][1]['fecha_fieldset']=1;


$form['USUARIO'][0]['nome']=USUARIOS;
$form['USUARIO'][0]['titulo']=$tdb[USUARIOS]['DESC'];
$form['USUARIO'][0]['obs']="";
$form['USUARIO'][0]['largura']=400;
$form['USUARIO'][0]['altura']=320;
$form['USUARIO'][0]['chave']="MID";

$form['USUARIO'][1]['abre_fieldset']=$tdb[USUARIOS]['DESC'];

$form['USUARIO'][1]['campo']="NOME";
$form['USUARIO'][1]['tipo']="texto";
$form['USUARIO'][1]['null']=0;
$form['USUARIO'][1]['tam']=30;
$form['USUARIO'][1]['max']=120;
$form['USUARIO'][1]['unico']=0;

$form['USUARIO'][2]['campo']="EMAIL";
$form['USUARIO'][2]['tipo']="texto";
$form['USUARIO'][2]['null']=0;
$form['USUARIO'][2]['tam']=30;
$form['USUARIO'][2]['max']=120;
$form['USUARIO'][2]['unico']=0;

$form['USUARIO'][3]['campo']="CELULAR";
$form['USUARIO'][3]['tipo']="texto";
$form['USUARIO'][3]['null']=1;
$form['USUARIO'][3]['tam']=20;
$form['USUARIO'][3]['max']=75;
$form['USUARIO'][3]['unico']=0;

$form['USUARIO'][4]['campo']="USUARIO";
$form['USUARIO'][4]['tipo']="texto";
$form['USUARIO'][4]['null']=0;
$form['USUARIO'][4]['tam']=30;
$form['USUARIO'][4]['max']=30;
$form['USUARIO'][4]['unico']=1;

$form['USUARIO'][5]['campo']="SENHA";
$form['USUARIO'][5]['tipo']="senha";
$form['USUARIO'][5]['null']=0;
$form['USUARIO'][5]['tam']=20;
$form['USUARIO'][5]['max']=75;
$form['USUARIO'][5]['unico']=0;

// APENAS ADMINISTRADOR PODE EDITAR ISSO
if ((int)$_SESSION[ManuSess]['user']['MID'] == 0) {
    $form['USUARIO'][6]['campo']="GRUPO";
    $form['USUARIO'][6]['tipo']="select_rela";
    $form['USUARIO'][6]['null']=0;
    $form['USUARIO'][6]['tam']=30;
    $form['USUARIO'][6]['max']=75;
    $form['USUARIO'][6]['unico']=0;
    
    $form['USUARIO'][7]['campo']="MID_FUNCIONARIO";
    $form['USUARIO'][7]['tipo']="select_rela";
    $form['USUARIO'][7]['null']=1;
    $form['USUARIO'][7]['unico']=0;
    
    $form['USUARIO'][7]['fecha_fieldset']=1;
}
// TODOS OS USU&Aacute;RIOS
else {
    $form['USUARIO'][5]['fecha_fieldset']=1;
}

// AREAS
$form['AREA'][0]['nome']=AREAS;
$form['AREA'][0]['titulo']=$tdb[AREAS]['DESC'];
$form['AREA'][0]['obs']="";
$form['AREA'][0]['largura']=500;
$form['AREA'][0]['altura']=180;
$form['AREA'][0]['chave']="MID";

$form['AREA'][1]['abre_fieldset']=$tdb[AREAS]['DESC'];

$form['AREA'][1]['campo']="MID_EMPRESA";
$form['AREA'][1]['tipo']="select_rela";
$form['AREA'][1]['null']=0;
$form['AREA'][1]['tam']=15;
$form['AREA'][1]['max']=50;

$form['AREA'][2]['campo']="COD";
$form['AREA'][2]['tipo']="texto";
$form['AREA'][2]['null']=0;
$form['AREA'][2]['tam']=15;
$form['AREA'][2]['max']=50;
$form['AREA'][2]['unico']=1;

$form['AREA'][3]['campo']="DESCRICAO";
$form['AREA'][3]['tipo']="texto";
$form['AREA'][3]['null']=0;
$form['AREA'][3]['tam']=30;
$form['AREA'][3]['max']=30;
$form['AREA'][3]['unico']=0;

$form['AREA'][3]['fecha_fieldset']=1;


// SETOR
$form['SETOR'][0]['nome']=SETORES;
$form['SETOR'][0]['titulo']=$tdb[SETORES]['DESC'];
$form['SETOR'][0]['obs']="";
$form['SETOR'][0]['largura']=570;
$form['SETOR'][0]['altura']=225;
$form['SETOR'][0]['chave']="MID";

$form['SETOR'][1]['abre_fieldset']=$tdb[SETORES]['DESC'];

$form['SETOR'][1]['campo']="MID_AREA";
$form['SETOR'][1]['tipo']="select_setor_autotag";
$form['SETOR'][1]['null']=0;

$form['SETOR'][2]['campo']="COD";
$form['SETOR'][2]['tipo']="texto_setor_autotag";
$form['SETOR'][2]['null']=0;
$form['SETOR'][2]['tam']=15;
$form['SETOR'][2]['max']=15;
$form['SETOR'][2]['unico']=1;

$form['SETOR'][3]['campo']="DESCRICAO";
$form['SETOR'][3]['tipo']="texto";
$form['SETOR'][3]['null']=0;
$form['SETOR'][3]['tam']=30;
$form['SETOR'][3]['max']=60;
$form['SETOR'][3]['unico']=0;

$form['SETOR'][3]['fecha_fieldset']=1;


// OBJ DE MANUTEN&Ccedil;&Atilde;O
$form['MAQUINA'][0]['nome']=MAQUINAS;
$form['MAQUINA'][0]['titulo']=$tdb[MAQUINAS]['DESC'];
$form['MAQUINA'][0]['largura']=560;
$form['MAQUINA'][0]['altura']=430;
$form['MAQUINA'][0]['chave']="MID";
$form['MAQUINA'][0]['dir']=1;

$form['MAQUINA'][1]['abre_fieldset']=$tdb[MAQUINAS]['DESC'];

$form['MAQUINA'][1]['campo']="MID_SETOR";
$form['MAQUINA'][1]['tipo']="select_rela";
if ($act == 2) {    
    $form['MAQUINA'][1]['tipo']="recb_valor";
    $form['MAQUINA'][1]['valor']=$form_recb_valor;
}
$form['MAQUINA'][1]['null']=0;
$form['MAQUINA'][1]['tam']=34;
$form['MAQUINA'][1]['max']=6;

$form['MAQUINA'][2]['campo']="CENTRO_DE_CUSTO";
$form['MAQUINA'][2]['tipo']="select_rela";
$form['MAQUINA'][2]['null']=0;
$form['MAQUINA'][2]['tam']=25;
$form['MAQUINA'][2]['max']=32;

$form['MAQUINA'][3]['campo']="FAMILIA";
$form['MAQUINA'][3]['tipo']="select_maq_autotag";
$form['MAQUINA'][3]['null']=0;
$form['MAQUINA'][3]['tam']=10;
$form['MAQUINA'][3]['max']=10;

$form['MAQUINA'][4]['campo']="COD";
$form['MAQUINA'][4]['tipo']="texto_maq_autotag";
$form['MAQUINA'][4]['null']=0;
$form['MAQUINA'][4]['tam']=30;
$form['MAQUINA'][4]['max']=50;
$form['MAQUINA'][4]['unico']=1;

$form['MAQUINA'][5]['campo']="DESCRICAO";
$form['MAQUINA'][5]['tipo']="texto";
$form['MAQUINA'][5]['null']=0;
$form['MAQUINA'][5]['tam']=40;
$form['MAQUINA'][5]['max']=70;

$form['MAQUINA'][5]['fecha_fieldset']=1;

$form['MAQUINA'][6]['abre_fieldset']=$ling['fildset_local3'];

$form['MAQUINA'][6]['campo']="FABRICANTE";
$form['MAQUINA'][6]['tipo']="select_rela";
$form['MAQUINA'][6]['null']=1;
$form['MAQUINA'][6]['tam']=8;
$form['MAQUINA'][6]['max']=8;

$form['MAQUINA'][7]['campo']="FORNECEDOR";
$form['MAQUINA'][7]['tipo']="select_rela";
$form['MAQUINA'][7]['null']=1;
$form['MAQUINA'][7]['tam']=8;
$form['MAQUINA'][7]['max']=8;

$form['MAQUINA'][8]['campo']="CLASSE";
$form['MAQUINA'][8]['tipo']="select_rela";
$form['MAQUINA'][8]['null']=1;
$form['MAQUINA'][8]['tam']=8;
$form['MAQUINA'][8]['max']=8;

$form['MAQUINA'][9]['campo']="STATUS";
$form['MAQUINA'][9]['tipo']="select_rela";
$form['MAQUINA'][9]['null']=1;
$form['MAQUINA'][9]['tam']=8;
$form['MAQUINA'][9]['max']=8;

$form['MAQUINA'][10]['campo']="LOCALIZACAO_FISICA";
$form['MAQUINA'][10]['tipo']="texto";
$form['MAQUINA'][10]['null']=1;
$form['MAQUINA'][10]['tam']=30;
$form['MAQUINA'][10]['max']=125;

$form['MAQUINA'][11]['campo']="MODELO";
$form['MAQUINA'][11]['tipo']="texto";
$form['MAQUINA'][11]['null']=1;
$form['MAQUINA'][11]['tam']=30;
$form['MAQUINA'][11]['max']=50;

$form['MAQUINA'][12]['campo']="NUMERO_DE_SERIE";
$form['MAQUINA'][12]['tipo']="texto";
$form['MAQUINA'][12]['null']=1;
$form['MAQUINA'][12]['tam']=15;
$form['MAQUINA'][12]['max']=20;

$form['MAQUINA'][13]['campo']="ANO_DE_FABRICACAO";
$form['MAQUINA'][13]['tipo']="data";
$form['MAQUINA'][13]['null']=1;
$form['MAQUINA'][13]['tam']=10;
$form['MAQUINA'][13]['max']=10;

$form['MAQUINA'][14]['campo']="GARANTIA";
$form['MAQUINA'][14]['tipo']="data";
$form['MAQUINA'][14]['null']=1;
$form['MAQUINA'][14]['tam']=10;
$form['MAQUINA'][14]['max']=10;

$form['MAQUINA'][15]['campo']="NUMERO_DO_PATRIMONIO";
$form['MAQUINA'][15]['tipo']="texto";
$form['MAQUINA'][15]['null']=1;
$form['MAQUINA'][15]['unico']=1;
$form['MAQUINA'][15]['tam']=15;
$form['MAQUINA'][15]['max']=20;

$form['MAQUINA'][16]['campo']="DADOS_TECNICO";
$form['MAQUINA'][16]['tipo']="blog";
$form['MAQUINA'][16]['null']=1;
$form['MAQUINA'][16]['tam']=50;
$form['MAQUINA'][16]['max']=5;

$form['MAQUINA'][16]['fecha_fieldset']=1;


// CONJUNTO
$form['CONJUNTO'][0]['nome']=MAQUINAS_CONJUNTO;
$form['CONJUNTO'][0]['titulo']=$tdb[MAQUINAS_CONJUNTO]['DESC'];
$form['CONJUNTO'][0]['obs']="";
$form['CONJUNTO'][0]['dir']=1;
$form['CONJUNTO'][0]['largura']=560;
$form['CONJUNTO'][0]['altura']=430;
$form['CONJUNTO'][0]['chave']="MID";

$form['CONJUNTO'][1]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO]['DESC'];

$form['CONJUNTO'][1]['campo']="TAG";
$form['CONJUNTO'][1]['tipo']="texto_conj_autotag";
$form['CONJUNTO'][1]['null']=0;
$form['CONJUNTO'][1]['tam']=30;
$form['CONJUNTO'][1]['max']=30;
$form['CONJUNTO'][1]['unico']=1;

$form['CONJUNTO'][2]['campo']="DESCRICAO";
$form['CONJUNTO'][2]['tipo']="texto";
$form['CONJUNTO'][2]['null']=0;
$form['CONJUNTO'][2]['tam']=30;
$form['CONJUNTO'][2]['max']=250;
$form['CONJUNTO'][2]['unico']=0;

$form['CONJUNTO'][3]['campo']="COMPLEMENTO";
$form['CONJUNTO'][3]['tipo']="texto";
$form['CONJUNTO'][3]['null']=1;
$form['CONJUNTO'][3]['tam']=30;
$form['CONJUNTO'][3]['max']=150;
$form['CONJUNTO'][3]['unico']=0;

$form['CONJUNTO'][3]['fecha_fieldset']=1;
$form['CONJUNTO'][4]['abre_fieldset']=$ling['fildset_local3'];

$form['CONJUNTO'][4]['campo']="FABRICANTE";
$form['CONJUNTO'][4]['tipo']="select_rela";
$form['CONJUNTO'][4]['null']=1;
$form['CONJUNTO'][4]['tam']=8;
$form['CONJUNTO'][4]['max']=8;

$form['CONJUNTO'][5]['campo']="MARCA";
$form['CONJUNTO'][5]['tipo']="texto";
$form['CONJUNTO'][5]['null']=1;
$form['CONJUNTO'][5]['tam']=30;
$form['CONJUNTO'][5]['max']=30;
$form['CONJUNTO'][5]['unico']=0;

$form['CONJUNTO'][6]['campo']="MODELO";
$form['CONJUNTO'][6]['tipo']="texto";
$form['CONJUNTO'][6]['null']=1;
$form['CONJUNTO'][6]['tam']=30;
$form['CONJUNTO'][6]['max']=30;
$form['CONJUNTO'][6]['unico']=0;

$form['CONJUNTO'][7]['campo']="NSERIE";
$form['CONJUNTO'][7]['tipo']="texto";
$form['CONJUNTO'][7]['null']=1;
$form['CONJUNTO'][7]['tam']=30;
$form['CONJUNTO'][7]['max']=30;
$form['CONJUNTO'][7]['unico']=0;

$form['CONJUNTO'][8]['campo']="POTENCIA";
$form['CONJUNTO'][8]['tipo']="texto";
$form['CONJUNTO'][8]['null']=1;
$form['CONJUNTO'][8]['tam']=30;
$form['CONJUNTO'][8]['max']=30;
$form['CONJUNTO'][8]['unico']=0;

$form['CONJUNTO'][9]['campo']="PRESSAO";
$form['CONJUNTO'][9]['tipo']="texto";
$form['CONJUNTO'][9]['null']=1;
$form['CONJUNTO'][9]['tam']=30;
$form['CONJUNTO'][9]['max']=30;
$form['CONJUNTO'][9]['unico']=0;

$form['CONJUNTO'][10]['campo']="CORRENTE";
$form['CONJUNTO'][10]['tipo']="texto";
$form['CONJUNTO'][10]['null']=1;
$form['CONJUNTO'][10]['tam']=30;
$form['CONJUNTO'][10]['max']=30;
$form['CONJUNTO'][10]['unico']=0;

$form['CONJUNTO'][11]['campo']="TENSAO";
$form['CONJUNTO'][11]['tipo']="texto";
$form['CONJUNTO'][11]['null']=1;
$form['CONJUNTO'][11]['tam']=30;
$form['CONJUNTO'][11]['max']=30;
$form['CONJUNTO'][11]['unico']=0;

$form['CONJUNTO'][12]['campo']="VAZAO";
$form['CONJUNTO'][12]['tipo']="texto";
$form['CONJUNTO'][12]['null']=1;
$form['CONJUNTO'][12]['tam']=30;
$form['CONJUNTO'][12]['max']=30;
$form['CONJUNTO'][12]['unico']=0;

$form['CONJUNTO'][13]['campo']="MID_MAQUINA";
$form['CONJUNTO'][13]['tipo']="recb_valor";
$form['CONJUNTO'][13]['valor']=$form_recb_valor;
$form['CONJUNTO'][13]['null']=0;
$form['CONJUNTO'][13]['tam']=0;
$form['CONJUNTO'][13]['max']=0;
$form['CONJUNTO'][13]['unico']=0;

$form['CONJUNTO'][14]['campo']="FICHA_TECNICA";
$form['CONJUNTO'][14]['tipo']="blog";
$form['CONJUNTO'][14]['null']=1;
$form['CONJUNTO'][14]['tam']=25;
$form['CONJUNTO'][14]['max']=10;

$form['CONJUNTO'][14]['fecha_fieldset']=1;

$form['CONJUNTO2'][0]['nome']=MAQUINAS_CONJUNTO;
$form['CONJUNTO2'][0]['titulo']=$tdb[MAQUINAS_CONJUNTO]['DESC'];
$form['CONJUNTO2'][0]['obs']="";
$form['CONJUNTO2'][0]['dir']=1;
$form['CONJUNTO2'][0]['largura']=560;
$form['CONJUNTO2'][0]['altura']=430;
$form['CONJUNTO2'][0]['chave']="MID";

$form['CONJUNTO2'][1]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO]['DESC'];

$cc=$_POST['cc'];

$form['CONJUNTO2'][1]['campo']="MID_CONJUNTO";
$form['CONJUNTO2'][1]['tipo']="select_rela";
$form['CONJUNTO2'][1]['valor']="WHERE MID_MAQUINA = '$form_recb_valor' AND MID_CONJUNTO = '0' AND MID != '".$_GET['foq']."'";
$form['CONJUNTO2'][1]['null']=1;
$form['CONJUNTO2'][1]['unico']=0;

$form['CONJUNTO2'][2]['campo']="TAG";
$form['CONJUNTO2'][2]['tipo']="texto_conj_autotag";
$form['CONJUNTO2'][2]['null']=0;
$form['CONJUNTO2'][2]['tam']=15;
$form['CONJUNTO2'][2]['max']=30;
$form['CONJUNTO2'][2]['unico']=1;

$form['CONJUNTO2'][3]['campo']="DESCRICAO";
$form['CONJUNTO2'][3]['tipo']="texto";
$form['CONJUNTO2'][3]['null']=0;
$form['CONJUNTO2'][3]['tam']=30;
$form['CONJUNTO2'][3]['max']=250;
$form['CONJUNTO2'][3]['unico']=0;

$form['CONJUNTO2'][4]['campo']="COMPLEMENTO";
$form['CONJUNTO2'][4]['tipo']="texto";
$form['CONJUNTO2'][4]['null']=1;
$form['CONJUNTO2'][4]['tam']=30;
$form['CONJUNTO2'][4]['max']=150;
$form['CONJUNTO2'][4]['unico']=0;


$form['CONJUNTO2'][4]['fecha_fieldset']=1;
$form['CONJUNTO2'][5]['abre_fieldset']=$ling['fildset_local3'];

$form['CONJUNTO2'][15]['campo']="FICHA_TECNICA";
$form['CONJUNTO2'][15]['tipo']="blog";
$form['CONJUNTO2'][15]['null']=1;
$form['CONJUNTO2'][15]['tam']=50;
$form['CONJUNTO2'][15]['max']=5;

$form['CONJUNTO2'][5]['campo']="FABRICANTE";
$form['CONJUNTO2'][5]['tipo']="select_rela";
$form['CONJUNTO2'][5]['null']=1;
$form['CONJUNTO2'][5]['tam']=8;
$form['CONJUNTO2'][5]['max']=8;

$form['CONJUNTO2'][6]['campo']="MARCA";
$form['CONJUNTO2'][6]['tipo']="texto";
$form['CONJUNTO2'][6]['null']=1;
$form['CONJUNTO2'][6]['tam']=30;
$form['CONJUNTO2'][6]['max']=30;
$form['CONJUNTO2'][6]['unico']=0;

$form['CONJUNTO2'][7]['campo']="MODELO";
$form['CONJUNTO2'][7]['tipo']="texto";
$form['CONJUNTO2'][7]['null']=1;
$form['CONJUNTO2'][7]['tam']=30;
$form['CONJUNTO2'][7]['max']=30;
$form['CONJUNTO2'][7]['unico']=0;

$form['CONJUNTO2'][8]['campo']="NSERIE";
$form['CONJUNTO2'][8]['tipo']="texto";
$form['CONJUNTO2'][8]['null']=1;
$form['CONJUNTO2'][8]['tam']=30;
$form['CONJUNTO2'][8]['max']=30;
$form['CONJUNTO2'][8]['unico']=0;

$form['CONJUNTO2'][9]['campo']="POTENCIA";
$form['CONJUNTO2'][9]['tipo']="texto";
$form['CONJUNTO2'][9]['null']=1;
$form['CONJUNTO2'][9]['tam']=30;
$form['CONJUNTO2'][9]['max']=30;
$form['CONJUNTO2'][9]['unico']=0;

$form['CONJUNTO2'][10]['campo']="PRESSAO";
$form['CONJUNTO2'][10]['tipo']="texto";
$form['CONJUNTO2'][10]['null']=1;
$form['CONJUNTO2'][10]['tam']=30;
$form['CONJUNTO2'][10]['max']=30;
$form['CONJUNTO2'][10]['unico']=0;

$form['CONJUNTO2'][11]['campo']="CORRENTE";
$form['CONJUNTO2'][11]['tipo']="texto";
$form['CONJUNTO2'][11]['null']=1;
$form['CONJUNTO2'][11]['tam']=30;
$form['CONJUNTO2'][11]['max']=30;
$form['CONJUNTO2'][11]['unico']=0;

$form['CONJUNTO2'][12]['campo']="TENSAO";
$form['CONJUNTO2'][12]['tipo']="texto";
$form['CONJUNTO2'][12]['null']=1;
$form['CONJUNTO2'][12]['tam']=30;
$form['CONJUNTO2'][12]['max']=30;
$form['CONJUNTO2'][12]['unico']=0;

$form['CONJUNTO2'][13]['campo']="VAZAO";
$form['CONJUNTO2'][13]['tipo']="texto";
$form['CONJUNTO2'][13]['null']=1;
$form['CONJUNTO2'][13]['tam']=30;
$form['CONJUNTO2'][13]['max']=30;
$form['CONJUNTO2'][13]['unico']=0;

$form['CONJUNTO2'][14]['campo']="MID_MAQUINA";
$form['CONJUNTO2'][14]['tipo']="recb_valor";
$form['CONJUNTO2'][14]['valor']=$form_recb_valor;
$form['CONJUNTO2'][14]['null']=0;
$form['CONJUNTO2'][14]['tam']=0;
$form['CONJUNTO2'][14]['max']=0;
$form['CONJUNTO2'][14]['unico']=0;

$form['CONJUNTO2'][15]['fecha_fieldset']=1;

// FORNECEDORES DA MAQUINA
$iform = 0;
$form['MAQUINAS_FORNECEDOR'][$iform]['nome']=MAQUINAS_FORNECEDOR;
$form['MAQUINAS_FORNECEDOR'][$iform]['titulo']=$tdb[MAQUINAS_FORNECEDOR]['DESC'];
$form['MAQUINAS_FORNECEDOR'][$iform]['obs']="";
$form['MAQUINAS_FORNECEDOR'][$iform]['largura']=500;
$form['MAQUINAS_FORNECEDOR'][$iform]['altura']=250;
$form['MAQUINAS_FORNECEDOR'][$iform]['chave']="MID";

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['abre_fieldset']=$tdb[MAQUINAS_FORNECEDOR]['DESC'];

$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="MID_FORNECEDOR";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="select_rela";
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=0;

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="OBS";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="blog";
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=1;
$form['MAQUINAS_FORNECEDOR'][$iform]['tam']=30;
$form['MAQUINAS_FORNECEDOR'][$iform]['max']=5;
$form['MAQUINAS_FORNECEDOR'][$iform]['unico']=0;

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="MID_MAQUINA";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="recb_valor";
$form['MAQUINAS_FORNECEDOR'][$iform]['valor']=$form_recb_valor;
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['tam']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['max']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['unico']=0;

$form['MAQUINAS_FORNECEDOR'][$iform]['fecha_fieldset']=1;


// FORMULARIO DE PONTO DE MONITORAMENTO

$form['PONTO_PREDITIVA'][0]['nome']=PONTOS_PREDITIVA;
$form['PONTO_PREDITIVA'][0]['titulo']=$tdb[PONTOS_PREDITIVA]['DESC'];
$form['PONTO_PREDITIVA'][0]['obs']="";
$form['PONTO_PREDITIVA'][0]['largura']=450;
$form['PONTO_PREDITIVA'][0]['altura']=320;
$form['PONTO_PREDITIVA'][0]['chave']="MID";

$form['PONTO_PREDITIVA'][1]['abre_fieldset']=$tdb[PONTOS_PREDITIVA]['DESC'];

$form['PONTO_PREDITIVA'][1]['campo']="PONTO";
$form['PONTO_PREDITIVA'][1]['tipo']="texto";
$form['PONTO_PREDITIVA'][1]['null']=0;
$form['PONTO_PREDITIVA'][1]['tam']=30;
$form['PONTO_PREDITIVA'][1]['max']=250;
$form['PONTO_PREDITIVA'][1]['unico']=0;

$form['PONTO_PREDITIVA'][2]['campo']="GRANDEZA";
$form['PONTO_PREDITIVA'][2]['tipo']="texto";
$form['PONTO_PREDITIVA'][2]['null']=0;
$form['PONTO_PREDITIVA'][2]['tam']=30;
$form['PONTO_PREDITIVA'][2]['max']=50;
$form['PONTO_PREDITIVA'][2]['unico']=0;

$form['PONTO_PREDITIVA'][3]['campo']="VALOR_MINIMO";
$form['PONTO_PREDITIVA'][3]['tipo']="texto_float";
$form['PONTO_PREDITIVA'][3]['null']=0;
$form['PONTO_PREDITIVA'][3]['tam']=15;
$form['PONTO_PREDITIVA'][3]['max']=30;
$form['PONTO_PREDITIVA'][3]['unico']=0;

$form['PONTO_PREDITIVA'][4]['campo']="VALOR_MAXIMO";
$form['PONTO_PREDITIVA'][4]['tipo']="texto_float";
$form['PONTO_PREDITIVA'][4]['null']=0;
$form['PONTO_PREDITIVA'][4]['tam']=15;
$form['PONTO_PREDITIVA'][4]['max']=30;
$form['PONTO_PREDITIVA'][4]['unico']=0;

$form['PONTO_PREDITIVA'][5]['campo']="MID_CONJUNTO";
$form['PONTO_PREDITIVA'][5]['tipo']="select_conjunto_filtrado";
$form['PONTO_PREDITIVA'][5]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA'][5]['null']=0;
$form['PONTO_PREDITIVA'][5]['tam']=30;
$form['PONTO_PREDITIVA'][5]['max']=250;
$form['PONTO_PREDITIVA'][5]['unico']=0;

$form['PONTO_PREDITIVA'][6]['campo']="OBSERVACAO";
$form['PONTO_PREDITIVA'][6]['tipo']="blog";
$form['PONTO_PREDITIVA'][6]['null']=1;
$form['PONTO_PREDITIVA'][6]['tam']=30;
$form['PONTO_PREDITIVA'][6]['max']=5;
$form['PONTO_PREDITIVA'][6]['unico']=0;

$form['PONTO_PREDITIVA'][7]['campo']="MID_MAQUINA";
$form['PONTO_PREDITIVA'][7]['tipo']="recb_valor";
$form['PONTO_PREDITIVA'][7]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA'][7]['null']=0;
$form['PONTO_PREDITIVA'][7]['tam']=30;
$form['PONTO_PREDITIVA'][7]['max']=250;
$form['PONTO_PREDITIVA'][7]['unico']=0;

$form['PONTO_PREDITIVA'][7]['fecha_fieldset']=1;


// FORMULARIO PONTOS DE LUBRIFICACAO

$form['PONTO_LUBRIFICACAO'][0]['nome']=PONTOS_LUBRIFICACAO;
$form['PONTO_LUBRIFICACAO'][0]['titulo']=$tdb[PONTOS_LUBRIFICACAO]['DESC'];
$form['PONTO_LUBRIFICACAO'][0]['obs']="";
$form['PONTO_LUBRIFICACAO'][0]['largura']=500;
$form['PONTO_LUBRIFICACAO'][0]['altura']=210;
$form['PONTO_LUBRIFICACAO'][0]['chave']="MID";

$form['PONTO_LUBRIFICACAO'][1]['abre_fieldset']=$ling['def_ponto'];

$form['PONTO_LUBRIFICACAO'][1]['campo']="PONTO";
$form['PONTO_LUBRIFICACAO'][1]['tipo']="texto";
$form['PONTO_LUBRIFICACAO'][1]['null']=0;
$form['PONTO_LUBRIFICACAO'][1]['tam']=30;
$form['PONTO_LUBRIFICACAO'][1]['max']=75;
$form['PONTO_LUBRIFICACAO'][1]['unico']=0;

$form['PONTO_LUBRIFICACAO'][2]['campo']="NPONTO";
$form['PONTO_LUBRIFICACAO'][2]['tipo']="texto";
$form['PONTO_LUBRIFICACAO'][2]['null']=0;
$form['PONTO_LUBRIFICACAO'][2]['tam']=3;
$form['PONTO_LUBRIFICACAO'][2]['max']=3;
$form['PONTO_LUBRIFICACAO'][2]['unico']=0;

$form['PONTO_LUBRIFICACAO'][3]['campo']="METODO";
$form['PONTO_LUBRIFICACAO'][3]['tipo']="texto";
$form['PONTO_LUBRIFICACAO'][3]['null']=0;
$form['PONTO_LUBRIFICACAO'][3]['tam']=30;
$form['PONTO_LUBRIFICACAO'][3]['max']=50;
$form['PONTO_LUBRIFICACAO'][3]['unico']=0;

$form['PONTO_LUBRIFICACAO'][4]['campo']="MID_CONJUNTO";
$form['PONTO_LUBRIFICACAO'][4]['tipo']="select_conjunto_filtrado";
$form['PONTO_LUBRIFICACAO'][4]['valor']=$form_recb_valor;
$form['PONTO_LUBRIFICACAO'][4]['null']=0;
$form['PONTO_LUBRIFICACAO'][4]['unico']=0;

$form['PONTO_LUBRIFICACAO'][5]['campo']="MID_MAQUINA";
$form['PONTO_LUBRIFICACAO'][5]['tipo']="recb_valor";
$form['PONTO_LUBRIFICACAO'][5]['valor']=$form_recb_valor;
$form['PONTO_LUBRIFICACAO'][5]['unico']=0;

$form['PONTO_LUBRIFICACAO'][5]['fecha_fieldset']=1;


// FORMULARIO CONTADOR MAQUINA
$form['CONTADOR_MAQ'][0]['nome']=MAQUINAS_CONTADOR;
$form['CONTADOR_MAQ'][0]['titulo']=$tdb[MAQUINAS_CONTADOR]['DESC'];
$form['CONTADOR_MAQ'][0]['obs']="";
$form['CONTADOR_MAQ'][0]['largura']=500;
$form['CONTADOR_MAQ'][0]['altura']=180;
$form['CONTADOR_MAQ'][0]['chave']="MID";

$form['CONTADOR_MAQ'][1]['abre_fieldset']=$tdb[MAQUINAS_CONTADOR]['DESC'];

$form['CONTADOR_MAQ'][1]['campo']="DESCRICAO";
$form['CONTADOR_MAQ'][1]['tipo']="texto";
$form['CONTADOR_MAQ'][1]['null']=0;
$form['CONTADOR_MAQ'][1]['tam']=35;
$form['CONTADOR_MAQ'][1]['max']=70;
$form['CONTADOR_MAQ'][1]['unico']=1;

$form['CONTADOR_MAQ'][2]['campo']="TIPO";
$form['CONTADOR_MAQ'][2]['tipo']="select_rela";
$form['CONTADOR_MAQ'][2]['null']=0;
$form['CONTADOR_MAQ'][2]['tam']=20;
$form['CONTADOR_MAQ'][2]['max']=13;
$form['CONTADOR_MAQ'][2]['unico']=0;

$form['CONTADOR_MAQ'][3]['campo']="ZERA";
$form['CONTADOR_MAQ'][3]['tipo']="texto_float";
$form['CONTADOR_MAQ'][3]['null']=0;
$form['CONTADOR_MAQ'][3]['tam']=20;
$form['CONTADOR_MAQ'][3]['max']=13;
$form['CONTADOR_MAQ'][3]['unico']=0;

$form['CONTADOR_MAQ'][4]['campo']="MID_MAQUINA";
$form['CONTADOR_MAQ'][4]['tipo']="recb_valor";
$form['CONTADOR_MAQ'][4]['valor']=$form_recb_valor;
$form['CONTADOR_MAQ'][4]['null']=0;
$form['CONTADOR_MAQ'][4]['unico']=0;
$form['CONTADOR_MAQ'][4]['fecha_fieldset']=1;


// FORMULARIO MATERIAL DO EQUIPAMENTO
$iform = 0;
$form['CONJUNTO_MATERIAL'][$iform]['nome']=MAQUINAS_CONJUNTO_MATERIAL;
$form['CONJUNTO_MATERIAL'][$iform]['titulo']=$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC'];
$form['CONJUNTO_MATERIAL'][$iform]['obs']="";
$form['CONJUNTO_MATERIAL'][$iform]['largura']=550;
$form['CONJUNTO_MATERIAL'][$iform]['altura']=200;
$form['CONJUNTO_MATERIAL'][$iform]['chave']="MID";

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC'];

$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_CONJUNTO";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_conjunto_filtrado";
$form['CONJUNTO_MATERIAL'][$iform]['valor']=$form_recb_valor;
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_MATERIAL";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_material";
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="QUANTIDADE";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="texto_unidade";
$form['CONJUNTO_MATERIAL'][$iform]['valor']=1;
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['tam']=5;
$form['CONJUNTO_MATERIAL'][$iform]['max']=5;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_CLASSE";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_rela";
$form['CONJUNTO_MATERIAL'][$iform]['null']=1;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_MAQUINA";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="recb_valor";
$form['CONJUNTO_MATERIAL'][$iform]['valor']=$form_recb_valor;
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$form['CONJUNTO_MATERIAL'][$iform]['fecha_fieldset']=1;



//FORMULARIO EQUIPAMENTO/COMPONENTE
$iform = 0;
$form['EQUIPAMENTO'][$iform]['nome']=EQUIPAMENTOS;
$form['EQUIPAMENTO'][$iform]['titulo']=$tdb[EQUIPAMENTOS]['DESC'];
$form['EQUIPAMENTO'][$iform]['dir']=1;
$form['EQUIPAMENTO'][$iform]['largura']=530;
$form['EQUIPAMENTO'][$iform]['altura']=450;
$form['EQUIPAMENTO'][$iform]['chave']="MID";

$iform ++;
$form['EQUIPAMENTO'][$iform]['abre_fieldset']=$tdb[EQUIPAMENTOS]['DESC'];

if (($act == 1) or ($act == 3)) {
    $form['EQUIPAMENTO'][$iform]['campo']="MID_MAQUINA";
    $form['EQUIPAMENTO'][$iform]['tipo']="select_maquina";
    $form['EQUIPAMENTO'][$iform]['null']=0;
    $form['EQUIPAMENTO'][$iform]['tam']=0;
    $form['EQUIPAMENTO'][$iform]['max']=0;
    $form['EQUIPAMENTO'][$iform]['unico']=0;

    $iform ++;
    $form['EQUIPAMENTO'][$iform]['campo']="MID_CONJUNTO";
    $form['EQUIPAMENTO'][$iform]['tipo']="select_conjunto_filtrado";
    $form['EQUIPAMENTO'][$iform]['null']=0;
    $form['EQUIPAMENTO'][$iform]['tam']=0;
    $form['EQUIPAMENTO'][$iform]['max']=0;
    $form['EQUIPAMENTO'][$iform]['unico']=0;
}
else {
    $form['EQUIPAMENTO'][$iform]['campo']="MID_MAQUINA";
    $form['EQUIPAMENTO'][$iform]['tipo']="recb_valor";
    $form['EQUIPAMENTO'][$iform]['null']=1;
    $form['EQUIPAMENTO'][$iform]['tam']=0;
    $form['EQUIPAMENTO'][$iform]['max']=0;
    $form['EQUIPAMENTO'][$iform]['unico']=0;

    $iform ++;
    $form['EQUIPAMENTO'][$iform]['campo']="MID_CONJUNTO";
    $form['EQUIPAMENTO'][$iform]['tipo']="recb_valor";
    $form['EQUIPAMENTO'][$iform]['null']=1;
    $form['EQUIPAMENTO'][$iform]['tam']=0;
    $form['EQUIPAMENTO'][$iform]['max']=0;
    $form['EQUIPAMENTO'][$iform]['unico']=0;
}

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="FAMILIA";
$form['EQUIPAMENTO'][$iform]['tipo']="select_equip_autotag";
$form['EQUIPAMENTO'][$iform]['null']=0;
$form['EQUIPAMENTO'][$iform]['tam']=10;
$form['EQUIPAMENTO'][$iform]['max']=10;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="COD";
$form['EQUIPAMENTO'][$iform]['tipo']="texto_equip_autotag";
$form['EQUIPAMENTO'][$iform]['null']=0;
$form['EQUIPAMENTO'][$iform]['tam']=20;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=1;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="DESCRICAO";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=0;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=255;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$form['EQUIPAMENTO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['EQUIPAMENTO'][$iform]['abre_fieldset']=$ling['fildset_local3'];

$form['EQUIPAMENTO'][$iform]['campo']="MID_STATUS";
$form['EQUIPAMENTO'][$iform]['tipo']="select_rela";
$form['EQUIPAMENTO'][$iform]['null']=0;
$form['EQUIPAMENTO'][$iform]['tam']=8;
$form['EQUIPAMENTO'][$iform]['max']=8;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="FORNECEDOR";
$form['EQUIPAMENTO'][$iform]['tipo']="select_rela";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=8;
$form['EQUIPAMENTO'][$iform]['max']=8;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="FABRICANTE";
$form['EQUIPAMENTO'][$iform]['tipo']="select_rela";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=8;
$form['EQUIPAMENTO'][$iform]['max']=8;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="MARCA";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="MODELO";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="NSERIE";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="POTENCIA";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="PRESSAO";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="CORRENTE";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="TENSAO";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="VAZAO";
$form['EQUIPAMENTO'][$iform]['tipo']="texto";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=30;
$form['EQUIPAMENTO'][$iform]['max']=30;
$form['EQUIPAMENTO'][$iform]['unico']=0;

$iform ++;
$form['EQUIPAMENTO'][$iform]['campo']="FICHA_TECNICA";
$form['EQUIPAMENTO'][$iform]['tipo']="blog";
$form['EQUIPAMENTO'][$iform]['null']=1;
$form['EQUIPAMENTO'][$iform]['tam']=36;
$form['EQUIPAMENTO'][$iform]['max']=5;

$form['EQUIPAMENTO'][$iform]['fecha_fieldset']=1;


//falta terminar


// COMPONENTES

// FORMULARIO FUNCIONARIOS

$iform = 0;
$form['FUNCIONARIO'][$iform]['nome']=FUNCIONARIOS;
$form['FUNCIONARIO'][$iform]['titulo']=$tdb[FUNCIONARIOS]['DESC'];
$form['FUNCIONARIO'][$iform]['obs']="";
$form['FUNCIONARIO'][$iform]['largura']=560;
$form['FUNCIONARIO'][$iform]['altura']=400;
$form['FUNCIONARIO'][$iform]['chave']="MID";

$form['FUNCIONARIO'][0]['nome']=FUNCIONARIOS;
$form['FUNCIONARIO'][0]['titulo']=$tdb[FUNCIONARIOS]['DESC'];
$form['FUNCIONARIO'][0]['obs']="";
$form['FUNCIONARIO'][0]['largura']=560;
$form['FUNCIONARIO'][0]['altura']=400;
$form['FUNCIONARIO'][0]['chave']="MID";

$form['FUNCIONARIO'][1]['abre_fieldset']=$tdb[FUNCIONARIOS]['DESC'];

$form['FUNCIONARIO'][1]['campo']="MID_EMPRESA";
$form['FUNCIONARIO'][1]['tipo']="select_rela";
$form['FUNCIONARIO'][1]['null']=0;
$form['FUNCIONARIO'][1]['tam']=35;
$form['FUNCIONARIO'][1]['max']=120;

$form['FUNCIONARIO'][2]['campo']="NOME";
$form['FUNCIONARIO'][2]['tipo']="texto";
$form['FUNCIONARIO'][2]['null']=0;
$form['FUNCIONARIO'][2]['tam']=35;
$form['FUNCIONARIO'][2]['max']=120;
$form['FUNCIONARIO'][2]['unico']=1;

$form['FUNCIONARIO'][3]['campo']="RG";
$form['FUNCIONARIO'][3]['tipo']="texto";
$form['FUNCIONARIO'][3]['null']=1;
$form['FUNCIONARIO'][3]['tam']=16;
$form['FUNCIONARIO'][3]['max']=16;
$form['FUNCIONARIO'][3]['unico']=0;

$form['FUNCIONARIO'][4]['campo']="CPF";
$form['FUNCIONARIO'][4]['tipo']="texto";
$form['FUNCIONARIO'][4]['null']=1;
$form['FUNCIONARIO'][4]['tam']=16;
$form['FUNCIONARIO'][4]['max']=18;
$form['FUNCIONARIO'][4]['unico']=0;

$form['FUNCIONARIO'][5]['campo']="DATA_NASCIMENTO";
$form['FUNCIONARIO'][5]['tipo']="data";
$form['FUNCIONARIO'][5]['null']=1;
$form['FUNCIONARIO'][5]['tam']=0;
$form['FUNCIONARIO'][5]['max']=0;
$form['FUNCIONARIO'][5]['unico']=0;

$form['FUNCIONARIO'][6]['campo']="MATRICULA";
$form['FUNCIONARIO'][6]['tipo']="texto";
$form['FUNCIONARIO'][6]['null']=1;
$form['FUNCIONARIO'][6]['tam']=16;
$form['FUNCIONARIO'][6]['max']=50;
$form['FUNCIONARIO'][6]['unico']=0;

$form['FUNCIONARIO'][7]['campo']="ESPECIALIDADE";
$form['FUNCIONARIO'][7]['tipo']="select_rela";
$form['FUNCIONARIO'][7]['null']=1;
$form['FUNCIONARIO'][7]['tam']=16;
$form['FUNCIONARIO'][7]['max']=50;
$form['FUNCIONARIO'][7]['unico']=0;

$form['FUNCIONARIO'][8]['campo']="NIVEL";
$form['FUNCIONARIO'][8]['tipo']="texto";
$form['FUNCIONARIO'][8]['null']=1;
$form['FUNCIONARIO'][8]['tam']=16;
$form['FUNCIONARIO'][8]['max']=50;
$form['FUNCIONARIO'][8]['unico']=0;

$form['FUNCIONARIO'][9]['campo']="EQUIPE";
$form['FUNCIONARIO'][9]['tipo']="select_rela";
$form['FUNCIONARIO'][9]['null']=0;
$form['FUNCIONARIO'][9]['tam']=16;
$form['FUNCIONARIO'][9]['max']=50;
$form['FUNCIONARIO'][9]['unico']=0;

$form['FUNCIONARIO'][10]['campo']="CENTRO_DE_CUSTO";
$form['FUNCIONARIO'][10]['tipo']="select_rela";
$form['FUNCIONARIO'][10]['null']=1;
$form['FUNCIONARIO'][10]['tam']=16;
$form['FUNCIONARIO'][10]['max']=50;
$form['FUNCIONARIO'][10]['unico']=0;

$form['FUNCIONARIO'][11]['campo']="VALOR_HORA";
$form['FUNCIONARIO'][11]['tipo']="texto_float";
$form['FUNCIONARIO'][11]['null']=1;
$form['FUNCIONARIO'][11]['tam']=16;
$form['FUNCIONARIO'][11]['max']=13;
$form['FUNCIONARIO'][11]['unico']=0;

$form['FUNCIONARIO'][12]['campo']="ENDERECO";
$form['FUNCIONARIO'][12]['tipo']="texto";
$form['FUNCIONARIO'][12]['null']=1;
$form['FUNCIONARIO'][12]['tam']=35;
$form['FUNCIONARIO'][12]['max']=150;
$form['FUNCIONARIO'][12]['unico']=0;

$form['FUNCIONARIO'][13]['campo']="BAIRRO";
$form['FUNCIONARIO'][13]['tipo']="texto";
$form['FUNCIONARIO'][13]['null']=1;
$form['FUNCIONARIO'][13]['tam']=35;
$form['FUNCIONARIO'][13]['max']=100;
$form['FUNCIONARIO'][13]['unico']=0;

$form['FUNCIONARIO'][14]['campo']="CEP";
$form['FUNCIONARIO'][14]['tipo']="texto";
$form['FUNCIONARIO'][14]['null']=1;
$form['FUNCIONARIO'][14]['tam']=16;
$form['FUNCIONARIO'][14]['max']=16;
$form['FUNCIONARIO'][14]['unico']=0;

$form['FUNCIONARIO'][15]['campo']="CIDADE";
$form['FUNCIONARIO'][15]['tipo']="texto";
$form['FUNCIONARIO'][15]['null']=1;
$form['FUNCIONARIO'][15]['tam']=35;
$form['FUNCIONARIO'][15]['max']=100;
$form['FUNCIONARIO'][15]['unico']=0;

$form['FUNCIONARIO'][16]['campo']="UF";
$form['FUNCIONARIO'][16]['tipo']="texto";
$form['FUNCIONARIO'][16]['null']=1;
$form['FUNCIONARIO'][16]['tam']=2;
$form['FUNCIONARIO'][16]['max']=2;
$form['FUNCIONARIO'][16]['unico']=0;

$form['FUNCIONARIO'][17]['campo']="TELEFONE";
$form['FUNCIONARIO'][17]['tipo']="texto";
$form['FUNCIONARIO'][17]['null']=1;
$form['FUNCIONARIO'][17]['tam']=16;
$form['FUNCIONARIO'][17]['max']=30;
$form['FUNCIONARIO'][17]['unico']=0;

$form['FUNCIONARIO'][18]['campo']="CELULAR";
$form['FUNCIONARIO'][18]['tipo']="texto";
$form['FUNCIONARIO'][18]['null']=1;
$form['FUNCIONARIO'][18]['tam']=16;
$form['FUNCIONARIO'][18]['max']=30;
$form['FUNCIONARIO'][18]['unico']=0;

$form['FUNCIONARIO'][19]['campo']="EMAIL";
$form['FUNCIONARIO'][19]['tipo']="texto";
$form['FUNCIONARIO'][19]['null']=1;
$form['FUNCIONARIO'][19]['tam']=35;
$form['FUNCIONARIO'][19]['max']=150;
$form['FUNCIONARIO'][19]['unico']=0;

$form['FUNCIONARIO'][20]['campo']="SITUACAO";
$form['FUNCIONARIO'][20]['tipo']="select_rela";
$form['FUNCIONARIO'][20]['null']=0;
$form['FUNCIONARIO'][20]['tam']=16;
$form['FUNCIONARIO'][20]['max']=50;
$form['FUNCIONARIO'][20]['unico']=0;

$form['FUNCIONARIO'][21]['campo']="TURNO";
$form['FUNCIONARIO'][21]['tipo']="select_rela";
$form['FUNCIONARIO'][21]['null']=1;
$form['FUNCIONARIO'][21]['tam']=16;
$form['FUNCIONARIO'][21]['max']=50;
$form['FUNCIONARIO'][21]['unico']=0;

$form['FUNCIONARIO'][22]['campo']="DATA_ADMISSAO";
$form['FUNCIONARIO'][22]['tipo']="data";
$form['FUNCIONARIO'][22]['null']=0;
$form['FUNCIONARIO'][22]['tam']=0;
$form['FUNCIONARIO'][22]['max']=0;
$form['FUNCIONARIO'][22]['unico']=0;

$form['FUNCIONARIO'][23]['campo']="DATA_DEMISSAO";
$form['FUNCIONARIO'][23]['tipo']="data";
$form['FUNCIONARIO'][23]['null']=1;
$form['FUNCIONARIO'][23]['tam']=0;
$form['FUNCIONARIO'][23]['max']=0;
$form['FUNCIONARIO'][23]['unico']=0;

$form['FUNCIONARIO'][24]['campo']="OBSERVACAO";
$form['FUNCIONARIO'][24]['tipo']="blog";
$form['FUNCIONARIO'][24]['null']=1;
$form['FUNCIONARIO'][24]['tam']=30;
$form['FUNCIONARIO'][24]['max']=10;
$form['FUNCIONARIO'][24]['unico']=0;

$form['FUNCIONARIO'][24]['fecha_fieldset']=1;



// FORMULARIO PRESTADORES DE SERVICO
$form['FUNCIONARIO2'][0]['nome']=FUNCIONARIOS;
$form['FUNCIONARIO2'][0]['titulo']=$ling['prestador'];
$form['FUNCIONARIO2'][0]['obs']="";
$form['FUNCIONARIO2'][0]['largura']=480;
$form['FUNCIONARIO2'][0]['altura']=250;
$form['FUNCIONARIO2'][0]['chave']="MID";

$form['FUNCIONARIO2'][1]['abre_fieldset']=$ling['prestador'];

$form['FUNCIONARIO2'][1]['campo']="MID_EMPRESA";
$form['FUNCIONARIO2'][1]['tipo']="select_rela";
$form['FUNCIONARIO2'][1]['null']=0;
$form['FUNCIONARIO2'][1]['tam']=35;
$form['FUNCIONARIO2'][1]['max']=120;

$form['FUNCIONARIO2'][2]['campo']="FORNECEDOR";
$form['FUNCIONARIO2'][2]['tipo']="select_rela";
$form['FUNCIONARIO2'][2]['null']=0;
$form['FUNCIONARIO2'][2]['tam']=16;
$form['FUNCIONARIO2'][2]['max']=16;
$form['FUNCIONARIO2'][2]['unico']=0;

$form['FUNCIONARIO2'][3]['campo']="NOME";
$form['FUNCIONARIO2'][3]['tipo']="texto";
$form['FUNCIONARIO2'][3]['null']=0;
$form['FUNCIONARIO2'][3]['tam']=35;
$form['FUNCIONARIO2'][3]['max']=120;
$form['FUNCIONARIO2'][3]['unico']=1;

$form['FUNCIONARIO2'][4]['campo']="ESPECIALIDADE";
$form['FUNCIONARIO2'][4]['tipo']="select_rela";
$form['FUNCIONARIO2'][4]['null']=1;
$form['FUNCIONARIO2'][4]['tam']=16;
$form['FUNCIONARIO2'][4]['max']=50;
$form['FUNCIONARIO2'][4]['unico']=0;

$form['FUNCIONARIO2'][5]['campo']="SITUACAO";
$form['FUNCIONARIO2'][5]['tipo']="select_rela";
$form['FUNCIONARIO2'][5]['null']=0;
$form['FUNCIONARIO2'][5]['tam']=16;
$form['FUNCIONARIO2'][5]['max']=50;

$form['FUNCIONARIO2'][6]['campo']="VALOR_HORA";
$form['FUNCIONARIO2'][6]['tipo']="texto";
$form['FUNCIONARIO2'][6]['null']=1;
$form['FUNCIONARIO2'][6]['tam']=13;
$form['FUNCIONARIO2'][6]['max']=13;
$form['FUNCIONARIO2'][6]['unico']=0;

$form['FUNCIONARIO2'][6]['fecha_fieldset']=1;


// FORMULARIO EQUIPE
$form['EQUIPE'][0]['nome']=EQUIPES;
$form['EQUIPE'][0]['titulo']=$tdb[EQUIPES]['DESC'];
$form['EQUIPE'][0]['obs']="";
$form['EQUIPE'][0]['largura']=500;
$form['EQUIPE'][0]['altura']=300;
$form['EQUIPE'][0]['chave']="MID";

$form['EQUIPE'][1]['abre_fieldset']=$tdb[EQUIPES]['DESC'];

$form['EQUIPE'][1]['campo']="MID_EMPRESA";
$form['EQUIPE'][1]['tipo']="select_rela";
$form['EQUIPE'][1]['null']=0;
$form['EQUIPE'][1]['tam']=35;
$form['EQUIPE'][1]['max']=120;

$form['EQUIPE'][2]['campo']="DESCRICAO";
$form['EQUIPE'][2]['tipo']="texto";
$form['EQUIPE'][2]['null']=0;
$form['EQUIPE'][2]['tam']=30;
$form['EQUIPE'][2]['max']=100;
$form['EQUIPE'][2]['unico']=1;

$form['EQUIPE'][3]['campo']="TIPO";
$form['EQUIPE'][3]['tipo']="texto";
$form['EQUIPE'][3]['null']=1;
$form['EQUIPE'][3]['tam']=30;
$form['EQUIPE'][3]['max']=50;
$form['EQUIPE'][3]['unico']=0;

$form['EQUIPE'][4]['campo']="FORNECEDOR";
$form['EQUIPE'][4]['tipo']="select_rela";
$form['EQUIPE'][4]['null']=1;
$form['EQUIPE'][4]['tam']=30;
$form['EQUIPE'][4]['max']=50;
$form['EQUIPE'][4]['unico']=0;

$form['EQUIPE'][5]['campo']="OBSERVACAO";
$form['EQUIPE'][5]['tipo']="blog";
$form['EQUIPE'][5]['null']=1;
$form['EQUIPE'][5]['tam']=35;
$form['EQUIPE'][5]['max']=5;
$form['EQUIPE'][5]['unico']=0;

$form['EQUIPE'][5]['fecha_fieldset']=1;

// FORMULARIO AFASTAMENTOS
$form['AFASTAMENTO'][0]['nome']=AFASTAMENTOS;
$form['AFASTAMENTO'][0]['titulo']=$tdb[AFASTAMENTOS]['DESC'];
$form['AFASTAMENTO'][0]['obs']="";
$form['AFASTAMENTO'][0]['largura']=500;
$form['AFASTAMENTO'][0]['altura']=210;
$form['AFASTAMENTO'][0]['chave']="MID";

$form['AFASTAMENTO'][1]['abre_fieldset']=$tdb[AFASTAMENTOS]['DESC'];

$form['AFASTAMENTO'][1]['campo']="MID_FUNCIONARIO";
$form['AFASTAMENTO'][1]['tipo']="select_rela";
$form['AFASTAMENTO'][1]['null']=0;
$form['AFASTAMENTO'][1]['tam']=0;
$form['AFASTAMENTO'][1]['max']=0;
$form['AFASTAMENTO'][1]['unico']=0;

$form['AFASTAMENTO'][2]['campo']="DATA_INICIAL";
$form['AFASTAMENTO'][2]['tipo']="data";
$form['AFASTAMENTO'][2]['null']=0;
$form['AFASTAMENTO'][2]['tam']=30;
$form['AFASTAMENTO'][2]['max']=100;
$form['AFASTAMENTO'][2]['unico']=0;

$form['AFASTAMENTO'][3]['campo']="DATA_FINAL";
$form['AFASTAMENTO'][3]['tipo']="data";
$form['AFASTAMENTO'][3]['null']=0;
$form['AFASTAMENTO'][3]['tam']=30;
$form['AFASTAMENTO'][3]['max']=100;
$form['AFASTAMENTO'][3]['unico']=0;

$form['AFASTAMENTO'][4]['campo']="MOTIVO";
$form['AFASTAMENTO'][4]['tipo']="texto";
$form['AFASTAMENTO'][4]['null']=1;
$form['AFASTAMENTO'][4]['tam']=30;
$form['AFASTAMENTO'][4]['max']=255;
$form['AFASTAMENTO'][4]['unico']=0;

$form['AFASTAMENTO'][4]['fecha_fieldset']=1;

// FORMULARIO AUSENCIAS
$form['AUSENCIA'][0]['nome']=AUSENCIAS;
$form['AUSENCIA'][0]['titulo']=$tdb[AUSENCIAS]['DESC'];
$form['AUSENCIA'][0]['obs']="";
$form['AUSENCIA'][0]['largura']=500;
$form['AUSENCIA'][0]['altura']=250;
$form['AUSENCIA'][0]['chave']="MID";
$form['AUSENCIA'][1]['abre_fieldset']=$tdb[AUSENCIAS]['DESC'];

$form['AUSENCIA'][1]['campo']="MID_FUNCIONARIO";
$form['AUSENCIA'][1]['tipo']="select_rela";
$form['AUSENCIA'][1]['null']=0;
$form['AUSENCIA'][1]['tam']=0;
$form['AUSENCIA'][1]['max']=0;
$form['AUSENCIA'][1]['unico']=0;

$form['AUSENCIA'][2]['campo']="HORA_INICIAL";
$form['AUSENCIA'][2]['tipo']="hora";
$form['AUSENCIA'][2]['null']=0;
$form['AUSENCIA'][2]['tam']=30;
$form['AUSENCIA'][2]['max']=30;
$form['AUSENCIA'][2]['unico']=0;

$form['AUSENCIA'][3]['campo']="HORA_FINAL";
$form['AUSENCIA'][3]['tipo']="hora";
$form['AUSENCIA'][3]['null']=0;
$form['AUSENCIA'][3]['tam']=30;
$form['AUSENCIA'][3]['max']=30;
$form['AUSENCIA'][3]['unico']=0;

$form['AUSENCIA'][4]['campo']="DATA";
$form['AUSENCIA'][4]['tipo']="data";
$form['AUSENCIA'][4]['null']=0;
$form['AUSENCIA'][4]['tam']=30;
$form['AUSENCIA'][4]['max']=100;
$form['AUSENCIA'][4]['unico']=0;

$form['AUSENCIA'][5]['campo']="MOTIVO";
$form['AUSENCIA'][5]['tipo']="texto";
$form['AUSENCIA'][5]['null']=1;
$form['AUSENCIA'][5]['tam']=30;
$form['AUSENCIA'][5]['max']=255;
$form['AUSENCIA'][5]['unico']=0;
$form['AUSENCIA'][5]['fecha_fieldset']=1;

// FORMULARIO ESPECIALIDADES
$form['ESPECIALIDADE'][0]['nome']=ESPECIALIDADES;
$form['ESPECIALIDADE'][0]['titulo']=$tdb[ESPECIALIDADES]['DESC'];
$form['ESPECIALIDADE'][0]['obs']="";
$form['ESPECIALIDADE'][0]['largura']=500;
$form['ESPECIALIDADE'][0]['altura']=160;
$form['ESPECIALIDADE'][0]['chave']="MID";

$form['ESPECIALIDADE'][1]['abre_fieldset']=$tdb[ESPECIALIDADES]['DESC'];

$form['ESPECIALIDADE'][1]['campo']="MID_EMPRESA";
$form['ESPECIALIDADE'][1]['tipo']="select_rela";
$form['ESPECIALIDADE'][1]['null']=1;
$form['ESPECIALIDADE'][1]['tam']=15;
$form['ESPECIALIDADE'][1]['max']=30;

$form['ESPECIALIDADE'][2]['campo']="DESCRICAO";
$form['ESPECIALIDADE'][2]['tipo']="texto";
$form['ESPECIALIDADE'][2]['null']=0;
$form['ESPECIALIDADE'][2]['tam']=30;
$form['ESPECIALIDADE'][2]['max']=250;
$form['ESPECIALIDADE'][2]['unico']=1;

$form['ESPECIALIDADE'][2]['fecha_fieldset']=1;

// FORMULARIO TURNOS
$form['TURNO'][0]['nome']=TURNOS;
$form['TURNO'][0]['titulo']=$tdb[TURNOS]['DESC'];
$form['TURNO'][0]['obs']="";
$form['TURNO'][0]['largura']=400;
$form['TURNO'][0]['altura']=200;
$form['TURNO'][0]['chave']="MID";

$form['TURNO'][1]['abre_fieldset']=$tdb[TURNOS]['DESC'];

$form['TURNO'][1]['campo']="MID_EMPRESA";
$form['TURNO'][1]['tipo']="select_rela";
$form['TURNO'][1]['null']=1;
$form['TURNO'][1]['tam']=15;
$form['TURNO'][1]['max']=30;

$form['TURNO'][2]['campo']="DESCRICAO";
$form['TURNO'][2]['tipo']="texto";
$form['TURNO'][2]['null']=0;
$form['TURNO'][2]['tam']=30;
$form['TURNO'][2]['max']=250;
$form['TURNO'][2]['unico']=1;

$form['TURNO'][2]['fecha_fieldset']=1;


$form['PONTO_PREDITIVA_EQUIP'][0]['nome']=PONTOS_PREDITIVA;
$form['PONTO_PREDITIVA_EQUIP'][0]['titulo']=$tdb[PONTOS_PREDITIVA]['DESC'];
$form['PONTO_PREDITIVA_EQUIP'][0]['obs']="";
$form['PONTO_PREDITIVA_EQUIP'][0]['largura']=450;
$form['PONTO_PREDITIVA_EQUIP'][0]['altura']=300;
$form['PONTO_PREDITIVA_EQUIP'][0]['chave']="MID";

$form['PONTO_PREDITIVA_EQUIP'][1]['abre_fieldset']=$tdb[PONTOS_PREDITIVA]['DESC'];

$form['PONTO_PREDITIVA_EQUIP'][1]['campo']="PONTO";
$form['PONTO_PREDITIVA_EQUIP'][1]['tipo']="texto";
$form['PONTO_PREDITIVA_EQUIP'][1]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][1]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][1]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][1]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][2]['campo']="GRANDEZA";
$form['PONTO_PREDITIVA_EQUIP'][2]['tipo']="texto";
$form['PONTO_PREDITIVA_EQUIP'][2]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][2]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][2]['max']=50;
$form['PONTO_PREDITIVA_EQUIP'][2]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][3]['campo']="VALOR_MINIMO";
$form['PONTO_PREDITIVA_EQUIP'][3]['tipo']="texto_float";
$form['PONTO_PREDITIVA_EQUIP'][3]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][3]['tam']=15;
$form['PONTO_PREDITIVA_EQUIP'][3]['max']=30;
$form['PONTO_PREDITIVA_EQUIP'][3]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][4]['campo']="VALOR_MAXIMO";
$form['PONTO_PREDITIVA_EQUIP'][4]['tipo']="texto_float";
$form['PONTO_PREDITIVA_EQUIP'][4]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][4]['tam']=15;
$form['PONTO_PREDITIVA_EQUIP'][4]['max']=30;
$form['PONTO_PREDITIVA_EQUIP'][4]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][5]['campo']="OBSERVACAO";
$form['PONTO_PREDITIVA_EQUIP'][5]['tipo']="blog";
$form['PONTO_PREDITIVA_EQUIP'][5]['null']=1;
$form['PONTO_PREDITIVA_EQUIP'][5]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][5]['max']=5;
$form['PONTO_PREDITIVA_EQUIP'][5]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][6]['campo']="MID_MAQUINA";
$form['PONTO_PREDITIVA_EQUIP'][6]['tipo']="recb_valor";
$form['PONTO_PREDITIVA_EQUIP'][6]['valor']=(int)VoltaValor(EQUIPAMENTOS,'MID_MAQUINA','MID',$form_recb_valor,$tdb[PONTOS_PREDITIVA]['dba']);
$form['PONTO_PREDITIVA_EQUIP'][6]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][6]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][6]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][6]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][7]['campo']="MID_EQUIPAMENTO";
$form['PONTO_PREDITIVA_EQUIP'][7]['tipo']="recb_valor";
$form['PONTO_PREDITIVA_EQUIP'][7]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA_EQUIP'][7]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][7]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][7]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][7]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][7]['fecha_fieldset']=1;


// FORMULARIO MATERIAL DO EQUIPAMENTO
$form['EQUIPAMENTO_MATERIAL'][0]['nome']=EQUIPAMENTOS_MATERIAL;
$form['EQUIPAMENTO_MATERIAL'][0]['titulo']=$tdb[EQUIPAMENTOS_MATERIAL]['DESC'];
$form['EQUIPAMENTO_MATERIAL'][0]['obs']="";
$form['EQUIPAMENTO_MATERIAL'][0]['largura']=550;
$form['EQUIPAMENTO_MATERIAL'][0]['altura']=200;
$form['EQUIPAMENTO_MATERIAL'][0]['chave']="MID";

$form['EQUIPAMENTO_MATERIAL'][1]['abre_fieldset']=$tdb[EQUIPAMENTOS_MATERIAL]['DESC'];

$form['EQUIPAMENTO_MATERIAL'][1]['campo']="MID_MATERIAL";
$form['EQUIPAMENTO_MATERIAL'][1]['tipo']="select_material";
$form['EQUIPAMENTO_MATERIAL'][1]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][1]['tam']=10;
$form['EQUIPAMENTO_MATERIAL'][1]['max']=10;
$form['EQUIPAMENTO_MATERIAL'][1]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][2]['campo']="QUANTIDADE";
$form['EQUIPAMENTO_MATERIAL'][2]['tipo']="texto_unidade";
$form['EQUIPAMENTO_MATERIAL'][2]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][2]['tam']=5;
$form['EQUIPAMENTO_MATERIAL'][2]['max']=5;
$form['EQUIPAMENTO_MATERIAL'][2]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][3]['campo']="MID_CLASSE";
$form['EQUIPAMENTO_MATERIAL'][3]['tipo']="select_rela";
$form['EQUIPAMENTO_MATERIAL'][3]['null']=1;
$form['EQUIPAMENTO_MATERIAL'][3]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][4]['campo']="MID_EQUIPAMENTO";
$form['EQUIPAMENTO_MATERIAL'][4]['tipo']="recb_valor";
$form['EQUIPAMENTO_MATERIAL'][4]['valor']=$form_recb_valor;
$form['EQUIPAMENTO_MATERIAL'][4]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['tam']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['max']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][4]['fecha_fieldset']=1;

// FORMULARIO MATERIAL DO EQUIPAMENTO
$form['ESTOQUE_ENTRADA'][0]['nome']=ESTOQUE_ENTRADA;
$form['ESTOQUE_ENTRADA'][0]['titulo']=$tdb[ESTOQUE_ENTRADA]['DESC'];
$form['ESTOQUE_ENTRADA'][0]['obs']="";
$form['ESTOQUE_ENTRADA'][0]['largura']=600;
$form['ESTOQUE_ENTRADA'][0]['altura']=440;
$form['ESTOQUE_ENTRADA'][0]['chave']="MID";

$form['ESTOQUE_ENTRADA'][1]['abre_fieldset']=$tdb[ESTOQUE_ENTRADA]['DESC'];

$form['ESTOQUE_ENTRADA'][1]['campo']="MID_ALMOXARIFADO";
$form['ESTOQUE_ENTRADA'][1]['tipo']="select_rela";
$form['ESTOQUE_ENTRADA'][1]['null']=0;
$form['ESTOQUE_ENTRADA'][1]['unico']=0;

$form['ESTOQUE_ENTRADA'][2]['campo']="MID_MATERIAL";
$form['ESTOQUE_ENTRADA'][2]['tipo']="select_material";
$form['ESTOQUE_ENTRADA'][2]['null']=0;
$form['ESTOQUE_ENTRADA'][2]['unico']=0;

$form['ESTOQUE_ENTRADA'][3]['campo']="QUANTIDADE";
$form['ESTOQUE_ENTRADA'][3]['tipo']="texto_unidade";
$form['ESTOQUE_ENTRADA'][3]['null']=0;
$form['ESTOQUE_ENTRADA'][3]['tam']=5;
$form['ESTOQUE_ENTRADA'][3]['max']=5;
$form['ESTOQUE_ENTRADA'][3]['onblur']="document.getElementById('cc[" . ESTOQUE_ENTRADA . "][5]').value = (roundNumber(parseFloat(this.value.replace(',', '.')) * parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][4]').value.replace(',', '.')), 2)).toString().replace('.', ',').replace('NaN', '')";
$form['ESTOQUE_ENTRADA'][3]['nao_negativo']=1;
$form['ESTOQUE_ENTRADA'][3]['nao_zero']=1;
$form['ESTOQUE_ENTRADA'][3]['campo_material']=2;

$form['ESTOQUE_ENTRADA'][4]['campo']="VALOR_UNITARIO";
$form['ESTOQUE_ENTRADA'][4]['tipo']="texto";
$form['ESTOQUE_ENTRADA'][4]['null']=0;
$form['ESTOQUE_ENTRADA'][4]['tam']=10;
$form['ESTOQUE_ENTRADA'][4]['max']=10;

$form['ESTOQUE_ENTRADA'][5]['campo']="VALOR_TOTAL";
$form['ESTOQUE_ENTRADA'][5]['tipo']="texto_float";
$form['ESTOQUE_ENTRADA'][5]['null']=0;
$form['ESTOQUE_ENTRADA'][5]['tam']=10;
$form['ESTOQUE_ENTRADA'][5]['max']=10;
$form['ESTOQUE_ENTRADA'][5]['onfocus']="this.value = (roundNumber(parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][3]').value.replace(',', '.')) * parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][4]').value.replace(',', '.')), 2)).toString().replace('.', ',').replace('NaN', '')";
$form['ESTOQUE_ENTRADA'][5]['nao_negativo']=1;

$form['ESTOQUE_ENTRADA'][6]['campo']="CENTRO_DE_CUSTO";
$form['ESTOQUE_ENTRADA'][6]['tipo']="select_rela";
$form['ESTOQUE_ENTRADA'][6]['null']=1;
$form['ESTOQUE_ENTRADA'][6]['unico']=0;

$form['ESTOQUE_ENTRADA'][7]['campo']="DATA";
$form['ESTOQUE_ENTRADA'][7]['tipo']="data";
$form['ESTOQUE_ENTRADA'][7]['valor']=1;
$form['ESTOQUE_ENTRADA'][7]['null']=0;
$form['ESTOQUE_ENTRADA'][7]['unico']=0;

$form['ESTOQUE_ENTRADA'][8]['campo']="HORA";
$form['ESTOQUE_ENTRADA'][8]['tipo']="hora";
$form['ESTOQUE_ENTRADA'][8]['valor']=1;
$form['ESTOQUE_ENTRADA'][8]['null']=0;
$form['ESTOQUE_ENTRADA'][8]['unico']=0;

$form['ESTOQUE_ENTRADA'][9]['campo']="MOTIVO";
$form['ESTOQUE_ENTRADA'][9]['tipo']="texto";
$form['ESTOQUE_ENTRADA'][9]['null']=1;
$form['ESTOQUE_ENTRADA'][9]['tam']=40;
$form['ESTOQUE_ENTRADA'][9]['max']=150;
$form['ESTOQUE_ENTRADA'][9]['unico']=0;

$form['ESTOQUE_ENTRADA'][10]['campo']="DOCUMENTO";
$form['ESTOQUE_ENTRADA'][10]['tipo']="texto";
$form['ESTOQUE_ENTRADA'][10]['null']=1;
$form['ESTOQUE_ENTRADA'][10]['tam']=40;
$form['ESTOQUE_ENTRADA'][10]['max']=150;
$form['ESTOQUE_ENTRADA'][10]['unico']=0;

$form['ESTOQUE_ENTRADA'][11]['campo']="OBSERVACAO";
$form['ESTOQUE_ENTRADA'][11]['tipo']="blog";
$form['ESTOQUE_ENTRADA'][11]['null']=1;
$form['ESTOQUE_ENTRADA'][11]['tam']=40;
$form['ESTOQUE_ENTRADA'][11]['max']=3;
$form['ESTOQUE_ENTRADA'][11]['unico']=0;

$form['ESTOQUE_ENTRADA'][11]['fecha_fieldset']=1;

// FORMULARIO MATERIAL DO EQUIPAMENTO
$form['ESTOQUE_SAIDA'][0]['nome']=ESTOQUE_SAIDA;
$form['ESTOQUE_SAIDA'][0]['titulo']=$tdb[ESTOQUE_SAIDA]['DESC'];
$form['ESTOQUE_SAIDA'][0]['obs']="";
$form['ESTOQUE_SAIDA'][0]['largura']=600;
$form['ESTOQUE_SAIDA'][0]['altura']=380;
$form['ESTOQUE_SAIDA'][0]['chave']="MID";

$form['ESTOQUE_SAIDA'][1]['abre_fieldset']=$tdb[ESTOQUE_SAIDA]['DESC'];

$form['ESTOQUE_SAIDA'][1]['campo']="MID_ALMOXARIFADO";
$form['ESTOQUE_SAIDA'][1]['tipo']="select_rela";
$form['ESTOQUE_SAIDA'][1]['null']=0;
$form['ESTOQUE_SAIDA'][1]['unico']=0;
$form['ESTOQUE_SAIDA'][1]['js']="document.forms[0].submit()";

// Mostrar campo somente depois de selecionar o almoxarifado
if ($_POST['cc']) {
    $form['ESTOQUE_SAIDA'][2]['campo']="MID_MATERIAL";
    $form['ESTOQUE_SAIDA'][2]['tipo']="select_material";
    $form['ESTOQUE_SAIDA'][2]['null']=0;
    $form['ESTOQUE_SAIDA'][2]['unico']=0;
}
else {
    $form['ESTOQUE_SAIDA'][2]['campo']="MID_MATERIAL";
    $form['ESTOQUE_SAIDA'][2]['tipo']="recb_valor";
    $form['ESTOQUE_SAIDA'][2]['null']=0;
    $form['ESTOQUE_SAIDA'][2]['unico']=0;
    $form['ESTOQUE_SAIDA'][2]['valor']=0;
}

$form['ESTOQUE_SAIDA'][3]['campo']="QUANTIDADE";
$form['ESTOQUE_SAIDA'][3]['tipo']="texto_unidade";
$form['ESTOQUE_SAIDA'][3]['null']=0;
$form['ESTOQUE_SAIDA'][3]['tam']=5;
$form['ESTOQUE_SAIDA'][3]['max']=5;
$form['ESTOQUE_SAIDA'][3]['unico']=0;

$form['ESTOQUE_SAIDA'][4]['campo']="CENTRO_DE_CUSTO";
$form['ESTOQUE_SAIDA'][4]['tipo']="select_rela";
$form['ESTOQUE_SAIDA'][4]['null']=1;
$form['ESTOQUE_SAIDA'][4]['unico']=0;

$form['ESTOQUE_SAIDA'][5]['campo']="DATA";
$form['ESTOQUE_SAIDA'][5]['tipo']="data";
$form['ESTOQUE_SAIDA'][5]['valor']=1;
$form['ESTOQUE_SAIDA'][5]['null']=0;
$form['ESTOQUE_SAIDA'][5]['unico']=0;

$form['ESTOQUE_SAIDA'][6]['campo']="HORA";
$form['ESTOQUE_SAIDA'][6]['tipo']="hora";
$form['ESTOQUE_SAIDA'][6]['valor']=1;
$form['ESTOQUE_SAIDA'][6]['null']=0;
$form['ESTOQUE_SAIDA'][6]['unico']=0;

$form['ESTOQUE_SAIDA'][7]['campo']="MOTIVO";
$form['ESTOQUE_SAIDA'][7]['tipo']="texto";
$form['ESTOQUE_SAIDA'][7]['null']=1;
$form['ESTOQUE_SAIDA'][7]['tam']=40;
$form['ESTOQUE_SAIDA'][7]['max']=150;
$form['ESTOQUE_SAIDA'][7]['unico']=0;

$form['ESTOQUE_SAIDA'][8]['campo']="ORDEM";
$form['ESTOQUE_SAIDA'][8]['tipo']="select_ordem";
$form['ESTOQUE_SAIDA'][8]['null']=1;
$form['ESTOQUE_SAIDA'][8]['tam']=10;
$form['ESTOQUE_SAIDA'][8]['max']=15;
$form['ESTOQUE_SAIDA'][8]['unico']=0;

$form['ESTOQUE_SAIDA'][9]['campo']="OBSERVACAO";
$form['ESTOQUE_SAIDA'][9]['tipo']="blog";
$form['ESTOQUE_SAIDA'][9]['null']=1;
$form['ESTOQUE_SAIDA'][9]['tam']=40;
$form['ESTOQUE_SAIDA'][9]['max']=3;
$form['ESTOQUE_SAIDA'][9]['unico']=0;

$form['ESTOQUE_SAIDA'][9]['fecha_fieldset']=1;

// FORMULARIO LANCAMENTO DO CONTADOR
$form['LANCA_PREDITIVA'][0]['nome']=LANC_PREDITIVA;
$form['LANCA_PREDITIVA'][0]['titulo']=$ling['lanc_pontos_preditiva_aponta'];
$form['LANCA_PREDITIVA'][0]['obs']="";
$form['LANCA_PREDITIVA'][0]['largura']=500;
$form['LANCA_PREDITIVA'][0]['altura']=350;
$form['LANCA_PREDITIVA'][0]['chave']="MID";

$form['LANCA_PREDITIVA'][1]['campo']="VALOR";
$form['LANCA_PREDITIVA'][1]['tipo']="texto";
$form['LANCA_PREDITIVA'][1]['null']=1;
$form['LANCA_PREDITIVA'][1]['tam']=20;
$form['LANCA_PREDITIVA'][1]['max']=13;
$form['LANCA_PREDITIVA'][1]['unico']=0;

$form['LANCA_PREDITIVA'][2]['campo']="DATA";
$form['LANCA_PREDITIVA'][2]['tipo']="data";
$form['LANCA_PREDITIVA'][2]['valor']=1; // Recebe dia ATUAL
$form['LANCA_PREDITIVA'][2]['null']=1;
$form['LANCA_PREDITIVA'][2]['tam']=20;
$form['LANCA_PREDITIVA'][2]['max']=13;
$form['LANCA_PREDITIVA'][2]['unico']=0;

$form['LANCA_PREDITIVA'][3]['campo']="OBSERVACAO";
$form['LANCA_PREDITIVA'][3]['tipo']="blog";
$form['LANCA_PREDITIVA'][3]['null']=1;
$form['LANCA_PREDITIVA'][3]['tam']=20;
$form['LANCA_PREDITIVA'][3]['max']=5;
$form['LANCA_PREDITIVA'][3]['unico']=0;

$form['LANCA_PREDITIVA'][4]['campo']="MID_PONTO";
$form['LANCA_PREDITIVA'][4]['tipo']="recb_valor";
$form['LANCA_PREDITIVA'][4]['valor']=$form_recb_valor;
$form['LANCA_PREDITIVA'][4]['null']=0;
$form['LANCA_PREDITIVA'][4]['tam']=0;
$form['LANCA_PREDITIVA'][4]['max']=0;
$form['LANCA_PREDITIVA'][4]['unico']=0;


// FORMULARIO MATRIX
$form['MATRIX'][0]['nome']=EMPRESAS;
$form['MATRIX'][0]['titulo']=$tdb[EMPRESAS]['DESC'];
$form['MATRIX'][0]['obs']="";
$form['MATRIX'][0]['largura']=500;
$form['MATRIX'][0]['altura']=530;
$form['MATRIX'][0]['chave']="MID";

$form['MATRIX'][1]['abre_fieldset']=$tdb[EMPRESAS]['DESC'];

$form['MATRIX'][1]['campo']="EMP_MATRIZ";
$form['MATRIX'][1]['tipo']="select_rela";
$form['MATRIX'][1]['null']=0;
$form['MATRIX'][1]['tam']=15;
$form['MATRIX'][1]['max']=21;

$form['MATRIX'][2]['campo']="NOME";
$form['MATRIX'][2]['tipo']="texto";
$form['MATRIX'][2]['null']=0;
$form['MATRIX'][2]['tam']=40;
$form['MATRIX'][2]['max']=48;
$form['MATRIX'][2]['unico']=1;

$form['MATRIX'][3]['campo']="FANTASIA";
$form['MATRIX'][3]['tipo']="texto";
$form['MATRIX'][3]['null']=1;
$form['MATRIX'][3]['tam']=40;
$form['MATRIX'][3]['max']=48;

$form['MATRIX'][4]['campo']="CNPJ";
$form['MATRIX'][4]['tipo']="texto";
$form['MATRIX'][4]['null']=0;
$form['MATRIX'][4]['tam']=14;
$form['MATRIX'][4]['max']=14;

$form['MATRIX'][5]['campo']="IE";
$form['MATRIX'][5]['tipo']="texto";
$form['MATRIX'][5]['null']=1;
$form['MATRIX'][5]['tam']=14;
$form['MATRIX'][5]['max']=15;

$form['MATRIX'][6]['campo']="ENDERECO";
$form['MATRIX'][6]['tipo']="texto";
$form['MATRIX'][6]['null']=1;
$form['MATRIX'][6]['tam']=40;
$form['MATRIX'][6]['max']=55;

$form['MATRIX'][7]['campo']="NUMERO";
$form['MATRIX'][7]['tipo']="texto";
$form['MATRIX'][7]['null']=1;
$form['MATRIX'][7]['tam']=5;
$form['MATRIX'][7]['max']=11;

$form['MATRIX'][8]['campo']="COMPLEMENTO";
$form['MATRIX'][8]['tipo']="texto";
$form['MATRIX'][8]['null']=1;
$form['MATRIX'][8]['tam']=25;
$form['MATRIX'][8]['max']=25;

$form['MATRIX'][9]['campo']="BAIRRO";
$form['MATRIX'][9]['tipo']="texto";
$form['MATRIX'][9]['null']=1;
$form['MATRIX'][9]['tam']=25;
$form['MATRIX'][9]['max']=35;

$form['MATRIX'][10]['campo']="CEP";
$form['MATRIX'][10]['tipo']="texto";
$form['MATRIX'][10]['null']=1;
$form['MATRIX'][10]['tam']=10;
$form['MATRIX'][10]['max']=10;

$form['MATRIX'][11]['campo']="CIDADE";
$form['MATRIX'][11]['tipo']="texto";
$form['MATRIX'][11]['null']=1;
$form['MATRIX'][11]['tam']=25;
$form['MATRIX'][11]['max']=32;

$form['MATRIX'][12]['campo']="UF";
$form['MATRIX'][12]['tipo']="texto";
$form['MATRIX'][12]['null']=1;
$form['MATRIX'][12]['tam']=2;
$form['MATRIX'][12]['max']=2;

$form['MATRIX'][13]['campo']="PAIS";
$form['MATRIX'][13]['tipo']="texto";
$form['MATRIX'][13]['null']=1;
$form['MATRIX'][13]['tam']=25;
$form['MATRIX'][13]['max']=32;

$form['MATRIX'][14]['campo']="TELEFONE_1";
$form['MATRIX'][14]['tipo']="tel";
$form['MATRIX'][14]['null']=1;
$form['MATRIX'][14]['tam']=15;
$form['MATRIX'][14]['max']=21;

$form['MATRIX'][15]['campo']="TELEFONE_2";
$form['MATRIX'][15]['tipo']="tel";
$form['MATRIX'][15]['null']=1;
$form['MATRIX'][15]['tam']=15;
$form['MATRIX'][15]['max']=21;

$form['MATRIX'][16]['campo']="FAX";
$form['MATRIX'][16]['tipo']="tel";
$form['MATRIX'][16]['null']=1;
$form['MATRIX'][16]['tam']=15;
$form['MATRIX'][16]['max']=21;

$form['MATRIX'][17]['campo']="OBSERVACAO";
$form['MATRIX'][17]['tipo']="blog";
$form['MATRIX'][17]['null']=1;
$form['MATRIX'][17]['tam']=36;
$form['MATRIX'][17]['max']=6;

$form['MATRIX'][17]['fecha_fieldset']=1;


// FORMULARIO MATRIX
$form['FORNECEDOR2'][0]['nome']=FABRICANTES;
$form['FORNECEDOR2'][0]['titulo']=$tdb[FABRICANTES]['DESC'];
$form['FORNECEDOR2'][0]['obs']="";
$form['FORNECEDOR2'][0]['largura']=500;
$form['FORNECEDOR2'][0]['altura']=430;
$form['FORNECEDOR2'][0]['chave']="MID";

$form['FORNECEDOR2'][1]['abre_fieldset']=$tdb[FABRICANTES]['DESC'];

$form['FORNECEDOR2'][1]['campo']="NOME";
$form['FORNECEDOR2'][1]['tipo']="texto";
$form['FORNECEDOR2'][1]['null']=0;
$form['FORNECEDOR2'][1]['tam']=40;
$form['FORNECEDOR2'][1]['max']=120;
$form['FORNECEDOR2'][1]['unico']=1;

$form['FORNECEDOR2'][2]['campo']="CNPJ";
$form['FORNECEDOR2'][2]['tipo']="texto";
$form['FORNECEDOR2'][2]['null']=1;
$form['FORNECEDOR2'][2]['tam']=14;
$form['FORNECEDOR2'][2]['max']=14;

$form['FORNECEDOR2'][3]['campo']="IE";
$form['FORNECEDOR2'][3]['tipo']="texto";
$form['FORNECEDOR2'][3]['null']=1;
$form['FORNECEDOR2'][3]['tam']=14;
$form['FORNECEDOR2'][3]['max']=15;

$form['FORNECEDOR2'][4]['campo']="ENDERECO";
$form['FORNECEDOR2'][4]['tipo']="texto";
$form['FORNECEDOR2'][4]['null']=1;
$form['FORNECEDOR2'][4]['tam']=40;
$form['FORNECEDOR2'][4]['max']=55;

$form['FORNECEDOR2'][5]['campo']="BAIRRO";
$form['FORNECEDOR2'][5]['tipo']="texto";
$form['FORNECEDOR2'][5]['null']=1;
$form['FORNECEDOR2'][5]['tam']=25;
$form['FORNECEDOR2'][5]['max']=50;

$form['FORNECEDOR2'][6]['campo']="CEP";
$form['FORNECEDOR2'][6]['tipo']="texto";
$form['FORNECEDOR2'][6]['null']=1;
$form['FORNECEDOR2'][6]['tam']=10;
$form['FORNECEDOR2'][6]['max']=10;

$form['FORNECEDOR2'][7]['campo']="CIDADE";
$form['FORNECEDOR2'][7]['tipo']="texto";
$form['FORNECEDOR2'][7]['null']=1;
$form['FORNECEDOR2'][7]['tam']=40;
$form['FORNECEDOR2'][7]['max']=150;

$form['FORNECEDOR2'][8]['campo']="ESTADO";
$form['FORNECEDOR2'][8]['tipo']="texto";
$form['FORNECEDOR2'][8]['null']=1;
$form['FORNECEDOR2'][8]['tam']=2;
$form['FORNECEDOR2'][8]['max']=2;

$form['FORNECEDOR2'][9]['campo']="TELEFONE";
$form['FORNECEDOR2'][9]['tipo']="texto";
$form['FORNECEDOR2'][9]['null']=1;
$form['FORNECEDOR2'][9]['tam']=15;
$form['FORNECEDOR2'][9]['max']=21;

$form['FORNECEDOR2'][10]['campo']="FAX";
$form['FORNECEDOR2'][10]['tipo']="texto";
$form['FORNECEDOR2'][10]['null']=1;
$form['FORNECEDOR2'][10]['tam']=15;
$form['FORNECEDOR2'][10]['max']=21;

$form['FORNECEDOR2'][11]['campo']="SITE";
$form['FORNECEDOR2'][11]['tipo']="texto";
$form['FORNECEDOR2'][11]['null']=1;
$form['FORNECEDOR2'][11]['tam']=40;
$form['FORNECEDOR2'][11]['max']=150;

$form['FORNECEDOR2'][12]['campo']="EMAIL";
$form['FORNECEDOR2'][12]['tipo']="texto";
$form['FORNECEDOR2'][12]['null']=1;
$form['FORNECEDOR2'][12]['tam']=40;
$form['FORNECEDOR2'][12]['max']=150;

$form['FORNECEDOR2'][13]['campo']="OBSERVACAO";
$form['FORNECEDOR2'][13]['tipo']="blog";
$form['FORNECEDOR2'][13]['null']=1;
$form['FORNECEDOR2'][13]['tam']=40;
$form['FORNECEDOR2'][13]['max']=6;
$form['FORNECEDOR2'][13]['fecha_fieldset']=1;


// FORMULARIO MATRIX
$form['FORNECEDOR'][0]['nome']=FORNECEDORES;
$form['FORNECEDOR'][0]['titulo']=$tdb[FORNECEDORES]['DESC'];
$form['FORNECEDOR'][0]['obs']="";
$form['FORNECEDOR'][0]['largura']=500;
$form['FORNECEDOR'][0]['altura']=430;
$form['FORNECEDOR'][0]['chave']="MID";

$form['FORNECEDOR'][1]['abre_fieldset']=$tdb[FORNECEDORES]['DESC'];

$form['FORNECEDOR'][1]['campo']="NOME";
$form['FORNECEDOR'][1]['tipo']="texto";
$form['FORNECEDOR'][1]['null']=0;
$form['FORNECEDOR'][1]['tam']=40;
$form['FORNECEDOR'][1]['max']=120;
$form['FORNECEDOR'][1]['unico']=1;

$form['FORNECEDOR'][2]['campo']="FANTASIA";
$form['FORNECEDOR'][2]['tipo']="texto";
$form['FORNECEDOR'][2]['null']=1;
$form['FORNECEDOR'][2]['tam']=40;
$form['FORNECEDOR'][2]['max']=120;
$form['FORNECEDOR'][2]['unico']=1;

$form['FORNECEDOR'][3]['campo']="CNPJ";
$form['FORNECEDOR'][3]['tipo']="texto";
$form['FORNECEDOR'][3]['null']=1;
$form['FORNECEDOR'][3]['tam']=14;
$form['FORNECEDOR'][3]['max']=18;

$form['FORNECEDOR'][4]['campo']="CGC";
$form['FORNECEDOR'][4]['tipo']="texto";
$form['FORNECEDOR'][4]['null']=1;
$form['FORNECEDOR'][4]['tam']=14;
$form['FORNECEDOR'][4]['max']=18;

$form['FORNECEDOR'][5]['campo']="ENDERECO";
$form['FORNECEDOR'][5]['tipo']="texto";
$form['FORNECEDOR'][5]['null']=1;
$form['FORNECEDOR'][5]['tam']=40;
$form['FORNECEDOR'][5]['max']=150;

$form['FORNECEDOR'][6]['campo']="NUMERO";
$form['FORNECEDOR'][6]['tipo']="texto";
$form['FORNECEDOR'][6]['null']=1;
$form['FORNECEDOR'][6]['tam']=4;
$form['FORNECEDOR'][6]['max']=10;

$form['FORNECEDOR'][7]['campo']="COMPLEMENTO";
$form['FORNECEDOR'][7]['tipo']="texto";
$form['FORNECEDOR'][7]['null']=1;
$form['FORNECEDOR'][7]['tam']=25;
$form['FORNECEDOR'][7]['max']=50;

$form['FORNECEDOR'][8]['campo']="BAIRRO";
$form['FORNECEDOR'][8]['tipo']="texto";
$form['FORNECEDOR'][8]['null']=1;
$form['FORNECEDOR'][8]['tam']=25;
$form['FORNECEDOR'][8]['max']=50;

$form['FORNECEDOR'][9]['campo']="CIDADE";
$form['FORNECEDOR'][9]['tipo']="texto";
$form['FORNECEDOR'][9]['null']=1;
$form['FORNECEDOR'][9]['tam']=40;
$form['FORNECEDOR'][9]['max']=150;

$form['FORNECEDOR'][10]['campo']="UF";
$form['FORNECEDOR'][10]['tipo']="texto";
$form['FORNECEDOR'][10]['null']=1;
$form['FORNECEDOR'][10]['tam']=2;
$form['FORNECEDOR'][10]['max']=2;

$form['FORNECEDOR'][11]['campo']="CEP";
$form['FORNECEDOR'][11]['tipo']="texto";
$form['FORNECEDOR'][11]['null']=1;
$form['FORNECEDOR'][11]['tam']=10;
$form['FORNECEDOR'][11]['max']=10;

$form['FORNECEDOR'][12]['campo']="TELEFONE_1";
$form['FORNECEDOR'][12]['tipo']="texto";
$form['FORNECEDOR'][12]['null']=1;
$form['FORNECEDOR'][12]['tam']=15;
$form['FORNECEDOR'][12]['max']=21;

$form['FORNECEDOR'][13]['campo']="TELEFONE_2";
$form['FORNECEDOR'][13]['tipo']="texto";
$form['FORNECEDOR'][13]['null']=1;
$form['FORNECEDOR'][13]['tam']=15;
$form['FORNECEDOR'][13]['max']=21;

$form['FORNECEDOR'][14]['campo']="TELEFONE_3";
$form['FORNECEDOR'][14]['tipo']="texto";
$form['FORNECEDOR'][14]['null']=1;
$form['FORNECEDOR'][14]['tam']=15;
$form['FORNECEDOR'][14]['max']=21;

$form['FORNECEDOR'][15]['campo']="SITE";
$form['FORNECEDOR'][15]['tipo']="texto";
$form['FORNECEDOR'][15]['null']=1;
$form['FORNECEDOR'][15]['tam']=40;
$form['FORNECEDOR'][15]['max']=150;

$form['FORNECEDOR'][16]['campo']="E_MAIL";
$form['FORNECEDOR'][16]['tipo']="texto";
$form['FORNECEDOR'][16]['null']=1;
$form['FORNECEDOR'][16]['tam']=40;
$form['FORNECEDOR'][16]['max']=150;

$form['FORNECEDOR'][16]['fecha_fieldset']=1;


// FORMULARIO MAQUINAS FAMILIA
$form['MAQUINAS_FAMILIA'][0]['nome']=MAQUINAS_FAMILIA;
$form['MAQUINAS_FAMILIA'][0]['titulo']=
$form['MAQUINAS_FAMILIA'][1]['abre_fieldset']=$tdb[MAQUINAS_FAMILIA]['DESC'];
$form['MAQUINAS_FAMILIA'][0]['obs']="";
$form['MAQUINAS_FAMILIA'][0]['largura']=500;
$form['MAQUINAS_FAMILIA'][0]['altura']=180;
$form['MAQUINAS_FAMILIA'][0]['chave']="MID";

$form['MAQUINAS_FAMILIA'][1]['abre_fieldset']=$tdb[MAQUINAS_FAMILIA]['DESC'];

$form['MAQUINAS_FAMILIA'][1]['campo']="MID_EMPRESA";
$form['MAQUINAS_FAMILIA'][1]['tipo']="select_rela";
$form['MAQUINAS_FAMILIA'][1]['null']=1;
$form['MAQUINAS_FAMILIA'][1]['tam']=15;
$form['MAQUINAS_FAMILIA'][1]['max']=30;

if ($_autotag_fammaq == 0) {
    $form['MAQUINAS_FAMILIA'][2]['campo']="COD";
    $form['MAQUINAS_FAMILIA'][2]['tipo']="texto";
    $form['MAQUINAS_FAMILIA'][2]['null']=0;
    $form['MAQUINAS_FAMILIA'][2]['tam']=15;
    $form['MAQUINAS_FAMILIA'][2]['max']=30;
    $form['MAQUINAS_FAMILIA'][2]['unico']=1;
}
else {
    $form['MAQUINAS_FAMILIA'][2]['campo']="COD";
    $form['MAQUINAS_FAMILIA'][2]['tipo']="texto_fammaq_autotag";
    $form['MAQUINAS_FAMILIA'][2]['null']=0;
    $form['MAQUINAS_FAMILIA'][2]['valor']=1;
    $form['MAQUINAS_FAMILIA'][2]['unico']=1;
}
$form['MAQUINAS_FAMILIA'][3]['campo']="DESCRICAO";
$form['MAQUINAS_FAMILIA'][3]['tipo']="texto";
$form['MAQUINAS_FAMILIA'][3]['null']=0;
$form['MAQUINAS_FAMILIA'][3]['tam']=30;
$form['MAQUINAS_FAMILIA'][3]['max']=30;
$form['MAQUINAS_FAMILIA'][3]['unico']=0;
$form['MAQUINAS_FAMILIA'][3]['fecha_fieldset']=1;

// FORMULARIO MAQUINAS CLASSE
$form['MAQUINAS_CLASSE'][0]['nome']=MAQUINAS_CLASSE;
$form['MAQUINAS_CLASSE'][0]['titulo']=$tdb[MAQUINAS_CLASSE]['DESC'];
$form['MAQUINAS_CLASSE'][0]['obs']="";
$form['MAQUINAS_CLASSE'][0]['largura']=500;
$form['MAQUINAS_CLASSE'][0]['altura']=150;
$form['MAQUINAS_CLASSE'][0]['chave']="MID";

$form['MAQUINAS_CLASSE'][1]['abre_fieldset']=$tdb[MAQUINAS_CLASSE]['DESC'];

$form['MAQUINAS_CLASSE'][1]['campo']="DESCRICAO";
$form['MAQUINAS_CLASSE'][1]['tipo']="texto";
$form['MAQUINAS_CLASSE'][1]['null']=0;
$form['MAQUINAS_CLASSE'][1]['tam']=30;
$form['MAQUINAS_CLASSE'][1]['max']=30;
$form['MAQUINAS_CLASSE'][1]['unico']=1;

$form['MAQUINAS_CLASSE'][1]['fecha_fieldset']=1;

// FORMULARIO CENTRO DE CUSTO
$form['CENTRO_DE_CUSTO'][0]['nome']=CENTRO_DE_CUSTO;
$form['CENTRO_DE_CUSTO'][0]['titulo']=$tdb[CENTRO_DE_CUSTO]['DESC'];
$form['CENTRO_DE_CUSTO'][0]['obs']="";
$form['CENTRO_DE_CUSTO'][0]['largura']=500;
$form['CENTRO_DE_CUSTO'][0]['altura']=200;
$form['CENTRO_DE_CUSTO'][0]['chave']="MID";

$form['CENTRO_DE_CUSTO'][1]['abre_fieldset']=$tdb[CENTRO_DE_CUSTO]['DESC'];

$form['CENTRO_DE_CUSTO'][1]['campo']="MID_EMPRESA";
$form['CENTRO_DE_CUSTO'][1]['tipo']="select_rela";
$form['CENTRO_DE_CUSTO'][1]['null']=0;
$form['CENTRO_DE_CUSTO'][1]['tam']=15;
$form['CENTRO_DE_CUSTO'][1]['max']=30;
$form['CENTRO_DE_CUSTO'][1]['js']="atualiza_area2('select_cc[".CENTRO_DE_CUSTO."][4]','parametros.php?id=filtro_ccp&campo_nome=cc[".CENTRO_DE_CUSTO."][4]&campo_id=cc[".CENTRO_DE_CUSTO."][4]&null=1&mid_emp='+this.options[this.selectedIndex].value)";

$form['CENTRO_DE_CUSTO'][2]['campo']="COD";
$form['CENTRO_DE_CUSTO'][2]['tipo']="texto";
$form['CENTRO_DE_CUSTO'][2]['null']=0;
$form['CENTRO_DE_CUSTO'][2]['tam']=15;
$form['CENTRO_DE_CUSTO'][2]['max']=30;
$form['CENTRO_DE_CUSTO'][2]['unico']=1;

$form['CENTRO_DE_CUSTO'][3]['campo']="DESCRICAO";
$form['CENTRO_DE_CUSTO'][3]['tipo']="texto";
$form['CENTRO_DE_CUSTO'][3]['null']=0;
$form['CENTRO_DE_CUSTO'][3]['tam']=30;
$form['CENTRO_DE_CUSTO'][3]['max']=120;
$form['CENTRO_DE_CUSTO'][3]['unico']=0;

$form['CENTRO_DE_CUSTO'][4]['campo']="PAI";
$form['CENTRO_DE_CUSTO'][4]['tipo']="select_rela";
$form['CENTRO_DE_CUSTO'][4]['null']=1;
$form['CENTRO_DE_CUSTO'][4]['unico']=0;
$form['CENTRO_DE_CUSTO'][4]['autofiltro']='N';
if ($act == 2) $form['CENTRO_DE_CUSTO'][4]['valor'] = "WHERE MID_EMPRESA = '".VoltaValor(CENTRO_DE_CUSTO,'MID_EMPRESA','MID',$_GET['foq'],$tdb[CENTRO_DE_CUSTO]['dba'])."'";
elseif ($_POST['cc']) $form['CENTRO_DE_CUSTO'][4]['valor'] = "WHERE MID_EMPRESA = '{$_POST['cc'][CENTRO_DE_CUSTO][1]}'";

$form['CENTRO_DE_CUSTO'][4]['fecha_fieldset']=1;

// FORMULARIO CONTADOR TIPO
$form['CONTADOR_TIPO'][0]['nome']=MAQUINAS_CONTADOR_TIPO;
$form['CONTADOR_TIPO'][0]['titulo']=$tdb[MAQUINAS_CONTADOR_TIPO]['DESC'];
$form['CONTADOR_TIPO'][0]['obs']="";
$form['CONTADOR_TIPO'][0]['largura']=500;
$form['CONTADOR_TIPO'][0]['altura']=180;
$form['CONTADOR_TIPO'][0]['chave']="MID";
$form['CONTADOR_TIPO'][1]['abre_fieldset']=$tdb[MAQUINAS_CONTADOR_TIPO]['DESC'];

$form['CONTADOR_TIPO'][1]['campo']="DESCRICAO";
$form['CONTADOR_TIPO'][1]['tipo']="texto";
$form['CONTADOR_TIPO'][1]['null']=0;
$form['CONTADOR_TIPO'][1]['tam']=30;
$form['CONTADOR_TIPO'][1]['max']=30;
$form['CONTADOR_TIPO'][1]['unico']=1;

$form['CONTADOR_TIPO'][1]['fecha_fieldset']=1;


// FORMULARIO EQUIPAMENTO FAMILIA
$form['EQUIPAMENTO_FAMILIA'][0]['nome']=EQUIPAMENTOS_FAMILIA;
$form['EQUIPAMENTO_FAMILIA'][0]['titulo']=$tdb[EQUIPAMENTOS_FAMILIA]['DESC'];
$form['EQUIPAMENTO_FAMILIA'][0]['obs']="";
$form['EQUIPAMENTO_FAMILIA'][0]['largura']=500;
$form['EQUIPAMENTO_FAMILIA'][0]['altura']=180;
$form['EQUIPAMENTO_FAMILIA'][0]['chave']="MID";

$form['EQUIPAMENTO_FAMILIA'][1]['abre_fieldset']=$tdb[EQUIPAMENTOS_FAMILIA]['DESC'];

$form['EQUIPAMENTO_FAMILIA'][1]['campo']="MID_EMPRESA";
$form['EQUIPAMENTO_FAMILIA'][1]['tipo']="select_rela";
$form['EQUIPAMENTO_FAMILIA'][1]['null']=1;
$form['EQUIPAMENTO_FAMILIA'][1]['tam']=15;
$form['EQUIPAMENTO_FAMILIA'][1]['max']=30;

if ($_autotag_famequip == 0) {
    $form['EQUIPAMENTO_FAMILIA'][2]['campo']="COD";
    $form['EQUIPAMENTO_FAMILIA'][2]['tipo']="texto";
    $form['EQUIPAMENTO_FAMILIA'][2]['null']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['tam']=15;
    $form['EQUIPAMENTO_FAMILIA'][2]['max']=30;
    $form['EQUIPAMENTO_FAMILIA'][2]['unico']=1;
}
else {
    $form['EQUIPAMENTO_FAMILIA'][2]['campo']="COD";
    $form['EQUIPAMENTO_FAMILIA'][2]['tipo']="texto_famequip_autotag";
    $form['EQUIPAMENTO_FAMILIA'][2]['null']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['unico']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['valor']=1;

}
$form['EQUIPAMENTO_FAMILIA'][3]['campo']="DESCRICAO";
$form['EQUIPAMENTO_FAMILIA'][3]['tipo']="texto";
$form['EQUIPAMENTO_FAMILIA'][3]['null']=0;
$form['EQUIPAMENTO_FAMILIA'][3]['tam']=30;
$form['EQUIPAMENTO_FAMILIA'][3]['max']=30;
$form['EQUIPAMENTO_FAMILIA'][3]['unico']=1;

$form['EQUIPAMENTO_FAMILIA'][3]['fecha_fieldset']=1;

// FORMULARIO MATERIAIS
$i_form=0;
$form['MATERIAIS'][0]['nome']=MATERIAIS;
$form['MATERIAIS'][0]['titulo']=$tdb[MATERIAIS]['DESC'];
$form['MATERIAIS'][0]['obs']="";
$form['MATERIAIS'][0]['largura']=600;
$form['MATERIAIS'][0]['altura']=360;
$form['MATERIAIS'][0]['chave']="MID";

$i_form++;
$form['MATERIAIS'][$i_form]['abre_fieldset']=$tdb[MATERIAIS]['DESC'];

$form['MATERIAIS'][$i_form]['campo']="MID_EMPRESA";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=15;
$form['MATERIAIS'][$i_form]['max']=15;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="FAMILIA";
$form['MATERIAIS'][$i_form]['tipo']="select_fam_material";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="MID_SUBFAMILIA";
$form['MATERIAIS'][$i_form]['tipo']="select_subfam_material";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="COD";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=40;
$form['MATERIAIS'][$i_form]['unico']=1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="DESCRICAO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=50;
$form['MATERIAIS'][$i_form]['unico']=0;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="COMPLEMENTO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=50;
$form['MATERIAIS'][$i_form]['unico']=0;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="FORNECEDOR";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="FABRICANTE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="UNIDADE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="LOCALIZACAO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=75;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="ESTOQUE_MINIMO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="ESTOQUE_MAXIMO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="CUSTO_UNITARIO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="MID_CRITICIDADE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="OBSERVACAO";
$form['MATERIAIS'][$i_form]['tipo']="blog";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=10;

$form['MATERIAIS'][$i_form]['fecha_fieldset']=1;

// FORMULARIO CRITICIDADE DE MATERIAL
$form['MATERIAL_CRITICIDADE'][0]['nome']=MATERIAIS_CRITICIDADE;
$form['MATERIAL_CRITICIDADE'][0]['titulo']=$tdb[MATERIAIS_CRITICIDADE]['DESC'];
$form['MATERIAL_CRITICIDADE'][0]['obs']="";
$form['MATERIAL_CRITICIDADE'][0]['largura']=500;
$form['MATERIAL_CRITICIDADE'][0]['altura']=160;
$form['MATERIAL_CRITICIDADE'][0]['chave']="MID";

$form['MATERIAL_CRITICIDADE'][1]['abre_fieldset']=$tdb[MATERIAIS_CRITICIDADE]['DESC'];

$form['MATERIAL_CRITICIDADE'][1]['campo']="DESCRICAO";
$form['MATERIAL_CRITICIDADE'][1]['tipo']="texto";
$form['MATERIAL_CRITICIDADE'][1]['null']=0;
$form['MATERIAL_CRITICIDADE'][1]['tam']=30;
$form['MATERIAL_CRITICIDADE'][1]['max']=30;
$form['MATERIAL_CRITICIDADE'][1]['unico']=0;

$form['MATERIAL_CRITICIDADE'][1]['fecha_fieldset']=1;

// FORMULARIO FAMILIA DE MATERIAL
$form['MATERIAL_FAMILIA'][0]['nome']=MATERIAIS_FAMILIA;
$form['MATERIAL_FAMILIA'][0]['titulo']=$tdb[MATERIAIS_FAMILIA]['DESC'];
$form['MATERIAL_FAMILIA'][0]['obs']="";
$form['MATERIAL_FAMILIA'][0]['largura']=500;
$form['MATERIAL_FAMILIA'][0]['altura']=160;
$form['MATERIAL_FAMILIA'][0]['chave']="MID";

$form['MATERIAL_FAMILIA'][1]['abre_fieldset']=$tdb[MATERIAIS_FAMILIA]['DESC'];

$form['MATERIAL_FAMILIA'][1]['campo']="COD";
$form['MATERIAL_FAMILIA'][1]['tipo']="texto";
$form['MATERIAL_FAMILIA'][1]['null']=0;
$form['MATERIAL_FAMILIA'][1]['tam']=15;
$form['MATERIAL_FAMILIA'][1]['max']=25;
$form['MATERIAL_FAMILIA'][1]['unico']=1;
$form['MATERIAL_FAMILIA'][1]['valor']=0;

$form['MATERIAL_FAMILIA'][2]['campo']="DESCRICAO";
$form['MATERIAL_FAMILIA'][2]['tipo']="texto";
$form['MATERIAL_FAMILIA'][2]['null']=0;
$form['MATERIAL_FAMILIA'][2]['tam']=30;
$form['MATERIAL_FAMILIA'][2]['max']=30;
$form['MATERIAL_FAMILIA'][2]['unico']=0;

$form['MATERIAL_FAMILIA'][2]['fecha_fieldset']=1;

// FORMULARIO FAMILIA DE MATERIAL
$form['MATERIAL_SUBFAMILIA'][0]['nome']=MATERIAIS_SUBFAMILIA;
$form['MATERIAL_SUBFAMILIA'][0]['titulo']=$tdb[MATERIAIS_SUBFAMILIA]['DESC'];
$form['MATERIAL_SUBFAMILIA'][0]['obs']="";
$form['MATERIAL_SUBFAMILIA'][0]['largura']=500;
$form['MATERIAL_SUBFAMILIA'][0]['altura']=180;
$form['MATERIAL_SUBFAMILIA'][0]['chave']="MID";

$form['MATERIAL_SUBFAMILIA'][1]['abre_fieldset']=$tdb[MATERIAIS_SUBFAMILIA]['DESC'];

$form['MATERIAL_SUBFAMILIA'][1]['campo']="MID_FAMILIA";
$form['MATERIAL_SUBFAMILIA'][1]['tipo']="select_rela";
$form['MATERIAL_SUBFAMILIA'][1]['null']=0;


$form['MATERIAL_SUBFAMILIA'][2]['campo']="COD"; ##### a ordem deste campo aparece em parametros.php
$form['MATERIAL_SUBFAMILIA'][2]['tipo']="texto";
$form['MATERIAL_SUBFAMILIA'][2]['null']=0;
$form['MATERIAL_SUBFAMILIA'][2]['tam']=15;
$form['MATERIAL_SUBFAMILIA'][2]['max']=25;
$form['MATERIAL_SUBFAMILIA'][2]['unico']=1;

$form['MATERIAL_SUBFAMILIA'][3]['campo']="DESCRICAO";
$form['MATERIAL_SUBFAMILIA'][3]['tipo']="texto";
$form['MATERIAL_SUBFAMILIA'][3]['null']=0;
$form['MATERIAL_SUBFAMILIA'][3]['tam']=30;
$form['MATERIAL_SUBFAMILIA'][3]['max']=30;
$form['MATERIAL_SUBFAMILIA'][3]['unico']=0;

$form['MATERIAL_SUBFAMILIA'][3]['fecha_fieldset']=1;


// FORMULARIO UNIDADE DE MATERAIS
$form['MATERIAL_UNIDADE'][0]['nome']=MATERIAIS_UNIDADE;
$form['MATERIAL_UNIDADE'][0]['titulo']=$tdb[MATERIAIS_UNIDADE]['DESC'];
$form['MATERIAL_UNIDADE'][0]['obs']="";
$form['MATERIAL_UNIDADE'][0]['largura']=500;
$form['MATERIAL_UNIDADE'][0]['altura']=180;
$form['MATERIAL_UNIDADE'][0]['chave']="MID";

$form['MATERIAL_UNIDADE'][1]['abre_fieldset']=$tdb[MATERIAIS_UNIDADE]['DESC'];

$form['MATERIAL_UNIDADE'][1]['campo']="COD";
$form['MATERIAL_UNIDADE'][1]['tipo']="texto";
$form['MATERIAL_UNIDADE'][1]['null']=0;
$form['MATERIAL_UNIDADE'][1]['tam']=15;
$form['MATERIAL_UNIDADE'][1]['max']=50;
$form['MATERIAL_UNIDADE'][1]['unico']=1;

$form['MATERIAL_UNIDADE'][2]['campo']="DESCRICAO";
$form['MATERIAL_UNIDADE'][2]['tipo']="texto";
$form['MATERIAL_UNIDADE'][2]['null']=0;
$form['MATERIAL_UNIDADE'][2]['tam']=30;
$form['MATERIAL_UNIDADE'][2]['max']=50;
$form['MATERIAL_UNIDADE'][2]['unico']=1;

$form['MATERIAL_UNIDADE'][2]['fecha_fieldset']=1;


// CLASSE DE MATERIAIS
$iform = 0;
$form['MATERIAIS_CLASSE'][$iform]['nome']=MATERIAIS_CLASSE;
$form['MATERIAIS_CLASSE'][$iform]['titulo']=$tdb[MATERIAIS_CLASSE]['DESC'];
$form['MATERIAIS_CLASSE'][$iform]['obs']="";
$form['MATERIAIS_CLASSE'][$iform]['largura']=500;
$form['MATERIAIS_CLASSE'][$iform]['altura']=160;
$form['MATERIAIS_CLASSE'][$iform]['chave']="MID";

$iform ++;
$form['MATERIAIS_CLASSE'][$iform]['abre_fieldset']=$tdb[MATERIAIS_CLASSE]['DESC'];

$form['MATERIAIS_CLASSE'][$iform]['campo']="DESCRICAO";
$form['MATERIAIS_CLASSE'][$iform]['tipo']="texto";
$form['MATERIAIS_CLASSE'][$iform]['null']=0;
$form['MATERIAIS_CLASSE'][$iform]['tam']=50;
$form['MATERIAIS_CLASSE'][$iform]['max']=250;
$form['MATERIAIS_CLASSE'][$iform]['unico']=1;

$form['MATERIAIS_CLASSE'][$iform]['fecha_fieldset']=1;


// FORMULARIO PLANO PADRAO
$form['PLANOPADRAO'][0]['nome']=PLANO_PADRAO;
$form['PLANOPADRAO'][0]['titulo']=$tdb[PLANO_PADRAO]['DESC'];
$form['PLANOPADRAO'][0]['obs']="";
$form['PLANOPADRAO'][0]['dir']=1;
$form['PLANOPADRAO'][0]['largura']=630;
$form['PLANOPADRAO'][0]['altura']=380;
$form['PLANOPADRAO'][0]['chave']="MID";

if (($act == 1) or ($act == 3)) {
    $form['PLANOPADRAO'][1]['abre_fieldset']=$ling['plano_aplicacao'];
    
    $form['PLANOPADRAO'][1]['campo']="MID_EMPRESA";
    $form['PLANOPADRAO'][1]['tipo']="select_rela";
    $form['PLANOPADRAO'][1]['null']=1;
    $form['PLANOPADRAO'][1]['js']="document.forms[0].submit()";
    

    $form['PLANOPADRAO'][2]['campo']="TIPO";
    $form['PLANOPADRAO'][2]['tipo']="select_rela";
    $form['PLANOPADRAO'][2]['null']=0;
    $form['PLANOPADRAO'][2]['js']="document.forms[0].submit()";
    
    $cc=$_POST['cc'];
    if ($cc[PLANO_PADRAO][2] == 1) {
        $form['PLANOPADRAO'][3]['campo']="MAQUINA_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="recb_valor";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;
        $form['PLANOPADRAO'][3]['valor']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="select_rela";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }
    
    elseif ($cc[PLANO_PADRAO][2] == 2) {
        $form['PLANOPADRAO'][3]['campo']="MAQUINA_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="select_rela";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }
    
    elseif($cc[PLANO_PADRAO][2] == 4) {
        $form['PLANOPADRAO'][3]['campo']="EQUIPAMENTO_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="select_rela";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }
    
    else {
        $form['PLANOPADRAO'][3]['campo']="EQUIPAMENTO_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="recb_valor";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;
        $form['PLANOPADRAO'][3]['valor']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }


    $form['PLANOPADRAO'][4]['fecha_fieldset']=1;

    $form['PLANOPADRAO'][5]['abre_fieldset']=$ling['plano_info'];
    
    $form['PLANOPADRAO'][5]['campo']="DESCRICAO";
    $form['PLANOPADRAO'][5]['tipo']="texto";
    $form['PLANOPADRAO'][5]['null']=0;
    $form['PLANOPADRAO'][5]['tam']=60;
    $form['PLANOPADRAO'][5]['max']=150;
    $form['PLANOPADRAO'][5]['unico']=0;

    $form['PLANOPADRAO'][6]['campo']="OBSERVACAO";
    $form['PLANOPADRAO'][6]['tipo']="blog";
    $form['PLANOPADRAO'][6]['null']=1;
    $form['PLANOPADRAO'][6]['tam']=60;
    $form['PLANOPADRAO'][6]['max']=10;
    $form['PLANOPADRAO'][6]['unico']=0;

    $form['PLANOPADRAO'][6]['fecha_fieldset']=1;
}
else {
    $form['PLANOPADRAO'][1]['abre_fieldset']=$ling['plano_info'];
    
    $form['PLANOPADRAO'][1]['campo']="DESCRICAO";
    $form['PLANOPADRAO'][1]['tipo']="texto";
    $form['PLANOPADRAO'][1]['null']=0;
    $form['PLANOPADRAO'][1]['tam']=60;
    $form['PLANOPADRAO'][1]['max']=150;
    $form['PLANOPADRAO'][1]['unico']=0;

    $form['PLANOPADRAO'][2]['campo']="OBSERVACAO";
    $form['PLANOPADRAO'][2]['tipo']="blog";
    $form['PLANOPADRAO'][2]['null']=1;
    $form['PLANOPADRAO'][2]['tam']=60;
    $form['PLANOPADRAO'][2]['max']=10;
    $form['PLANOPADRAO'][2]['unico']=0;

    $form['PLANOPADRAO'][2]['fecha_fieldset']=1;
}


// FORMULARIO ATIVIDADES
$form['ATIVIDADE'][0]['nome']=ATIVIDADES;
$form['ATIVIDADE'][0]['titulo']=$tdb[ATIVIDADES]['DESC'];
$form['ATIVIDADE'][0]['obs']="";
$form['ATIVIDADE'][0]['largura']=550;
$form['ATIVIDADE'][0]['altura']=420;
$form['ATIVIDADE'][0]['chave']="MID";

$form['ATIVIDADE'][1]['abre_fieldset']=$ling['plano_peri'];

$form['ATIVIDADE'][1]['campo']="PERIODICIDADE";
$form['ATIVIDADE'][1]['tipo']="select_peri";
$form['ATIVIDADE'][1]['null']=0;
$form['ATIVIDADE'][1]['unico']=0;
$cc=$_POST['cc'];

if ((is_array($cc) && $cc['atividades'][1] != 4) || (! is_array($cc) && VoltaValor(ATIVIDADES, "PERIODICIDADE", "MID", $_GET['foq'], 0) != 4)) {
    $form['ATIVIDADE'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE'][2]['tipo']="texto";
    $form['ATIVIDADE'][2]['null']=0;
    $form['ATIVIDADE'][2]['tam']=4;
    $form['ATIVIDADE'][2]['max']=4;
    $form['ATIVIDADE'][2]['unico']=0;

    $form['ATIVIDADE'][3]['campo']="CONTADOR";
    $form['ATIVIDADE'][3]['tipo']="recb_valor";
    $form['ATIVIDADE'][3]['null']=1;
    $form['ATIVIDADE'][3]['valor']=0;
    $form['ATIVIDADE'][3]['unico']=0;

    $form['ATIVIDADE'][4]['campo']="DISPARO";
    $form['ATIVIDADE'][4]['tipo']="recb_valor";
    $form['ATIVIDADE'][4]['null']=1;
    $form['ATIVIDADE'][4]['valor']=0;
    $form['ATIVIDADE'][4]['unico']=0;
}
else {
    $form['ATIVIDADE'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE'][2]['tipo']="recb_valor";
    $form['ATIVIDADE'][2]['null']=1;
    $form['ATIVIDADE'][2]['valor']=0;
    $form['ATIVIDADE'][2]['unico']=0;

    $form['ATIVIDADE'][3]['campo']="CONTADOR";
    $form['ATIVIDADE'][3]['tipo']="select_contador";
    $form['ATIVIDADE'][3]['null']=0;
    $form['ATIVIDADE'][3]['unico']=0;

    $form['ATIVIDADE'][4]['campo']="DISPARO";
    $form['ATIVIDADE'][4]['tipo']="texto";
    $form['ATIVIDADE'][4]['tam']=7;
    $form['ATIVIDADE'][4]['max']=13;
    $form['ATIVIDADE'][4]['null']=0;
    $form['ATIVIDADE'][4]['unico']=0;
}
$form['ATIVIDADE'][4]['fecha_fieldset']=1;

$form['ATIVIDADE'][5]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['ATIVIDADE'][5]['campo']="NUMERO";
$form['ATIVIDADE'][5]['tipo']="texto";
$form['ATIVIDADE'][5]['null']=0;
$form['ATIVIDADE'][5]['tam']=2;
$form['ATIVIDADE'][5]['max']=3;
$form['ATIVIDADE'][5]['unico']=0;

$form['ATIVIDADE'][6]['campo']="MID_CONJUNTO";
$form['ATIVIDADE'][6]['tipo']="recb_valor";
$form['ATIVIDADE'][6]['null']=1;
$form['ATIVIDADE'][6]['unico']=0;
$form['ATIVIDADE'][6]['valor']=0;

$form['ATIVIDADE'][7]['campo']="PARTE";
$form['ATIVIDADE'][7]['tipo']="texto";
$form['ATIVIDADE'][7]['null']=1;
$form['ATIVIDADE'][7]['tam']=40;
$form['ATIVIDADE'][7]['max']=150;
$form['ATIVIDADE'][7]['unico']=0;

$form['ATIVIDADE'][8]['campo']="TAREFA";
$form['ATIVIDADE'][8]['tipo']="texto";
$form['ATIVIDADE'][8]['null']=0;
$form['ATIVIDADE'][8]['tam']=40;
$form['ATIVIDADE'][8]['max']=150;
$form['ATIVIDADE'][8]['unico']=0;

$form['ATIVIDADE'][9]['campo']="INSTRUCAO_DE_TRABALHO";
$form['ATIVIDADE'][9]['tipo']="texto";
$form['ATIVIDADE'][9]['null']=1;
$form['ATIVIDADE'][9]['tam']=40;
$form['ATIVIDADE'][9]['max']=150;
$form['ATIVIDADE'][9]['unico']=0;

$form['ATIVIDADE'][10]['campo']="OBSERVACAO";
$form['ATIVIDADE'][10]['tipo']="texto";
$form['ATIVIDADE'][10]['tam']=40;
$form['ATIVIDADE'][10]['max']=150;
$form['ATIVIDADE'][10]['null']=1;
$form['ATIVIDADE'][10]['unico']=0;

$form['ATIVIDADE'][11]['campo']="MAQUINA_PARADA";
$form['ATIVIDADE'][11]['tipo']="select_rela";
$form['ATIVIDADE'][11]['null']=0;
$form['ATIVIDADE'][11]['unico']=0;

$form['ATIVIDADE'][11]['fecha_fieldset']=1;

$form['ATIVIDADE'][12]['abre_fieldset']=$ling['plano_prev_mo'];

$form['ATIVIDADE'][12]['campo']="ESPECIALIDADE";
$form['ATIVIDADE'][12]['tipo']="select_rela";
$form['ATIVIDADE'][12]['null']=0;
$form['ATIVIDADE'][12]['unico']=0;

$form['ATIVIDADE'][13]['campo']="QUANTIDADE_MO";
$form['ATIVIDADE'][13]['tipo']="texto_int";
$form['ATIVIDADE'][13]['null']=0;
$form['ATIVIDADE'][13]['tam']=5;
$form['ATIVIDADE'][13]['unico']=0;

$form['ATIVIDADE'][14]['campo']="TEMPO_PREVISTO";
$form['ATIVIDADE'][14]['tipo']="texto_float";
$form['ATIVIDADE'][14]['null']=0;
$form['ATIVIDADE'][14]['tam']=5;
$form['ATIVIDADE'][14]['unico']=0;

$form['ATIVIDADE'][15]['campo']="MID_PLANO_PADRAO";
$form['ATIVIDADE'][15]['tipo']="recb_valor";
$form['ATIVIDADE'][15]['valor']=$form_recb_valor;
$form['ATIVIDADE'][15]['null']=0;
$form['ATIVIDADE'][15]['unico']=0;

$form['ATIVIDADE'][15]['fecha_fieldset']=1;

$form['ATIVIDADE'][16]['abre_fieldset']=$ling['plano_prev_mat'];

$form['ATIVIDADE'][16]['campo']="MID_MATERIAL";
$form['ATIVIDADE'][16]['tipo']="select_material";
$form['ATIVIDADE'][16]['null']=1;
$form['ATIVIDADE'][16]['tam']=0;
$form['ATIVIDADE'][16]['max']=0;
$form['ATIVIDADE'][16]['unico']=0;

$form['ATIVIDADE'][17]['campo']="QUANTIDADE";
$form['ATIVIDADE'][17]['tipo']="texto_unidade";
$form['ATIVIDADE'][17]['null']=1;
$form['ATIVIDADE'][17]['tam']=5;
$form['ATIVIDADE'][17]['max']=5;
$form['ATIVIDADE'][17]['unico']=0;

$form['ATIVIDADE'][17]['fecha_fieldset']=1;

$form['ATIVIDADE2'][0]['nome']=ATIVIDADES;
$form['ATIVIDADE2'][0]['titulo']=$tdb[ATIVIDADES]['DESC'];
$form['ATIVIDADE2'][0]['obs']="";
$form['ATIVIDADE2'][0]['largura']=550;
$form['ATIVIDADE2'][0]['altura']=420;
$form['ATIVIDADE2'][0]['chave']="MID";

$form['ATIVIDADE2'][1]['abre_fieldset']=$ling['plano_peri'];

$form['ATIVIDADE2'][1]['campo']="PERIODICIDADE";
$form['ATIVIDADE2'][1]['tipo']="select_peri";
$form['ATIVIDADE2'][1]['null']=1;
$form['ATIVIDADE2'][1]['unico']=0;

$cc=$_POST['cc'];
if ($cc['atividades'][1] != 4) {
    $form['ATIVIDADE2'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE2'][2]['tipo']="texto";
    $form['ATIVIDADE2'][2]['null']=0;
    $form['ATIVIDADE2'][2]['tam']=4;
    $form['ATIVIDADE2'][2]['max']=4;
    $form['ATIVIDADE2'][2]['unico']=0;

    $form['ATIVIDADE2'][3]['campo']="CONTADOR";
    $form['ATIVIDADE2'][3]['tipo']="recb_valor";
    $form['ATIVIDADE2'][3]['null']=1;
    $form['ATIVIDADE2'][3]['valor']=0;
    $form['ATIVIDADE2'][3]['unico']=0;

    $form['ATIVIDADE2'][4]['campo']="DISPARO";
    $form['ATIVIDADE2'][4]['tipo']="recb_valor";
    $form['ATIVIDADE2'][4]['null']=1;
    $form['ATIVIDADE2'][4]['valor']=0;
    $form['ATIVIDADE2'][4]['unico']=0;
}
else {
    $form['ATIVIDADE2'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE2'][2]['tipo']="recb_valor";
    $form['ATIVIDADE2'][2]['null']=1;
    $form['ATIVIDADE2'][2]['valor']=0;
    $form['ATIVIDADE2'][2]['unico']=0;

    $form['ATIVIDADE2'][3]['campo']="CONTADOR";
    $form['ATIVIDADE2'][3]['tipo']="select_rela";
    $form['ATIVIDADE2'][3]['null']=0;
    $form['ATIVIDADE2'][3]['unico']=0;

    $form['ATIVIDADE2'][4]['campo']="DISPARO";
    $form['ATIVIDADE2'][4]['tipo']="texto";
    $form['ATIVIDADE2'][4]['tam']=7;
    $form['ATIVIDADE2'][4]['max']=13;
    $form['ATIVIDADE2'][4]['null']=0;
    $form['ATIVIDADE2'][4]['unico']=0;
}
$form['ATIVIDADE2'][4]['fecha_fieldset']=1;

$form['ATIVIDADE2'][5]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['ATIVIDADE2'][5]['campo']="NUMERO";
$form['ATIVIDADE2'][5]['tipo']="texto";
$form['ATIVIDADE2'][5]['null']=0;
$form['ATIVIDADE2'][5]['tam']=2;
$form['ATIVIDADE2'][5]['max']=3;
$form['ATIVIDADE2'][5]['unico']=0;

$form['ATIVIDADE2'][6]['campo']="MID_CONJUNTO";
$form['ATIVIDADE2'][6]['tipo']="select_conjunto_filtrado";
$form['ATIVIDADE2'][6]['null']=1;
$form['ATIVIDADE2'][6]['unico']=0;
$form['ATIVIDADE2'][6]['valor']=VoltaValor(PLANO_PADRAO,"MID_MAQUINA","MID",$form_recb_valor,0);

$form['ATIVIDADE2'][7]['campo']="PARTE";
$form['ATIVIDADE2'][7]['tipo']="texto";
$form['ATIVIDADE2'][7]['null']=1;
$form['ATIVIDADE2'][7]['tam']=40;
$form['ATIVIDADE2'][7]['max']=150;
$form['ATIVIDADE2'][7]['unico']=0;

$form['ATIVIDADE2'][8]['campo']="TAREFA";
$form['ATIVIDADE2'][8]['tipo']="texto";
$form['ATIVIDADE2'][8]['null']=0;
$form['ATIVIDADE2'][8]['tam']=40;
$form['ATIVIDADE2'][8]['max']=150;
$form['ATIVIDADE2'][8]['unico']=0;

$form['ATIVIDADE2'][9]['campo']="INSTRUCAO_DE_TRABALHO";
$form['ATIVIDADE2'][9]['tipo']="texto";
$form['ATIVIDADE2'][9]['null']=1;
$form['ATIVIDADE2'][9]['tam']=40;
$form['ATIVIDADE2'][9]['max']=150;
$form['ATIVIDADE2'][9]['unico']=0;

$form['ATIVIDADE2'][10]['campo']="OBSERVACAO";
$form['ATIVIDADE2'][10]['tipo']="texto";
$form['ATIVIDADE2'][10]['tam']=40;
$form['ATIVIDADE2'][10]['max']=150;
$form['ATIVIDADE2'][10]['null']=1;
$form['ATIVIDADE2'][10]['unico']=0;

$form['ATIVIDADE2'][11]['campo']="MAQUINA_PARADA";
$form['ATIVIDADE2'][11]['tipo']="select_rela";
$form['ATIVIDADE2'][11]['null']=0;
$form['ATIVIDADE2'][11]['unico']=0;

$form['ATIVIDADE2'][11]['fecha_fieldset']=1;

$form['ATIVIDADE2'][12]['abre_fieldset']=$ling['plano_prev_mo'];

$form['ATIVIDADE2'][12]['campo']="ESPECIALIDADE";
$form['ATIVIDADE2'][12]['tipo']="select_rela";
$form['ATIVIDADE2'][12]['null']=0;
$form['ATIVIDADE2'][12]['unico']=0;

$form['ATIVIDADE2'][13]['campo']="QUANTIDADE_MO";
$form['ATIVIDADE2'][13]['tipo']="texto_int";
$form['ATIVIDADE2'][13]['null']=0;
$form['ATIVIDADE2'][13]['tam']=5;
$form['ATIVIDADE2'][13]['unico']=0;

$form['ATIVIDADE2'][14]['campo']="TEMPO_PREVISTO";
$form['ATIVIDADE2'][14]['tipo']="texto_float";
$form['ATIVIDADE2'][14]['null']=0;
$form['ATIVIDADE2'][14]['tam']=5;
$form['ATIVIDADE2'][14]['unico']=0;

$form['ATIVIDADE2'][15]['campo']="MID_PLANO_PADRAO";
$form['ATIVIDADE2'][15]['tipo']="recb_valor";
$form['ATIVIDADE2'][15]['valor']=$form_recb_valor;
$form['ATIVIDADE2'][15]['null']=0;
$form['ATIVIDADE2'][15]['unico']=0;

$form['ATIVIDADE2'][15]['fecha_fieldset']=1;

$form['ATIVIDADE2'][16]['abre_fieldset']=$ling['plano_prev_mat'];

$form['ATIVIDADE2'][16]['campo']="MID_MATERIAL";
$form['ATIVIDADE2'][16]['tipo']="select_material";
$form['ATIVIDADE2'][16]['null']=1;
$form['ATIVIDADE2'][16]['tam']=0;
$form['ATIVIDADE2'][16]['max']=0;
$form['ATIVIDADE2'][16]['unico']=0;

$form['ATIVIDADE2'][17]['campo']="QUANTIDADE";
$form['ATIVIDADE2'][17]['tipo']="texto_unidade";
$form['ATIVIDADE2'][17]['null']=1;
$form['ATIVIDADE2'][17]['tam']=5;
$form['ATIVIDADE2'][17]['max']=5;
$form['ATIVIDADE2'][17]['unico']=0;

$form['ATIVIDADE2'][17]['fecha_fieldset']=1;



// FORMULARIO LUBRIFICACAO
$form['PLANO_ROTA'][0]['nome']=PLANO_ROTAS;
$form['PLANO_ROTA'][0]['titulo']=$tdb[PLANO_ROTAS]['DESC'];
$form['PLANO_ROTA'][0]['obs']="";
$form['PLANO_ROTA'][0]['dir']=1;
$form['PLANO_ROTA'][0]['largura']=530;
$form['PLANO_ROTA'][0]['altura']=250;
$form['PLANO_ROTA'][0]['chave']="MID";

$form['PLANO_ROTA'][1]['abre_fieldset']=$tdb[PLANO_ROTAS]['DESC'];

$form['PLANO_ROTA'][1]['campo']="MID_EMPRESA";
$form['PLANO_ROTA'][1]['tipo']="select_rela";
$form['PLANO_ROTA'][1]['null']=0;
$form['PLANO_ROTA'][1]['tam']=40;

$form['PLANO_ROTA'][2]['campo']="DESCRICAO";
$form['PLANO_ROTA'][2]['tipo']="texto";
$form['PLANO_ROTA'][2]['null']=0;
$form['PLANO_ROTA'][2]['tam']=40;
$form['PLANO_ROTA'][2]['max']=150;
$form['PLANO_ROTA'][2]['unico']=1;

$form['PLANO_ROTA'][3]['campo']="OBSERVACAO";
$form['PLANO_ROTA'][3]['tipo']="blog";
$form['PLANO_ROTA'][3]['null']=1;
$form['PLANO_ROTA'][3]['tam']=40;
$form['PLANO_ROTA'][3]['max']=4;
$form['PLANO_ROTA'][3]['unico']=0;

$form['PLANO_ROTA'][3]['fecha_fieldset']=1;

// FORMULARIO PREDITIVA
$form['PLANO_PREDITIVA'][0]['nome']=PLANO_PREDITIVA;
$form['PLANO_PREDITIVA'][0]['titulo']=$tdb[PLANO_PREDITIVA]['DESC'];
$form['PLANO_PREDITIVA'][0]['obs']="";
$form['PLANO_PREDITIVA'][0]['dir']=1;
$form['PLANO_PREDITIVA'][0]['largura']=520;
$form['PLANO_PREDITIVA'][0]['altura']=280;
$form['PLANO_PREDITIVA'][0]['chave']="MID";

$form['PLANO_PREDITIVA'][1]['abre_fieldset']=$tdb[PLANO_PREDITIVA]['DESC'];


$form['PLANO_PREDITIVA'][1]['campo']="DESCRICAO";
$form['PLANO_PREDITIVA'][1]['tipo']="texto";
$form['PLANO_PREDITIVA'][1]['null']=0;
$form['PLANO_PREDITIVA'][1]['tam']=30;
$form['PLANO_PREDITIVA'][1]['max']=150;
$form['PLANO_PREDITIVA'][1]['unico']=1;

$form['PLANO_PREDITIVA'][2]['campo']="PERIODICIDADE";
$form['PLANO_PREDITIVA'][2]['tipo']="select_parm";
$form['PLANO_PREDITIVA'][2]['null']=1;
$form['PLANO_PREDITIVA'][2]['parm'][1]="DIAS";
$form['PLANO_PREDITIVA'][2]['parm'][2]="SEMANAS";
$form['PLANO_PREDITIVA'][2]['unico']=0;

$form['PLANO_PREDITIVA'][3]['campo']="FREQUENCIA";
$form['PLANO_PREDITIVA'][3]['tipo']="texto";
$form['PLANO_PREDITIVA'][3]['null']=1;
$form['PLANO_PREDITIVA'][3]['tam']=5;
$form['PLANO_PREDITIVA'][3]['max']=5;
$form['PLANO_PREDITIVA'][3]['unico']=0;

$form['PLANO_PREDITIVA'][4]['campo']="CONTADOR";
$form['PLANO_PREDITIVA'][4]['tipo']="select_rela";
$form['PLANO_PREDITIVA'][4]['null']=1;
$form['PLANO_PREDITIVA'][4]['unico']=0;

$form['PLANO_PREDITIVA'][5]['campo']="DISPARO";
$form['PLANO_PREDITIVA'][5]['tipo']="texto";
$form['PLANO_PREDITIVA'][5]['tam']=7;
$form['PLANO_PREDITIVA'][5]['max']=13;
$form['PLANO_PREDITIVA'][5]['null']=1;
$form['PLANO_PREDITIVA'][5]['unico']=0;

$form['PLANO_PREDITIVA'][6]['campo']="OBSERVACAO";
$form['PLANO_PREDITIVA'][6]['tipo']="blog";
$form['PLANO_PREDITIVA'][6]['null']=1;
$form['PLANO_PREDITIVA'][6]['tam']=28;
$form['PLANO_PREDITIVA'][6]['max']=6;
$form['PLANO_PREDITIVA'][6]['unico']=0;

$form['PLANO_PREDITIVA'][6]['fecha_fieldset']=1;

$form['LINK_PREDITIVA'][0]['nome']=LINK_PREDITIVA;
$form['LINK_PREDITIVA'][0]['titulo']=$tdb[LINK_PREDITIVA]['DESC'];
$form['LINK_PREDITIVA'][0]['obs']="";
$form['LINK_PREDITIVA'][0]['largura']=650;
$form['LINK_PREDITIVA'][0]['altura']=300;
$form['LINK_PREDITIVA'][0]['chave']="MID";

$form['LINK_PREDITIVA'][1]['abre_fieldset']=$tdb[LINK_PREDITIVA]['DESC'];

$form['LINK_PREDITIVA'][1]['campo']="NUMERO";
$form['LINK_PREDITIVA'][1]['tipo']="texto";
$form['LINK_PREDITIVA'][1]['null']=0;
$form['LINK_PREDITIVA'][1]['tam']=2;
$form['LINK_PREDITIVA'][1]['max']=2;
$form['LINK_PREDITIVA'][1]['unico']=0;

$form['LINK_PREDITIVA'][2]['campo']="MID_MAQUINA";
$form['LINK_PREDITIVA'][2]['tipo']="select_maquina";
$form['LINK_PREDITIVA'][2]['null']=0;
$form['LINK_PREDITIVA'][2]['tam']=0;
$form['LINK_PREDITIVA'][2]['max']=0;
$form['LINK_PREDITIVA'][2]['unico']=0;

$form['LINK_PREDITIVA'][3]['campo']="MID_PONTO";
$form['LINK_PREDITIVA'][3]['tipo']="select_ponto_pred";
$form['LINK_PREDITIVA'][3]['null']=0;
$form['LINK_PREDITIVA'][3]['tam']=0;
$form['LINK_PREDITIVA'][3]['max']=0;
$form['LINK_PREDITIVA'][3]['unico']=0;

$form['LINK_PREDITIVA'][4]['campo']="MID_PLANO";
$form['LINK_PREDITIVA'][4]['tipo']="recb_valor";
$form['LINK_PREDITIVA'][4]['valor']=$form_recb_valor;
$form['LINK_PREDITIVA'][4]['null']=0;
$form['LINK_PREDITIVA'][4]['tam']=0;
$form['LINK_PREDITIVA'][4]['max']=0;
$form['LINK_PREDITIVA'][4]['unico']=0;

$form['LINK_PREDITIVA'][6]['campo']="OBSERVACAO";
$form['LINK_PREDITIVA'][6]['tipo']="blog";
$form['LINK_PREDITIVA'][6]['null']=1;
$form['LINK_PREDITIVA'][6]['tam']=20;
$form['LINK_PREDITIVA'][6]['max']=5;
$form['LINK_PREDITIVA'][6]['unico']=0;

$form['LINK_PREDITIVA'][5]['campo']="TEMPO_PREVISTO";
$form['LINK_PREDITIVA'][5]['tipo']="hora";
$form['LINK_PREDITIVA'][5]['null']=1;
$form['LINK_PREDITIVA'][5]['unico']=0;

$form['LINK_PREDITIVA'][6]['fecha_fieldset']=1;


// FORMULARIO LINK DA LUBRIFICACAO NO PLANO
$iform = 0;
$form['LINK_LUBRIFICACAO'][$iform]['nome']=LINK_ROTAS;
$form['LINK_LUBRIFICACAO'][$iform]['titulo']=$tdb[LINK_ROTAS]['DESC'];
$form['LINK_LUBRIFICACAO'][$iform]['obs']="";
$form['LINK_LUBRIFICACAO'][$iform]['largura']=670;
$form['LINK_LUBRIFICACAO'][$iform]['altura']=380;
$form['LINK_LUBRIFICACAO'][$iform]['chave']="MID";

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="NUMERO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=4;
$form['LINK_LUBRIFICACAO'][$iform]['max']=4;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="TAREFA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=50;
$form['LINK_LUBRIFICACAO'][$iform]['max']=150;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['prog_material'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_MATERIAL";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_material";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=0;
$form['LINK_LUBRIFICACAO'][$iform]['max']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="QUANTIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_unidade";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['max']=5;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['periodicidade_'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="PERIODICIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_rela";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;
$form['LINK_LUBRIFICACAO'][$iform]['valor']="WHERE MID != 4";

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="FREQUENCIA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=4;
$form['LINK_LUBRIFICACAO'][$iform]['max']=4;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['plano_ponto'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="TIPO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="recb_valor";
$form['LINK_LUBRIFICACAO'][$iform]['null']=1;
$form['LINK_LUBRIFICACAO'][$iform]['valor']=1;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_MAQUINA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_maquina";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_PONTO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_ponto_lub";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_PLANO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="recb_valor";
$form['LINK_LUBRIFICACAO'][$iform]['valor']=$form_recb_valor;
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['plano_prev_mo'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="ESPECIALIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_rela";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="QUANTIDADE_MO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_int";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="TEMPO_PREVISTO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_float";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;


// FORMULARIO LINK DA INSPECAO NO PLANO
$iform = 0;
$form['LINK_INSPECAO'][$iform]['nome']=LINK_ROTAS;
$form['LINK_INSPECAO'][$iform]['titulo']=$tdb[LINK_ROTAS]['DESC'];
$form['LINK_INSPECAO'][$iform]['obs']="";
$form['LINK_INSPECAO'][$iform]['largura']=670;
$form['LINK_INSPECAO'][$iform]['altura']=380;
$form['LINK_INSPECAO'][$iform]['chave']="MID";

$iform++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['atividade_a'];

$form['LINK_INSPECAO'][$iform]['campo']="NUMERO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=4;
$form['LINK_INSPECAO'][$iform]['max']=4;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform++;
$form['LINK_INSPECAO'][$iform]['campo']="TAREFA";
$form['LINK_INSPECAO'][$iform]['tipo']="texto";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=50;
$form['LINK_INSPECAO'][$iform]['max']=150;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['periodicidade_'];

$form['LINK_INSPECAO'][$iform]['campo']="PERIODICIDADE";
$form['LINK_INSPECAO'][$iform]['tipo']="select_peri";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform++;
$form['LINK_INSPECAO'][$iform]['campo']="FREQUENCIA";
$form['LINK_INSPECAO'][$iform]['tipo']="texto";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=4;
$form['LINK_INSPECAO'][$iform]['max']=4;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$tdb[MAQUINAS]['DESC'];

$form['LINK_INSPECAO'][$iform]['campo']="TIPO";
$form['LINK_INSPECAO'][$iform]['tipo']="recb_valor";
$form['LINK_INSPECAO'][$iform]['null']=1;
$form['LINK_INSPECAO'][$iform]['valor']=2;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_MAQUINA";
$form['LINK_INSPECAO'][$iform]['tipo']="select_maquina";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_CONJUNTO";
$form['LINK_INSPECAO'][$iform]['tipo']="select_conjunto_filtrado";
$form['LINK_INSPECAO'][$iform]['null']=1;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_PLANO";
$form['LINK_INSPECAO'][$iform]['tipo']="recb_valor";
$form['LINK_INSPECAO'][$iform]['valor']=$form_recb_valor;
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['plano_prev_mo'];

$form['LINK_INSPECAO'][$iform]['campo']="ESPECIALIDADE";
$form['LINK_INSPECAO'][$iform]['tipo']="select_rela";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="QUANTIDADE_MO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto_int";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=5;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="TEMPO_PREVISTO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto_float";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=5;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;


// FORMULARIO PROGRAMA&Ccedil;&Atilde;O GERAL
$form['PROGRAMACAO'][0]['nome']=PROGRAMACAO_DATA;
$form['PROGRAMACAO'][0]['titulo']=$tdb[PROGRAMACAO_DATA]['DESC'];
$form['PROGRAMACAO'][0]['obs']="";
$form['PROGRAMACAO'][0]['largura']=450;
$form['PROGRAMACAO'][0]['altura']=250;
$form['PROGRAMACAO'][0]['chave']="MID";

$form['PROGRAMACAO'][1]['abre_fieldset']=$ling['prog_ajuste'];

$form['PROGRAMACAO'][1]['campo']="DATA";
$form['PROGRAMACAO'][1]['tipo']="data";
$form['PROGRAMACAO'][1]['null']=0;
$form['PROGRAMACAO'][1]['unico']=0;

$form['PROGRAMACAO'][2]['campo']="TEXTO";
$form['PROGRAMACAO'][2]['tipo']="texto";
$form['PROGRAMACAO'][2]['tam']=40;
$form['PROGRAMACAO'][2]['max']=255;
$form['PROGRAMACAO'][2]['null']=0;
$form['PROGRAMACAO'][2]['unico']=0;

$form['PROGRAMACAO'][2]['fecha_fieldset']=1;

$form['PROGRAMACAO_MATERIAL'][0]['nome']=PROGRAMACAO_MATERIAIS;
$form['PROGRAMACAO_MATERIAL'][0]['titulo']=$tdb[PROGRAMACAO_MATERIAIS]['DESC'];
$form['PROGRAMACAO_MATERIAL'][0]['obs']="";
$form['PROGRAMACAO_MATERIAL'][0]['largura']=600;
$form['PROGRAMACAO_MATERIAL'][0]['altura']=300;
$form['PROGRAMACAO_MATERIAL'][0]['chave']="MID";

$form['PROGRAMACAO_MATERIAL'][1]['abre_fieldset']=$tdb[PROGRAMACAO_MATERIAIS]['DESC'];


$form['PROGRAMACAO_MATERIAL'][2]['campo']="QUANTIDADE";
$form['PROGRAMACAO_MATERIAL'][2]['tipo']="texto";
$form['PROGRAMACAO_MATERIAL'][2]['null']=0;
$form['PROGRAMACAO_MATERIAL'][2]['tam']=5;
$form['PROGRAMACAO_MATERIAL'][2]['max']=5;
$form['PROGRAMACAO_MATERIAL'][2]['unico']=0;

$form['PROGRAMACAO_MATERIAL'][1]['campo']="MID_MATERIAL";
$form['PROGRAMACAO_MATERIAL'][1]['tipo']="select_material";
$form['PROGRAMACAO_MATERIAL'][1]['null']=0;
$form['PROGRAMACAO_MATERIAL'][1]['tam']=10;
$form['PROGRAMACAO_MATERIAL'][1]['max']=10;
$form['PROGRAMACAO_MATERIAL'][1]['unico']=0;

$form['PROGRAMACAO_MATERIAL'][3]['campo']="MID_PROGRAMACAO";
$form['PROGRAMACAO_MATERIAL'][3]['tipo']="recb_valor";
$form['PROGRAMACAO_MATERIAL'][3]['valor']=$form_recb_valor;
$form['PROGRAMACAO_MATERIAL'][3]['null']=0;
$form['PROGRAMACAO_MATERIAL'][3]['tam']=0;
$form['PROGRAMACAO_MATERIAL'][3]['max']=0;
$form['PROGRAMACAO_MATERIAL'][3]['unico']=0;

$form['PROGRAMACAO_MATERIAL'][3]['fecha_fieldset']=1;

// FORMULARIO MAO DE OBRA
$form['PROGRAMACAO_MAODEOBRA'][0]['nome']=PROGRAMACAO_MAODEOBRA;
$form['PROGRAMACAO_MAODEOBRA'][0]['titulo']=$tdb[PROGRAMACAO_MAODEOBRA]['DESC'];
$form['PROGRAMACAO_MAODEOBRA'][0]['obs']="";
$form['PROGRAMACAO_MAODEOBRA'][0]['largura']=400;
$form['PROGRAMACAO_MAODEOBRA'][0]['altura']=300;
$form['PROGRAMACAO_MAODEOBRA'][0]['chave']="MID";

$form['PROGRAMACAO_MAODEOBRA'][1]['abre_fieldset']=$tdb[PROGRAMACAO_MAODEOBRA]['DESC'];

$form['PROGRAMACAO_MAODEOBRA'][1]['campo']="TEMPO";
$form['PROGRAMACAO_MAODEOBRA'][1]['tipo']="hora";
$form['PROGRAMACAO_MAODEOBRA'][1]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][1]['tam']=8;
$form['PROGRAMACAO_MAODEOBRA'][1]['max']=8;
$form['PROGRAMACAO_MAODEOBRA'][1]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][2]['campo']="MID_EQUIPE";
$form['PROGRAMACAO_MAODEOBRA'][2]['tipo']="select_rela";
$form['PROGRAMACAO_MAODEOBRA'][2]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][2]['tam']=10;
$form['PROGRAMACAO_MAODEOBRA'][2]['max']=10;
$form['PROGRAMACAO_MAODEOBRA'][2]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][3]['campo']="MID_PROGRAMACAO";
$form['PROGRAMACAO_MAODEOBRA'][3]['tipo']="recb_valor";
$form['PROGRAMACAO_MAODEOBRA'][3]['valor']=$form_recb_valor;
$form['PROGRAMACAO_MAODEOBRA'][3]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['tam']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['max']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][3]['fecha_fieldset']=1;


// FORMULARIO FERIADOS
$form['FERIADO'][0]['nome']=FERIADOS;
$form['FERIADO'][0]['titulo']="CADASTRO DE FERIADOS";
$form['FERIADO'][0]['obs']="";
$form['FERIADO'][0]['largura']=500;
$form['FERIADO'][0]['altura']=180;
$form['FERIADO'][0]['chave']="MID";

$form['FERIADO'][1]['abre_fieldset']=$tdb[FERIADOS]['DESC'];

$form['FERIADO'][1]['campo']="DATA";
$form['FERIADO'][1]['tipo']="data";
$form['FERIADO'][1]['null']=0;
$form['FERIADO'][1]['tam']=30;
$form['FERIADO'][1]['max']=250;
$form['FERIADO'][1]['unico']=0;

$form['FERIADO'][2]['campo']="DESCRICAO";
$form['FERIADO'][2]['tipo']="texto";
$form['FERIADO'][2]['null']=0;
$form['FERIADO'][2]['tam']=40;
$form['FERIADO'][2]['max']=250;
$form['FERIADO'][2]['unico']=0;
$form['FERIADO'][2]['fecha_fieldset']=1;

$form['SOL_SEGURANCA'][0]['nome']=SOLICITACAO_SITUACAO_SEGURANCA;
$form['SOL_SEGURANCA'][0]['titulo']=$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC'];
$form['SOL_SEGURANCA'][0]['obs']="";
$form['SOL_SEGURANCA'][0]['largura']=500;
$form['SOL_SEGURANCA'][0]['altura']=180;
$form['SOL_SEGURANCA'][0]['chave']="MID";
$form['SOL_SEGURANCA'][1]['abre_fieldset']=$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC'];

$form['SOL_SEGURANCA'][1]['campo']="DESCRICAO";
$form['SOL_SEGURANCA'][1]['tipo']="texto";
$form['SOL_SEGURANCA'][1]['null']=0;
$form['SOL_SEGURANCA'][1]['tam']=30;
$form['SOL_SEGURANCA'][1]['max']=250;
$form['SOL_SEGURANCA'][1]['unico']=1;

$form['SOL_SEGURANCA'][2]['campo']="NIVEL";
$form['SOL_SEGURANCA'][2]['tipo']="texto";
$form['SOL_SEGURANCA'][2]['null']=0;
$form['SOL_SEGURANCA'][2]['tam']=2;
$form['SOL_SEGURANCA'][2]['max']=2;
$form['SOL_SEGURANCA'][2]['unico']=0;

$form['SOL_SEGURANCA'][2]['fecha_fieldset']=1;

$form['SOL_PRODUCAO'][0]['nome']=SOLICITACAO_SITUACAO_PRODUCAO;
$form['SOL_PRODUCAO'][0]['titulo']=$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC'];
$form['SOL_PRODUCAO'][0]['obs']="";
$form['SOL_PRODUCAO'][0]['largura']=500;
$form['SOL_PRODUCAO'][0]['altura']=180;
$form['SOL_PRODUCAO'][0]['chave']="MID";
$form['SOL_PRODUCAO'][1]['abre_fieldset']=$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC'];

$form['SOL_PRODUCAO'][1]['campo']="DESCRICAO";
$form['SOL_PRODUCAO'][1]['tipo']="texto";
$form['SOL_PRODUCAO'][1]['null']=0;
$form['SOL_PRODUCAO'][1]['tam']=30;
$form['SOL_PRODUCAO'][1]['max']=250;
$form['SOL_PRODUCAO'][1]['unico']=1;

$form['SOL_PRODUCAO'][2]['campo']="NIVEL";
$form['SOL_PRODUCAO'][2]['tipo']="texto";
$form['SOL_PRODUCAO'][2]['null']=0;
$form['SOL_PRODUCAO'][2]['tam']=2;
$form['SOL_PRODUCAO'][2]['max']=2;
$form['SOL_PRODUCAO'][2]['unico']=0;

$form['SOL_PRODUCAO'][2]['fecha_fieldset']=1;

// FORMULARIO PROGRAMACAO ROTA
$form['SOLICITACAO'][0]['nome']=SOLICITACOES;
$form['SOLICITACAO'][0]['titulo']=$tdb[SOLICITACOES]['DESC'];
$form['SOLICITACAO'][0]['obs']="";
$form['SOLICITACAO'][0]['largura']=550;
$form['SOLICITACAO'][0]['altura']=400;
$form['SOLICITACAO'][0]['chave']="MID";

$form['SOLICITACAO'][1]['abre_fieldset']=$tdb[SOLICITACOES]['DESC'];

$form['SOLICITACAO'][1]['campo']="MID_MAQUINA";
$form['SOLICITACAO'][1]['tipo']="select_rela";
$form['SOLICITACAO'][1]['null']=0;
$form['SOLICITACAO'][1]['tam']=0;
$form['SOLICITACAO'][1]['max']=0;
$form['SOLICITACAO'][1]['unico']=0;
$form['SOLICITACAO'][1]['js']="atualiza_area2('select_cc[" . SOLICITACOES . "][2]', 'parametros.php?id=filtra_conj&campo_id=cc[" . SOLICITACOES . "][2]&campo_nome=cc[" . SOLICITACOES . "][2]&null=1&mid_maq=' + this.value);";

$form['SOLICITACAO'][2]['campo']="MID_CONJUNTO";
$form['SOLICITACAO'][2]['tipo']="select_rela";
$form['SOLICITACAO'][2]['null']=1;
$form['SOLICITACAO'][2]['tam']=0;
$form['SOLICITACAO'][2]['max']=0;
$form['SOLICITACAO'][2]['unico']=0;

$form['SOLICITACAO'][3]['campo']="DESCRICAO";
$form['SOLICITACAO'][3]['tipo']="texto";
$form['SOLICITACAO'][3]['null']=0;
$form['SOLICITACAO'][3]['tam']=50;
$form['SOLICITACAO'][3]['max']=38;
$form['SOLICITACAO'][3]['unico']=0;

$form['SOLICITACAO'][4]['campo']="TEXTO";
$form['SOLICITACAO'][4]['tipo']="blog";
$form['SOLICITACAO'][4]['null']=0;
$form['SOLICITACAO'][4]['tam']=50;
$form['SOLICITACAO'][4]['max']=8;
$form['SOLICITACAO'][4]['unico']=0;

$form['SOLICITACAO'][5]['campo']="DATA";
$form['SOLICITACAO'][5]['tipo']="datafixa";
$form['SOLICITACAO'][5]['null']=0;
$form['SOLICITACAO'][5]['tam']=10;
$form['SOLICITACAO'][5]['max']=10;
$form['SOLICITACAO'][5]['unico']=0;

$form['SOLICITACAO'][6]['campo']="HORA";
$form['SOLICITACAO'][6]['tipo']="horafixa";
$form['SOLICITACAO'][6]['null']=0;
$form['SOLICITACAO'][6]['tam']=10;
$form['SOLICITACAO'][6]['max']=10;
$form['SOLICITACAO'][6]['unico']=0;

$form['SOLICITACAO'][7]['campo']="USUARIO";
$form['SOLICITACAO'][7]['tipo']="recb_valor";
$form['SOLICITACAO'][7]['valor']=(int)$_SESSION[ManuSess]['user']['MID'];
$form['SOLICITACAO'][7]['null']=0;
$form['SOLICITACAO'][7]['tam']=0;
$form['SOLICITACAO'][7]['max']=0;
$form['SOLICITACAO'][7]['unico']=0;

$form['SOLICITACAO'][7]['fecha_fieldset']=1;

$form['SOLICITACAO'][8]['abre_fieldset']=$ling['sol_criticidade'];

$form['SOLICITACAO'][8]['campo']="PRODUCAO";
$form['SOLICITACAO'][8]['tipo']="select_rela";
$form['SOLICITACAO'][8]['null']=1;
$form['SOLICITACAO'][8]['tam']=0;
$form['SOLICITACAO'][8]['max']=0;
$form['SOLICITACAO'][8]['unico']=0;

$form['SOLICITACAO'][9]['campo']="SEGURANCA";
$form['SOLICITACAO'][9]['tipo']="select_rela";
$form['SOLICITACAO'][9]['null']=1;
$form['SOLICITACAO'][9]['tam']=0;
$form['SOLICITACAO'][9]['max']=0;
$form['SOLICITACAO'][9]['unico']=0;

$form['SOLICITACAO'][10]['campo']="STATUS";
$form['SOLICITACAO'][10]['tipo']="recb_valor";
$form['SOLICITACAO'][10]['null']=0;
$form['SOLICITACAO'][10]['tam']=0;
$form['SOLICITACAO'][10]['max']=0;
$form['SOLICITACAO'][10]['unico']=0;

$form['SOLICITACAO'][10]['fecha_fieldset']=1;


// FORMULARIO PROGRAMACAO ROTA
$form['REQUISICAO'][0]['nome']=REQUISICAO;
$form['REQUISICAO'][0]['titulo']=$tdb[REQUISICAO]['DESC'];
$form['REQUISICAO'][0]['obs']="";
$form['REQUISICAO'][0]['largura']=500;
$form['REQUISICAO'][0]['altura']=340;
$form['REQUISICAO'][0]['chave']="MID";

$form['REQUISICAO'][1]['abre_fieldset']=$tdb[REQUISICAO]['DESC'];

$form['REQUISICAO'][1]['campo']="USUARIO";
$form['REQUISICAO'][1]['tipo']="recb_valor";
$form['REQUISICAO'][1]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['REQUISICAO'][1]['null']=0;
$form['REQUISICAO'][1]['tam']=0;
$form['REQUISICAO'][1]['max']=0;
$form['REQUISICAO'][1]['unico']=0;

$form['REQUISICAO'][2]['campo']="FUNCIONARIO";
$form['REQUISICAO'][2]['tipo']="select_rela";
$form['REQUISICAO'][2]['null']=1;
$form['REQUISICAO'][2]['tam']=0;
$form['REQUISICAO'][2]['max']=0;
$form['REQUISICAO'][2]['unico']=0;

$form['REQUISICAO'][3]['campo']="MID_ALMOXARIFADO";
$form['REQUISICAO'][3]['tipo']="select_rela";
$form['REQUISICAO'][3]['null']=0;
$form['REQUISICAO'][3]['tam']=0;
$form['REQUISICAO'][3]['max']=0;
$form['REQUISICAO'][3]['unico']=0;

$form['REQUISICAO'][4]['campo']="MOTIVO";
$form['REQUISICAO'][4]['tipo']="texto";
$form['REQUISICAO'][4]['null']=0;
$form['REQUISICAO'][4]['tam']=40;
$form['REQUISICAO'][4]['max']=200;
$form['REQUISICAO'][4]['unico']=0;

$form['REQUISICAO'][5]['campo']="ORDEM";
$form['REQUISICAO'][5]['tipo']="texto";
$form['REQUISICAO'][5]['null']=1;
$form['REQUISICAO'][5]['tam']=10;
$form['REQUISICAO'][5]['max']=30;
$form['REQUISICAO'][5]['unico']=0;

$form['REQUISICAO'][6]['campo']="PRAZO";
$form['REQUISICAO'][6]['tipo']="data";
$form['REQUISICAO'][6]['valor']=0;
$form['REQUISICAO'][6]['null']=0;
$form['REQUISICAO'][6]['tam']=10;
$form['REQUISICAO'][6]['max']=10;
$form['REQUISICAO'][6]['unico']=0;

$form['REQUISICAO'][7]['campo']="MATERIAIS";
$form['REQUISICAO'][7]['tipo']="blog";
$form['REQUISICAO'][7]['valor']=0;
$form['REQUISICAO'][7]['null']=0;
$form['REQUISICAO'][7]['tam']=40;
$form['REQUISICAO'][7]['max']=6;
$form['REQUISICAO'][7]['unico']=0;

$form['REQUISICAO'][8]['campo']="STATUS";
$form['REQUISICAO'][8]['tipo']="recb_valor";
$form['REQUISICAO'][8]['valor']=0;
$form['REQUISICAO'][8]['null']=0;
$form['REQUISICAO'][8]['tam']=0;
$form['REQUISICAO'][8]['max']=0;
$form['REQUISICAO'][8]['unico']=0;

$form['REQUISICAO'][9]['campo']="DATA";
$form['REQUISICAO'][9]['tipo']="datafixa";
$form['REQUISICAO'][9]['null']=0;
$form['REQUISICAO'][9]['tam']=10;
$form['REQUISICAO'][9]['max']=10;
$form['REQUISICAO'][9]['unico']=0;

$form['REQUISICAO'][10]['campo']="HORA";
$form['REQUISICAO'][10]['tipo']="horafixa";
$form['REQUISICAO'][10]['tam']=10;
$form['REQUISICAO'][10]['max']=10;
$form['REQUISICAO'][10]['null']=0;
$form['REQUISICAO'][10]['unico']=0;

$form['REQUISICAO'][10]['fecha_fieldset']=1;

$form['REQUISICAO_RESP'][0]['nome']=REQUISICAO_RESPOSTA;
$form['REQUISICAO_RESP'][0]['titulo']=$tdb[REQUISICAO_RESPOSTA]['DESC'];
$form['REQUISICAO_RESP'][0]['obs']="";
$form['REQUISICAO_RESP'][0]['largura']=500;
$form['REQUISICAO_RESP'][0]['altura']=300;
$form['REQUISICAO_RESP'][0]['chave']="MID";

$form['REQUISICAO_RESP'][1]['abre_fieldset']=$tdb[REQUISICAO_RESPOSTA]['DESC'];

$form['REQUISICAO_RESP'][1]['campo']="DATA";
$form['REQUISICAO_RESP'][1]['tipo']="datafixa";
$form['REQUISICAO_RESP'][1]['tam']=10;
$form['REQUISICAO_RESP'][1]['max']=10;
$form['REQUISICAO_RESP'][1]['null']=0;
$form['REQUISICAO_RESP'][1]['unico']=0;

$form['REQUISICAO_RESP'][2]['campo']="TEXTO";
$form['REQUISICAO_RESP'][2]['tipo']="blog";
$form['REQUISICAO_RESP'][2]['null']=0;
$form['REQUISICAO_RESP'][2]['tam']=40;
$form['REQUISICAO_RESP'][2]['max']=8;
$form['REQUISICAO_RESP'][2]['unico']=0;

$form['REQUISICAO_RESP'][3]['campo']="MID_REQUISICAO";
$form['REQUISICAO_RESP'][3]['null']=0;
$form['REQUISICAO_RESP'][3]['tipo']="recb_valor";
$form['REQUISICAO_RESP'][3]['valor']=$form_recb_valor;
$form['REQUISICAO_RESP'][3]['unico']=1;

$form['REQUISICAO_RESP'][4]['campo']="USUARIO";
$form['REQUISICAO_RESP'][4]['null']=0;
$form['REQUISICAO_RESP'][4]['tipo']="recb_valor";
$form['REQUISICAO_RESP'][4]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['REQUISICAO_RESP'][4]['unico']=0;

$form['REQUISICAO_RESP'][5]['campo']="HORA";
$form['REQUISICAO_RESP'][5]['tipo']="horafixa";
$form['REQUISICAO_RESP'][5]['tam']=10;
$form['REQUISICAO_RESP'][5]['max']=10;
$form['REQUISICAO_RESP'][5]['null']=0;
$form['REQUISICAO_RESP'][5]['unico']=0;

$form['REQUISICAO_RESP'][5]['fecha_fieldset']=1;


$form['SOLICITACAO_RESP'][0]['nome']=SOLICITACAO_RESPOSTA;
$form['SOLICITACAO_RESP'][0]['titulo']=$tdb[SOLICITACAO_RESPOSTA]['DESC'];
$form['SOLICITACAO_RESP'][0]['obs']="";
$form['SOLICITACAO_RESP'][0]['largura']=600;
$form['SOLICITACAO_RESP'][0]['altura']=260;
$form['SOLICITACAO_RESP'][0]['chave']="MID";

$form['SOLICITACAO_RESP'][1]['abre_fieldset']=$tdb[SOLICITACAO_RESPOSTA]['DESC'];

$form['SOLICITACAO_RESP'][1]['campo']="DATA";
$form['SOLICITACAO_RESP'][1]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][1]['valor']=date("Y-m-d");
$form['SOLICITACAO_RESP'][1]['null']=1;
$form['SOLICITACAO_RESP'][1]['unico']=0;

$form['SOLICITACAO_RESP'][5]['campo']="HORA";
$form['SOLICITACAO_RESP'][5]['tipo']="horafixa";
$form['SOLICITACAO_RESP'][5]['valor']=date("H:i:s");
$form['SOLICITACAO_RESP'][5]['null']=1;
$form['SOLICITACAO_RESP'][5]['unico']=0;

$form['SOLICITACAO_RESP'][2]['campo']="TEXTO";
$form['SOLICITACAO_RESP'][2]['tipo']="blog";
$form['SOLICITACAO_RESP'][2]['null']=0;
$form['SOLICITACAO_RESP'][2]['tam']=40;
$form['SOLICITACAO_RESP'][2]['max']=8;
$form['SOLICITACAO_RESP'][2]['unico']=0;

$form['SOLICITACAO_RESP'][3]['campo']="MID_SOLICITACAO";
$form['SOLICITACAO_RESP'][3]['null']=0;
$form['SOLICITACAO_RESP'][3]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][3]['valor']=$form_recb_valor;
$form['SOLICITACAO_RESP'][3]['unico']=0;

$form['SOLICITACAO_RESP'][4]['campo']="USUARIO";
$form['SOLICITACAO_RESP'][4]['null']=0;
$form['SOLICITACAO_RESP'][4]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][4]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['SOLICITACAO_RESP'][4]['unico']=0;

$form['SOLICITACAO_RESP'][4]['fecha_fieldset']=1;



$form['NATUREZA'][0]['nome']=NATUREZA_SERVICOS;
$form['NATUREZA'][0]['titulo']=$tdb[NATUREZA_SERVICOS]['DESC'];
$form['NATUREZA'][0]['obs']="";
$form['NATUREZA'][0]['largura']=500;
$form['NATUREZA'][0]['altura']=160;
$form['NATUREZA'][0]['chave']="MID";

$form['NATUREZA'][1]['abre_fieldset']=$tdb[NATUREZA_SERVICOS]['DESC'];

$form['NATUREZA'][1]['campo']="MID_EMPRESA";
$form['NATUREZA'][1]['tipo']="select_rela";
$form['NATUREZA'][1]['null']=1;
$form['NATUREZA'][1]['tam']=15;
$form['NATUREZA'][1]['max']=30;

$form['NATUREZA'][2]['campo']="DESCRICAO";
$form['NATUREZA'][2]['tipo']="texto";
$form['NATUREZA'][2]['null']=0;
$form['NATUREZA'][2]['tam']=30;
$form['NATUREZA'][2]['max']=250;
$form['NATUREZA'][2]['unico']=1;

$form['NATUREZA'][2]['fecha_fieldset']=1;

$iform = 0;
$form['TIPO_SERVICO'][$iform]['nome']=TIPOS_SERVICOS;
$form['TIPO_SERVICO'][$iform]['titulo']=$tdb[TIPOS_SERVICOS]['DESC'];
$form['TIPO_SERVICO'][$iform]['obs']="";
$form['TIPO_SERVICO'][$iform]['largura']=500;
$form['TIPO_SERVICO'][$iform]['altura']=230;
$form['TIPO_SERVICO'][$iform]['chave']="MID";

$iform ++;
$form['TIPO_SERVICO'][$iform]['abre_fieldset']=$tdb[TIPOS_SERVICOS]['DESC'];

$form['TIPO_SERVICO'][$iform]['campo']="MID_EMPRESA";
$form['TIPO_SERVICO'][$iform]['tipo']="select_rela";
$form['TIPO_SERVICO'][$iform]['null']=1;
$form['TIPO_SERVICO'][$iform]['tam']=15;
$form['TIPO_SERVICO'][$iform]['max']=30;

$iform ++;
$form['TIPO_SERVICO'][$iform]['campo']="DESCRICAO";
$form['TIPO_SERVICO'][$iform]['tipo']="texto";
$form['TIPO_SERVICO'][$iform]['null']=0;
$form['TIPO_SERVICO'][$iform]['tam']=30;
$form['TIPO_SERVICO'][$iform]['max']=250;
$form['TIPO_SERVICO'][$iform]['unico']=1;

$iform++;
$form['TIPO_SERVICO'][$iform]['campo'] = "SPEED_CHECK";
$form['TIPO_SERVICO'][$iform]['tipo'] = "select_rela";
$form['TIPO_SERVICO'][$iform]['null'] = 0;
$form['TIPO_SERVICO'][$iform]['sql'] = '';
$form['TIPO_SERVICO'][$iform]['js'] = 'this.form.submit()';
$iform++;
$form['TIPO_SERVICO'][$iform]['campo'] = "MID_PLANO";

$form['TIPO_SERVICO'][$iform]['null'] = 0;
$form['TIPO_SERVICO'][$iform]['sql'] = '';
$form['TIPO_SERVICO'][$iform]['js'] = '';
if($_POST['cc'][TIPOS_SERVICOS][$iform-1] == 1 OR (($_GET['act'] == 2 OR $_GET['act'] == 3) AND VoltaValor(TIPOS_SERVICOS, "SPEED_CHECK", "MID", $_GET['foq']) == 1) ){
	
	$form['TIPO_SERVICO'][$iform]['tipo'] = "select_rela";
}
else{
	$form['TIPO_SERVICO'][$iform]['valor'] = 0;
	$form['TIPO_SERVICO'][$iform]['tipo'] = "recb_valor";
}


$form['TIPO_SERVICO'][$iform]['fecha_fieldset']=1;

$form['CAUSA'][0]['nome']=CAUSA;
$form['CAUSA'][0]['titulo']=$tdb[CAUSA]['DESC'];
$form['CAUSA'][0]['obs']="";
$form['CAUSA'][0]['largura']=500;
$form['CAUSA'][0]['altura']=160;
$form['CAUSA'][0]['chave']="MID";

$form['CAUSA'][1]['abre_fieldset']=$tdb[CAUSA]['DESC'];

$form['CAUSA'][1]['campo']="MID_EMPRESA";
$form['CAUSA'][1]['tipo']="select_rela";
$form['CAUSA'][1]['null']=1;
$form['CAUSA'][1]['tam']=15;
$form['CAUSA'][1]['max']=30;

$form['CAUSA'][2]['campo']="DESCRICAO";
$form['CAUSA'][2]['tipo']="texto";
$form['CAUSA'][2]['null']=0;
$form['CAUSA'][2]['tam']=30;
$form['CAUSA'][2]['max']=250;
$form['CAUSA'][2]['unico']=1;

$form['CAUSA'][2]['fecha_fieldset']=1;


$form['DEFEITO'][0]['nome']=DEFEITO;
$form['DEFEITO'][0]['titulo']=$tdb[DEFEITO]['DESC'];
$form['DEFEITO'][0]['obs']="";
$form['DEFEITO'][0]['largura']=500;
$form['DEFEITO'][0]['altura']=160;
$form['DEFEITO'][0]['chave']="MID";

$form['DEFEITO'][1]['abre_fieldset']=$tdb[DEFEITO]['DESC'];

$form['DEFEITO'][1]['campo']="MID_EMPRESA";
$form['DEFEITO'][1]['tipo']="select_rela";
$form['DEFEITO'][1]['null']=1;
$form['DEFEITO'][1]['tam']=15;
$form['DEFEITO'][1]['max']=30;

$form['DEFEITO'][2]['campo']="DESCRICAO";
$form['DEFEITO'][2]['tipo']="texto";
$form['DEFEITO'][2]['null']=0;
$form['DEFEITO'][2]['tam']=30;
$form['DEFEITO'][2]['max']=250;
$form['DEFEITO'][2]['unico']=1;

$form['DEFEITO'][2]['fecha_fieldset']=1;

$form['SOLUCAO'][0]['nome']=SOLUCAO;
$form['SOLUCAO'][0]['titulo']=$tdb[SOLUCAO]['DESC'];
$form['SOLUCAO'][0]['obs']="";
$form['SOLUCAO'][0]['largura']=500;
$form['SOLUCAO'][0]['altura']=160;
$form['SOLUCAO'][0]['chave']="MID";

$form['SOLUCAO'][1]['abre_fieldset']=$tdb[SOLUCAO]['DESC'];

$form['SOLUCAO'][1]['campo']="MID_EMPRESA";
$form['SOLUCAO'][1]['tipo']="select_rela";
$form['SOLUCAO'][1]['null']=1;
$form['SOLUCAO'][1]['tam']=15;
$form['SOLUCAO'][1]['max']=30;

$form['SOLUCAO'][2]['campo']="DESCRICAO";
$form['SOLUCAO'][2]['tipo']="texto";
$form['SOLUCAO'][2]['null']=0;
$form['SOLUCAO'][2]['tam']=30;
$form['SOLUCAO'][2]['max']=250;
$form['SOLUCAO'][2]['unico']=1;

$form['SOLUCAO'][2]['fecha_fieldset']=1;




$form['PENDENCIAS'][0]['nome']=PENDENCIAS;
$form['PENDENCIAS'][0]['titulo']=$tdb[PENDENCIAS]['DESC'];
$form['PENDENCIAS'][0]['obs']="";
$form['PENDENCIAS'][0]['largura']=600;
$form['PENDENCIAS'][0]['altura']=280;
$form['PENDENCIAS'][0]['chave']="MID";
$form['PENDENCIAS'][1]['abre_fieldset']=$tdb[PENDENCIAS]['DESC'];

$form['PENDENCIAS'][1]['campo']="MID_MAQUINA";
$form['PENDENCIAS'][1]['tipo']="select_rela";
$form['PENDENCIAS'][1]['null']=0;
$form['PENDENCIAS'][1]['tam']=0;
$form['PENDENCIAS'][1]['max']=0;
$form['PENDENCIAS'][1]['unico']=0;
$form['PENDENCIAS'][1]['js']="atualiza_area2('conj_os', 'parametros.php?id=filtro_pend&conj[campo_id]=cc[" . PENDENCIAS . "][2]&conj[campo_nome]=cc[" . PENDENCIAS . "][2]&conj[null]=1&os[campo_id]=cc[" . PENDENCIAS . "][3]&os[campo_nome]=cc[" . PENDENCIAS . "][3]&os[null]=1&mid_maq=' + this.value);";

$form['PENDENCIAS'][2]['abre_div']="conj_os";

$form['PENDENCIAS'][2]['campo']="MID_CONJUNTO";
$form['PENDENCIAS'][2]['tipo']="select_rela";
$form['PENDENCIAS'][2]['null']=1;
$form['PENDENCIAS'][2]['tam']=0;
$form['PENDENCIAS'][2]['max']=0;
$form['PENDENCIAS'][2]['unico']=0;

if (($act == 2) or $_POST['cc']) {
    if (($form_recb_valor != "") and ($form_recb_valor != "undefined")) {
        $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
        $form['PENDENCIAS'][3]['tipo']="recb_valor";
        $form['PENDENCIAS'][3]['valor']=$form_recb_valor;
        $form['PENDENCIAS'][3]['null']=0;
        $form['PENDENCIAS'][3]['tam']=30;
        $form['PENDENCIAS'][3]['max']=3;
        $form['PENDENCIAS'][3]['unico']=0;
    }
    else{
        $maq = ($act == 2) ? VoltaValor(PENDENCIAS,'MID_MAQUINA','MID',$_GET['foq'],$tdb[PENDENCIAS]['dba']) : $_POST['cc'][PENDENCIAS][1];
        $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
        $form['PENDENCIAS'][3]['tipo']="select_rela";
        $form['PENDENCIAS'][3]['null']=1;
        $form['PENDENCIAS'][3]['tam']=10;
        $form['PENDENCIAS'][3]['max']=10;
        $form['PENDENCIAS'][3]['unico']=0;
        $form['PENDENCIAS'][3]['autofiltro']='N';   
        $form['PENDENCIAS'][3]['valor']="WHERE STATUS = 1 AND (MID_MAQUINA = '$maq' OR MID IN (SELECT DISTINCT MID_ORDEM FROM ".ORDEM_LUB." WHERE MID_MAQUINA = '$maq'))";
    }
}
else {
    $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
    $form['PENDENCIAS'][3]['tipo']="recb_valor";
    $form['PENDENCIAS'][3]['valor']='';
    $form['PENDENCIAS'][3]['null']=0;
    $form['PENDENCIAS'][3]['tam']=30;
    $form['PENDENCIAS'][3]['max']=3;
    $form['PENDENCIAS'][3]['unico']=0;
    
}

$form['PENDENCIAS'][3]['fecha_div']=1;

$form['PENDENCIAS'][4]['campo']="DATA";
$form['PENDENCIAS'][4]['tipo']="data";
$form['PENDENCIAS'][4]['null']=0;
$form['PENDENCIAS'][4]['tam']=30;
$form['PENDENCIAS'][4]['max']=3;
$form['PENDENCIAS'][4]['unico']=0;

$form['PENDENCIAS'][5]['campo']="DESCRICAO";
$form['PENDENCIAS'][5]['tipo']="blog";
$form['PENDENCIAS'][5]['null']=0;
$form['PENDENCIAS'][5]['tam']=50;
$form['PENDENCIAS'][5]['max']=3;
$form['PENDENCIAS'][5]['unico']=0;

$form['PENDENCIAS'][5]['fecha_fieldset']=1;


// FORMULARIO CHECKLIST
$form['PLANO_CHECKLIST'][0]['nome']=PLANO_CHECKLIST;
$form['PLANO_CHECKLIST'][0]['titulo']=$tdb[PLANO_CHECKLIST]['DESC'];
$form['PLANO_CHECKLIST'][0]['obs']="";
$form['PLANO_CHECKLIST'][0]['dir']=1;
$form['PLANO_CHECKLIST'][0]['largura']=530;
$form['PLANO_CHECKLIST'][0]['altura']=280;
$form['PLANO_CHECKLIST'][0]['chave']="MID";

$form['PLANO_CHECKLIST'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST]['DESC'];

$form['PLANO_CHECKLIST'][1]['campo']="DESCRICAO";
$form['PLANO_CHECKLIST'][1]['tipo']="texto";
$form['PLANO_CHECKLIST'][1]['null']=0;
$form['PLANO_CHECKLIST'][1]['tam']=30;
$form['PLANO_CHECKLIST'][1]['max']=150;
$form['PLANO_CHECKLIST'][1]['unico']=1;

$form['PLANO_CHECKLIST'][2]['campo']="USAR_TURNOS";
$form['PLANO_CHECKLIST'][2]['tipo']="select_rela";
$form['PLANO_CHECKLIST'][2]['null']=0;
$form['PLANO_CHECKLIST'][2]['unico']=0;


$form['PLANO_CHECKLIST'][3]['campo']="LAYOUT";
$form['PLANO_CHECKLIST'][3]['tipo']="select_rela";
$form['PLANO_CHECKLIST'][3]['null']=0;
$form['PLANO_CHECKLIST'][3]['unico']=0;


$form['PLANO_CHECKLIST'][4]['campo']="FREQUENCIA";
$form['PLANO_CHECKLIST'][4]['tipo']="texto";
$form['PLANO_CHECKLIST'][4]['null']=0;
$form['PLANO_CHECKLIST'][4]['tam']=4;
$form['PLANO_CHECKLIST'][4]['max']=3;
$form['PLANO_CHECKLIST'][4]['unico']=0;

$form['PLANO_CHECKLIST'][4]['fecha_fieldset']=1;

// FORMULARIO CHECKLIST ATIVIDADE
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['nome']=PLANO_CHECKLIST_ATIVIDADES;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['titulo']=$tdb[PLANO_CHECKLIST_ATIVIDADES]['DESC'];
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['obs']="";
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['dir']=1;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['largura']=530;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['altura']=280;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['chave']="MID";

$form['PLANO_CHECKLIST_ATIVIDADES'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST_ATIVIDADES]['DESC'];

$form['PLANO_CHECKLIST_ATIVIDADES'][1]['campo']="MID_CHECKLIST";
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['tipo']="recb_valor";
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['valor']=$form_recb_valor;
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][2]['campo']="NUMERO";
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['tipo']="texto";
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['tam']=5;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['max']=5;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][3]['campo']="TAREFA";
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['tipo']="texto";
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['tam']=40;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['max']=150;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][3]['fecha_fieldset']=1;


// FORMULARIO CHECKLIST PROGRAMACAO
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['nome']=PLANO_CHECKLIST_PROGRAMACAO;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['titulo']=$tdb[PLANO_CHECKLIST_PROGRAMACAO]['DESC'];
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['obs']="";
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['dir']=1;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['largura']=530;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['altura']=280;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['chave']="MID";

$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST_PROGRAMACAO]['DESC'];

$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['campo']="MID_CHECKLIST";
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['tipo']="select_rela";
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['campo']="MID_MAQUINA";
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['tipo']="select_rela";
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['campo']="DATA_INICIAL";
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['tipo']="data";
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['tam']=30;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['max']=10;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['fecha_fieldset']=1;


// FORMULARIO ALMOXARIFADO
$form['ALMOXARIFADO'][0]['nome']=ALMOXARIFADO;
$form['ALMOXARIFADO'][0]['titulo']=$tdb[ALMOXARIFADO]['DESC'];
$form['ALMOXARIFADO'][0]['obs']="";
$form['ALMOXARIFADO'][0]['dir']=1;
$form['ALMOXARIFADO'][0]['largura']=530;
$form['ALMOXARIFADO'][0]['altura']=280;
$form['ALMOXARIFADO'][0]['chave']="MID";

$form['ALMOXARIFADO'][1]['abre_fieldset']=$tdb[ALMOXARIFADO]['DESC'];

$form['ALMOXARIFADO'][1]['campo']="MID_EMPRESA";
$form['ALMOXARIFADO'][1]['tipo']="select_rela";
$form['ALMOXARIFADO'][1]['null']=0;
$form['ALMOXARIFADO'][1]['unico']=0;

$form['ALMOXARIFADO'][2]['campo']="COD";
$form['ALMOXARIFADO'][2]['tipo']="texto";
$form['ALMOXARIFADO'][2]['null']=0;
$form['ALMOXARIFADO'][2]['tam']=20;
$form['ALMOXARIFADO'][2]['max']=50;
$form['ALMOXARIFADO'][2]['unico']=1;

$form['ALMOXARIFADO'][3]['campo']="DESCRICAO";
$form['ALMOXARIFADO'][3]['tipo']="texto";
$form['ALMOXARIFADO'][3]['null']=0;
$form['ALMOXARIFADO'][3]['tam']=40;
$form['ALMOXARIFADO'][3]['max']=200;

$form['ALMOXARIFADO'][3]['fecha_fieldset']=1;

// GRUPOS DE GR�FICO
$form['GRUPO_GRAFICO'][0]['nome']=GRUPO_GRAFICO;
$form['GRUPO_GRAFICO'][0]['titulo']=$tdb[GRUPO_GRAFICO]['DESC'];
$form['GRUPO_GRAFICO'][0]['obs']="";
$form['GRUPO_GRAFICO'][0]['largura']=500;
$form['GRUPO_GRAFICO'][0]['altura']=160;
$form['GRUPO_GRAFICO'][0]['chave']="MID";

$form['GRUPO_GRAFICO'][1]['abre_fieldset']=$tdb[GRUPO_GRAFICO]['DESC'];

$form['GRUPO_GRAFICO'][1]['campo']="DESCRICAO";
$form['GRUPO_GRAFICO'][1]['tipo']="texto";
$form['GRUPO_GRAFICO'][1]['null']=0;
$form['GRUPO_GRAFICO'][1]['tam']=50;
$form['GRUPO_GRAFICO'][1]['max']=250;
$form['GRUPO_GRAFICO'][1]['unico']=1;

$form['GRUPO_GRAFICO'][1]['fecha_fieldset']=1;


// SPEED CHECK
$iForm = 0;
$form['SPEED_CHECK'][$iForm]['nome']=SPEED_CHECK;
$form['SPEED_CHECK'][$iForm]['titulo']=$tdb[SPEED_CHECK]['DESC'];
$form['SPEED_CHECK'][$iForm]['obs']="";
$form['SPEED_CHECK'][$iForm]['dir']=1;
$form['SPEED_CHECK'][$iForm]['largura']=530;
$form['SPEED_CHECK'][$iForm]['altura']=350;
$form['SPEED_CHECK'][$iForm]['chave']="MID";

$iForm++;
$form['SPEED_CHECK'][$iForm]['abre_fieldset']=$tdb[SPEED_CHECK]['DESC'];

$form['SPEED_CHECK'][$iForm]['campo']="MID_EMPRESA";
$form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
$form['SPEED_CHECK'][$iForm]['null']=0;
$form['SPEED_CHECK'][$iForm]['unico']=0;
$form['SPEED_CHECK'][$iForm]['js']="this.form.submit();";

$iForm++;
$form['SPEED_CHECK'][$iForm]['campo']="MID_EQUIPE";
$form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
$form['SPEED_CHECK'][$iForm]['null']=0;
$form['SPEED_CHECK'][$iForm]['unico']=0;

$iForm++;
$form['SPEED_CHECK'][$iForm]['campo']="MID_TIPO_PLANO";
$form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
$form['SPEED_CHECK'][$iForm]['null']=0;
$form['SPEED_CHECK'][$iForm]['unico']=0;
$form['SPEED_CHECK'][$iForm]['sql']= "WHERE MID IN (2, 4)";
$form['SPEED_CHECK'][$iForm]['js']= "this.form.submit()";

$tipo = (int)$_POST['cc'][SPEED_CHECK][3];

/**
 * caso seja um novo cadastro, a fam�lia para o cadastro muda de acordo com a aplicac�o do plano
 * conforme abaixo 
 */
if($tipo == 2){
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo']="MID_FAMILIA";
    $form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
    $form['SPEED_CHECK'][$iForm]['null']=0;
    $form['SPEED_CHECK'][$iForm]['unico']=0;
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo'] ="MID_FAMILIA_COMPONENTE";
    $form['SPEED_CHECK'][$iForm]['tipo'] = "recb_valor";
    $form['SPEED_CHECK'][$iForm]['null'] = 0;
    $form['SPEED_CHECK'][$iForm]['valor'] = 0;
    
    
    
}

elseif($tipo == 4){
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo']="MID_FAMILIA_COMPONENTE";
    $form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
    $form['SPEED_CHECK'][$iForm]['null']=0;
    $form['SPEED_CHECK'][$iForm]['unico']=0;
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo'] ="MID_FAMILIA";
    $form['SPEED_CHECK'][$iForm]['tipo'] = "recb_valor";
    $form['SPEED_CHECK'][$iForm]['null'] = 0;
    $form['SPEED_CHECK'][$iForm]['valor'] = 0;
    
}
elseif(($act == 2 or $act == 3) and (VoltaValor(SPEED_CHECK, 'MID_TIPO_PLANO', 'MID', (int)$_GET['foq']) == 2)){
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo']="MID_FAMILIA";
    $form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
    $form['SPEED_CHECK'][$iForm]['null']=0;
    $form['SPEED_CHECK'][$iForm]['unico']=0;
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo'] ="MID_FAMILIA_COMPONENTE";
    $form['SPEED_CHECK'][$iForm]['tipo'] = "recb_valor";
    $form['SPEED_CHECK'][$iForm]['null'] = 0;
    $form['SPEED_CHECK'][$iForm]['valor'] = 0;
    
}

elseif(($act == 2 or $act == 3) and (VoltaValor(SPEED_CHECK, 'MID_TIPO_PLANO', 'MID', (int)$_GET['foq']) == 4 or $tipo == 4)){
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo']="MID_FAMILIA_COMPONENTE";
    $form['SPEED_CHECK'][$iForm]['tipo']="select_rela";
    $form['SPEED_CHECK'][$iForm]['null']=0;
    $form['SPEED_CHECK'][$iForm]['unico']=0;
    
    $iForm++;
    $form['SPEED_CHECK'][$iForm]['campo'] ="MID_FAMILIA";
    $form['SPEED_CHECK'][$iForm]['tipo'] = "recb_valor";
    $form['SPEED_CHECK'][$iForm]['null'] = 0;
    $form['SPEED_CHECK'][$iForm]['valor'] = 0;
}

$iForm++;
$form['SPEED_CHECK'][$iForm]['campo']="DESCRICAO";
$form['SPEED_CHECK'][$iForm]['tipo']="texto";
$form['SPEED_CHECK'][$iForm]['null']=0;
$form['SPEED_CHECK'][$iForm]['tam']=20;
$form['SPEED_CHECK'][$iForm]['max']=255;
$form['SPEED_CHECK'][$iForm]['unico']=1;

$iForm++;
$form['SPEED_CHECK'][$iForm]['campo']="OBSERVACAO";
$form['SPEED_CHECK'][$iForm]['tipo']="blog";
$form['SPEED_CHECK'][$iForm]['null']=1;
$form['SPEED_CHECK'][$iForm]['tam']=40;
$form['SPEED_CHECK'][$iForm]['max']=5;
$form['SPEED_CHECK'][$iForm]['unico']=0;

$form['SPEED_CHECK'][$iForm]['fecha_fieldset']=1;

// SPEED CHECK ATIVIDADES
$form['SPEED_CHECK_ATIVIDADE'][0]['nome']=SPEED_CHECK_ATIVIDADES;
$form['SPEED_CHECK_ATIVIDADE'][0]['titulo']=$tdb[SPEED_CHECK_ATIVIDADES]['DESC'];
$form['SPEED_CHECK_ATIVIDADE'][0]['obs']="";
$form['SPEED_CHECK_ATIVIDADE'][0]['dir']=1;
$form['SPEED_CHECK_ATIVIDADE'][0]['largura']=530;
$form['SPEED_CHECK_ATIVIDADE'][0]['altura']=280;
$form['SPEED_CHECK_ATIVIDADE'][0]['chave']="MID";

$form['SPEED_CHECK_ATIVIDADE'][1]['abre_fieldset']=$tdb[SPEED_CHECK_ATIVIDADES]['DESC'];

$form['SPEED_CHECK_ATIVIDADE'][1]['campo']="SPEED_CHECK_ID";
$form['SPEED_CHECK_ATIVIDADE'][1]['tipo']="recb_valor";
$form['SPEED_CHECK_ATIVIDADE'][1]['valor']=$form_recb_valor;
$form['SPEED_CHECK_ATIVIDADE'][1]['null']=0;
$form['SPEED_CHECK_ATIVIDADE'][1]['tam']=5;
$form['SPEED_CHECK_ATIVIDADE'][1]['max']=5;

$form['SPEED_CHECK_ATIVIDADE'][2]['campo']="NUMERO";
$form['SPEED_CHECK_ATIVIDADE'][2]['tipo']="texto";
$form['SPEED_CHECK_ATIVIDADE'][2]['null']=0;
$form['SPEED_CHECK_ATIVIDADE'][2]['tam']=5;
$form['SPEED_CHECK_ATIVIDADE'][2]['max']=5;
$form['SPEED_CHECK_ATIVIDADE'][2]['unico']=0;

$form['SPEED_CHECK_ATIVIDADE'][3]['campo']="DESCRICAO";
$form['SPEED_CHECK_ATIVIDADE'][3]['tipo']="texto";
$form['SPEED_CHECK_ATIVIDADE'][3]['null']=0;
$form['SPEED_CHECK_ATIVIDADE'][3]['tam']=40;
$form['SPEED_CHECK_ATIVIDADE'][3]['max']=255;
$form['SPEED_CHECK_ATIVIDADE'][3]['unico']=0;

$form['SPEED_CHECK_ATIVIDADE'][4]['campo']="ETAPA";
$form['SPEED_CHECK_ATIVIDADE'][4]['tipo']="select_rela";
$form['SPEED_CHECK_ATIVIDADE'][4]['null']=0;
$form['SPEED_CHECK_ATIVIDADE'][4]['js']='';
$form['SPEED_CHECK_ATIVIDADE'][4]['sql']='';


$form['SPEED_CHECK_ATIVIDADE'][4]['fecha_fieldset']=1;

//FORMULARIO ETAPAS DO PLANO
$form['ETAPAS_PLANO'][0]['nome']=ETAPAS_PLANO;
$form['ETAPAS_PLANO'][0]['titulo']= $tdb[ETAPAS_PLANO]['DESC'];
$form['ETAPAS_PLANO'][0]['obs']="";
$form['ETAPAS_PLANO'][0]['largura']=500;
$form['ETAPAS_PLANO'][0]['altura']=180;
$form['ETAPAS_PLANO'][0]['chave']="MID";

$form['ETAPAS_PLANO'][1]['abre_fieldset']= $tdb[ETAPAS_PLANO]['DESC'];

$form['ETAPAS_PLANO'][1]['campo']="DESCRICAO";
$form['ETAPAS_PLANO'][1]['tipo']="texto";
$form['ETAPAS_PLANO'][1]['null']=0;
$form['ETAPAS_PLANO'][1]['tam']=35;
$form['ETAPAS_PLANO'][1]['max']=100;
$form['ETAPAS_PLANO'][1]['unico']=0;

$form['ETAPAS_PLANO'][2]['campo']="ORDEM";
$form['ETAPAS_PLANO'][2]['tipo']="texto_int";
$form['ETAPAS_PLANO'][2]['null']=0;
$form['ETAPAS_PLANO'][2]['tam']=3;
$form['ETAPAS_PLANO'][2]['max']=11;
$form['ETAPAS_PLANO'][2]['unico']=1;

$form['ETAPAS_PLANO'][2]['fecha_fieldset']=1;
