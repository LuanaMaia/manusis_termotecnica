<?

/**
 * Gera os formularios dinamicamente
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package manusis
 * @subpackage engine
 */

/**
 * Gera dinamicamente um formulario
 *
 * @param array $d
 * @param int $oq
 * @param int $act
 */
function GeraForm($d, $oq, $act) {
	global $dba, $_autotag_maq, $_autotag_setor, $_autotag_conj, $_autotag_equip, $tdb, $tdb_datas, $id, $oq, $exe, $op, $ling, $f, $manusis, $form_recb_valor, $foq, $form, $ling_meses;
	$cc = $_POST['cc'];
	if (!$d) {
		erromsg("{$ling['form_formulario_n_encontrado']} $d");
		exit;
	}
	$form_parm = (int) $_GET['form_parm'];
	// a��es = $act
	// 1- Cadastrar
	// 2 - Editar
	if ($_POST['form_botao']) {
		$i = 1;
		while ($d[$i]['campo'] != "") {
			$cc[$d[0]['nome']][$i] = LimpaTexto($cc[$d[0]['nome']][$i]);
			switch ($d[$i]['tipo']) {
				// Campo Texto Normal
				case "texto_setor_autotag":
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i];
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i];
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i];
						}
					}
					break;

				case "select_setor_autotag" :

					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == 0))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "select_maq_autotag" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == "" OR $cc[$d[0]['nome']][$i] == 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "texto_maq_autotag":
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {

						$campoatualiza.=" , " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "' ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "texto_equip_autotag":
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {

						$campoatualiza.=" , " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "' ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "select_equip_autotag" :

					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == "" OR $cc[$d[0]['nome']][$i] == 0))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "texto_conj_autotag":
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "select_conjunto_autotag" :

					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "texto_famequip_autotag" :
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$cc[$d[0]['nome']][$i] = geracodigo(EQUIPAMENTOS_FAMILIA, strtoupper($cc[$d[0]['nome']][2]));
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "texto_fammaq_autotag" :
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$cc[$d[0]['nome']][$i] = geracodigo(MAQUINAS_FAMILIA, strtoupper($cc[$d[0]['nome']][2]));
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;

				case "mes" :
				case "texto" :
                                    
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
                                        break;

				case "texto_reais" :
					$cc[$d[0]['nome']][$i] = trim(str_replace('.', '', $cc[$d[0]['nome']][$i]));
				case "texto_float" :
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if ($cc[$d[0]['nome']][$i] != '') {
						$cc[$d[0]['nome']][$i] = trim(str_replace(',', '.', $cc[$d[0]['nome']][$i]));
					}
                    if(($d[$i]['null'] == 1) and ($cc[$d[0]['nome']][$i] == "")){
                        $cc[$d[0]['nome']][$i] = 0;
                    }
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (!is_numeric($cc[$d[0]['nome']][$i])) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>:" . $ling['campo_e_numerico'] . "</li>";
					}
					elseif (($d[$i]['nao_negativo'] == 1) and ($cc[$d[0]['nome']][$i] < 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_negativo'] . "</li>";
					}
					elseif (($d[$i]['nao_zero'] == 1) and ($cc[$d[0]['nome']][$i] == 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_zero'] . "</li>";
					}
					elseif (($d[$i]['menor_que'] > 0) and ($cc[$d[0]['nome']][$i] > $cc[$d[0]['nome']][$d[$i]['menor_que']])) {
						$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['maior_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['menor_que']]['campo']]}</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="" . $cc[$d[0]['nome']][$i] . ", ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						$tmp_valor = $cc[$d[0]['nome']][$i];

						if (($d[$i]['null'] == 1) and ($cc[$d[0]['nome']][$i] == "")) {
							$tmp_valor = "NULL";
						}

						if ($i > 1) {
							$campoatualiza .= ", ";
						}

						$campoatualiza .= $d[$i]['campo'] . " = " . $tmp_valor . "";
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "texto_int" :
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if ($cc[$d[0]['nome']][$i] != '') {
						$cc[$d[0]['nome']][$i] = (int) trim($cc[$d[0]['nome']][$i]);
					}

					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (!is_numeric($cc[$d[0]['nome']][$i])) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['campo_e_numerico'] . "</li>";
					}
					elseif (($d[$i]['nao_negativo'] == 1) and ($cc[$d[0]['nome']][$i] < 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_negativo'] . "</li>";
					}
					elseif (($d[$i]['nao_zero'] == 1) and ($cc[$d[0]['nome']][$i] == 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_zero'] . "</li>";
					}
					elseif (($d[$i]['menor_que'] > 0) and ($cc[$d[0]['nome']][$i] > $cc[$d[0]['nome']][$d[$i]['menor_que']])) {
						$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['maior_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['menor_que']]['campo']]}</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="" . $cc[$d[0]['nome']][$i] . ", ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						$tmp_valor = $cc[$d[0]['nome']][$i];

						if (($d[$i]['null'] == 1) and ($cc[$d[0]['nome']][$i] == "")) {
							$tmp_valor = "NULL";
						}

						if ($i > 1) {
							$campoatualiza .= ", ";
						}

						$campoatualiza .= $d[$i]['campo'] . " = " . $tmp_valor . "";
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "texto_autotag_fammat" :
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
					break;

				case "texto_autotag_subfammat" :
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
					break;

				case "texto_calcula_valor_total" :
					if ($manusis['caixaalta'] == 1)
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max'])
						alerta_seguranca("GeraForm", 1);
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
					break;
				// Recebe o valor das horas a serem comparadas horaIni e horaFim e verifica se a hora inicial � menor que a hora final
				case "compara_hora":

					$hIni = explode(":", $d[$i]['horaIni']);
					$hFim = explode(":", $d[$i]['horaFim']);

					$hIniComp = mktime($hIni[0], $hIni[1], $hIni[2]);
					$hFimComp = mktime($hFim[0], $hFim[1], $hFim[2]);

					$msg .= "<li> Ini Comp {$hIniComp}</li><li> Ini Comp {$hFimComp}</li>";

					break;

				case "hora" :
					$h = explode(':', $cc[$d[0]['nome']][$i]);

					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					// VALIDANDO A HORA
					elseif (($h[0] > 23) or ($h[1] > 59) or ($h[2] > 59)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['preencha_a_hora_corretamente'] . "</li>";
					}

					// Se este campo deve ser menor que outro
					if (($d[$i]['menor_que'] > 0) and ($cc[$d[0]['nome']][$i] != "") and ($cc[$d[0]['nome']][$d[$i]['menor_que']] != "")) {
						$horaFim = explode(":", $cc[$d[0]['nome']][$d[$i]['menor_que']]);
						$horaIni = explode(":", $cc[$d[0]['nome']][$i]);
						$ano = explode("/", date("d/m/Y"));
						$horaIniCompara = mktime((int) $horaIni[0], (int) $horaIni[1], (int) $horaIni[2], $ano[1], $ano[0], $ano[2]);
						$horaFimCompara = mktime((int) $horaFim[0], (int) $horaFim[1], (int) $horaFim[2], $ano[1], $ano[0], $ano[2]);

						if ($horaIniCompara > $horaFimCompara) {
							$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['maior_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['menor_que']]['campo']]}</li>";
						}
					}

					// Se este campo deve ser menor que outro
					if (($d[$i]['maior_que'] > 0) and ($cc[$d[0]['nome']][$i] != "") and ($cc[$d[0]['nome']][$d[$i]['maior_que']] != "")) {
						$horaFim = explode(":", $cc[$d[0]['nome']][$d[$i]['maior_que']]);
						$horaIni = explode(":", $cc[$d[0]['nome']][$i]);
						$ano = explode("/", date("d/m/Y"));
						$horaIniCompara = mktime((int) $horaIni[0], (int) $horaIni[1], (int) $horaIni[2], $ano[1], $ano[0], $ano[2]);
						$horaFimCompara = mktime((int) $horaFim[0], (int) $horaFim[1], (int) $horaFim[2], $ano[1], $ano[0], $ano[2]);

						if ($horaIniCompara > $horaFimCompara) {
							$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['menor_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['maior_que']]['campo']]}</li>";
						}
					}

					

					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1) and ($msg == "")) {
						$campoinsere .= $d[$i]['campo'] . ", ";
						
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$valorinsere .= "TO_DATE('" . $cc[$d[0]['nome']][$i] . "', 'HH24:MI:SS'), ";
						}
						else {
							$valorinsere .= "'" . $cc[$d[0]['nome']][$i] . "', ";
						}
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif (($act == 2) and ($msg == "")) {
						$campoatualiza .= ($i == 1)? "" : ", ";
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$campoatualiza .= $d[$i]['campo'] . " = TO_DATE('" . $cc[$d[0]['nome']][$i] . "', 'HH24:MI:SS')";
						}
						else {
							$campoatualiza .= $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
						}
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "senha" :
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max'])
						alerta_seguranca("GeraForm", 1);
					elseif ($cc[$d[0]['nome']][$i] != $_POST['senha_confirma'])
						$msg .="<li><strong>" . $ling['err08'] . "</strong></li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . md5($cc[$d[0]['nome']][$i]) . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1)
							$campoatualiza.=$d[$i]['campo'] . " = '" . md5($cc[$d[0]['nome']][$i]) . "'";
						else
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . md5($cc[$d[0]['nome']][$i]) . "'";
					}
					break;

				case "texto_unidade" :

					if ($cc[$d[0]['nome']][$i] != "") {
						$cc[$d[0]['nome']][$i] = trim(str_replace(',', '.', $cc[$d[0]['nome']][$i]));
					}

					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif ($cc[$d[0]['nome']][$i] and !is_numeric($cc[$d[0]['nome']][$i])) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: {$ling['form_campo_numerico']}</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($d[$i]['nao_negativo'] == 1) and ($cc[$d[0]['nome']][$i] < 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_negativo'] . "</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($d[$i]['nao_zero'] == 1) and ($cc[$d[0]['nome']][$i] == 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['valor_nao_zero'] . "</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($d[$i]['menor_que'] > 0) and ($cc[$d[0]['nome']][$i] > $cc[$d[0]['nome']][$d[$i]['menor_que']])) {
						$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['maior_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['menor_que']]['campo']]}</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere .= $d[$i]['campo'] . ", ";
						$valorinsere .= "" . $cc[$d[0]['nome']][$i] . ", ";

						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {

						$tmp_valor = $cc[$d[0]['nome']][$i];

						if (($d[$i]['null'] == 1) and ($cc[$d[0]['nome']][$i] == "")) {

                            //VERIFICANDO O SE O BANCO VAI RECEBER NULO OU ZERO
                            $infoFiled = GetDataType($d[0]['nome'], $d[$i]['campo']);
                            if($infoFiled['NAO_NULO'] == 1){
                                $tmp_valor = 0;
                            }
                            else{
                                $tmp_valor = "NULL";
                            }

						}
                        
						if ($i > 1) {
							$campoatualiza .= ", ";
						}

						$campoatualiza .= $d[$i]['campo'] . " = " . $tmp_valor . "";
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "texto_unidade_lub" :
					if ($manusis['caixaalta'] == 1)
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max'])
						alerta_seguranca("GeraForm", 1);
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
					break;

				// Telefone e Fax
				case "tel" :
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i]['tel'] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (strlen($cc[$d[0]['nome']][$i]['tel']) > $d[$i]['max'])
						alerta_seguranca("GeraForm", 1);
					elseif (strlen($cc[$d[0]['nome']][$i]['ddd']) > 2)
						alerta_seguranca("GeraForm", 1);
					elseif (($cc[$d[0]['nome']][$i]['tel'] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'(" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": (" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . " <br>";
					}
					elseif (($cc[$d[0]['nome']][$i]['tel'] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '(" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": (" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '(" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": (" . $cc[$d[0]['nome']][$i]['ddd'] . ") " . $cc[$d[0]['nome']][$i]['tel'] . " <br>";
						}
					}
					break;
				case "datafixa" :
					if ($manusis['caixaalta'] == 1) {
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					}
					
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max']) {
						alerta_seguranca("GeraForm", 1);
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere .= $d[$i]['campo'] . ", ";
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$valorinsere .= "TO_DATE('" . date("Y-m-d") . "', 'YYYY-MM-DD'), ";
						}
						else {
							$valorinsere .= "'" . date("Y-m-d") . "', ";
						}
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . date("Y-m-d") . " <br>";
					}
					elseif ($act == 2) {
						$campoatualiza .= ($i == 1)? "" : ", ";
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$campoatualiza .= $d[$i]['campo'] . " = TO_DATE('" . $cc[$d[0]['nome']][$i] . "', 'YYYY-MM-DD')";
						}
						else {
							$campoatualiza .= $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
						}
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				case "horafixa" :
					if ($manusis['caixaalta'] == 1)
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (strlen($cc[$d[0]['nome']][$i]) > $d[$i]['max'])
						alerta_seguranca("GeraForm", 1);
					if ($act == 1) {
						$campoinsere .= $d[$i]['campo'] . ", ";
						
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$valorinsere .= "TO_DATE('" . date("H:i:s") . "', 'HH24:MI:SS'), ";
						}
						else {
							$valorinsere .= "'" . date("H:i:s") . "', ";
						}
						
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . date("H:i:s") . " <br>";
					}
					elseif ($act == 2) {
						$campoatualiza .= ($i == 1)? "" : ", ";
						// Oracle
						if ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$campoatualiza .= $d[$i]['campo'] . " = TO_DATE('" . $cc[$d[0]['nome']][$i] . "', 'HH24:MI:SS')";
						}
						else {
							$campoatualiza .= $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
						}
							$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					break;

				// Data
				case "data" :
					$erro = 0;
					$tmp_data = explode("/", $cc[$d[0]['nome']][$i]);
					if (($cc[$d[0]['nome']][$i] != "") and (!checkdate((int) $tmp_data[1], (int) $tmp_data[0], (int) $tmp_data[2]))) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: {$ling['data_invalida']}</li>";
						$erro = 1;
					}
					// Verifica se o campo pode ser preenchido em branco, seguran�a e outros
					elseif (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == "")) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
						$erro = 1;
					}
					// Se este campo deve ser menor que outro
					elseif (strlen($tmp_data[0]) > 2) {
						alerta_seguranca("GeraForm", 1);
						$erro = 1;
					}


					if (($d[$i]['menor_que'] > 0) and ($cc[$d[0]['nome']][$i] != "") and ($cc[$d[0]['nome']][$d[$i]['menor_que']] != "")) {
						$dataFim = explode("/", $cc[$d[0]['nome']][$d[$i]['menor_que']]);
						$dataIni = explode("/", $cc[$d[0]['nome']][$i]);
						$dataIniCompara = mktime((int) $dataIni[1], (int) $dataIni[0], (int) $dataIni[2]);
						$dataFimCompara = mktime((int) $dataFim[1], (int) $dataFim[0], (int) $dataFim[2]);

						if ($dataIniCompara > $dataFimCompara) {
							$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['maior_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['menor_que']]['campo']]}</li>";
							$erro = 1;
						}
					}

					if (($d[$i]['maior_que'] > 0) and ($cc[$d[0]['nome']][$i] != "") and ($cc[$d[0]['nome']][$d[$i]['maior_que']] != "")) {
						$dataFim = explode("/", $cc[$d[0]['nome']][$d[$i]['maior_que']]);
						$dataIni = explode("/", $cc[$d[0]['nome']][$i]);
						$dataIniCompara = mktime((int) $dataIni[1], (int) $dataIni[0], (int) $dataIni[2]);
						$dataFimCompara = mktime((int) $dataFim[1], (int) $dataFim[0], (int) $dataFim[2]);

						if ($dataIniCompara < $dataFimCompara) {
							$msg .= "<li><strong>{$tdb[$d[0]['nome']][$d[$i]['campo']]}:</strong> {$ling['menor_que']} {$tdb[$d[0]['nome']][$d[$d[$i]['maior_que']]['campo']]}</li>";
							$erro = 1;
						}
					}

					if (($erro == 0) and ($act == 1)) {
						$campoinsere .= $d[$i]['campo'] . ", ";
						
						if ($cc[$d[0]['nome']][$i] == "") {
							$valorinsere .= "NULL, ";
						}// Oracle
						elseif ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') {
							$valorinsere .= "TO_DATE('" . $tmp_data[2] . "-" . $tmp_data[1] . "-" . $tmp_data[0] . "', 'YYYY-MM-DD'), ";
						}
						else {
							$valorinsere .= "'" . $tmp_data[2] . "-" . $tmp_data[1] . "-" . $tmp_data[0] . "', ";
						}
						$loga .= $tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $tmp_data[2] . "-" . $tmp_data[1] . "-" . $tmp_data[0] . " <br>";
					}
					elseif (($erro == 0) and ($act == 2)) {
						$campoatualiza .= ($i == 1)? "" : ", ";
						
						if ($cc[$d[0]['nome']][$i] == "") {
							$campoatualiza .= "{$d[$i]['campo']} = NULL";
						}
						// Oracle
						elseif ($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') { 
							$campoatualiza .= "{$d[$i]['campo']} = TO_DATE('" . $tmp_data[2] . "-" . $tmp_data[1] . "-" . $tmp_data[0] . "', 'YYYY-MM-DD')";
						}
						else {
							$campoatualiza .= "{$d[$i]['campo']} = '" . $tmp_data[2] . "-" . $tmp_data[1] . "-" . $tmp_data[0] . "'";
						}
						
					}
					break;

				// Campo para Blog/Observa��o
				case "blog" :
					if ($manusis['caixaalta'] == 1)
						$cc[$d[0]['nome']][$i] = strtoupper($cc[$d[0]['nome']][$i]);
					$cc[$d[0]['nome']][$i] = strip_tags($cc[$d[0]['nome']][$i]);
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<strong><li>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.=$d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
						}
					}
					break;

				// Campo para RECEBER VALOR EXTERNO... MID_EMPRESA MID_ALGUMACOISA
				case "recb_valor" :
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							if ($recb)
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
							else
								$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
						}
					}
					break;
				case "numos" :
					if ($act == 1)
						$cc[$d[0]['nome']][$i] = num_os();
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . " <br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_material" :
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == 0))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . (int) $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], (int) $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . (int) $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], (int) $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . (int) $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], (int) $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_modulo" :
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_permissao" :
					if (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.="" . $d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_maquina" :
					$cc[$d[0]['nome']][$i] = (int) $_POST['filtra_maq'];
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				// Select Simples de relacionamento
				case "select_rela" :
					if (($d[$i]['null'] == false) and (($cc[$d[0]['nome']][$i] == "") or ($cc[$d[0]['nome']][$i] == 0)))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_area_estoque" :
					if (($d[$i]['null'] == false) and (($cc[$d[0]['nome']][$i] == "") or ($cc[$d[0]['nome']][$i] == 0)))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_fam_material" :
					if (($d[$i]['null'] == false) and (($cc[$d[0]['nome']][$i] == "") or ($cc[$d[0]['nome']][$i] == 0)))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_autotag_fam_material" :
					if (($d[$i]['null'] == false) and (($cc[$d[0]['nome']][$i] == "") or ($cc[$d[0]['nome']][$i] == 0)))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_subfam_material" :
					if (($d[$i]['null'] == false) and (($cc[$d[0]['nome']][$i] == "") or ($cc[$d[0]['nome']][$i] == 0)))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_peri" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						if ($recb['campo'] != "")
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						else
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . $cc[$d[0]['nome']][$i] . "<br>";
					}
					elseif ($act == 2) {

						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_tipo_plano" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_ordem" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_contador" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif ($act == 2) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;


				case "select_parm" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_tipo_prog" :
					$cc[$d[0]['nome']][$i] = $_POST['filtra_prog'];
					if (($d[$i]['null'] == 0) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_plano_prog" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_conjunto_filtrado" :
					if (($d[$i]['null'] == 0) and ((int) $cc[$d[0]['nome']][$i] == 0)) {
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_conjunto_filtrado_sol" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_subconjunto" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_ponto_pred" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;

				case "select_ponto_lub" :
					if (($d[$i]['null'] == false) and ($cc[$d[0]['nome']][$i] == ""))
						$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form01'] . "</li>";
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 1)) {
						$campoinsere.=$d[$i]['campo'] . ", ";
						$valorinsere.="'" . $cc[$d[0]['nome']][$i] . "', ";
						$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
						$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
					}
					elseif (($cc[$d[0]['nome']][$i] != "") and ($act == 2)) {
						if ($i == 1) {
							$campoatualiza.="" . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
						else {
							$campoatualiza.=", " . $d[$i]['campo'] . " = '" . $cc[$d[0]['nome']][$i] . "'";
							$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
							$loga.=$tdb[$d[0]['nome']][$d[$i]['campo']] . ": " . VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $cc[$d[0]['nome']][$i], $tdb[$recb['tb']]['dba']) . "<br>";
						}
					}
					break;
			}
			// Caso campo seja unico
			if ($d[$i]['unico'] == 1) {
				// O unico passa a ser por empresa
				$filtro_emp = "";
				$campo_emp = VoltaCampoFiltroEmpresa($d[0]['nome']);

				if ($f == 'FUNCIONARIO2') { //tabela de funcionario tem 2 campos com rela��o por empresa, seta na m�o
					$campo_emp = 'MID_EMPRESA';
				}

				$pos_emp = VoltaPosForm($f, $campo_emp);

				if (($campo_emp != "") and ($pos_emp != 0)) {
					// Tabela
					$tab_emp = VoltaRelacao($d[0]['nome'], $campo_emp);
					$filtro_mid = VoltaFiltroEmpresaGeral($tab_emp['tb'], $cc[$d[0]['nome']][$pos_emp]);

					if (count($filtro_mid) > 0) {
						$filtro_emp .= $campo_emp . " IN (" . implode(', ', $filtro_mid) . ") AND ";
					}
				}

				if ($act == 2) {
					$filtro_emp .= "MID != " . $cc[$d[0]['nome']]['oq'] . " AND ";
				}

				$tmp = VoltaValor($d[0]['nome'], $d[$i]['campo'], $filtro_emp . $d[$i]['campo'], strtoupper($cc[$d[0]['nome']][$i]), $tdb[$d[0]['nome']]['dba']);
				if ($tmp != '') {
					$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</strong>: " . $ling['form02'] . "</li>";
				}
			}
			$i++;
		}

		// validar a quantidade de saida de material.
		if (($act == 1) and ($d[0]['nome'] == ESTOQUE_SAIDA)) {
			$estoq_mat = htmlentities(VoltaValor(MATERIAIS_ALMOXARIFADO, "ESTOQUE_ATUAL", "MID_MATERIAL", $cc[$d[0]['nome']][2]));
			$mat = $cc[$d[0]['nome']][3];

			if ($mat > $estoq_mat) {
				$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[3]['campo']] . "</strong>: " . $ling['form03'] . "</li>";
			}
			elseif (($msg == "") and ($act == 1) and ($d[0]['nome'] == ESTOQUE_SAIDA)) {
				AtualizaEstoqueAlmox(2, $cc[$d[0]['nome']][1], $cc[$d[0]['nome']][2], $cc[$d[0]['nome']][3], 0, $cc[$d[0]['nome']][8]);
			}
		}

		if (($msg == "") and ($act == 1) and ($d[0]['nome'] == ESTOQUE_ENTRADA)) {
			AtualizaEstoqueAlmox(1, $cc[$d[0]['nome']][1], $cc[$d[0]['nome']][2], $cc[$d[0]['nome']][3], $cc[$d[0]['nome']][4]);
		}


		if (($msg == "") and ($d[0]['nome'] == EMPRESAS)) {
			$emp_matriz = $cc[$d[0]['nome']][1];
			$matriz = (int) VoltaValor(EMPRESAS, "MID", "EMP_MATRIZ", 1);
			if (($matriz != 0) and ($act == 1) and ($emp_matriz == 1)) {
				$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[1]['campo']] . "</strong>: {$ling['form_empresa_matriz_cadastrada']}</li>";
			}
			elseif (($matriz != 0) and ($act == 2) and ($emp_matriz == 1) and ($matriz != $cc[$d[0]['nome']]['oq'])) {
				$msg .= "<li><strong>" . $tdb[$d[0]['nome']][$d[1]['campo']] . "</strong>: {$ling['form_empresa_matriz_cadastrada']}</li>";
			}
		}

		if (($msg == "") and ($act == 1)) {
			$log_extra = "";
			$tmp_mid = GeraMid($d[0]['nome'], $d[0]['chave'], $tdb[$d[0]['nome']]['dba']);

			// Define a Chave e o valor que ela tera
			$campoinsere.=$d[0]['chave'];
			$valorinsere.="'$tmp_mid'";

			// PARA CADASTRO DE COMPONENTES SALVAR MID_EMPRESA
			if ($d[0]['nome'] == EQUIPAMENTOS) {
				$n = VoltaPosForm($f, 'MID_MAQUINA');
				$emp_tmp = VoltaValor(MAQUINAS, 'MID_SETOR', 'MID', $cc[$d[0]['nome']][$n], $tdb[MAQUINAS]['dba']);
				$emp_tmp = VoltaValor(SETORES, 'MID_AREA', 'MID', $emp_tmp, $tdb[SETORES]['dba']);
				$emp_tmp = VoltaValor(AREAS, 'MID_EMPRESA', 'MID', $emp_tmp, $tdb[AREAS]['dba']);

				// ADICIONANDO AO SELECT
				$campoinsere .= ", MID_EMPRESA";
				$valorinsere .= ", '$emp_tmp'";
			}

			// PARA CADASTRO DE MAQUINAS SALVAR MID_EMPRESA
			if ($d[0]['nome'] == MAQUINAS) {
				$n = VoltaPosForm($f, 'MID_SETOR');
				$emp_tmp = VoltaValor(SETORES, 'MID_AREA', 'MID', $cc[$d[0]['nome']][$n], $tdb[SETORES]['dba']);
				$emp_tmp = VoltaValor(AREAS, 'MID_EMPRESA', 'MID', $emp_tmp, $tdb[AREAS]['dba']);

				// ADICIONANDO AO SELECT
				$campoinsere .= ", MID_EMPRESA";
				$valorinsere .= ", '$emp_tmp'";
			}

			// PARA CADASTRO DE SOLICITA��O GERAR NUMERO BASEADO NA EMPRESA E SALVAR A EMPRESA
			if ($d[0]['nome'] == SOLICITACOES) {
				// Buscando a empresa apartir da Maquina
				$n = VoltaPosForm($f, 'MID_MAQUINA');
				$emp_tmp = VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $cc[$d[0]['nome']][$n], $tdb[MAQUINAS]['dba']);

				// ADICIONANDO O MID_EMPRESA AO INSERT
				$campoinsere .= ", MID_EMPRESA";
				$valorinsere .= ", '$emp_tmp'";

				// ADICIONANDO O NUMERO
				$campoinsere .= ", NUMERO";
				$valorinsere .= ", '" . GeraNumEmp($emp_tmp, SOLICITACOES, 'NUMERO') . "'";
			}

			// PARA CADASTRO DE PENDENCIAS GERAR NUMERO BASEADO NA EMPRESA E SALVAR A EMPRESA
			if ($d[0]['nome'] == PENDENCIAS) {
				// Buscando a empresa apartir da Maquina
				$n = VoltaPosForm($f, 'MID_MAQUINA');
				$emp_tmp = VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $cc[$d[0]['nome']][$n], $tdb[MAQUINAS]['dba']);

				// ADICIONANDO O MID_EMPRESA AO INSERT
				$campoinsere .= ", MID_EMPRESA";
				$valorinsere .= ", '$emp_tmp'";

				// ADICIONANDO O NUMERO
				$campoinsere .= ", NUMERO";
				$valorinsere .= ", '" . GeraNumEmp($emp_tmp, PENDENCIAS, 'NUMERO') . "'";
			}

			// Verifica se deve atualizar programa��o ou n�o
			if ($d[0]['nome'] == ATIVIDADES) {
				// Peridiocidade por contador??
				$pos_peri = VoltaPosForm($f, 'PERIODICIDADE');

				// Tem algum PLANO, ou seja n�o � um plano novo??
				$pos_plano = VoltaPosForm($f, 'MID_PLANO_PADRAO');
				$prog = (int) VoltaValor(PROGRAMACAO, 'MID', array('STATUS != 3 AND MID_PLANO', 'TIPO'), array($cc[$d[0]['nome']][$pos_plano], 1));


				// Sen�o for por contador atualize
				if (($cc[$d[0]['nome']][$pos_peri] != 4) and ($prog != 0)) {
					$campoinsere .= ", STATUS";
					$valorinsere .= ", 2";
				}
			}

			// Atualizar programa��o sempre
			if ($d[0]['nome'] == LINK_ROTAS) {
				// Tem algum PLANO, ou seja n�o � um plano novo??
				$pos_plano = VoltaPosForm($f, 'MID_PLANO');
				$prog = (int) VoltaValor(PROGRAMACAO, 'MID', array('STATUS != 3 AND MID_PLANO', 'TIPO'), array($cc[$d[0]['nome']][$pos_plano], 2));

				if ($prog != 0) {
					$campoinsere .= ", STATUS";
					$valorinsere .= ", 2";
				}
			}

			$sql = "INSERT INTO " . $d[0]['nome'] . " ($campoinsere) VALUES ($valorinsere)";
			$tmp = $dba[$tdb[$d[0]['nome']]['dba']]->Execute($sql);
			if (!$tmp) {
				erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$d[0]['nome']]['dba']]->ErrorMsg() . "<br />" . $sql);
			}
			else {
				logar(3, $log_extra, $d[0]['nome'], $d[0]['chave'], $tmp_mid);

				$smsg = 1;
				if ($d[0]['nome'] == SOLICITACAO_RESPOSTA OR $d[0]['nome'] == SOLICITACOES) {

					if($d[0]['nome'] == SOLICITACAO_RESPOSTA) $sol = (int) VoltaValor(SOLICITACAO_RESPOSTA, 'MID_SOLICITACAO', 'MID', $tmp_mid, $tdb[SOLICITACAO_RESPOSTA]['dba']);
                    if($d[0]['nome'] == SOLICITACOES) $sol = $tmp_mid;
                    
                    if ($sol) {
						envia_email_sol($sol);
					}
				}
			}
		}
		elseif (($msg == "") and ($act == 2)) {
			$log_extra = "";

			// EDITANDO ATIVIDADE DE PREVENTIVA OU ROTAS
			if (($d[0]['nome'] == ATIVIDADES) or ($d[0]['nome'] == LINK_ROTAS)) {
				// Verifica se deve mudar o valor do campo status ou n�o

                $pos_peri = VoltaPosForm($f, 'PERIODICIDADE');

				if (MudaStatusAtv($cc[$d[0]['nome']]['oq'], $d[0]['nome'], $f, $cc[$d[0]['nome']])) {
					$campoatualiza .= ", STATUS = 2";
				}
				else {
					$log_extra .= "FORAM FEITAS ALTERA��ES AUTOMATICAS EM ORDENS DE SERVI�O ABERTAS QUE CONTENHAM ESSA ATIVIDADE.\n";
				}
			}


			$sql = "UPDATE " . $d[0]['nome'] . " SET $campoatualiza WHERE " . $d[0]['chave'] . " = '" . $cc[$d[0]['nome']]['oq'] . "'";
			$tmp = $dba[$tdb[$d[0]['nome']]['dba']]->Execute($sql);

			if (!$tmp) {
				erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$d[0]['nome']]['dba']]->ErrorMsg() . "<br />" . $sql);
			}
			else {
				$smsg = $ling['form_operacao_efetuada_sucesso'];
				logar(4, $log_extra, $d[0]['nome'], $d[0]['chave'], $cc[$d[0]['nome']]['oq']);
			}
		}
	}

	// Gerando Formul�rio no momento de entrar no formul�rio
	if ((($act == 2) or ($act == 3)) and (count($_POST) == 0)) {
		// Montando o SELECT
		$campos_sql = "{$d[0]['nome']}.*";
		
		if (($manusis['db'][$tdb[$d[0]['nome']]['dba']]['driver'] == 'oci8') and (is_array($tdb_datas[$d[0]['nome']]))) {
			foreach ($tdb_datas[$d[0]['nome']] as $tipo => $campos) {
				if ($tipo == 'T') {
					foreach ($campos as $campo) {
						$campos_sql .= ", TO_CHAR({$d[0]['nome']}.{$campo}, 'HH24:MI:SS') AS {$campo}";
					}
				}
				if ($tipo == 'DT') {
					foreach ($campos as $campo) {
						$campos_sql .= ", TO_CHAR({$d[0]['nome']}.{$campo}, 'YYYY-MM-DD HH24:MI:SS') AS {$campo}";
					}
				}
			}
		}
		
		$sql = "SELECT {$campos_sql} FROM {$d[0]['nome']} WHERE {$d[0]['chave']} = {$foq}";
		$edita = $dba[$tdb[$d[0]['nome']]['dba']]->Execute($sql);
		if (! $edita) {
			erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$d[0]['nome']]['dba']]->ErrorMsg() . "<br />" . $sql);
		}
		else {
			$edita = $edita->fields;
		}
	}
	// Depois que ja entrou, alguma a��o que rola no submit
	elseif (($act == 2) or ($act == 3)) {
		$i = 1;
		while ($d[$i]['campo'] != "") {
			$edita[$d[$i]['campo']] = $cc[$d[0]['nome']][$i];
			$i++;
		}
	}

	if (($act == 1) or ($act == 3))
		$acao = 1;
	elseif ($act == 2)
		$acao = 2;

	if ($_GET['atualiza'] == "")
		$atualiza_area = "corpo";
	else
		$atualiza_area = $_GET['atualiza'];

	// verifica se ainda pode cadastrar
	if (($act == 1) and ($f == 'MATRIX') and $manusis['empresas']) {
		$res = $dba[0]->Execute("SELECT COUNT(MID) AS NUM FROM " . EMPRESAS);
		$num_empresas = $res->fields('NUM');
		if ($num_empresas >= $manusis['empresas'])
			die("<script>alert('{$ling['limite_empresas']}'); parent.location.href=parent.location.href</script><span>{$ling['limite_empresas']}</span>");
	}

	echo "\n
    <form action=\"form.php?id=$id&exe=$exe&op=$op&act=$acao&f=$f&foq=$foq&oq=$oq&form_recb_valor=$form_recb_valor&atualiza=$atualiza_area\" name=\"Fo\" id=\"Fo\" method=\"post\">
<div>";
	// Instru��es de Cadastro
	if ($d[0]['obs'] != "")
		echo "<span>" . $d[0]['obs'] . "</span>";
	// Caso tenha alguma mensagem de erro
	if ($msg != "") {
		blocomsg($msg, 1);
	}
	elseif (($smsg != "") and ($act == 2) or ($act == 3)) {
		if ($_POST['form_parm'] == 1)
			echo "<script language=\"javascript\">opener.document.forms[0].submit(); window.close();</script>";
		else
			echo "<script language=\"javascript\">atualiza_area_frame2('" . $_GET['atualiza'] . "','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq'); </script>";
	}
	elseif (($smsg != "") and ($act == 1)) {
		if ($_POST['form_parm'] == 1)
			echo "<script language=\"javascript\">opener.document.forms[0].submit(); window.close();</script>";
		else
			echo "<script language=\"javascript\">atualiza_area_frame2('" . $_GET['atualiza'] . "','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq'); </script>";
		unset($cc);
		unset($_POST['cc']);
	}
	// Formulario
	$i = 1;
	while ($d[$i]['campo'] != "") {
		// Caso Exista um Fildset definido, abrimos ele aqui
		if ($d[$i]['abre_fieldset'] != "")
			echo "<fieldset><legend>" . $d[$i]['abre_fieldset'] . "</legend>";
		// Caso Exista um Div definido, abrimos ele aqui
		if ($d[$i]['abre_div'] != "")
			echo "<div id=\"" . $d[$i]['abre_div'] . "\" style=\"text-align: left\">";
		// Cria um label para o campo caso ele nao seja um campo hidden
		if (($d[$i]['tipo'] != "recb_valor") and ($d[$i]['tipo'] != "datafixa") and ($d[$i]['tipo'] != "horafixa") and ($d[$i]['tipo'] != "numos"))
			echo "<label for=\"cc[" . $d[0]['nome'] . "][$i]\">" . $tdb[$d[0]['nome']][$d[$i]['campo']] . " </label>\n";
		// Criamos campos dependendo da configura��o do mesmo.
		switch ($d[$i]['tipo']) {

			case "select_setor_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if ($act == 2) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];

					if ($_autotag_setor['status'] == 1) {
						$textblock = " \" disabled=\"disabled\"";
						echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
					}
				}
				else {
					$textblock = "";
				}

				if ($cc[$d[0]['nome']][$i]) {

					$qtd = volta_qtd_maquianas_areas($cc[$d[0]['nome']][$i]);
					if ($qtd > 0) {
						$frase = sprintf($ling['nao_possivel_editar_localizacao1'], $qtd);
						echo "" . $frase . "<br />\n";
						echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
						break;
					}
				}

				if ($_autotag_setor['status'] == 1) {
					FormSelectD("COD", "DESCRICAO", AREAS, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "atualiza_area2('cod_setor_autotag','parametros.php?id=setor_autotag&valor=' + this.options[this.selectedIndex].value) $textblock", $d[$i]['valor'], "A", "COD");
				}
				else {
					FormSelectD("COD", "DESCRICAO", AREAS, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "", $d[$i]['valor'], "A", "COD");
				}
				break;


			case "texto_setor_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					if (($act == 2) and ($_autotag_setor['status'] == 1)) {
						$textblock = "disabled=\"disabled\"";
						echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
					}
				}
				echo "<div id=\"cod_setor_autotag\"   style=\"text-align:left\"><input class=\"$tmp_class\" type=\"text\" $textblock id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" /></div>\n";

				break;

			case "select_maq_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_select";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_select_ob";
				}

				if ($act == 2) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];

					if ($_autotag_maq['status'] == 1) {
						$textblock = " \" disabled=\"disabled\"";
						echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
					}
				}
				else {
					$textblock = "";
				}
				if ($_autotag_maq['status'] == 1) {
					FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", '', $tmp_class, "atualiza_area2('cod_maq_autotag','parametros.php?id=maq_autotag&valor=' + this.options[this.selectedIndex].value) $textblock", $d[$i]['sql'], "A", "COD");
				}
				else {
					FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", '', $tmp_class, "", $d[$i]['sql'], "A", "COD");
				}

				break;

			case "texto_maq_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					if (($act == 2) and ($_autotag_maq['status'] == 1)) {
						$textblock = "disabled=\"disabled\"";
						echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
					}
				}
				echo "<div id=\"cod_maq_autotag\" style=\"text-align:left\"><input $textblock class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" /></div>\n";
				break;

			case "texto_equip_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<div id=\"cod_equip_autotag\" style=\"text-align:left\"><input $textblock class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" /></div>\n";
				break;

			case "select_equip_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_select";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_select_ob";
				}

				if ($act == 2) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}

				if (($act == 2) and ($_autotag_equip['status'] == 1)) {
					$texto = VoltaValor(EQUIPAMENTOS_FAMILIA, "DESCRICAO", "MID", $edita[$d[$i]['campo']], 0);

					echo "<input disabled=\"disabled\"  class=\"campo_text\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"30\" value=\"$texto\" />";
					echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"30\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
				}
				else {
					if ($_autotag_equip['status'] == 1) {
						FormSelectD('COD', 'DESCRICAO', EQUIPAMENTOS_FAMILIA, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", 'MID', '', $tmp_class, "atualiza_area2('cod_equip_autotag','parametros.php?id=equip_autotag&valor=' + this.options[this.selectedIndex].value)", $d[$i]['sql'], 'N');
					}
					else {
						FormSelectD('COD', 'DESCRICAO', EQUIPAMENTOS_FAMILIA, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", 'MID', '', $tmp_class, "", $d[$i]['sql'], 'N');
					}
				}


				break;

			case "texto_conj_autotag" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_select";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_select_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\"  value=\"" . $cc[$d[0]['nome']][$i] . "\" />";
				}
				if ($act == 1) {

					if ($_autotag_conj['status'] == 1) {
						$cod = VoltaValor($_autotag_conj['tabela'], "COD", "MID", $form_recb_valor, 0);
						$sql = "SELECT TAG FROM " . MAQUINAS_CONJUNTO . " WHERE TAG LIKE '$cod%' ORDER BY TAG DESC";
						$tmp = $dba[0]->SelectLimit($sql, 1, 0);
						$campo = $tmp->fields;
						if ($_autotag_conj['caracter'] == "") {
							$c = explode($cod, $campo['TAG']);
						}
						else {
							$c = explode($_autotag_conj['caracter'], $campo['TAG']);
						}

						$num = (int) $c[1];
						$num = $num + $_autotag_conj['incrementa'];
						$zerofill = $_autotag_conj['casasdecimais'] - strlen($num);
						$num = str_repeat("0", $zerofill) . "$num";
						$cod = $cod . "{$_autotag_conj['caracter']}" . $num;
						$cc[$d[0]['nome']][$i] = $cod;
					}
				}
				echo "<div id=\"cod_conj_autotag\" style=\"text-align:left\"><input $textblock class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" /></div>\n";
				break;


			case "select_conjunto_autotag" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				if ($d[$i]['valor'] != "")
					$filtra_maq = $d[$i]['valor'];
				elseif ($cc[$d[0]['nome']][$i] != "")
					$filtra_maq = $cc[$d[0]['nome']][$i];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" onchange=\"atualiza_area2('cod_conj_autotag','parametros.php?maq=$form_recb_valor&id=conj_autotag&valor=' + this.options[this.selectedIndex].value)\">
                \n<option value=\"\">" . $ling['select_opcao'] . "</option>";
				if ($filtra_maq != "") {
					if ($act == 2)
						$tmp = $dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->Execute("SELECT TAG,DESCRICAO,MID FROM " . MAQUINAS_CONJUNTO . " WHERE MID_MAQUINA = '$filtra_maq' AND MID_CONJUNTO = 0 AND MID != '$foq' ORDER BY TAG ASC");
					else
						$tmp=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->Execute("SELECT TAG,DESCRICAO,MID FROM " . MAQUINAS_CONJUNTO . " WHERE MID_MAQUINA = '$filtra_maq' AND MID_CONJUNTO = 0 OR MID_CONJUNTO IS NULL ORDER BY TAG ASC");
					if (!$tmp)
						erromsg($dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->ErrorMsg());
					$ifm = 0;
					while (!$tmp->EOF) {
						$campo = $tmp->fields;
						if ($cc[$d[0]['nome']][$i] == $campo['MID']
							)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['TAG'] . "-" . $campo['DESCRICAO'] . "</option>\n";
						else
							echo "<option value=\"" . $campo['MID'] . "\">" . $campo['TAG'] . "-" . $campo['DESCRICAO'] . "</option>\n";
						$tmp->MoveNext();
						$ifm++;
					}
				}
				echo "</select>";
				break;

			case "texto_famequip_autotag" :
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
				break;


			case "texto_fammaq_autotag" :
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
				break;

			case "select_area_estoque" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				elseif (($act == 1) and ($d[$i]['valor'] != "") and ($cc[$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
				if ($recb['cod'] != "") {
					$campo1 = $recb['cod'];
					$campo2 = $recb['campo'];
				}
				else {
					$campo1 = $recb['campo'];
					$campo2 = $recb['cod'];
				}

				if (strtoupper($_SESSION[ManuSess]['user']['MID']) != 'ROOT') {
					$perm_sql = "";
					$sql = "SELECT MID_AREA FROM " . USUARIOS_PERMISAO_SOLICITACAO . " WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}'";
					$tmp = $dba[0]->Execute($sql);
					while (!$tmp->EOF) {
						$campo = $tmp->fields;
						AddStr($perm_sql, ' OR ', "MID = '{$campo['MID_AREA']}'");
						$tmp->MoveNext();
					}
					if ($perm_sql)
						$perm_sql = "($perm_sql)";
					else
						$perm_sql = "MID = '-1'"; // forma 0 resultados

				}
				else
					$perm_sql = "1"; // forma 0 resultados

					if ($recb['cadastra'] == 1)
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, "", "WHERE $perm_sql", "A", $recb['campo']);
				else
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, "", "WHERE $perm_sql", "A", $recb['campo']);
				break;

			case "select_subfam_material" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";

				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				elseif (($act == 1) and ($d[$i]['valor'] != "") and ($cc[$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];

				$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);

				if ($recb['cod'] != "") {
					$campo1 = $recb['cod'];
					$campo2 = $recb['campo'];
				}
				else {
					$campo1 = $recb['campo'];
					$campo2 = $recb['cod'];
				}


				$acao = "";
				echo "<span id=\"select_subfam_material\" />";

				if ($recb['cadastra'] == 1)
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);
				else
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);

				echo "</span>";
				break;

			case "select_fam_material" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				elseif (($act == 1) and ($d[$i]['valor'] != "") and ($cc[$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
				if ($recb['cod'] != "") {
					$campo1 = $recb['cod'];
					$campo2 = $recb['campo'];
				}
				else {
					$campo1 = $recb['campo'];
					$campo2 = $recb['cod'];
				}

				$acao = "atualiza_area2('select_subfam_material','parametros.php?id=select_subfam_material&nome=cc[" . $d[0]['nome'] . "][$i]&fam=' + this.options[this.selectedIndex].value)";
				if ($recb['cadastra'] == 1)
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);
				else
					FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);
				break;

			case "select_autotag_fam_material" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				elseif (($act == 1) and ($d[$i]['valor'] != "") and ($cc[$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
				if ($recb['cod'] != "") {
					$campo1 = $recb['cod'];
					$campo2 = $recb['campo'];
				}
				else {
					$campo1 = $recb['campo'];
					$campo2 = $recb['cod'];
				}

				$acao = "atualiza_area2('texto_autotag_subfammat','parametros.php?id=texto_autotag_subfammat&fam=' + this.options[this.selectedIndex].value)";
				if ($act == 2) {
					$old_mid = $cc[$d[0]['nome']][$i];
					$old_cod = VoltaValor($recb['tb'], $campo1, $recb['mid'], $old_mid, 0);
					$old_desc = VoltaValor($recb['tb'], $campo2, $recb['mid'], $old_mid, 0);
					if ($old_desc)
						AddStr($old_cod, '-', $old_desc);
					echo "<input disabled=\"disabled\" class=\"campo_text_ob\" type=\"text\" id=\"\" name=\"\" size=\"25\" maxlength=\"25\" value=\"$old_cod\" />";
					echo "<input type=\"hidden\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"25\" maxlength=\"25\" value=\"$old_cod\" />";
				}
				else {
					if ($recb['cadastra'] == 1)
						FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);
					else
						FormSelectD($campo1, $campo2, $recb['tb'], $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $acao, "", "A", $recb['campo']);
					//  else FormSelectD("cc[".$d[0]['nome']."][$i]",$recb['tb'],$cc[$d[0]['nome']][$i],$recb['campo'],$recb['mid'],$recb['dba'],0,"",$tmp_class);
				}
				break;

			// Campo Texto Normal
			case "texto_reais" :
				if (($act == 2) or ($act == 3)) {
					$edita[$d[$i]['campo']] = number_format($edita[$d[$i]['campo']], 2, ',', '.');
				}
			case "texto_float" :
			case "texto_int" :
			case "texto" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}

				$d[$i]['js'] = "";

				if ($d[$i]['onfocus'] != "") {
					$d[$i]['js'] .= " onfocus=\"" . $d[$i]['onfocus'] . "\"";
				}

				if ($d[$i]['onblur'] != "") {
					$d[$i]['js'] .= " onblur=\"" . $d[$i]['onblur'] . "\"";
				}

				echo "<input class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\"" . $d[$i]['js'] . " />\n";



				break;

			case "texto_travado" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<input disabled=\"disabled\" class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
				break;

			case "texto_calcula_valor_total" :
                            if ($d[$i]['null'] == 1)
                               $tmp_class = "campo_text";
                            elseif ($d[$i]['null'] == 0)
                               $tmp_class = "campo_text_ob";
                            if (($act == 2) or ($act == 3))
                               $cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
                            echo "<input onfocus=\"calcula_total(this)\" class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
		break;

			case "texto_autotag_fammat" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				if ($act == 2) {
					$old_cod = $cc[$d[0]['nome']][$i];
					echo "<input disabled=\"disabled\" class=\"campo_text_ob\" type=\"text\" id=\"\" name=\"\" size=\"25\" maxlength=\"25\" value=\"$old_cod\" />";
					echo "<input type=\"hidden\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"25\" maxlength=\"25\" value=\"$old_cod\" />";
				}
				else {
					echo "<span id=\"texto_autotag_fammat\">
                    <input class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />
                    </span>\n";
				}
				break;

			case "texto_autotag_subfammat" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<span id=\"texto_autotag_subfammat\">
                <input disabled=\"disabled\" class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />
                </span>\n";
				break;

			// Campo Senha
			case "senha" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_text";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_text_ob";
				echo "<input class=\"$tmp_class\" type=\"password\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" />\n<br clear=\"all\">";
				echo "<label for=\"senha_confirma\">" . $ling['confirmar_senha'] . "</label> <input class=\"$tmp_class\" type=\"password\" id=\"senha_confirma\" name=\"senha_confirma\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" />\n";
				break;

			// Campo texto para Adicionar a Unidade no Campo de Quantidade
			case "texto_unidade" :
                $tmp_unidade='';
				if ($d[$i]['null'] == 0)
					$tmp_class = "campo_text_ob";
				elseif ($d[$i]['null'] == 1)
					$tmp_class = "campo_text";
				if (($act == 2) or ($act == 3)){
					$cc[$d[0]['nome']][$i] = ($edita[$d[$i]['campo']] == 0 ? "" : $edita[$d[$i]['campo']]);
                    $tmp_unidade = VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', VoltaValor(MATERIAIS, 'UNIDADE', 'MID', $cc[$d[0]['nome']][$d[$i]['campo_material']]));
                }
				$d[$i]['js'] = "";

				if ($d[$i]['onfocus'] != "") {
					$d[$i]['js'] .= " onfocus=\"" . $d[$i]['onfocus'] . "\"";
				}

				if ($d[$i]['onblur'] != "") {
					$d[$i]['js'] .= " onblur=\"" . $d[$i]['onblur'] . "\"";
				}

				echo "<input class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i] . "\"" . $d[$i]['js'] . " /> <span id=\"c_unidade\"><i>$tmp_unidade</i></span>\n";
				break;

			case "select_modulo" :
				if ($d[$i]['null'] == 0)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 1)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">";
				for ($iff = 0; $manusis['modulo'][$iff]['nome'] != ""; $iff++) {
					if ($iff == $cc[$d[0]['nome']][$iff])
						echo "<option selected=\"selected\" value=\"$iff\">" . $manusis['modulo'][$iff]['nome'] . "</option>";
					else
						echo "<option value=\"$iff\">" . $manusis['modulo'][$iff]['nome'] . "</option>";
				}
				echo "</select>";
				break;

			case "select_permissao" :

				if ($d[$i]['null'] == 0)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 1)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">
            <option value=\"1\">" . $ling['permissao1'] . "</option>
            <option value=\"2\">" . $ling['permissao2'] . "</option>
            <option value=\"3\">" . $ling['permissao3'] . "</option>
            </select>";
				break;

			// Campo Data
			case "data" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_text";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_text_ob";
				}
				if (($act == 2) or ($act == 3) and ($_POST['cc'][$d[0]['nome']][$i] == "") and ($edita[$d[$i]['campo']] != "")) {
					$tmp_data = explode("-", $edita[$d[$i]['campo']]);
					if (($tmp_data[0] != "0000") and ($tmp_data[1] != "00") and ($tmp_data[1] != "") and ($tmp_data[0] != ""))
						$cc[$d[0]['nome']][$i] = $tmp_data[2] . "/" . $tmp_data[1] . "/" . $tmp_data[0];
				}
				elseif (($d[$i]['valor'] == 1) and ($_POST['cc'][$d[0]['nome']][$i] == "")) {
					$cc[$d[0]['nome']][$i] = date('d/m/Y');
				}
				else {
					$cc[$d[0]['nome']][$i] = $_POST['cc'][$d[0]['nome']][$i];
				}

				FormData('', "cc[" . $d[0]['nome'] . "][$i]", $cc[$d[0]['nome']][$i], '', '', $tmp_class, "cc_" . $d[0]['nome'] . "_$i");
				break;

			case "hora" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_text";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_text_ob";
				if (($act == 2) or ($act == 3) and ($_POST['cc'][$d[0]['nome']][$i] == "") and ($edita[$d[$i]['campo']] != "")) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				elseif (($d[$i]['valor'] == 1) and ($_POST['cc'][$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = date('h:i:s');
				else
					$cc[$d[0]['nome']][$i] = $_POST['cc'][$d[0]['nome']][$i];
				echo "<input onkeypress=\"return ajustar_hora(this, event)\" class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" size=\"8\" maxlength=\"8\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";

				break;

			// Timestamp Fixo
			case "datafixa" :
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = date("Y-m-d");
				echo "<input type=\"hidden\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />  \n";
				break;

			case "mes" :
				if ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				elseif ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">
            <option value=\"\"></option>
            <option value=\"1\" " . (($cc[$d[0]['nome']][$i] == 1) ? "selected=\"selected\"" : '') . ">" . $ling_meses[1] . "</option>
            <option value=\"2\" " . (($cc[$d[0]['nome']][$i] == 2) ? "selected=\"selected\"" : '') . ">" . $ling_meses[2] . "</option>
            <option value=\"3\" " . (($cc[$d[0]['nome']][$i] == 3) ? "selected=\"selected\"" : '') . ">" . $ling_meses[3] . "</option>
            <option value=\"4\" " . (($cc[$d[0]['nome']][$i] == 4) ? "selected=\"selected\"" : '') . ">" . $ling_meses[4] . "</option>
            <option value=\"5\" " . (($cc[$d[0]['nome']][$i] == 5) ? "selected=\"selected\"" : '') . ">" . $ling_meses[5] . "</option>
            <option value=\"6\" " . (($cc[$d[0]['nome']][$i] == 6) ? "selected=\"selected\"" : '') . ">" . $ling_meses[6] . "</option>
            <option value=\"7\" " . (($cc[$d[0]['nome']][$i] == 7) ? "selected=\"selected\"" : '') . ">" . $ling_meses[7] . "</option>
            <option value=\"8\" " . (($cc[$d[0]['nome']][$i] == 8) ? "selected=\"selected\"" : '') . ">" . $ling_meses[8] . "</option>
            <option value=\"9\" " . (($cc[$d[0]['nome']][$i] == 9) ? "selected=\"selected\"" : '') . ">" . $ling_meses[9] . "</option>
            <option value=\"10\" " . (($cc[$d[0]['nome']][$i] == 10) ? "selected=\"selected\"" : '') . ">" . $ling_meses[10] . "</option>
            <option value=\"11\" " . (($cc[$d[0]['nome']][$i] == 11) ? "selected=\"selected\"" : '') . ">" . $ling_meses[11] . "</option>
            <option value=\"12\" " . (($cc[$d[0]['nome']][$i] == 12) ? "selected=\"selected\"" : '') . ">" . $ling_meses[12] . "</option>
            </select>";
				break;

			// Telefone e Fax
			case "tel" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_text";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_text_ob";
				if ((($act == 2) or ($act == 3)) and ($edita[$d[$i]['campo']] != "")) {
                    if(!is_array($edita[$d[$i]['campo']])){
                        $ddd = explode(") ", $edita[$d[$i]['campo']]);
                        $cc[$d[0]['nome']][$i]['tel'] = $ddd[1];
                        $cc[$d[0]['nome']][$i]['ddd'] = str_replace("(", "", $ddd[0]);
                    }
                    else{

                        $ddd = $edita[$d[$i]['campo']];
                        $cc[$d[0]['nome']][$i]['tel'] = $ddd['tel'];
                        $cc[$d[0]['nome']][$i]['ddd'] = $ddd['ddd'];

                    }
				}
//                if ($cc[$d[0]['nome']][$i]['ddd'] == 0) $cc[$d[0]['nome']][$i]['ddd']="";
				echo "<input class=\"$tmp_class\" type=\"text\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i][ddd]\" size=\"2\" maxlength=\"2\" value=\"" . $cc[$d[0]['nome']][$i]['ddd'] . "\" />
                <input type=\"text\" class=\"$tmp_class\" name=\"cc[" . $d[0]['nome'] . "][$i][tel]\" size=\"" . $d[$i]['tam'] . "\" maxlength=\"" . $d[$i]['max'] . "\" value=\"" . $cc[$d[0]['nome']][$i]['tel'] . "\" />\n";
				break;

			case "horafixa" :
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = date("H:i:s");
				echo "<input type=\"hidden\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"1\" />  \n";
				break;
			// Telefone e Fax

			case "blog" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_text";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_text_ob";
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				echo "<textarea class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" rows=\"" . $d[$i]['max'] . "\" cols=\"" . $d[$i]['tam'] . "\" " . ($d[$i]['style'] ? "style=\"{$d[$i]['style']}\"" : '') . ">" . $cc[$d[0]['nome']][$i] . "</textarea>\n";
				break;

			case "recb_valor" :
				// Campo para RECEBER VALOR EXTERNO... MID_EMPRESA MID_ALGUMACOISA
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];

				if ($d[0]['nome'] == (REQUISICAO or $d[0]['nome'] == REQUISICAO_RESPOSTA) and $cc[$d[0]['nome']][$i] == "ROOT") {
					$cc[$d[0]['nome']][$i] = 0;
				}

				if ($d[0]['nome'] == ATIVIDADES) {
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];
				}

				echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
				break;

			case "numos" :	  // Campo para RECEBER VALOR EXTERNO... MID_EMPRESA MID_ALGUMACOISA
				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				else
					$cc[$d[0]['nome']][$i] = 0;

				echo "<input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"" . $cc[$d[0]['nome']][$i] . "\" />\n";
				break;

			case "select_rela" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";

				if (!$d[$i]['autofiltro'])
					$d[$i]['autofiltro'] = 'A';

				if (($act == 2) or ($act == 3))
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				elseif (($act == 1) and ($d[$i]['valor'] != "") and ($cc[$d[0]['nome']][$i] == ""))
					$cc[$d[0]['nome']][$i] = $d[$i]['valor'];

				$recb = VoltaRelacao($d[0]['nome'], $d[$i]['campo']);
				if ($recb['cod'] != "") {
					$campo1 = $recb['cod'];
					$campo2 = $recb['campo'];
				}
				else {
					$campo1 = $recb['campo'];
					$campo2 = $recb['cod'];
				}
				//mark
				// MOSTRANDO APENAS SUBFAMILIAS DA FAMILIA CORRETA
				if (($d[0]['nome'] == MATERIAIS) and
						($d[$i]['campo'] == "MID_SUBFAMILIA") and
						($cc[$d[0]['nome']][VoltaPosForm($f, 'FAMILIA')] != 0)) {
					$fam = $cc[$d[0]['nome']][VoltaPosForm($f, 'FAMILIA')];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "MID_FAMILIA=$fam";
				}

				// MOSTRANDO APENAS AS MAQUINAS NA EMPRESA DO PLANO
				if (($d[0]['nome'] == PLANO_PADRAO) and ($recb['tb'] == MAQUINAS) and ($cc[$d[0]['nome']][1] != 0)) {
					$emp = $cc[$d[0]['nome']][1];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "MID_SETOR IN (SELECT S.MID FROM " . SETORES . " S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp)";
				}

				// MOSTRANDO APENAS AS MAQUINAS DO CENTRO DE CUSTO DO USUARIO
				if (($d[0]['nome'] == SOLICITACOES) and ($recb['tb'] == MAQUINAS) and ($_SESSION[ManuSess]['user']['MID'] != 'ROOT')) {
					if (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'CENTRO_DE_CUSTO', 'USUARIO', $_SESSION[ManuSess]['user']['MID'], 0)) {
						$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
						$d[$i]['sql'] .= "CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM " . USUARIOS_PERMISAO_CENTRODECUSTO . " WHERE USUARIO = {$_SESSION[ManuSess]['user']['MID']})";
					}
				}

				// MOSTRANDO APENAS AS FAMILIA DE OBJ NA EMPRESA DO PLANO OU SEM FAMILIA
				if (($d[0]['nome'] == PLANO_PADRAO) and ($recb['tb'] == MAQUINAS_FAMILIA) and ($cc[$d[0]['nome']][1] != 0)) {
					$emp = $cc[$d[0]['nome']][1];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "(MID_EMPRESA = $emp OR MID_EMPRESA = 0)";
				}
				
				// MOSTRANDO APENAS AS EQUIPES DA EMPRESA
				if (($d[0]['nome'] == SPEED_CHECK) and ($recb['tb'] == EQUIPES) and ($cc[$d[0]['nome']][1] != 0)) {
					$emp = (int)$cc[$d[0]['nome']][1];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "(MID_EMPRESA = $emp OR MID_EMPRESA = 0)";
				}

				// MOSTRANDO APENAS AS FAMILIA DE OBJ NA EMPRESA DO PLANO OU SEM FAMILIA
				if (($d[0]['nome'] == SPEED_CHECK) and ($recb['tb'] == MAQUINAS_FAMILIA) and ($cc[$d[0]['nome']][1] != 0)) {
					$emp = (int)$cc[$d[0]['nome']][1];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "(MID_EMPRESA = $emp OR MID_EMPRESA = 0)";
				}

				// MOSTRANDO APENAS AS FAMILIA DE COMPONENTE NA EMPRESA DO PLANO OU SEM FAMILIA
				if (($d[0]['nome'] == PLANO_PADRAO) and ($recb['tb'] == EQUIPAMENTOS_FAMILIA) and ($cc[$d[0]['nome']][1] != 0)) {
					$emp = $cc[$d[0]['nome']][1];
					$d[$i]['sql'] .= ( $d[$i]['sql'] == '') ? "WHERE " : " AND ";
					$d[$i]['sql'] .= "(MID_EMPRESA = $emp OR MID_EMPRESA = 0)";
				}

				if ($recb['cadastra'] == 1) {
					FormSelectD($campo1, $campo2, $recb['tb'], (int)$cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $d[$i]['js'], $d[$i]['sql'], $d[$i]['autofiltro'], $recb['campo']);
				}
				else {
					FormSelectD($campo1, $campo2, $recb['tb'], (int)$cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", $recb['mid'], $recb['form'], $tmp_class, $d[$i]['js'], $d[$i]['sql'], $d[$i]['autofiltro'], $recb['campo']);
				}
				break;

			case "select_conjunto_filtrado" :
				if ($d[$i]['null'] == 1) {
					$tmp_class = "campo_select";
				}
				elseif ($d[$i]['null'] == 0) {
					$tmp_class = "campo_select_ob";
				}
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					if ($filtra_maq == "")
						$filtra_maq = VoltaValor(MAQUINAS_CONJUNTO, "MID_MAQUINA", "MID", $cc[$d[0]['nome']][$i], 0);
				}
				if ($d[$i]['valor'] != "") {
					$filtra_maq = $d[$i]['valor'];
				}
				if ($filtra_maq != "") {
					FormSelectD("TAG", "DESCRICAO", MAQUINAS_CONJUNTO, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "", "WHERE MID_MAQUINA = '$filtra_maq'", "A", "TAG");
				}
				else {
					FormSelectD("TAG", "DESCRICAO", MAQUINAS_CONJUNTO, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "", "", "A", "TAG");
				}

				break;


			case "select_ordem" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">\n<option value=\"\"> </option>";
				$tmp = $dba[$tdb[ORDEM_PLANEJADO]['dba']]->Execute("SELECT NUMERO,STATUS,MID FROM " . ORDEM_PLANEJADO . " WHERE STATUS = 1 ORDER BY NUMERO ASC");
				if (!$tmp)
					erromsg($dba[$tdb[ORDEM_PLANEJADO]['dba']]->ErrorMsg());
				$ifm = 0;
				while (!$tmp->EOF) {
					$campo = $tmp->fields;
					if ($cc[$d[0]['nome']][$i] == $campo['MID']
						)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['NUMERO'] . "</option>\n";
					else
						echo "<option value=\"" . $campo['MID'] . "\">" . $campo['NUMERO'] . "</option>\n";
					$tmp->MoveNext();
					$ifm++;
				}
				echo "</select>";
				break;

			case "select_material" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";

				if ((($act == 2) or ($act == 3)) and ($cc[$d[0]['nome']][$i] == "")) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				$formulario_mat = $d[0]['nome'];
				if ($d[0]['nome'] == ESTOQUE_SAIDA) {
					$d[$i]['valor'] = ($d[$i]['valor'] == "") ? "WHERE " : $d[$i]['valor'] . " AND ";
					$d[$i]['valor'] .= "MID IN (SELECT MID_MATERIAL FROM " . MATERIAIS_ALMOXARIFADO . " WHERE MID_ALMOXARIFADO = '{$_POST['cc'][$d[0]['nome']][1]}')";
				}
				if (($d[0]['nome'] == MAQUINAS_CONJUNTO_MATERIAL) OR ($d[0]['nome'] == EQUIPAMENTOS_MATERIAL)) {
					// Buscando os almoxarifados da mesma empresa da maquina
					if ($d[0]['nome'] == EQUIPAMENTOS_MATERIAL) {
						$emp = VoltaValor(EQUIPAMENTOS, 'MID_EMPRESA', 'MID', $form_recb_valor);
					}
					else {
						$emp = VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $form_recb_valor);
					}

					$almox = VoltaValor2(ALMOXARIFADO, 'MID', array('MID_EMPRESA', $emp));

					$d[$i]['valor'] = ($d[$i]['valor'] == "") ? "WHERE " : $d[$i]['valor'] . " AND ";
					if (count($almox) > 0) {

						$mids = is_array($almox['MID']) ? implode(", ", $almox['MID']) : $almox['MID'];

						$d[$i]['valor'] .= "MID IN (SELECT MID_MATERIAL FROM " . MATERIAIS_ALMOXARIFADO . " WHERE MID_ALMOXARIFADO IN (" . $mids . "))";
					}
					else {
						$d[$i]['valor'] .= "MID=0";
					}
				}

				FormSelectD("COD", "DESCRICAO", MATERIAIS, $cc[$d[0]['nome']][$i], "cc[" . $d[0]['nome'] . "][$i]", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "atualiza_area2('c_unidade','parametros.php?id=c_unidade&formulario=$formulario_mat&valor=' + this.options[this.selectedIndex].value)", $d[$i]['valor'], "A", "COD");
				break;


			case "select_maquina" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if ($_POST['filtra_maq'] != "")
					$filtra_maq = (int) $_POST['filtra_maq'];
				elseif (($act == 2) or ($act == 3) and ($_POST['filtra_maq'] == "")) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					$filtra_maq = $cc[$d[0]['nome']][$i];
				}

				$where = "";
				// MOSTRANDO APENAS AS MAQUINAS NA EMPRESA DO PLANO
				if ($d[0]['nome'] == LINK_ROTAS) {
					$emp = VoltaValor(PLANO_ROTAS, 'MID_EMPRESA', 'MID', $form_recb_valor);
					$where = "WHERE MID_SETOR IN (SELECT S.MID FROM " . SETORES . " S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp)";
				}

				if ($recb['cadastra'] == 1)
					FormSelectD("COD", "DESCRICAO", MAQUINAS, $filtra_maq, "filtra_maq", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "document.forms[0].submit()", $where, "A", "COD");
				else
					FormSelectD("COD", "DESCRICAO", MAQUINAS, $filtra_maq, "filtra_maq", "cc[" . $d[0]['nome'] . "][$i]", "MID", "", $tmp_class, "document.forms[0].submit()", $where, "A", "COD");
				break;

			case "select_contador" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">\n<option value=\"\"> </option>";
				$where = "";

				$emp = (int) VoltaValor(PLANO_PADRAO, 'MID_EMPRESA', 'MID', $form_recb_valor);
				$maq_cont = (int) VoltaValor(PLANO_PADRAO, 'MID_MAQUINA', 'MID', $form_recb_valor);
				;
				($maq_cont != 0) ? $where = " AND C.MID_MAQUINA = $maq_cont" : 0;
				$sql = "SELECT C.MID, C.DESCRICAO, M.COD
                FROM " . MAQUINAS_CONTADOR . " AS C, " . MAQUINAS . " AS M
                WHERE C.MID_MAQUINA = M.MID AND  M.MID_SETOR IN
                (SELECT S.MID FROM " . SETORES . " S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp) ORDER BY M.COD ASC";
				die($sql);
				$tmp = $dba[$tdb[MAQUINAS_CONTADOR]['dba']]->Execute($sql);
				if (!$tmp)
					erromsg($dba[$tdb[MAQUINAS_CONTADOR]['dba']]->ErrorMsg());
				$ifm = 0;
				while (!$tmp->EOF) {
					$campo = $tmp->fields;
					if ($cc[$d[0]['nome']][$i] == $campo['MID']
						)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['COD'] . "-" . $campo['DESCRICAO'] . "</option>\n";
					else
						echo "<option value=\"" . $campo['MID'] . "\">" . $campo['COD'] . "-" . $campo['DESCRICAO'] . "</option>\n";
					$tmp->MoveNext();
					$ifm++;
				}
				echo "</select>";
				break;


			case "select_ponto_pred" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				if ($d[$i]['valor'] != "")
					$filtra_maq = $d[$i]['valor'];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">\n<option value=\"\">" . $ling['select_opcao'] . "</option>";
				if ($filtra_maq != "") {
					$tmp = $dba[$tdb[PONTOS_PREDITIVA]['dba']]->Execute("SELECT PONTO,MID FROM " . PONTOS_PREDITIVA . " WHERE MID_MAQUINA = '$filtra_maq' ORDER BY PONTO ASC");
					if (!$tmp)
						erromsg($dba[$tdb[PONTOS_PREDITIVA]['dba']]->ErrorMsg());
					$ifm = 0;
					while (!$tmp->EOF) {
						$campo = $tmp->fields;
						if ($cc[$d[0]['nome']][$i] == $campo['MID']
							)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['PONTO'] . "</option>\n";
						else
							echo "<option value=\"" . $campo['MID'] . "\">" . $campo['PONTO'] . "</option>\n";
						$tmp->MoveNext();
						$ifm++;
					}
				}
				echo "</select>";
				break;
			case "select_ponto_lub" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				if ($d[$i]['valor'] != "")
					$filtra_maq = $d[$i]['valor'];
				echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">\n<option value=\"\">" . $ling['select_opcao'] . "</option>";
				if ($filtra_maq != "") {
					$tmp = $dba[$tdb[PONTOS_LUBRIFICACAO]['dba']]->Execute("SELECT PONTO,MID FROM " . PONTOS_LUBRIFICACAO . " WHERE MID_MAQUINA = '$filtra_maq' ORDER BY PONTO ASC");
					if (!$tmp)
						erromsg($dba[$tdb[PONTOS_LUBRIFICACAO]['dba']]->ErrorMsg());
					$ifm = 0;

					while (!$tmp->EOF) {
						$campo = $tmp->fields;
						if ($cc[$d[0]['nome']][$i] == $campo['MID']
							)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['PONTO'] . "</option>\n";
						else
							echo "<option value=\"" . $campo['MID'] . "\">" . $campo['PONTO'] . "</option>\n";
						$tmp->MoveNext();
						$ifm++;
					}
				}
				echo "</select>";
				break;

			case "select_tipo_prog" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3) and ($_POST['filtra_prog'] == "")) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
					$filtra_prog = $cc[$d[0]['nome']][$i];
				}
				else
					$filtra_prog=(int) $_POST['filtra_prog'];
				echo "<select class=\"$tmp_class\"  id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"filtra_prog\" onchange=\"document.forms[0].submit();\">";
				$tmp = $dba[$tdb[PROGRAMACAO_TIPO]['dba']]->Execute("SELECT * FROM " . PROGRAMACAO_TIPO . " ORDER BY DESCRICAO ASC");
				$ifm = 0;
				echo "<option value=\"\">" . $ling['select_opcao'] . "</option>";
				while (!$tmp->EOF) {
					$campo = $tmp->fields;
					if ($filtra_prog == $campo['MID'])
						echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['DESCRICAO'] . "</option>";
					else
						echo "<option value=\"" . $campo['MID'] . "\">" . $campo['DESCRICAO'] . "</option>";
					$tmp->MoveNext();
					$ifm++;
				}
				echo "</select>";
				unset($tmp);
				unset($campo);
				break;

			case "select_peri" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					if ($cc[$d[0]['nome']][$i] == "")
						$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}

				echo "<select class=\"$tmp_class\"  id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" onchange=\"document.forms[0].submit();\">";

				//Retirar contador para rota
				//RETIRAR CONTADOR QUANDO APLICA��O DO PLANO N�O � PARA UNICO OBJETO DE MANUTEN��O
				$fam = VoltaValor(PLANO_PADRAO, "MAQUINA_FAMILIA", "MID", $form_recb_valor);
				if (($d[0]['nome'] == LINK_ROTAS) || ($fam != 0)) {
					$where = ($fam != 0) ? "WHERE MID != 4" : " ";
				}

				$sql_peri = "SELECT * FROM " . PROGRAMACAO_PERIODICIDADE . " $where ORDER BY DESCRICAO ASC";

				$tmp = $dba[$tdb[PROGRAMACAO_PERIODICIDADE]['dba']]->Execute($sql_peri);
				$ifm = 0;
				echo "<option value=\"\">" . $ling['select_opcao'] . "</option>";
				while (!$tmp->EOF) {
					$campo = $tmp->fields;
					if ($cc[$d[0]['nome']][$i] == $campo['MID'])
						echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['DESCRICAO'] . "</option>";
					else
						echo "<option value=\"" . $campo['MID'] . "\">" . $campo['DESCRICAO'] . "</option>";
					$tmp->MoveNext();
					$ifm++;
				}
				echo "</select>";
				unset($tmp);
				unset($campo);
				break;

			case "select_tipo_plano" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if (($act == 2) or ($act == 3)) {
					if ($cc[$d[0]['nome']][$i] == "")
						$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				echo "<select class=\"$tmp_class\"  id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" onchange=\"document.forms[0].submit();\">";
				$tmp = $dba[$tdb[TIPO_PLANOS]['dba']]->Execute("SELECT * FROM " . TIPO_PLANOS . " ORDER BY DESCRICAO ASC");
				$ifm = 0;
				echo "<option value=\"\">" . $ling['select_opcao'] . "</option>";
				while (!$tmp->EOF) {
					$campo = $tmp->fields;
					if ($cc[$d[0]['nome']][$i] == $campo['MID'])
						echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['DESCRICAO'] . "</option>";
					else
						echo "<option value=\"" . $campo['MID'] . "\">" . $campo['DESCRICAO'] . "</option>";
					$tmp->MoveNext();
					$ifm++;
				}
				echo "</select>";
				unset($tmp);
				unset($campo);
				break;


			case "select_plano_prog" :
				if ($d[$i]['null'] == 1)
					$tmp_class = "campo_select";
				elseif ($d[$i]['null'] == 0)
					$tmp_class = "campo_select_ob";
				if ($d[$i]['valor'] != "")
					$filtra_prog = $d[$i]['valor'];
				elseif ($_POST['filtra_prog'] != "")
					$filtra_prog = (int) $_POST['filtra_prog'];
				elseif (($act == 2) or ($act == 3)) {
					$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				if ($filtra_prog != "") {
					if ($filtra_prog == 1)
						$recb['tb'] = PLANO_PADRAO;
					if ($filtra_prog == 2)
						$recb['tb'] = PLANO_PREDITIVA;
					if ($filtra_prog == 3)
						$recb['tb'] = PLANO_LUBRIFICACAO;
					if ($filtra_prog == 4)
						$recb['tb'] = SOLICITACOES;
					If ($filtra_prog == 5)
						$recb['tb'] = PLANO_INSPECAO;
					echo "<select class=\"$tmp_class\" id=\"cc[" . $d[0]['nome'] . "][$i]\" name=\"cc[" . $d[0]['nome'] . "][$i]\">\n";
					if ($filtra_prog != 4)
						$tmp = $dba[$tdb[$recb['tb']]['dba']]->Execute("SELECT * FROM " . $recb['tb'] . " ORDER BY DESCRICAO ASC");
					else
						$tmp=$dba[$tdb[$recb['tb']]['dba']]->Execute("SELECT * FROM " . $recb['tb'] . " WHERE STATUS = '1' ORDER BY DESCRICAO ASC");
					if (!$tmp)
						erromsg($dba[0]->ErrorMsg());
					$ifm = 0;
					while (!$tmp->EOF) {
						$campo = $tmp->fields;
						if ($cc[$d[0]['nome']][$i] == $campo['MID']
							)echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . $campo['DESCRICAO'] . "</option>";
						else
							echo "<option value=\"" . $campo['MID'] . "\">" . $campo['DESCRICAO'] . "</option>";
						$tmp->MoveNext();
						$ifm++;
					}
					echo "</select>";
					unset($tmp);
					unset($campo);
				}
				break;

			case "select_parm" :
				if (($act == 2) or ($act == 3)) {
					if ($cc[$d[0]['nome']][$i] == "")
						$cc[$d[0]['nome']][$i] = $edita[$d[$i]['campo']];
				}
				$ifm = 1;
				while ($d[$i]['parm'][$ifm]) {

					if ($cc[$d[0]['nome']][$i] == $ifm)
						echo "<input type=\"radio\" id=\"parm[$ifm]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"$ifm\" class=\"campo_check\" onclick=\"document.forms[0].submit();\" checked=\"checked\" /> <label for=\"parm[$ifm]\">" . $d[$i]['parm'][$ifm] . "</label>";
					else
						echo "<input type=\"radio\" id=\"parm[$ifm]\" name=\"cc[" . $d[0]['nome'] . "][$i]\" value=\"$ifm\" class=\"campo_check\" onclick=\"document.forms[0].submit();\" /> <label for=\"parm[$ifm]\">" . $d[$i]['parm'][$ifm] . "</label>";
					$ifm++;
				}
				unset($tmp);
				unset($campo);
				break;
		}
		if ($d[$i]['fecha_div'] != "")
			echo "</div>";
		if ($d[$i]['fecha_fieldset'] != "")
			echo "</fieldset><br />";
		if ((!$d[$i]['fecha_div']) and
				(!$d[$i]['fecha_fieldset']) and
				($d[$i]['tipo'] != "texto_conj_autotag") and
				($d[$i]['tipo'] != "recb_valor") and
				($d[$i]['tipo'] != "datafixa") and
				($d[$i]['tipo'] != "horafixa") and
				($d[$i]['tipo'] != "numos")) {
			echo "<br clear=\"all\" />";
		}
		$i++;
	}
	if ($form_parm == 1)
		echo "\n<input type=\"hidden\" name=\"form_parm\" value=\"1\" />\n";
	echo "<br />
        <input type=\"hidden\" name=\"cc[" . $d[0]['nome'] . "][oq]\" value=\"$foq\" />
        <span>";

	# caso usuario no modulo de solicita��esz
	if ((VoltaPermissao($id, $op) != 3) or ($d[0]['nome'] == SOLICITACOES) or ($d[0]['nome'] == USUARIOS) or ($d[0]['nome'] == SOLICITACAO_RESPOSTA)) {
		echo "<input class=\"botao\" type=\"submit\" name=\"form_botao\" id=\"form_botao\" value=\"Enviar Formulario\">";
	}

	echo "</span>
        </form>";
	if ($d[1]['campo'] == "texto") {
		echo "<script>tmpfocus = document.getElementById('Fo').getElementsByTagName('INPUT')
        tmpfocus[0].focus()
        </script>";
	}
}

// Variaveis de direcionamento
$st = (int) $_GET['st'];
$id = (int) $_GET['id']; // Modulo
$op = (int) $_GET['op']; // Opera��o do modulo
$exe = (int) $_GET['exe'];
$oq = (int) $_GET['oq'];
$desc = $_GET['desc'];
if ($op == "")
	$op = 1;
$act = (int) $_GET['act'];
$form_recb_valor = $_GET['form_recb_valor'];
$atualiza = $_GET['atualiza'];
$foq = (int) $_GET['foq'];
$f = $_GET['f'];

// Fun��es do Sistema
if (!require("lib/mfuncoes.php"))
	die($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php"))
	die($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/" . $manusis['idioma'][0] . ".php"))
	die($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php"))
	die($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php"))
	die($ling['bd01']);
elseif (!require("lib/delcascata.php"))
	die($ling['bd01']);
// Formul�rios
elseif (!require("lib/autent.php"))
	die($ling['autent01']);
elseif (!require("lib/forms.php"))
	die($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php"))
	die($ling['mod01']);

// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array(
	"MSIE",
	"OPERA",
	"MOZILLA",
	"NETSCAPE",
	"FIREFOX",
	"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$fpos = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $$fpos, 5);
	$version = preg_replace('/[^0-9,.]/', '', $version);

	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"" . $ling['xml'] . "\">
<head>
<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\" />
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/" . $manusis['tema'] . "/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"" . $manusis['tema'] . "\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE")
	echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">
<div id=\"div_formulario_corpo\">";
if ($_GET['desc'] != '') {
    $var = $_GET['desc'];
}
else {
	$var = $ling['anexar_arq'];
     }
if ($_GET['idf'] == "arq") {
	echo "<form method=\"POST\" action=\"form.php?idf=arq&oq=$oq&f=$f\" enctype=\"multipart/form-data\">
<fieldset><legend>".$var."</legend>";



	if (VoltaPermissao($id, $op) != 3) {
		echo "<input type=\"file\" id=\"anexar\" name=\"uuimg\" size=\"40\" class=\"campo_text\" />&nbsp;
            <input type=\"submit\" name=\"evi\" class=\"botao\" value=\"{$ling['enviar']}\" />
            <br clear=\"all\" />";
	}
	
    
	if ($_POST['evi']) {
		$arquivo = isset($_FILES['uuimg']) ? $_FILES['uuimg'] : FALSE;
		$arquivo_final = $arquivo['name'];
		$arquivo_final = str_replace(" ", "_", $arquivo_final);
		$arquivo_final = remove_acentos($arquivo_final);
		$arquivo_final = explode(".", $arquivo_final);
		$arquivo_final[0] = strtoupper($arquivo_final[0]);
		switch ($arquivo_final[1]) {
			case "exe" :
				erromsg($ling['err04']);
				break;
			case "com" :
				erromsg($ling['err04']);
				break;
			case "php" :
				erromsg($ling['err04']);
				break;
			case "bat" :
				erromsg($ling['err04']);
				break;
			case "bin" :
				erromsg($ling['err04']);
				break;
			case "asp" :
				erromsg($ling['err04']);
				break;
			default: $valido = 1;
		}
		if ($arquivo['size'] > $manusis['upload']['tamanho'])
			erromsg($ling['err03'] . " " . de_bytes($manusis['upload']['tamanho']));
		elseif ($valido == 1) {
			$imagem_dir = $manusis['dir'][$_GET['f']] . "/$oq/" . $arquivo_final[0] . "." . $arquivo_final[1];
			if (!move_uploaded_file($arquivo['tmp_name'], $imagem_dir)
				)erromsg($ling['err05'] . "<strong> $imagem_dir (" . $arquivo['tmp_name'] . ")</strong>");
		}
	}
	ListaDirDoc($f, $oq, "");
}
else {
	if ($oq == "")
		GeraForm($form[$f], "", $act);
	else
		GeraForm($form[$f], $oq, $act);
}
echo "
<div id=\"div_formulario_parm\" class=\"moverobj\">";
if ($form == 4) {
	$f = $_GET['f'];
	$frame_altura = $form[$f][0]['altura'] - 16;
	echo "<span id=\"div_formulario_cab\">
<a href=\"javascript:fecha_janela_parm()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"" . $ling['fechar'] . "\" /></a>
</span>
<div id=\"div_formulario_corpo_parm\">
<iframe width=\"100%\" height=\"$frame_altura\" name=\"frame_parm\" id=\"frame_parm\" frameborder=\"0\" src=\"form.php?f=$f&act=1&oq=$oq&id=$id&op=$op&exe=$exe&foq=$foq&form_recb_valor=" . $_GET['form_recb_valor'] . "&atualiza=" . $_GET['atualiza'] . "&form_parm=" . $_GET['form_parm'] . "\"> </iframe>
</div>";
}
echo "</div>
</body>
</html>";
?>
