<?
/**
 * Manusis 3.0I
 * Repete atividade de
 *
 * Autor: Fernando Cosentino
 */

// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Formul�rios
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);


// Inicializa��o da sess�o
if (!$_SESSION[ManuSess]['progra']['mac']) $_SESSION[ManuSess]['progra']['mac'] = array();

$id=(int)$_GET['id']; // Modulo
$op=(int)$_GET['op']; // Opera��o do modulo
$exe=(int)$_GET['exe'];
$oq=(int)$_GET['oq'];

// Variaveis de direcionamento
$ajax=$_GET['ajax'];

// Vari�veis que s�o na verdade constantes:
$tipo_rota = 2; // inspe��o
$phpself = 'repeteativ_insp.php';

$mid_plano = (int)$_GET['mid_plano'];


// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $fpos = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $$fpos, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);

    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}

function MostraLista() { // lista de MAQUINAS
    global $_SESSION, $tdb;

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>".$tdb[MAQUINAS]['DESC']."</th>
    <th>".$tdb[MAQUINAS_CONJUNTO]['DESC']."</th>
    <th> </th>
    </tr>";

    if (is_array($_SESSION[ManuSess]['progra']['mac'])) {
        foreach($_SESSION[ManuSess]['progra']['mac'] as $key => $curr) {
            $maq=htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$curr[0],0));
            $pon=htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$curr[1],0));
            echo "<tr class=\"cor2\">
            <td>$maq</td>
            <td>$pon</td>
            <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('gravama','$phpself?ajax=del&del=$key')\" />
            </td></tr>";
        }
    }

    echo "</table>";
}


if ($ajax == 'dponto') {
    $fmaq=(int)$_GET['fmaq'];

    $tmp=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT DESCRICAO,MID FROM ".MAQUINAS_CONJUNTO." WHERE MID_MAQUINA = '$fmaq' ORDER BY DESCRICAO ASC");
    if (!$tmp) erromsg($dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> ErrorMsg());

    echo "<label class=\"campo_label\" for=\"pon\">".$tdb[MAQUINAS_CONJUNTO]['DESC']."</label>";
    FormSelectD('TAG','DESCRICAO',MAQUINAS_CONJUNTO,$_GET['pon'],'pon','pon','MID','','','',"WHERE MID_MAQUINA = '$fmaq'",'A','TAG');

    echo "<input class=\"botao\" type=\"button\" name=\"fadd\" value=\"".$ling['adicionar']."\" onclick=\"atualiza_area2('gravama','$phpself?ajax=add&maq=' + document.getElementById('maq').options[document.getElementById('maq').selectedIndex].value + '&pon=' + document.getElementById('pon').options[document.getElementById('pon').selectedIndex].value)\" />";
    exit();
}


if ($ajax == 'add') {
    $addarray = array((int)$_GET['maq'],(int)$_GET['pon']);
    if (($_GET['maq']) and (in_array($addarray,$_SESSION[ManuSess]['progra']['mac']) === false)) {
        // m�quina+ponto ainda n�o est� adicionada, adiciona
        $_SESSION[ManuSess]['progra']['mac'][] = $addarray;
        krsort($_SESSION[ManuSess]['progra']['mac']);
    }
    MostraLista();
    exit();
}

if ($ajax == 'del') {
    unset($_SESSION[ManuSess]['progra']['mac'][$_GET['del']]);
    krsort($_SESSION[ManuSess]['progra']['mac']);

    MostraLista();
    exit();
}


if (($ajax == 'dperiod') and ($_GET['period'])) {
    $period = $_GET['period'];

    echo "<label class=\"campo_label\" for=\"frequencia\">{$tdb[LINK_ROTAS]['FREQUENCIA']}</label>
    <input type=\"text\" id=\"frequencia\" class=\"campo_text_ob\" value=\"{$_GET['frequencia']}\" name=\"frequencia\" size=\"6\" maxlength=\"20\" />";

    exit();
}



if ($ajax == "") {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
    <body class=\"body_form\">
    <div id=\"formularioos\">";

    if ($_POST['env_form'] != "")  {
        $mid_plano = (int)$_POST['mid_plano'];
        
        $num    = $_POST['numero'];
        $tarefa = strtoupper($_POST['tarefa']);
        
        $mat = (int) $_POST['mat'];
        $qtd = (float) str_replace(',', '.', $_POST['qtd']);
        
        $periodicidade = (int) $_POST['periodicidade'];
        $frequencia    = (int) $_POST['frequencia'];
        
        $espec      = (int) $_POST['especialidade'];
        $quant_mo   = (int) $_POST['quant_mo'];
        $tempo_prev = (float) str_replace(',', '.', $_POST['tempo_prev']);
        
        
        
        $erromsg = '';
        
        if (!$_POST['numero']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['NUMERO']}</b>: {$ling['form01']}</li>";
        }
        if (!$_POST['tarefa']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['TAREFA']}</b>: {$ling['form01']}</li>";
        }

        if (!$_POST['periodicidade']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['PERIODICIDADE']}</b>: {$ling['form01']}</li>";
        }
        elseif (!$_POST['frequencia']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['FREQUENCIA']}</b>: {$ling['form01']}</li>";
        }
        
        if (!$_POST['especialidade']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['ESPECIALIDADE']}</b>: {$ling['form01']}</li>";
        }
        if (!$_POST['tempo_prev']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['TEMPO_PREVISTO']}</b>: {$ling['form01']}</li>";
        }
        if (!$_POST['quant_mo']) {
            $erromsg .= "<li><b>{$tdb[LINK_ROTAS]['QUANTIDADE_MO']}</b>: {$ling['form01']}</li>";
        }
        
        if (!$_SESSION[ManuSess]['progra']['mac']) {
            $erromsg .= "<li><b>{$tdb[MAQUINAS]['DESC']} e {$tdb[PONTOS_LUBRIFICACAO]['DESC']}</b>: {$ling['form01']}</li>";
        }
        
        if ($erromsg) $erromsg = "<ul>$erromsg</ul>";
        
        // ENVIANDO
        if ($erromsg) {
            erromsg($erromsg);
        }
        else {
                
            if (is_array($_SESSION[ManuSess]['progra']['mac'])) {
                foreach ($_SESSION[ManuSess]['progra']['mac'] as $curr) {
                    // para cada par MAQUINA+PONTO
                    $maq=$curr[0];
                    $pon=$curr[1];
                    $mid=GeraMid(LINK_ROTAS,"MID",0);

                    $sql = "INSERT INTO " . LINK_ROTAS . " SET
                     `TIPO` = '$tipo_rota',
                     `MID_PLANO` = '$mid_plano',
                     `MID_MAQUINA` = '$maq',
                     `MID_CONJUNTO` = '$pon',
                     `NUMERO` = '$num',
                     `TAREFA` = '$tarefa',
                     `ESPECIALIDADE` = '$espec',
                     `QUANTIDADE_MO` = '$quant_mo',
                     `TEMPO_PREVISTO` = '$tempo_prev',
                     `PERIODICIDADE` = '$periodicidade',
                     `FREQUENCIA` = '$frequencia',
                     `MID` = '$mid'";
                    
                    $tmp = $dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql);
                    
                    if (!$tmp) {
                        erromsg($dba[$tdb[LINK_ROTAS]['dba']] -> ErrorMsg());
                        $erromsg = 1;
                    }
                    else {
                        logar(3, "", LINK_ROTAS, "MID", $mid);
                        // Mudando STATUS das atividades para atualiza��o da programa��o
                        MudaStatusAtv($mid, LINK_ROTAS);
                    }
                }
                
                
                // Se n�o aconteceram erros durante o processamento
                if ($erromsg == '') {
                    unset($_SESSION[ManuSess]['progra']['mac']);
                    
                    echo "<script>
                    atualiza_area_frame2('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq');
                    </script>";
                    
                    // Limpando os campos
                    $num = '';
                    $tarefa = '';
                    
                    $mat = '';
                    $qtd = '';
        
                    $periodicidade = '';
                    $frequencia    = '';
                    
                    $espec      = '';
                    $quant_mo   = '';
                    $tempo_prev = '';
                }
            }
        }
    }
    
    
    
    
    echo "<form name=\"form\" method=\"POST\" action=\"?id=$id&op=$op&exe=$exe&oq=$oq\">
    <input type=\"hidden\" name=\"mid_plano\" value=\"{$mid_plano}\">

    <fieldset><legend>{$ling['atividade_a']}</legend>
    
    <label for=\"numero\" class=\"campo_label\">{$tdb[LINK_ROTAS]['NUMERO']} </label>
    <input class=\"campo_text_ob\" type=\"text\" id=\"numero\" name=\"numero\" size=4 maxlength=4 value=\"{$num}\" />
    <br clear=\"all\" />
    
    <label for=\"tarefa\" class=\"campo_label\">{$tdb[LINK_ROTAS]['TAREFA']} </label>
    <input class=\"campo_text_ob\" type=\"text\" id=\"tarefa\" name=\"tarefa\" size=50 maxlength=150 value=\"{$tarefa}\" />
    
    </fieldset>
            
    <fieldset><legend>Periodicidade</legend>
    <label class=\"campo_label\" for=\"periodicidade\">{$tdb[LINK_ROTAS]['PERIODICIDADE']}</label>";
    FormSelectD('DESCRICAO', '', PROGRAMACAO_PERIODICIDADE, $periodicidade, 'periodicidade', 'periodicidade', 'MID', '', 'campo_select_ob', "", "WHERE MID!=4");
    echo "<br clear=\"all\" />";
    
    echo "<label class=\"campo_label\" for=\"frequencia\">{$tdb[LINK_ROTAS]['FREQUENCIA']}</label>
    <input type=\"text\" id=\"frequencia\" class=\"campo_text_ob\" value=\"$frequencia\" name=\"frequencia\" size=\"6\" maxlength=\"20\" />
    
    </fieldset>

    <fieldset><legend>{$ling['plano_prev_mo']}</legend>
    
    <label for=\"especialidade\" class=\"campo_label\">{$tdb[LINK_ROTAS]['ESPECIALIDADE']}</label>";
    FormSelectD('DESCRICAO', '', ESPECIALIDADES, $especialidade, 'especialidade', 'especialidade', 'MID', '', 'campo_select_ob');
    echo "<br clear=\"all\" />
    
    <label for=\"quant_mo\" class=\"campo_label\">{$tdb[LINK_ROTAS]['QUANTIDADE_MO']}</label>
    <input class=\"campo_text_ob\" type=\"text\" id=\"quant_mo\" name=\"quant_mo\" size=8 maxlength=11 value=\"{$quant_mo}\"/>
    <br clear=\"all\" />
    
    <label for=\"tempo_prev\" class=\"campo_label\">{$tdb[LINK_ROTAS]['TEMPO_PREVISTO']}</label>
    <input class=\"campo_text_ob\" type=\"text\" id=\"tempo_prev\" name=\"tempo_prev\" size=8 maxlength=13 value=\"{$tempo_prev}\"/>
    
    </fieldset>
    
    <fieldset><legend>{$ling['plano_ponto']}</legend>
    <div id=\"dobj\">";
    
    // MOSTRANDO APENAS AS MAQUINAS NA EMPRESA DO PLANO
    $emp = VoltaValor(PLANO_ROTAS, 'MID_EMPRESA', 'MID', $mid_plano);
    $where = "WHERE MID_SETOR IN (SELECT S.MID FROM " . SETORES . " S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp)";
    
    echo "<label class=\"campo_label\" for=\"maq\">{$tdb[MAQUINAS]['DESC']}</label>";
    FormSelectD('COD','DESCRICAO',MAQUINAS,'','maq','maq','MID','','campo_select_ob',"atualiza_area2('dponto','$phpself?ajax=dponto&fmaq=' + this.options[this.selectedIndex].value)", $where);
    echo "
    </div>
    <br clear=\"all\"/> 
    <div id=\"dponto\"></div>
    <br clear=\"all\"/> 
    <div id=\"gravama\">";
    
    MostraLista();

    echo "</div>
    </fieldset>
    </div>  
    </div>";
    //</fieldset>
    echo "
        
    
    <div id=\"checkfinal\" align=\"center\"> </div>
    <br /><input type=\"submit\" class=\"botao\" name=\"env_form\" value=\"{$ling['cadastrar_atv']}\" />";

    echo "
    </form></div>
    </body>
    </html>";
    //}
}
?>
