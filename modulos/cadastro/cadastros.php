<?
/** 
* Cadastros Auxiliares, tabelas que n�o pertencem a modulo algum ou parametros para uma tabela como tipo de seriv�o, classes etc.
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package cadastro
* @subpackage estrutura
*/

$table = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"  align=\"center\">";
$td_a = "<td align=\"left\" valign=\"top\" width=\"20%\">";
$td_f = "</td>";

echo "<br />";
if ($exe == "") $exe=1;
echo "<div id=\"lt\">";
echo "<tr> <td align=\"center\">";

echo "<fieldset><legend>".$ling['cadastros']."</legend>

{$table}<tr>{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=1\">".'
<img src="imagens/icones/22x22/fam_maquinas.png" border="0"> '.
(($exe == 1)?"<strong>{$tdb[MAQUINAS_FAMILIA]['DESC']}</strong>":"{$tdb[MAQUINAS_FAMILIA]['DESC']}")."
</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=2\">
<img src=\"imagens/icones/22x22/cla_maquinas.png\" border=\"0\" /> ".
(($exe == 2)?"<strong>{$tdb[MAQUINAS_CLASSE]['DESC']}</strong>":"{$tdb[MAQUINAS_CLASSE]['DESC']}").
"</a>
{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/cifra.png\" border=\"0\" /> ".
(($exe == 3)?"<strong>{$tdb[CENTRO_DE_CUSTO]['DESC']}</strong>":"{$tdb[CENTRO_DE_CUSTO]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=4\">
<img src=\"imagens/icones/22x22/fam_equip.png\" border=\"0\" /> ".
(($exe == 4)?"<strong>{$tdb[EQUIPAMENTOS_FAMILIA]['DESC']}</strong>":"{$tdb[EQUIPAMENTOS_FAMILIA]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=21\">
<img src=\"imagens/icones/22x22/branco.png\" border=\"0\" /> ".
(($exe == 21)?"<strong>{$tdb[MATERIAIS_CLASSE]['DESC']}</strong>":"{$tdb[MATERIAIS_CLASSE]['DESC']}").
"</a>

{$td_f}

</tr>

<tr>{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=8\">
<img src=\"imagens/icones/22x22/especialidades.png\" border=\"0\" /> ".
(($exe == 8)?"<strong>{$tdb[ESPECIALIDADES]['DESC']}</strong>":"{$tdb[ESPECIALIDADES]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"lancacontador.php?id=$id&op=$op\" target=\"_blank\">
<img src=\"imagens/icones/22x22/aponta_contador.png\" border=\"0\" /> "
."{$tdb[LANCA_CONTADOR]['DESC']}".
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=9\">
<img src=\"imagens/icones/22x22/feriados.png\" border=\"0\" /> ".
(($exe == 9)?"<strong>{$tdb[FERIADOS]['DESC']}</strong>":"{$tdb[FERIADOS]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=11\">
<img src=\"imagens/icones/22x22/empresa.png\" border=\"0\" /> ".
(($exe == 11)?"<strong>{$tdb[FABRICANTES]['DESC']}</strong>":"{$tdb[FABRICANTES]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=10\">
<img src=\"imagens/icones/22x22/grupos.png\" border=\"0\" /> ".
(($exe == 10)?"<strong>{$tdb[FORNECEDORES]['DESC']}</strong>":"{$tdb[FORNECEDORES]['DESC']}").
"</a>

{$td_f}

</tr>

<tr>

{$td_a}


<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=19\">
<img src=\"imagens/icones/22x22/unidades.png\" border=\"0\" /> ".
(($exe == 19)?"<strong>{$tdb[MAQUINAS_DISPONIBILIDADE]['DESC']}</strong>":"{$tdb[MAQUINAS_DISPONIBILIDADE]['DESC']}").
"</a>


{$td_f}

{$td_a}


<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=22\">
<img src=\"imagens/icones/22x22/branco.png\" border=\"0\" /> ".
(($exe == 22)?"<strong>{$tdb[ETAPAS_PLANO]['DESC']}</strong>":"{$tdb[ETAPAS_PLANO]['DESC']}").
"</a>


{$td_f}

</tr>

</table>

</fieldset>
";

echo 
"<fieldset><legend>{$ling['estoque']}</legend>

{$table}<tr>{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=5\">
<img src=\"imagens/icones/22x22/fam_materiais.png\" border=\"0\" /> ".
(($exe == 5)?"<strong>{$tdb[MATERIAIS_FAMILIA]['DESC']}</strong>":"{$tdb[MATERIAIS_FAMILIA]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=55\">
<img src=\"imagens/icones/22x22/fam_materiais.png\" border=\"0\" /> ".
(($exe == 55)?"<strong>Sub-Familia de Materiais</strong>":"Sub-Familia de Materiais").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=56\">
<img src=\"imagens/icones/22x22/material.png\" border=\"0\" /> ".
(($exe == 56)?"<strong>{$tdb[MATERIAIS_CRITICIDADE]['DESC']}</strong>":"{$tdb[MATERIAIS_CRITICIDADE]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=6\">
<img src=\"imagens/icones/22x22/unidades.png\" border=\"0\" /> ".
(($exe == 6)?"<strong>{$tdb[MATERIAIS_UNIDADE]['DESC']}</strong>":"{$tdb[MATERIAIS_UNIDADE]['DESC']}").
"</a>
{$td_f}
<td colspan=2></td>

</tr>

</table>

</fieldset>
";

echo "<fieldset><legend>{$ling['os']}</legend>

{$table}<tr>
{$td_a}
<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=12\">
<img src=\"imagens/icones/22x22/tipo_serv.png\" border=\"0\" /> ".
(($exe == 12)?"<strong>{$tdb[TIPOS_SERVICOS]['DESC']}</strong>":"{$tdb[TIPOS_SERVICOS]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=13\">
<img src=\"imagens/icones/22x22/natur_serv.png\" border=\"0\" /> ".
(($exe == 13)?"<strong>".$ling['tabela_natur_serv']."</strong>":$ling['tabela_natur_serv']).
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=15\">
<img src=\"imagens/icones/22x22/defeito.png\" border=\"0\" /> ".
(($exe == 15)?"<strong>".$ling['tabela_defeito']."</strong>":$ling['tabela_defeito']).
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=14\">
<img src=\"imagens/icones/22x22/causa.png\" border=\"0\" /> ".
(($exe == 14)?"<strong>".$ling['tabela_causa']."</strong>":$ling['tabela_causa']).
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=16\">
<img src=\"imagens/icones/22x22/solucao.png\" border=\"0\" /> ".
(($exe == 16)?"<strong>".$ling['tabela_solucao']."</strong>":$ling['tabela_solucao']).
"</a>
{$td_f}

</tr>

<tr>{$td_a}


<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=18\">
<img src=\"imagens/icones/22x22/cla_maquinas.png\" border=\"0\" /> ".
(($exe == 18)?"<strong>".$ling['tabela_sol_producao']."</strong>":$ling['tabela_sol_producao']).
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=17\">
<img src=\"imagens/icones/22x22/funcionario.png\" border=\"0\" /> ".
(($exe == 17)?"<strong>".$ling['tabela_sol_seguranca']."</strong>":$ling['tabela_sol_seguranca']).
"</a>

{$td_f}

</tr>

</table>

</fieldset>
";

echo "<fieldset><legend>".$ling['graficos']."</legend>

{$table}<tr>

{$td_a}
<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=20\">
<img src=\"imagens/icones/22x22/branco.png\" border=\"0\" /> ".
(($exe == 20)?"<strong>{$tdb[GRUPO_GRAFICO]['DESC']}</strong>":"{$tdb[GRUPO_GRAFICO]['DESC']}").
"</a>

{$td_f}{$td_a}

{$td_f}

</tr>

</table>

</fieldset>
";

if (($exe == 1) or ($exe == "")) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    $campos[2]="MID_EMPRESA";
    ListaTabela (MAQUINAS_FAMILIA,"MID",$campos,"MAQUINAS_FAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 2) {
    $campos[0]="DESCRICAO";
    ListaTabela (MAQUINAS_CLASSE,"MID",$campos,"MAQUINAS_CLASSE","","",1,1,1,1,1,1,1,1,1,1,"");

}
elseif ($exe == 3) {
    $campos[0]="MID_EMPRESA";
    $campos[1]="COD";
    $campos[2]="DESCRICAO";
    $campos[3]="PAI";
    ListaTabela (CENTRO_DE_CUSTO,"MID",$campos,"CENTRO_DE_CUSTO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 4) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    $campos[2]="MID_EMPRESA";
    ListaTabela (EQUIPAMENTOS_FAMILIA,"MID",$campos,"EQUIPAMENTO_FAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 5) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    ListaTabela (MATERIAIS_FAMILIA,"MID",$campos,"MATERIAL_FAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 6) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    ListaTabela (MATERIAIS_UNIDADE,"MID",$campos,"MATERIAL_UNIDADE","","",1,1,1,1,1,1,1,1,1,1,"");

}
elseif ($exe == 7) {
    $campos[0]="DESCRICAO";
    ListaTabela (MAQUINAS_CONTADOR_TIPO,"MID",$campos,"CONTADOR_TIPO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 55) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    $campos[2]="MID_FAMILIA";
    ListaTabela (MATERIAIS_SUBFAMILIA,"MID",$campos,"MATERIAL_SUBFAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 56) {
    $campos[0]="DESCRICAO";
    ListaTabela (MATERIAIS_CRITICIDADE,"MID",$campos,"MATERIAL_CRITICIDADE","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 8) {
    $campos[0]="DESCRICAO";
    $campos[1]="MID_EMPRESA";
    ListaTabela (ESPECIALIDADES,"MID",$campos,"ESPECIALIDADE","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 9) {
    $campos[0]="DATA";
    $campos[1]="DESCRICAO";
    ListaTabela (FERIADOS,"MID",$campos,"FERIADO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 10) {
    $campos[0]="NOME";
    $campos[1]="FANTASIA";
    $campos[2]="E_MAIL";
    $campos[3]="TELEFONE_1";
    $campos[4]="ENDERECO";
    $campos[5]="NUMERO";
    $campos[6]="COMPLEMENTO";
    $campos[7]="BAIRRO";
    $campos[8]="CIDADE";
    $campos[9]="UF";
    ListaTabela (FORNECEDORES,"MID",$campos,"FORNECEDOR","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 11) {
    $campos[0]="NOME";
    $campos[1]="EMAIL";
    $campos[2]="TELEFONE";
    $campos[3]="ENDERECO";
    $campos[4]="BAIRRO";
    $campos[5]="CIDADE";
    $campos[6]="ESTADO";
    ListaTabela (FABRICANTES,"MID",$campos,"FORNECEDOR2","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 12) {
    $campos = array(); 
    $campos[]="DESCRICAO";
    $campos[]="SPEED_CHECK";
    $campos[]="MID_EMPRESA";
    $campos[]="MID_PLANO";
    ListaTabela (TIPOS_SERVICOS,"MID",$campos,"TIPO_SERVICO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 13) {
    $campos[0]="DESCRICAO";
    $campos[1]="MID_EMPRESA";
    ListaTabela (NATUREZA_SERVICOS,"MID",$campos,"NATUREZA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 14) {
    $campos[0]="DESCRICAO";
    $campos[1]="MID_EMPRESA";
    ListaTabela (CAUSA,"MID",$campos,"CAUSA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 15) {
    $campos[0]="DESCRICAO";
        $campos[1]="MID_EMPRESA";
    ListaTabela (DEFEITO,"MID",$campos,"DEFEITO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 16) {
    $campos[0]="DESCRICAO";
        $campos[1]="MID_EMPRESA";
    ListaTabela (SOLUCAO,"MID",$campos,"SOLUCAO","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 17) {
    $campos[0]="DESCRICAO";
    $campos[1]="NIVEL";
    ListaTabela (SOLICITACAO_SITUACAO_SEGURANCA,"MID",$campos,"SOL_SEGURANCA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 18) {
    $campos[0]="DESCRICAO";
    $campos[1]="NIVEL";
    ListaTabela (SOLICITACAO_SITUACAO_PRODUCAO,"MID",$campos,"SOL_PRODUCAO","","",1,1,1,1,1,1,1,1,1,1,"");
}

elseif ($exe == 19) {
    $campos[0]="MID_MAQUINA";
    $campos[1]="MES";
    $campos[2]="ANO";
    $campos[3]="HORAS";
    ListaTabela (MAQUINAS_DISPONIBILIDADE,"MID",$campos,"MAQUINA_DISPONIBILIDADE","","",1,1,1,1,1,1,1,1,1,1,"");
}

elseif ($exe == 20) {
    $campos = array();
    $campos[]="DESCRICAO";
    
    ListaTabela (GRUPO_GRAFICO,"MID",$campos,"GRUPO_GRAFICO","","",1,1,1,1,1,1,1,1,1,1,"");
}

elseif ($exe == 21) {
    $campos = array();
    $campos[]="DESCRICAO";
    
    ListaTabela (MATERIAIS_CLASSE,"MID",$campos,"MATERIAIS_CLASSE","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 22) {
	$campos = array();
	$campos[]="DESCRICAO";

	ListaTabela (ETAPAS_PLANO,"MID",$campos,"ETAPAS_PLANO","","",1,1,1,1,1,1,1,1,1,1,"");
}

echo "</td>
</td></tr></table></div>
<br>";

?>
