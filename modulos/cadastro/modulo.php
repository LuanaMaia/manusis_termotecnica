<?
/** 
* Arquivo principal do modulo cadastro
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage cadastro
*/

$exe=(int)$_GET['exe'];
$op=(int)$_GET['op'];
if ($op == 0) $op=1;
if ($op == 1) {
	if (!include("modulos/cadastro/estrutura.php")) erromsg($ling['err01']);
}
elseif ($op == 2) {
	if (!include("modulos/cadastro/maodeobra.php")) erromsg($ling['err01']);
}
elseif ($op == 3) {
	if (!include("modulos/cadastro/cadastros.php")) erromsg($ling['err01']);
}
?>