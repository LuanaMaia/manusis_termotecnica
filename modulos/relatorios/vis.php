<?
/**
* Visualiza��o dos graficos do assistente de Gr�ficos
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/autent.php")) die ($ling['autent01']);
elseif (!require("../../lib/delcascata.php")) die ($ling['bd01']);
elseif (!require("funcoes.php")) die ($ling['bd01']);
elseif (!require("conf.php")) die ($ling['bd01']);
if ($_GET['xls']){
    $arq=urldecode($_GET['arq']);
    if ($arq == "") {
        echo "<br /><br />";
    }
    else {
        $dir="../../".$manusis['dir']['graficos'];
        $xml = new DOMDocument();
        $xml->formatOutput = true;
        $xml -> load("$dir/$arq");
        $descricao_grafico=$xml -> getElementsByTagName('descricao');
        $descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
        $xls=$_SESSION['ag']['xls'];
        $_SESSION['ag']['xls']="";
        AG_ExportaXLS($descricao_grafico,$xls);
        exit;
    }
}
if (!$_REQUEST['print']) {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>Manusis</title>
    <link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
    <style>
    .excel_tdi {
        border-bottom:1px solid gray;
        border-right:1px solid gray;
        font:11px arial;
    }
    </style>
    <script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
    echo "</head>
    <body>
    <div style=\"padding:5px\">
    ";

    if ($_REQUEST['pre']) {
        // gera QUERY_STRING para link (impress�o)
        $qry = '';
        $REQ = $_GET + $_POST;
        unset($REQ['pre'],$REQ['exp']);
        $reqkeys = array_keys($REQ);
        foreach ($reqkeys as $estereq) {
            AddStr($qry, '&', $estereq.'='.$REQ[$estereq]);
        }
        // gera QUERY_STRING para MSWORD
        $qrydoc = $qry;
        AddStr($qry,'&','print=nav');
        AddStr($qrydoc,'&','print=word');
        echo "<div align=\"left\">Vers&atilde;o para impress&atilde;o: &nbsp;
        <a class=\"link\" href=\"vis.php?$qry\" target=\"_blank\">
        <img src=\"../../imagens/icones/22x22/imprimir.png\" border=0> Navegador</a>
        &nbsp; &nbsp; 
        <a class=\"link\" href=\"vis.php?$qrydoc\" target=\"_blank\">
        <img src=\"../../imagens/icones/22x22/imprimir.png\" border=0> MS Word</a>
        </div>";
    }

    $tablestyle='border:2px inset gray';
    $tableclass='excel_tdi';
    $cellstyle='';
    $trcolor='gray';

}
else { // vers�o para impress�o
    $tablestyle='border: 1px solid #555555';
    $tableclass='';
    $cellstyle="style=\"border: 1px solid #999999\"";
    $trcolor='#CCCCCC';
}
// estilos ficam diferentes em previsualizar ou impressao
// (mas nao foram alterados para o excel)
// tablestyle aparece em <table>
// cellstyle aparece em <td>
// trcolor aparece em <tr bgcolor=
// e tableclass aparece sempre que existe um class="

$arq=urldecode($_GET['arq']);
if ($arq == "") {
    echo "<br /><br />";
}
else {
    // a variavel $echo conter� os HTMLs que sofrer�o um echo no final
    $echo = '';
    $iim = 0;
    $filtro_relatorio = '';

    $dir="../../".$manusis['dir']['graficos'];
    $xml = new DOMDocument();
    $xml->formatOutput = true;
    $xml -> load("$dir/$arq");
    $descricao_grafico=$xml -> getElementsByTagName('descricao');
    $descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
    $echo .= "<h2>$descricao_grafico</h2><br /><br />";
    $params = $xml->getElementsByTagName('tabela');
    $i=0;
    foreach ($params as $param) {
        $tbid=$param -> getAttribute('id');
        $tabela=AG_PegaTabela($xml,$tbid);
        $tbinfo=AG_InfoTabela($xml,$tbid);
        // Campo data que ser� usado para pesquisa ex: DATA_INICIO
        $tbdatat=LimpaTexto($_REQUEST['tdata_'.$tbid]);

        $echo .= "<h3 align=\"left\" style=\"padding:2px\">".$tbinfo['titulo'];
        $xls.="<tr height=17 style='height:12.75pt'>
        <td class=xl26 height=17 style='font-size:16px;height:14.75pt' colspan=12>".$tbinfo['titulo'];

        // Monto o criterio para o filtro sql
        if ($tbinfo['tipo'] == 1){
            $tbano=(int)$_REQUEST['ano_'.$tbid];
            if ($tbano == "") $tbano=date('Y');
            // montagem de sql para a op��o de data de apontamento //
            if($tbdatat == "DATA_APONTA"){
                $sql_filtro = "WHERE " . ORDEM_MADODEOBRA . ".DATE_FORMAT(`DATA_INICIO`,'%Y') = '$tbano' AND " . ORDEM_MADODEOBRA . ".DATE_FORMAT(`DATA_FINAL`,'%Y') = '$tbano'";

            }
            else {
                $sql_filtro="WHERE DATE_FORMAT(`$tbdatat`,'%Y') = '$tbano' ";
            }
            $info_data="Ano: $tbano";
            $datai="$tbano-01-01";
            $dataf="$tbano-12-31";

        }
        elseif ($tbinfo['tipo'] == 2){
            $tbdatai=$_REQUEST['datai_'.$tbid];
            $tbdataf=$_REQUEST['dataf_'.$tbid];
            $tmp_datai=explode("/",$tbdatai);
            $tmp_dataf=explode("/",$tbdataf);
            if (!checkdate((int)$tmp_datai[1],(int)$tmp_datai[0],(int)$tmp_datai[2])) {
                erromsg("A data - $tbdatai � inv�lida");
                $echo .= "<br />";
                $erro=1;
                $xls_erro=1;
            }
            elseif (!checkdate((int)$tmp_dataf[1],(int)$tmp_dataf[0],(int)$tmp_dataf[2])) {
                erromsg("A data - $tbdataf � inv�lida");
                $echo .= "<br />";
                $erro=1;
                $xls_erro=1;
            }
            // Aqui eu uso DATE_FORMAT, pois n�o quero ficar com codigo mais limpo
            $info_data="Per�odo de: $tbdatai � $tbdataf";
            $datai=explode("/",$tbdatai);
            $datai=$datai[2]."-".$datai[1]."-".$datai[0];
            $dataf=explode("/",$tbdataf);
            $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
            
            // montagem de sql para a op��o de data de apontamento //
            if($tbdatat == "DATA_APONTA"){
                $sql_filtro= ORDEM_MADODEOBRA . ".DATA_INICIO >= '$datai' AND " . ORDEM_MADODEOBRA . ".DATA_FINAL <= '$dataf'";
            }
            else {
                $sql_filtro="WHERE $tbdatat >= '$datai' AND $tbdatat <= '$dataf' ";
            }
        }
        
        if ($info_data) {
            $echo .= "($info_data)";
            $xls.="($info_data)";
        }
        $echo .= "</h3>";
        $xls.="</td></tr>\n";
        if (!$tbinfo['coluna_mestre']){
            erromsg("Nenhuma Coluna Mestre foi definida, favor use a op��o editar tabela para modificar suas informa��es");
            $echo .= "<br />";
            $erro=1;
            $xls_erro=1;
        }
        if (!$tbinfo['coluna_dados']){
            erromsg("Nenhuma Coluna de Dados foi definida, favor use a op��o editar tabela para modificar suas informa��es");
            $echo .= "<br />";
            $erro=1;
            $xls_erro=1;
        }
        if (!$erro) {
            // Monto o restante dos filtros definidos pelo usuario
            $filtros = $tabela ->getElementsByTagName('filtro');
            $if_operador=0;
            $filtro_relatorio_tmp = '';
            
            // Filtros passados a coluna mestre
            $filtro_adicional = array();

            foreach ($filtros as $filtro) {
                $cond=$filtro -> getAttribute('condicao');
                $condicao=$AG_cond_sinal[$cond];
                $campo=$filtro ->getElementsByTagName('campo') ->  item(0) -> nodeValue;
                $valor=$filtro ->getElementsByTagName('valor') -> item(0) -> nodeValue;
                $operador=$filtro ->getElementsByTagName('operador') -> item(0) -> nodeValue;
                // Pego rela��o do campo deo filtro na tabela ORDEM e mostro resultado
                $tmp_filtro=VoltaRelacao(ORDEM_PLANEJADO,$campo);
                $tmp_filtro=VoltaValor($tmp_filtro['tb'],$tmp_filtro['campo'],$tmp_filtro['mid'],$valor,$tmp_filtro['dba']);
                // Uma entrada para o filtro SQL outra para Apresenta��o dos filtros usados
                if ($if_operador == 0){
                    // Campo existe na OS
                    if($tdb[ORDEM][$campo]) {
                        $sql_filtro.=" AND ($campo $condicao '$valor' ";
                    }
                    // Campo l�gico
                    elseif($AG_filtros[$campo]['COND']) {
                        if(is_array($AG_filtros[$campo]['COND'])) {
                            $sql_filtro.=" AND (" . $AG_filtros[$campo]['COND'][$valor];
                        }
                        else {
                            $sql_filtro.=" AND (" . str_replace('%%COND%%', $condicao, str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']));
                        }
                    }
                    
                    $info_filtro.=$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro<br />";
                    AddStr($filtro_relatorio_tmp,', ',$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro");
                }
                else {
                    // Campo existe na OS
                    if($tdb[ORDEM][$campo]) {
                        $sql_filtro.=" ".$AG_oper_sinal[$operador]." $campo $condicao '$valor' ";
                    }
                    // Campo l�gico
                    elseif($AG_filtros[$campo]['COND']) {
                        if(is_array($AG_filtros[$campo]['COND'])) {
                            $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . $AG_filtros[$campo]['COND'][$valor];
                        }
                        else {
                            $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . str_replace('%%COND%%', $condicao, str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']));
                        }
                    }
                    
                    $info_filtro.="<u>".$AG_oper[$operador]."</u> ".$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro<br />";
                    AddStr($filtro_relatorio_tmp,', ',"<u>".$AG_oper[$operador]."</u> ".$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro");
                }
                
                if ($campo == "MID_EQUIPE") {
                    $filtro_adicional['MID_EQUIPE'][] = array($AG_oper_sinal[$operador], $condicao, $valor);
                }

                $if_operador++;
            }
            if ($if_operador != 0) {
                $sql_filtro.=")";
            }
            if ($filtro_relatorio_tmp) AddStr($filtro_relatorio,' / ',$filtro_relatorio_tmp);

            
            // Executo o SQL
            if ($tbdatat == "DATA_APONTA") {
                
                $sql="SELECT O.* FROM ".ORDEM_PLANEJADO." O, " . ORDEM_MADODEOBRA . " WHERE " . ORDEM_MADODEOBRA . ".MID_ORDEM = O.MID AND $sql_filtro GROUP BY O.MID";
            }
            else {
                $sql="SELECT * FROM ".ORDEM_PLANEJADO." O $sql_filtro";
            }
          
            $resultado=$dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql);
            if (!$resultado) {
                erromsg("Ocorreu um erro durante o processamento dessa consulta: ".$dba[$tdb[ORDEM_PLANEJADO]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                $echo .= "<br><br>";
                $xls_erro=1;
            }
            else {
                // INICIO A APRESENTA��O GRAFICA
                $echo .= "<table align=\"center\" border=\"0\" width=\"99%\" cellspacing=\"0\" style=\"$tablestyle\">\n";

                // POR EVOLU��O ANUAL
                if ($tbinfo['tipo'] == 1){

                    // Titulo da coluna mestre
                    $echo .= "<tr bgcolor=\"$trcolor\">\n
                    <th class=\"$tableclass\">".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</th>\n";
                    $xls.="<tr height=17 style='height:12.75pt'>
                    <td class=xl26 height=17 style='height:12.75pt;font-size:15px'>".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</td>";
                    foreach ($tbinfo['coluna_dados'] as $col => $valor){
                        $unidade_mostra = "".$AG_colDados[$col]['UNIDADE']."";
                    }
                    // Titulo das colunas de dados (meses)
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Jan $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Fev $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Mar $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Abr $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Mai $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Jun $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Jul $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Ago $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Set $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Out $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Nov $unidade_mostra</th>\n";
                    $echo .= "<th class=\"$tableclass\" $cellstyle>Dez $unidade_mostra</th>\n";
                    $echo .= "</tr>";

                    $xls.= "<td class=xl26>Jan $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Fev $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Mar $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Abr $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Mai $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Jun $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Jul $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Ago $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Set $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Out $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Nov $unidade_mostra</td>\n";
                    $xls.= "<td class=xl26>Dez $unidade_mostra</td>\n";
                    $xls.= "</tr>";

                    $ncols=12;
                }
                // POR PERIODO
                if ($tbinfo['tipo'] == 2){
                    // Titulo da coluna mestre
                    $echo .= "<tr bgcolor=\"$trcolor\">\n
                    <th class=\"$tableclass\" $cellstyle>".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</th>\n";
                    $xls.="<tr height=17 style='height:12.75pt'>
                    <td class=xl26 height=17 style='height:12.75pt'>".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</td>";

                    // Titulo das colunas de dados
                    $ncols=0;
                    foreach ($tbinfo['coluna_dados'] as $col => $valor){
                        $echo .= "<th class=\"$tableclass\" $cellstyle>".$AG_colDados[$col]['DESCRICAO']." ".$AG_colDados[$col]['UNIDADE']."</th>\n";
                        $xls.= "<td class=xl26>".$AG_colDados[$col]['DESCRICAO']." ".$AG_colDados[$col]['UNIDADE']."</td>";
                        $ncols++;

                    }
                    $echo .= "</tr>";
                    $xls.= "</tr>";
                }
                if ((int)$titu == 0) $titu=5;
                $y_col=$ncols;
                $x_lin=$titu;
                switch ($tbinfo['coluna_mestre_tipo']){

                    /* PARA CASOS MAIS AVAN�ADOS � NECESSARIO CRIAR UM NOVO TIPO DE COLUNA MESTRE
                    DESSA FORMA PODE SE USAR DE RELA��ES COM OUTRAS TABELAS.
                    POR PADR�O O CASE 1 (TIPO 1) � O BASICO, GERA BOA PARTE DOS GRAFICOS
                    SO CRIE MAIS TIPOS SE FOR REALMENTE NESSES�RIO.*/

                    case 1:
                        // Processo o resultado
                        $tmp=AG_ColunaDados($resultado,$xml,$tbid,$tbdatat,$datai,$dataf);
                        $mostra=$tmp['campos']; // Colunas que contem resultado
                        $dados=$tmp['dados']; // Dados das colunas
                        
                        // FILTROS ADICIONAIS
                        $filtro_sql = "";
                        if (($tbinfo['coluna_mestre'] == FUNCIONARIOS) and (count($filtro_adicional['MID_EQUIPE']) > 0)) {
                            foreach ($filtro_adicional['MID_EQUIPE'] as $valores) {
                                $filtro_sql .= ($filtro_sql == "")? " WHERE " : " " . $valores[0] . " ";
                                $filtro_sql .= "EQUIPE  " . $valores[1] . " " . $valores[2];
                            }
                        }
                        
                        // Busco na tabela da coluna mestre as informa��es recolhidas
                        $sql="SELECT * FROM ".$tbinfo['coluna_mestre'] . "" . $filtro_sql;
                        $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                        if (!$resultado2) {
                            // Deu revestrez!
                            erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                            $xls_erro=1;
                        }
                        else {
                            while (!$resultado2->EOF) {
                                $campo=$resultado2->fields;
                                if ($mostra[$campo['MID']]){
                                    $iim++;
                                    $x_lin++;
                                    $echo .= "<tr bgcolor=\"white\">";
                                    $xls.="<tr height=17 style='height:12.75pt'>";
                                    // Descubro o nome de apresenta��o e monto ele
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>";
                                    $xls.= "<td class=xl26>";
                                    foreach ($AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'] as $ca => $valor) {
                                        $echo .= $campo[$valor]." ";
                                        $xls.= $campo[$valor]." ";
                                    }
                                    $echo .= "</td>\n";
                                    $xls.= "</td>";
                                    // Preencho todas Colunas com os valores encontrados
                                    if ($tbinfo['tipo'] == 1){
                                        for ($n=1; $n <= $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n][$campo['MID']];
                                            $col_total[$n]++;

                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".$dados[$n][$campo['MID']]."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",$dados[$n][$campo['MID']])."\">".str_replace(".",",",$dados[$n][$campo['MID']])."</td>";

                                        }
                                    }
                                    if ($tbinfo['tipo'] == 2){
                                        for ($n=0; $n < $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n][$campo['MID']];
                                            $col_total[$n]++;
                                            
                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".$dados[$n][$campo['MID']]."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",$dados[$n][$campo['MID']])."\">".str_replace(".",",",$dados[$n][$campo['MID']])."</td>";
                                        }
                                    }
                                    $echo .= "</tr>";
                                    $xls.= "</tr>";
                                }
                                $resultado2->MoveNext();

                            }
                            
                            /**
                             * MEDIA 
                             */
                            $echo .= "<tr bgcolor=\"white\"><td class=\"$tableclass\" align=\"left\" $cellstyle><strong>M�DIA</strong></td>";
                            $xls .= "<tr height=17 style='height:12.75pt'><td class=xl26><strong>M�DIA</strong></td>";

                            if ($tbinfo['tipo'] == 1) {
                                for ($n= 1; $n <= $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {

                                        $valormedia= 0;
                                    } else {
                                        $valormedia=  round($val_total[$n] / $col_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";

                                }
                            }
                            if ($tbinfo['tipo'] == 2) {
                                for ($n= 0; $n < $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    } else {
                                        $valormedia=  round($val_total[$n] / $col_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";
                                }

                            }
                            $echo .= "</tr>";
                            $xls .= "</tr>";
                            
                            /**
                             * TOTAL
                             */
                            $echo .= "<tr bgcolor=\"white\"><td class=\"$tableclass\" align=\"left\" $cellstyle><strong>TOTAL</strong></td>";
                            $xls .= "<tr height=17 style='height:12.75pt'><td class=xl26><strong>TOTAL</strong></td>";

                            if ($tbinfo['tipo'] == 1) {
                                for ($n= 1; $n <= $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    } else {
                                        $valormedia= round($val_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";

                                }
                            }
                            if ($tbinfo['tipo'] == 2) {
                                for ($n= 0; $n < $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    }
                                    $valormedia= round($val_total[$n],1);
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";
                                }
                            }
                            $echo .= "</tr>";
                            $xls .= "</tr>";
                        }
                        break;



                        /* CONDI��O ESPECIAL PARA LISTAR TIPOS DE SERVI�O
                        E TIPOS DE PROGRAMA��O JUNTOS */
                    case 2:
                        // Processo o resultado
                        $tmp=AG_ColunaDados($resultado,$xml,$tbid,$tbdatat,$datai,$dataf);
                        $mostra=$tmp['campos']; // Colunas que contem resultado
                        $dados=$tmp['dados']; // Dados das colunas
                        // Busco na tabela da coluna mestre as informa��es recolhidas

                        $sql="SELECT * FROM ".PROGRAMACAO_TIPO;

                        $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                        if (!$resultado2) {
                            // Deu revestrez!
                            erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                            $xls_erro=1;
                        }
                        else {
                            while (!$resultado2->EOF) {
                                $campo=$resultado2->fields;
                                if ($mostra["P".$campo['MID']]){
                                    $iim++;
                                    $x_lin++;
                                    $echo .= "<tr bgcolor=\"white\">";
                                    $xls.="<tr height=17 style='height:12.75pt'>";
                                    // Descubro o nome de apresenta��o e monto ele
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>";
                                    $xls.= "<td class=xl26>";

                                    $echo .= $campo["DESCRICAO"]." ";
                                    $xls.= $campo["DESCRICAO"]." ";

                                    $echo .= "</td>\n";
                                    $xls.= "</td>";
                                    // Preencho todas Colunas com os valores encontrados
                                    if ($tbinfo['tipo'] == 1){
                                        for ($n=1; $n <= $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n]["P".$campo['MID']]){
                                                $dados[$n]["P".$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n]["P".$campo['MID']];
                                            $col_total[$n]++;
                                            
                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".round($dados[$n]["P".$campo['MID']],3)."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",round($dados[$n]["P".$campo['MID']],2))."\">".str_replace(".",",",round($dados[$n]["P".$campo['MID']],2))."</td>";
                                        }
                                    }
                                    if ($tbinfo['tipo'] == 2){
                                        for ($n=0; $n < $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n]["P".$campo['MID']]){
                                                $dados[$n]["P".$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n]["P".$campo['MID']];
                                            $col_total[$n]++;
                                            
                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".round($dados[$n]["P".$campo['MID']],3)."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",round($dados[$n]["P".$campo['MID']],2))."\">".str_replace(".",",",round($dados[$n]["P".$campo['MID']],2))."</td>";
                                        }
                                    }
                                    $echo .= "</tr>";
                                    $xls.= "</tr>";
                                }
                                $resultado2->MoveNext();
                            }
                        }

                        $sql="SELECT * FROM ".TIPOS_SERVICOS;

                        $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                        if (!$resultado2) {
                            // Deu revestrez!
                            erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                            $xls_erro=1;
                        }
                        else {
                            while (!$resultado2->EOF) {
                                $campo=$resultado2->fields;
                                if ($mostra[$campo['MID']]){
                                    $iim++;
                                    $x_lin++;
                                    $echo .= "<tr bgcolor=\"white\">";
                                    $xls.="<tr height=17 style='height:12.75pt'>";
                                    // Descubro o nome de apresenta��o e monto ele
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>";
                                    $xls.= "<td class=xl26>";

                                    $echo .= $campo["DESCRICAO"]." ";
                                    $xls.= $campo["DESCRICAO"]." ";

                                    $echo .= "</td>\n";
                                    $xls.= "</td>";
                                    // Preencho todas Colunas com os valores encontrados
                                    if ($tbinfo['tipo'] == 1){
                                        for ($n=1; $n <= $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n][$campo['MID']];
                                            $col_total[$n]++;
                                            
                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".round($dados[$n][$campo['MID']],3)."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",round($dados[$n][$campo['MID']],2))."\">".str_replace(".",",",round($dados[$n][$campo['MID']],2))."</td>";
                                        }
                                    }
                                    if ($tbinfo['tipo'] == 2){
                                        for ($n=0; $n < $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            
                                            $val_total[$n] += $dados[$n][$campo['MID']];
                                            $col_total[$n]++;
                                            
                                            $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>".round($dados[$n][$campo['MID']],3)."</td>";
                                            $xls.= "<td class=xl26 x:num=\"".str_replace(",",".",round($dados[$n][$campo['MID']],2))."\">".str_replace(".",",",round($dados[$n][$campo['MID']],2))."</td>";
                                        }
                                    }
                                    $echo .= "</tr>";
                                    $xls.= "</tr>";
                                }
                                $resultado2->MoveNext();
                            }
                            
                            /**
                             * MEDIA 
                             */
                            $echo .= "<tr bgcolor=\"white\"><td class=\"$tableclass\" align=\"left\" $cellstyle><strong>M�DIA</strong></td>";
                            $xls .= "<tr height=17 style='height:12.75pt'><td class=xl26><strong>M�DIA</strong></td>";

                            if ($tbinfo['tipo'] == 1) {
                                for ($n= 1; $n <= $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {

                                        $valormedia= 0;
                                    } else {
                                        $valormedia=  round($val_total[$n] / $col_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";

                                }
                            }
                            if ($tbinfo['tipo'] == 2) {
                                for ($n= 0; $n < $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    } else {
                                        $valormedia=  round($val_total[$n] / $col_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";
                                }

                            }
                            $echo .= "</tr>";
                            $xls .= "</tr>";
                            
                            /**
                             * TOTAL
                             */
                            $echo .= "<tr bgcolor=\"white\"><td class=\"$tableclass\" align=\"left\" $cellstyle><strong>TOTAL</strong></td>";
                            $xls .= "<tr height=17 style='height:12.75pt'><td class=xl26><strong>TOTAL</strong></td>";

                            if ($tbinfo['tipo'] == 1) {
                                for ($n= 1; $n <= $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    } else {
                                        $valormedia= round($val_total[$n],1);
                                    }
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";

                                }
                            }
                            if ($tbinfo['tipo'] == 2) {
                                for ($n= 0; $n < $ncols; $n++) {
                                    /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                    pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                    if (!$val_total[$n]) {
                                        $valormedia= 0;
                                    }
                                    $valormedia= round($val_total[$n],1);
                                    $echo .= "<td class=\"$tableclass\" align=\"left\" $cellstyle>" . $valormedia . "</td>";
                                    $xls .= "<td class=xl26 x:num=\"" . str_replace(",", ".", $valormedia) . "\">" . str_replace(".", ",", $valormedia) . "</td>";
                                }
                            }
                            $echo .= "</tr>";
                            $xls .= "</tr>";
                        }


                        break;
                }
                $echo .= "</table>";


                $letra=GetLetter($ncols+1);
                $xls.=" <tr height=17 style='height:12.75pt'>
                  <td height=17 style='height:12.75pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
                   id=\"_x0000_t201\" coordsize=\"21600,21600\" o:spt=\"201\" path=\"m,l,21600r21600,l21600,xe\">
                   <v:stroke joinstyle=\"miter\"/>
                   <v:path shadowok=\"f\" o:extrusionok=\"f\" strokeok=\"f\" fillok=\"f\"
                    o:connecttype=\"rect\"/>
                   <o:lock v:ext=\"edit\" shapetype=\"t\"/>
                  </v:shapetype><v:shape id=\"_x0000_s1026\" type=\"#_x0000_t201\" style='position:absolute;
                   margin-left:1.5pt;margin-top:.75pt;width:738pt;height:4in;z-index:2'
                   fillcolor=\"window [78]\" strokecolor=\"windowText [77]\" o:insetmode=\"auto\">
                   <v:fill color2=\"windowText [77]\"/>
                   <o:lock v:ext=\"edit\" rotation=\"t\" text=\"t\"/>
                   <x:ClientData ObjectType=\"Chart\">
                    <x:WebChart>
                     <x:Scaling>
                      <x:ScaleID>0</x:ScaleID>
                      <x:Orientation>MinMax</x:Orientation>
                     </x:Scaling>
                     <x:Scaling>
                      <x:ScaleID>1</x:ScaleID>
                      <x:Orientation>MinMax</x:Orientation>
                     </x:Scaling>
                     <x:Chart>
                      <x:Name>Gr�fico 2</x:Name>
                      <x:Title>
                       <x:Caption>
                        <x:DataSource>-1</x:DataSource>
                        <x:Data>&quot;<x:Font></x:Font>&quot;</x:Data>
                       </x:Caption>
                       <x:Font>
                        <x:FontName>Arial</x:FontName>
                        <x:Size>8</x:Size>
                        <x:AutoScale/>
                       </x:Font>
                       <x:Border>
                        <x:ColorIndex>None</x:ColorIndex>
                       </x:Border>
                       <x:Interior>
                        <x:ColorIndex>None</x:ColorIndex>
                       </x:Interior>
                      </x:Title>
                      <x:Options>
                       <x:SizeWithWindow/>
                      </x:Options>
                      <x:PageSetup>
                       <x:ChartSize>FullPage</x:ChartSize>
                       <x:Header Margin=\"0.49212598499999999\"/>
                       <x:Footer Margin=\"0.49212598499999999\"/>
                       <x:PageMargins Bottom=\"0.984251969\" Left=\"0.78740157499999996\"
                        Right=\"0.78740157499999996\" Top=\"0.984251969\"/>
                      </x:PageSetup>
                      <x:Font>
                       <x:FontName>Arial</x:FontName>
                       <x:Size>8</x:Size>
                       <x:AutoScale/>
                      </x:Font>
                      <x:Left>0</x:Left>
                      <x:Top>-509.9853515625</x:Top>
                      <x:Width>14774.9853515625</x:Width>
                      <x:Height>6284.970703125</x:Height>
                      <x:ChartGrowth>
                       <x:HorzGrowth>1</x:HorzGrowth>
                       <x:VertGrowth>1.0588226318359375</x:VertGrowth>
                      </x:ChartGrowth>
                      <x:PlotArea>
                       <x:Left>609.4681457519531</x:Left>
                       <x:Top>769.90891113281248</x:Top>
                       <x:Width>14014.073605957032</x:Width>
                       <x:Height>4650.8783203125004</x:Height>
                       <x:Border>
                        <x:ColorIndex>15</x:ColorIndex>
                        <x:LineStyle>Solid</x:LineStyle>
                        <x:Weight>Narrow</x:Weight>
                       </x:Border>
                       <x:Interior>
                        <x:ColorIndex>14</x:ColorIndex>
                        <x:BGColorIndex>Neutral</x:BGColorIndex>
                       </x:Interior>
                       <x:Font>
                        <x:FontName>Arial</x:FontName>
                        <x:Size>8</x:Size>
                        <x:AutoScale/>
                       </x:Font>
                       <x:Graph>
                        <x:Type>Column</x:Type>
                        <x:SubType>Clustered</x:SubType>
                        <x:Overlap>0</x:Overlap>
                        <x:ScaleID>0</x:ScaleID>
                        <x:ScaleID>1</x:ScaleID>";


                //if ($tbinfo['tipo'] == 2) {
                for ($n=$titu+1; $n <= $x_lin; $n++) {
                    $xls.= "
                    <x:Series>
                     <x:Index>$n</x:Index>
                     <x:Caption>
                      <x:DataSource>0</x:DataSource>
                      <x:Data>Plan1!\$A\$".$n."</x:Data>
                     </x:Caption>
                     <x:Name></x:Name>
                     <x:Category>
                      <x:DataSource>0</x:DataSource>
                      <x:Data>Plan1!\$B$".$titu.":\$".$letra."$".$titu."</x:Data>
                     </x:Category>
                     <x:Value>
                      <x:DataSource>0</x:DataSource>
                      <x:Data>Plan1!\$B$".$n.":\$".$letra."$".$n."</x:Data>
                     </x:Value>
                    </x:Series>";
                }

                //}

                $xls.= "
                <x:DataLabels>
                <x:Border>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Border>
                <x:Font>
                <x:FontName>Arial</x:FontName>
                <x:Size>8</x:Size>
                <x:AutoScale/>
                </x:Font>
                <x:Interior>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Interior>
                <x:Number>
                <x:SourceLinked/>
                <x:BuiltInFormat>0</x:BuiltInFormat>
                </x:Number>
                <x:ShowValue/>
                </x:DataLabels>
                <x:PlotVisible/>
                </x:Graph>
                <x:Axis>
                <x:Placement>Bottom</x:Placement>
                <x:AxisID>0</x:AxisID>
                <x:ScaleID>0</x:ScaleID>
                <x:Title>
                <x:Caption>
                <x:DataSource>-1</x:DataSource>
                <x:Data>&quot;<x:Font>Meses</x:Font>&quot;</x:Data>
                </x:Caption>
                <x:Font>
                <x:FontName>Arial</x:FontName>
                <x:Size>8</x:Size>
                <x:AutoScale/>
                </x:Font>
                <x:Border>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Border>
                <x:Interior>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Interior>
                </x:Title>
                <x:CrossingAxis>1</x:CrossingAxis>
                <x:Font>
                <x:FontName>Arial</x:FontName>
                <x:Size>8</x:Size>
                <x:AutoScale/>
                </x:Font>
                <x:Number>
                <x:SourceLinked/>
                <x:BuiltInFormat>0</x:BuiltInFormat>
                </x:Number>
                <x:Type>Category</x:Type>
                </x:Axis>
                <x:Axis>
                <x:Delete/>
                <x:Placement>Left</x:Placement>
                <x:AxisID>1</x:AxisID>
                <x:ScaleID>1</x:ScaleID>
                <x:MajorGridlines/>
                <x:Title>
                <x:Caption>
                <x:DataSource>-1</x:DataSource>
                <x:Data>&quot;<x:Font>Ordens</x:Font>&quot;</x:Data>
                </x:Caption>
                <x:Font>
                <x:FontName>Arial</x:FontName>
                <x:Size>8</x:Size>
                <x:AutoScale/>
                </x:Font>
                <x:Border>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Border>
                <x:Interior>
                <x:ColorIndex>None</x:ColorIndex>
                </x:Interior>
                </x:Title>
                <x:CrossingAxis>0</x:CrossingAxis>
                <x:CrossesAt>Minimum</x:CrossesAt>
                <x:CrossBetween>Between</x:CrossBetween>
                <x:Type>Value</x:Type>
                </x:Axis>
                </x:PlotArea>
                <x:Legend>
                <x:Placement>Bottom</x:Placement>
                <x:Font>
                <x:FontName>Arial</x:FontName>
                <x:Size>8</x:Size>
                <x:AutoScale/>
                </x:Font>
                </x:Legend>
                </x:Chart>
                </x:WebChart>
                </x:ClientData>
                </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
                position:absolute;z-index:2;margin-left:2px;margin-top:1px;width:985px;
                height:385px'><![endif]><![if !vml]></span><![endif]><span style='mso-ignore:
                vglayout2'>
                <table cellpadding=0 cellspacing=0>
                <tr>
                <td height=17 width=120 style='height:12.75pt;width:90pt'></td>
                </tr>
                </table>
                </span></td>
                <td colspan=14 style='mso-ignore:colspan'></td>
                </tr>
                <tr height=374 style='height:280.5pt;mso-xlrowspan:22'>
                <td height=374 colspan=15 style='height:280.5pt;mso-ignore:colspan'></td>
                </tr>";

                $titu=$x_lin+26;

                if ($info_filtro) {
                    $titu=$titu+2;
                    $tmp_ncols=$ncols+1;
                    $echo .= "<p align=\"left\" style=\"font-size:9px\"><b>FILTROS:</b><br>$info_filtro</ul></p>";
                    $xls.= "<tr height=17 style='height:12.75pt;font-size:9px'>
                    <td class=xl26 style='font-size:9px' colspan=$tmp_ncols><b>FILTROS:</b><br>$info_filtro</ul></td></tr>";
                }
            }
            // Finalizo, limpo as variaveis e inicio outra tabela
            $echo .= "<br / ><br />";
            $xls.= "<tr height=17 style='height:30.75pt;font-size:9px'>
            <td class=xl26 style='font-size:9px' colspan=$tmp_ncols>&nbsp;</td></tr>";

            unset($ncols);
            unset($sql_filtro);
            unset($info_filtro);
            unset($info_data);
            unset($erro);
        }
    }
    $i++;
    if ($_REQUEST['exp']){
        if ($xls_erro == 1) $echo .= "<script>alert('Ocorreram erros durante o processo, favor verifique as mensagens no quadro abaixo.')</script>";
        else {
            $_SESSION['ag']['xls']=$xls;
            $echo .= "<iframe name=\"exporta\" src=\"vis.php?arq=".$_GET['arq']."&xls=1\" width=\"500\" height=\"600\" frameborder=\"0\" scrolling=\"yes\"> </iframe>";
        }
    }
    if ($_GET['print']) {
        $basedir = '../..';
        if ($_GET['print'] == 'word') exportar_word($tbinfo['titulo'],$filtro_relatorio,$iim,$echo,1);
        else relatorio_padrao($tbinfo['titulo'],$filtro_relatorio,$iim,$echo,1);
    }
    else echo "$echo";
}

if (!$_GET['print']) echo "</div></body></html>";
?>
