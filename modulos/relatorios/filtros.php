<?
/**
* Define os filtros de uma tabela do gr�fico
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 

if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/delcascata.php")) die ($ling['bd01']);
elseif (!require("funcoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("conf.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">
<div id=\"div_formulario_corpo\">";
$tb=(int)$_GET['tb'];
$arq=urldecode($_GET['arq']);
$arq_envia=urlencode($_GET['arq']);
if ($arq == "") {
	erromsg("Selecione um arquivo primeiro.");
}
if ($tb == 0) {
	erromsg("Selecione uma tabela primeiro.");
}
else {
	$dir="../../".$manusis['dir']['graficos'];
	$xml = new DOMDocument();
	$xml->formatOutput = true;
	$xml -> load("$dir/$arq");
	$descricao_grafico=$xml -> getElementsByTagName('descricao');
	$descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
	$tabela_dados=AG_InfoTabela($xml,$tb);
	echo "
<h2 style=\"font-size:15px\">$descricao_grafico</h2>
<form method=\"POST\" action=\"\" enctype=\"multipart/form-data\">
<fieldset><legend>Edi��o dos Filtros</legend>";
	echo "<label for=\"campo\">Campo</label>
	<select name=\"campo\" id=\"campo\" class=\"campo_select\" onchange=\"atualiza_area('mostra_selecao','parametros.php?op=5&tb=$tb&arq=".$_GET['arq']."&campo=' + document.getElementById('campo').options[document.getElementById('campo').selectedIndex].value)\">";
	echo "<option value=\"\"> </option>\n";
	foreach ($AG_filtros as $valor => $des) {
		echo "<option value=\"$valor\">".$des['DESCRICAO']."</option>\n";
	}
	echo "</select><br clear=\"all\" />
<label for=\"condicao\">Condi��o</label>	
<select name=\"condicao\" id=\"condicao\" class=\"campo_select\">";
	foreach ($AG_cond as $valor => $des) {
		echo "<option value=\"$valor\">$des</option>\n";
	}
	echo "</select>
	<div id=\"mostra_selecao\" style=\"text-align:left\"></div><br clear=\"all\" />
	<input class=\"botao\" type=\"button\" name=\"adicionar_submit\" value=\"Adicionar Filtro\" onclick=\"atualiza_area('tb_filtros','parametros.php?op=6&arq=$arq_envia&tb=$tb&campo=' + document.getElementById('campo').options[document.getElementById('campo').selectedIndex].value + '&condicao=' + document.getElementById('condicao').options[document.getElementById('condicao').selectedIndex].value + '&valor=' + document.getElementById('valor').options[document.getElementById('valor').selectedIndex].value)\" />
	</fieldset>
	<fieldset><legend>Filtros Definidos</legend>
	<div id=\"tb_filtros\" style=\"text-align:left\">";
	echo "
		<div id=\"lt_tabela\"><table id=\"lt_tabela\">
		<tr>
		<th>Operador</th>
		<th>Campo</th>
		<th>Condi&ccedil;&atilde;o</th>
		<th>Valor</th>		
		<th width=\"22\">Op&ccedil;&otilde;es</th>
		</tr>";
	$tabela=AG_PegaTabela($xml,$tb);
	$params = $tabela->getElementsByTagName('filtro');
	$i=0;
	foreach ($params as $param) {
		$info_filtro=AG_InfoFiltro($xml,$tb,$i);
		$tmp=VoltaRelacao(ORDEM_PLANEJADO,$info_filtro['campo']);
		if ($tmp) {
			$tmp=htmlentities(VoltaValor($tmp['tb'],$tmp['campo'],$tmp['mid'],$info_filtro['valor'],$tmp['dba']));
		}
		else {
			$tmp=htmlentities($info_filtro['valor']);
		}
		if ($info_filtro['operador'] == 1){
			$selected_1="checked=\"checked\"";
			$selected_2="";
		}
		elseif ($info_filtro['operador'] == 2){
			$selected_2="checked=\"checked\"";
			$selected_1="";
		}
		echo "<tr class=\"cor1\"><td align=\"left\">";
		if ($i != 0) {
			echo "<input type=\"radio\" name=\"ope$i\" $selected_1 value=\"1\" onclick=\"atualiza_area('tb_filtros','parametros.php?op=6&arq=".$_GET['arq']."&ope=1&tb=$tb&oq=$i')\"> E";
			echo "<input type=\"radio\" name=\"ope$i\" $selected_2 value=\"2\" onclick=\"atualiza_area('tb_filtros','parametros.php?op=6&arq=".$_GET['arq']."&ope=2&tb=$tb&oq=$i')\"> OU";
		}
		echo "</td>
			<td align=\"left\">".$AG_filtros[$info_filtro['campo']]['DESCRICAO']."</td>
			<td align=\"left\">".$AG_cond[$info_filtro['condicao']]."</td>
			<td align=\"left\">".$tmp."</td>
			<td><a href=\"javascript:atualiza_area('tb_filtros','parametros.php?op=6&arq=".$_GET['arq']."&del=1&oq=$i&tb=$tb')\"> <img src=\"../../imagens/icones/22x22/del.png\" border=0 align=\"middle\" alt=\"Excluir\" title=\"Excluir\" />
			</td>
			</tr>";
		$i++;
	}

	echo "</table></div>";
	echo "</div>";
	echo "</fieldset>";
}
echo "</div></body></html>";

?>