<?php
/**
 * Gera gr�ficos do assistente de gr�ficos que comecem com "ge_". *
 *
 * @author Felipe Matos Malinoski
 * @version 1.0
 *
 *
 * */

// Debug?????
$debug = (int) $_GET['debug'];

// O que mostrar
$id  = (int) $_GET['id'];
$op  = (int) $_GET['op'];
$exe = (int) $_GET['exe'];
$exe = ($exe == 0)? 1 : $exe;
$grupo = (int) $_GET['grupo'];

// Filtros
$datai = (empty($_GET['datai']))? date('d/m/Y',mktime(0,0,0,date('m'),date('d')-30,date('Y'))) : $_GET['datai'];
$dataf = (empty($_GET['dataf']))? date('d/m/Y') : $_GET['dataf'];


$filtro_emp   = (int) $_GET['filtro_emp'];
$filtro_area  = (int) $_GET['filtro_area'];
$filtro_setor = (int) $_GET['filtro_setor'];
$filtro_maq   = (int) $_GET['filtro_maq'];
$filtro_classe = (int) $_GET['filtro_classe'];
$filtro_top = ((int) $_GET['filtro_top'] == 0)? 1 : (int) $_GET['filtro_top'];
$st = (! $st)? $_GET['st'] : $st;

// Para montar a imagem
$small = (int) $_GET['small'];
if($small) {
    $width = 485;
    $height = 300;
    $margin_bottom = 50;
}
else {
    $width = 1000;
    $height = 500;
    $margin_bottom = 150;
}


if ($st == 11) {
    // Montando XML do Arquivo
    //Header("Content-Type: application/xhtml+xml");
    $Navegador = array (
    "MSIE",
    "OPERA",
    "MOZILLA",
    "NETSCAPE",
    "FIREFOX",
    "SAFARI"
    );

    $info[browser] = "OTHER";
    foreach ($Navegador as $parent) {
        $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
        $f = $s + strlen($parent);
        $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
        $version = preg_replace('/[^0-9,.]/','',$version);
        if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
            $tmp_navegador[browser] = $parent;
            $tmp_navegador[version] = $version;
        }
    }

    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
<body><div id=\"cab_relatorio\">
<h1>Relat&oacute;rios Gerenciais</h1>
</div>";    
}

if (($exe == 1)) {
    // Arquivos necess�rios para trabalhar com gr�ficos
    include_once("modulos/relatorios/conf.php");
    include_once("modulos/relatorios/funcoes.php");


    $style_div = ($_GET['bot_data'] != "")? "style=\"display:none\"" : "";
    
    // Para dar certo os filtros
    $_GET['dir'] = ".";

    echo "<div id=\"lt\">
    <br clear=\"all\" />
    <div id=\"lt_cab\">
    <h3 onclick=\"abre_div('corpo_relatorio', 'filtro_area')\" style=\"cursor:pointer;\" title=\"Clique para ver os filtros.\">" . htmlentities("Relat�rios Gerenciais") . "</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div id=\"corpo_relatorio\" $style_div>
    <fieldset>
    <legend>Filtros</legend>    
    <form action=\"\" style=\"border:0; margin:0; padding:0;\" method=\"GET\">
    <input type=\"hidden\" name=\"id\" value=\"$id\" />
    <input type=\"hidden\" name=\"op\" value=\"$op\" />
    <input type=\"hidden\" name=\"exe\" value=\"$exe\" />
    <input type=\"hidden\" name=\"oq\" value=\"$oq\" />
    <input type=\"hidden\" name=\"st\" value=\"$st\" />
    <input type=\"hidden\" name=\"grupo\" value=\"$grupo\" />
    <div id=\"filtro_relatorio\">";

    // FILTROS DO RELATORIO
    FiltrosRelatorio(1, 1, 1, 1);

    echo "</div>";

    //echo "<br clear=\"all\" />";


    echo "<div>";
    echo "<label class=\"campo_label \"for=\"filtro_classe\">".$tdb[MAQUINAS_CLASSE]['DESC'].":</label>";
    FormSelectD("DESCRICAO", "", MAQUINAS_CLASSE, $_GET['filtro_classe'], "filtro_classe", "filtro_classe", "MID", "");

    echo "</div>
    <br clear=\"all\" />";

    echo "
    <div>
    <label for=\"campo_data\">Data a considerar</label>
    <select name=\"campo_data\" id=\"campo_data\" class=\"campo_select\">
        <option value=\"DATA_INICIO\"      ".(($_GET['campo_data'] == 'DATA_INICIO')?"selected=\"selected\"":'').">{$tdb[ORDEM]['DATA_INICIO']}</option>
        <option value=\"DATA_FINAL\"       ".(($_GET['campo_data'] == 'DATA_FINAL')?"selected=\"selected\"":'').">{$tdb[ORDEM]['DATA_FINAL']}</option>
        <option value=\"DATA_PROG\"        ".(($_GET['campo_data'] == 'DATA_PROG')?"selected=\"selected\"":'').">{$tdb[ORDEM]['DATA_PROG']}</option>
        <option value=\"DATA_ABRE\"        ".(($_GET['campo_data'] == 'DATA_ABRE')?"selected=\"selected\"":'').">{$tdb[ORDEM]['DATA_ABRE']}</option>
        <option value=\"DATA_APONTAMENTO\" ".(($_GET['campo_data'] == 'DATA_APONTAMENTO')?"selected=\"selected\"":'').">Data Apontamento</option>
    </select>
    </div><br clear=\"all\" />
    
    <div>";

    FormData("Data in&iacute;cio:", "datai", $datai);

    echo "</div><br clear=\"all\" />
    
    
    <div>";
    FormData("Data fim:", "dataf", $dataf);

    echo "</div><br clear=\"all\" />";

    $check1 = ($filtro_top == 1)? "checked=\"checked\"" : "";
    $check2 = ($filtro_top == 2)? "checked=\"checked\"" : "";

    echo "<div>
    <label class=\"campo_label \"for=\"filtro_top1\">Top 10:</label>
    <input type=\"radio\" name=\"filtro_top\" id=\"filtro_top1\" $check1 value=\"1\" /> N&atilde;o
    <input type=\"radio\" name=\"filtro_top\" id=\"filtro_top2\" $check2 value=\"2\" /> Sim
    </div>
    <br clear=\"all\" />
<label for=\"bot_data\"></label><input type=\"submit\" name=\"bot_data\" id=\"bot_data\" value=\"Enviar\" class=\"botao\" />
</form>
</fieldset>
</div>";


    if ($_GET['bot_data'] != "") {
        echo "<table width=\"99%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">
<tr height=\"18\">";
        // Buscando todos os grupos
        $sql = "SELECT * FROM " . GRUPO_GRAFICO . " ORDER BY DESCRICAO ASC";
        if (! $rs =$dba[0]->Execute($sql)){
            erromsg("Arquivo: graficos_ger.php <br />" . $dba[0]->ErrorMsg() . "<br />" . $sql);
        }

        $url = "manusis.php?id=$id&op=$op&oq=$oq&st=$st&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_classe=$filtro_classe&datai=$datai&dataf=$dataf&campo_data={$_GET['campo_data']}&filtro_top=$filtro_top&bot_data={$_GET['bot_data']}";
        $i = 1;
        while (! $rs->EOF) {
            if (($grupo == 0) and ($i == 1)) {
                $grupo = $rs->fields['MID'];
            }
            
            $sel = ($grupo == $rs->fields['MID'])? "_sel" : "";

            echo "<th class=\"tabs_col1$sel\"><a class=\"tabs_a\" href=\"$url&grupo=" . $rs->fields['MID'] . "\">" . $rs->fields['DESCRICAO'] . "</a></th>";

            $rs->MoveNext();
            $i++;
        }

        echo "</tr>\n
        <tr><td colspan=\"$i\" class=\"tabs_col1\">\n
        <center>\n";

        $dir = "arquivos/graficos/";

        if ($handle = opendir($dir)) {
            /* This is the correct way to loop over the directory. */
            while ((false !== ($file = readdir($handle)))) {

                $tipo = explode("_", $file);

                if (($file == ".") || ($file == "..") || ($file == "Thumb.db") || ($file == ".svn") || ($tipo[0] != "ge")) {
                    continue;
                }

                $filename = explode(".", $file);
                $filename = $filename[0];

                $xml = new DOMDocument();
                $xml -> formatOutput = true;
                $xml -> load("$dir/$file");

                $descricao_grafico = $xml -> getElementsByTagName('descricao');
                $descricao_grafico = utf8_decode($descricao_grafico -> item(0) -> nodeValue);
                $param = $xml->getElementsByTagName('tabela')->item(0);
                //            $param = $params->item(0);

                $tbid=$param -> getAttribute('id');
                $tabela=AG_PegaTabela($xml,$tbid);
                $tbinfo=AG_InfoTabela($xml,$tbid);

                // Mostra apenas os gr�ficos do grupo selecionado
                if($tbinfo['grupo'] == $grupo){
                    // Colunas
                    echo "<label for=\"coltop_$filename\"><strong>Ordenar por:</strong></label>&nbsp;&nbsp;\n
                         <select id=\"coltop_$filename\" id=\"coltop_$filename\" class=\"campo_select\" onchange=\"document.getElementById('imggraf_$filename').src = 'modulos/relatorios/graficos_ger.php?exe=2&arq=$file&datai=$datai&dataf=$dataf&campo_data={$_GET['campo_data']}&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_classe=$filtro_classe&filtro_top=$filtro_top&coltop=' + this.value + '&colsentido=' + document.getElementById('colsentido_$filename').value\">\n";
                    $i = 0;
                    foreach ($tbinfo['coluna_dados'] as $col => $valor) {
                        echo "<option value=\"$i\">" . htmlentities($valor) . "</option>";
                        $i++;
                    }

                    echo "</select>&nbsp;&nbsp;&nbsp;&nbsp;\n";
                    // Sentido
                    echo "<label for=\"colsentido_$filename\"><strong>Sentido:</strong></label>&nbsp;&nbsp;\n
                          <select id=\"colsentido_$filename\" id=\"colsentido_$filename\" class=\"campo_select\" onchange=\"document.getElementById('imggraf_$filename').src = 'modulos/relatorios/graficos_ger.php?exe=2&arq=$file&datai=$datai&dataf=$dataf&campo_data={$_GET['campo_data']}&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_classe=$filtro_classe&filtro_top=$filtro_top&coltop=' + document.getElementById('coltop_$filename').value + '&colsentido=' + this.value\">\n";
                    echo "<option value=\"1\">Crescente</option>\n";
                    echo "<option value=\"2\">Decrescente</option>\n";
                    echo "</select>\n";

                    $ano = explode("/", $datai);
                    $ano = $ano[2];

                    echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"modulos/relatorios/vis.php?arq=$file&datai_1=$datai&dataf_1=$dataf&ano_1=$ano&tdata_1={$_GET['campo_data']}&datai_2=$datai&dataf_2=$dataf&ano_2=$ano&tdata_2={$_GET['campo_data']}&datai_3=$datai&dataf_3=$dataf&ano_3=$ano&tdata_3={$_GET['campo_data']}&print=nav&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_classe=$filtro_classe\" target=\"_blank\" title=\"Ver dados assistente\">Ver informa&ccedil;&otilde;es</a>\n";

                    echo "<br clear=\"all\">\n";

                    echo "<img id=\"imggraf_$filename\"  src=\"modulos/relatorios/graficos_ger.php?exe=2&arq=$file&datai=$datai&dataf=$dataf&campo_data={$_GET['campo_data']}&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_classe=$filtro_classe&filtro_top=$filtro_top&small=$small\" />\n";

                    echo "<br clear=\"all\" />\n";
                    echo "<br clear=\"all\" />\n";
                }
            }
        }

        echo "</center>\n
        </td></tr>\n
        </table>\n";
    }


    if ($st == 11) {
        echo "</body>
        </html>";

    }
}

elseif ($exe == 2) {

    // Fun��es do Sistema
    if (!require_once("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
    // Configura��es
    elseif (!require_once("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
    // Idioma
    elseif (!require_once("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
    // Biblioteca de abstra��o de dados
    elseif (!require_once("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
    // Informa��es do banco de dados
    elseif (!require_once("../../lib/bd.php")) die ($ling['bd01']);
    elseif (!require_once("../../lib/delcascata.php")) die ($ling['bd01']);

    // Defini��es necess�rias

    // Arquivos necess�rios
    // Arquivos necess�rios para trabalhar com gr�ficos
    include_once("conf.php");
    include_once("funcoes.php");
    include ("../../lib/jpgraph/src/jpgraph.php");
    include ("../../lib/jpgraph/src/jpgraph_line.php");
    include ("../../lib/jpgraph/src/jpgraph_bar.php");

    // Corres para as colunas e linhas
    $cor   = array();
    $cor[] = 'steelblue';
    $cor[] = 'brown';
    $cor[] = 'goldenrod3';
    $cor[] = 'darkseagreen4';
    $cor[] = 'snow3';
    $cor[] = 'deepskyblue3';
    $cor[] = 'lightgreen';
    $cor[] = 'lightseagreen';
    $cor[] = 'lightpink';
    $cor[] = 'lemonchiffon4';
    $cor[] = 'lightcyan4';
    $cor[] = 'lightblue4';
    $cor[] = 'navy';
    $cor[] = 'palegreen4';
    $cor[] = 'peru';
    $cor[] = 'yellow';
    $cor[] = 'yellowgreen';
    $cor[] = 'yellow4';
    $cor[] = 'sienna';
    $cor[] = 'orange';
    $cor[] = 'limegreen';
    $cor[] = 'beige';
    $cor[] = 'chocolate4';
    $cor[] = 'darkorchid';
    $cor[] = 'black';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';
    $cor[] = 'white';

    
    // CASO SEJA UM GR�FICO PEQUENO CORTAR MAIS AS DESCRI��ES
    $max_desc = 30;
    if($small) {
        $max_desc = 15;
    }
    

    // Filtros
    $sql_filtro2 = "";
    $sql_filtro_maq = "";
    $texto_filtro = "";
    
    if ($filtro_maq != 0) {
        $sql_filtro2 .= " AND MID_MAQUINA = '$filtro_maq'";
        $texto_filtro[] = unhtmlentities($tdb[ORDEM]['MID_MAQUINA']) . ": " . VoltaValor(MAQUINAS, "DESCRICAO", "MID", $filtro_maq, 0);
    }
    elseif ($filtro_setor != 0) {
        $sql_filtro2 .= " AND MID_SETOR = '$filtro_setor'";
        $texto_filtro[] = unhtmlentities($tdb[ORDEM]['MID_SETOR']) . ": " . VoltaValor(SETORES, "DESCRICAO", "MID", $filtro_setor, 0);
    }
    elseif ($filtro_area != 0) {
        $sql_filtro2 .= " AND MID_AREA = '$filtro_area'";
        $texto_filtro[] = unhtmlentities($tdb[ORDEM]['MID_AREA']) . ": " . VoltaValor(AREAS, "DESCRICAO", "MID", $filtro_area, 0);
    }
    elseif ($filtro_emp != 0) {
        $sql_filtro2 .= " AND MID_EMPRESA = '$filtro_emp'";
        $texto_filtro[] = unhtmlentities($tdb[ORDEM]['MID_EMPRESA']) . ": " . VoltaValor(EMPRESAS, "DESCRICAO", "MID", $filtro_emp, 0);
    }

    if ($filtro_classe != 0) {
            $sql_filtro2 .= " AND MID_CLASSE = '$filtro_classe'";
            $texto_filtro[] = unhtmlentities($tdb[MAQUINAS_CLASSE]['DESC']) . ": " . VoltaValor(MAQUINAS_CLASSE, "DESCRICAO", "MID", $filtro_classe, 0);
    }


    $file = $_GET['arq'];
    $dir = "../../arquivos/graficos";

    $xml = new DOMDocument();
    $xml -> formatOutput = true;
    $xml -> load("$dir/$file");

    $descricao_grafico = $xml -> getElementsByTagName('descricao');
    $descricao_grafico = utf8_decode($descricao_grafico -> item(0) -> nodeValue);
    $param = $xml->getElementsByTagName('tabela')->item(0);


    $tbid=$param -> getAttribute('id');
    $tabela=AG_PegaTabela($xml,$tbid);
    $tbinfo=AG_InfoTabela($xml,$tbid);
    // Campo data que ser� usado para pesquisa ex: DATA_INICIO
    //$tbdatat="DATA_PROG";
    $tbdatat=$_GET['campo_data'];
    if ($tbinfo['tipo'] == 1){
        $tbdatai = (empty($_GET['datai']))? date('d/m/Y',mktime(0,0,0,date('m'),date('d')-30,date('Y'))) : $_GET['datai'];
        $tbdataf = (empty($_GET['dataf']))? date('d/m/Y') : $_GET['dataf'];

        $tmp_datai=explode("/",$tbdatai);
        $tmp_dataf=explode("/",$tbdataf);

        $tbano = $tmp_datai[2];
        
        // montagem de sql para a op��o de data de apontamento //
        if($tbdatat == "DATA_APONTA"){
            $sql_filtro = "WHERE " . ORDEM_MADODEOBRA . ".DATE_FORMAT(`DATA_INICIO`,'%Y') = '$tbano' AND " . ORDEM_MADODEOBRA . ".DATE_FORMAT(`DATA_FINAL`,'%Y') = '$tbano'";

        }
        else {
            $sql_filtro="WHERE DATE_FORMAT(`$tbdatat`,'%Y') = '$tbano' ";
        }
        
        $datai="$tbano-01-01";
        $dataf="$tbano-12-31";

        $info_data="Ano: $tbano";
    }
    elseif ($tbinfo['tipo'] == 2){
        $tbdatai = (empty($_GET['datai']))? date('d/m/Y',mktime(0,0,0,date('m'),date('d')-30,date('Y'))) : $_GET['datai'];
        $tbdataf = (empty($_GET['dataf']))? date('d/m/Y') : $_GET['dataf'];

        $tmp_datai=explode("/",$tbdatai);
        $tmp_dataf=explode("/",$tbdataf);
        if (!checkdate((int)$tmp_datai[1],(int)$tmp_datai[0],(int)$tmp_datai[2])) {
            erromsg("A data - $tbdatai � inv�lida");
            $erro=1;
            $xls_erro=1;
        }
        elseif (!checkdate((int)$tmp_dataf[1],(int)$tmp_dataf[0],(int)$tmp_dataf[2])) {
            erromsg("A data - $tbdataf � inv�lida");
            $erro=1;
            $xls_erro=1;
        }
        // Aqui eu uso DATE_FORMAT, pois n�o quero ficar com codigo mais limpo
        $info_data="Per�odo de: $tbdatai � $tbdataf";
        $datai=explode("/",$tbdatai);
        $datai=$datai[2]."-".$datai[1]."-".$datai[0];
        $dataf=explode("/",$tbdataf);
        $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
        
        // montagem de sql para a op��o de data de apontamento //
        if($tbdatat == "DATA_APONTA"){
            $sql_filtro= ORDEM_MADODEOBRA . ".DATA_INICIO >= '$datai' AND " . ORDEM_MADODEOBRA . ".DATA_FINAL <= '$dataf'";
        }
        else {
            $sql_filtro="WHERE $tbdatat >= '$datai' AND $tbdatat <= '$dataf' ";
        }
        
    }

    if (!$tbinfo['coluna_mestre']){
        erromsg("Nenhuma Coluna Mestre foi definida, favor use a op��o editar tabela para modificar suas informa��es");
        $erro=1;
        $xls_erro=1;
    }
    if (!$tbinfo['coluna_dados']){
        erromsg("Nenhuma Coluna de Dados foi definida, favor use a op��o editar tabela para modificar suas informa��es");
        $erro=1;
        $xls_erro=1;
    }
    if (!$erro) {
        // Monto o restante dos filtros definidos pelo usuario
        $filtros = $tabela ->getElementsByTagName('filtro');
        $ifiltro = 0;
                
        $if_operador=0;
        // Filtros passados a coluna mestre
        $filtro_adicional = array();

        foreach ($filtros as $filtro) {
            $cond=$filtro -> getAttribute('condicao');
            $condicao=$AG_cond_sinal[$cond];
            $campo=$filtro ->getElementsByTagName('campo') ->  item(0) -> nodeValue;
            $valor=$filtro ->getElementsByTagName('valor') -> item(0) -> nodeValue;
            $operador=$filtro ->getElementsByTagName('operador') -> item(0) -> nodeValue;
            
            // Uma entrada para o filtro SQL outra para Apresenta��o dos filtros usados
            if ($if_operador == 0){
                // Campo existe na OS
                if($tdb[ORDEM][$campo]) {
                    $sql_filtro.=" AND ($campo $condicao '$valor' ";
                }
                // Campo l�gico
                elseif($AG_filtros[$campo]['COND']) {
                    if(is_array($AG_filtros[$campo]['COND'])) {
                        $sql_filtro.=" AND (" . $AG_filtros[$campo]['COND'][$valor];
                    }
                    else {
                        $sql_filtro.=" AND (" . str_replace('%%COND%%', $condicao, str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']));
                    }
                }
            }
            else {
                // Campo existe na OS
                if($tdb[ORDEM][$campo]) {
                    $sql_filtro.=" ".$AG_oper_sinal[$operador]." $campo $condicao '$valor' ";
                }
                // Campo l�gico
                elseif($AG_filtros[$campo]['COND']) {
                    if(is_array($AG_filtros[$campo]['COND'])) {
                        $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . $AG_filtros[$campo]['COND'][$valor];
                    }
                    else {
                        $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . str_replace('%%COND%%', $condicao, str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']));
                    }
                }
            }
            
            if ($campo == "MID_EQUIPE") {
                $filtro_adicional['MID_EQUIPE'][] = array($AG_oper_sinal[$operador], $condicao, $valor);
            }
            
            // Pego rela��o do campo deo filtro na tabela ORDEM e mostro resultado
            $tmp_filtro=VoltaRelacao(ORDEM_PLANEJADO,$campo);
            $tmp_filtro=VoltaValor($tmp_filtro['tb'],$tmp_filtro['campo'],$tmp_filtro['mid'],$valor,$tmp_filtro['dba']);
            $texto_filtro[] = $AG_oper[$operador] . " " . unhtmlentities($AG_filtros[$campo]['DESCRICAO']) . " " .  $AG_cond[$cond] . " " . $tmp_filtro;

            $if_operador++;
        }
        
        if ($if_operador != 0) {
            $sql_filtro.=")";
        }

        // Formatando o filtro
        $texto_filtro = ($texto_filtro != "")? $info_data . "\n" . implode(" ", $texto_filtro) : $info_data;


        $sql_filtro_maq .= ($sql_filtro_maq == "")? "" : ")";

        // Executo o SQL
        if ($tbdatat == "DATA_APONTA") {
                $sql="SELECT ".ORDEM.".* FROM ".ORDEM_PLANEJADO.", " . ORDEM_MADODEOBRA . " WHERE " . ORDEM_MADODEOBRA . ".MID_ORDEM = ".ORDEM_PLANEJADO.".MID AND $sql_filtro $sql_filtro2 GROUP BY ".ORDEM.".MID";
            }
            else {
                $sql="SELECT * FROM ".ORDEM_PLANEJADO." O $sql_filtro $sql_filtro2";
            }

        $resultado=$dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql);
        if (!$resultado) {
            erromsg("Ocorreu um erro durante o processamento dessa consulta: <br />".$dba[$tdb[ORDEM_PLANEJADO]['dba']] ->ErrorMsg()."<br />SQL: $sql");
            $xls_erro=1;
        }
        else {
            // Se n�o tiver o que mostrar fecho aqui e mostro uma imagem
            if (($resultado->EOF) && !(
            ($tbinfo['coluna_mestre'] == MAQUINAS) &&
            ($tbinfo['coluna_dados']['mtbf'] != "" || $tbinfo['coluna_dados']['mtbf2'] != "" || $tbinfo['coluna_dados']['dis_cal'] != "" || $tbinfo['coluna_dados']['dis_ope'] != "")
            )){
                if (! $debug) {
                    include("../../imagens/alerta_grafico.png");
                    exit();
                }
                else {
                    echo erromsg("Nenhum dado encontrado");
                }
            }

            // CARREGO O TITULO DAS TABELAS
            // POR EVOLU��O ANUAL
            if ($tbinfo['tipo'] == 1){
                // Titulo das colunas de dados (meses)
                $ordena_ano=1;
                $xnomes = array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
            }
            else {
                $xnomes = array();
            }

            // Processo o resultado
            $tmp    = AG_ColunaDados($resultado,$xml,$tbid,$tbdatat,$datai,$dataf, $sql_filtro_maq);
            $mostra = $tmp['campos']; // Colunas que contem resultado
            $dados  = $tmp['dados']; // Dados das colunas

            $i = 0;
            if ($tbinfo['tipo'] != 1) {
                foreach ($tbinfo['coluna_dados'] as $col => $valor) {
                    $barras[$i]['legenda'] = $valor;
                    $barras[$i]['modo']    = $tbinfo['coluna_dados_modo'][$col];
                    $barras[$i]['escala']  = $tbinfo['coluna_dados_escala'][$col];
                    $barras[$i]['unidade'] = $AG_colDados[$col]['UNIDADE'];
                    $i++;
                }
            }
            else {
                // FILTROS ADICIONAIS
                $filtro_sql = "";
                if (($tbinfo['coluna_mestre'] == FUNCIONARIOS) and (count($filtro_adicional['MID_EQUIPE']) > 0)) {
                    foreach ($filtro_adicional['MID_EQUIPE'] as $valores) {
                        $filtro_sql .= ($filtro_sql == "")? " WHERE " : " " . $valores[0] . " ";
                        $filtro_sql .= "EQUIPE  " . $valores[1] . " " . $valores[2];
                    }
                }
                
                // Busco na tabela da coluna mestre as informa��es recolhidas
                $sql="SELECT * FROM ".$tbinfo['coluna_mestre'] . "" . $filtro_sql;
                $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);

                if (!$resultado2) {
                    // Deu revestrez!
                    erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                    $xls_erro=1;
                }
                else {
                    while (!$resultado2->EOF) {
                        $campo=$resultado2->fields;
                        if ($mostra[$campo['MID']]){
                            if ($AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]) {
                                $nome = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]] . "-" . $campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]], 0, $max_desc);
                                $nome .= (strlen($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]] . "-" . $campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]]) > $max_desc)? "." : "";
                            }
                            else {
                                $nome = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]], 0, $max_desc);
                                $nome .= (strlen($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]]) > $max_desc)? "." : "";
                            }
                            //usado depois para ver se precisa de m�dia
                            $col_dados = "";
                            foreach ($tbinfo['coluna_dados'] as $col => $valor) {
                                if (! in_array($col, $AG_colMedias)) {
                                    $barras[$campo['MID']]['legenda'] = $nome;
                                    $barras[$campo['MID']]['modo']    = $tbinfo['coluna_dados_modo'][$col];
                                    $barras[$campo['MID']]['escala']  = $tbinfo['coluna_dados_escala'][$col];
                                    $barras[$campo['MID']]['unidade'] = $AG_colDados[$col]['UNIDADE'];
                                }
                                else {
                                    $barras[0]['legenda'] = $valor;
                                    $barras[0]['modo']    = $tbinfo['coluna_dados_modo'][$col];
                                    $barras[0]['escala']  = $tbinfo['coluna_dados_escala'][$col];
                                    $barras[0]['unidade'] = $AG_colDados[$col]['UNIDADE'];
                                }
                                $col_dados = $col;
                            }
                        }
                        $resultado2->MoveNext();
                    }
                }
            }



            switch ($tbinfo['coluna_mestre_tipo']){
                case 1:
                    // Busco na tabela da coluna mestre as informa��es recolhidas
                    $sql="SELECT * FROM ".$tbinfo['coluna_mestre'];

                    $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);

                    if (!$resultado2) {
                        // Deu revestrez!
                        erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                        $xls_erro=1;
                    }
                    else {
                        while (!$resultado2->EOF) {
                            $campo=$resultado2->fields;
                            if ($mostra[$campo['MID']]){
                                // Nome das colunas
                                if ($tbinfo['tipo'] != 1) {
                                    if ($AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]) {
                                        $nome = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]] . "-" . $campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]], 0, $max_desc);
                                        $nome .= (strlen($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]] . "-" . $campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][1]]) > $max_desc)? "." : "";
                                    }
                                    else {
                                        $nome = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]], 0, $max_desc);
                                        $nome .= (strlen($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]]) > $max_desc)? "." : "";
                                    }

                                    $xnomes[] = $nome;
                                    if (is_array($dados)) {
                                        foreach ($dados as $k => $dcol) {
                                            if (! $dcol[$campo['MID']]) {
                                                $dcol[$campo['MID']] = 0;
                                            }
                                            $barras[$k]['dados'][] = (float) $dcol[$campo['MID']];
                                        }
                                    }

                                }
                                else {
                                    foreach ($dados as $k => $dcol) {
                                        if (! $dcol[$campo['MID']]) {
                                            $dcol[$campo['MID']] = 0;
                                        }
                                        if (! in_array($col_dados, $AG_colMedias)) {
                                            $barras[$campo['MID']]['dados'][$k-1] = (float) $dcol[$campo['MID']];
                                        }
                                        else {
                                            $barras[0]['dados'][$k-1] += (float) $dcol[$campo['MID']];
                                        }
                                        $total[$k-1]++;

                                    }
                                }
                            }
                            //fim desta linha da tabela
                            $resultado2->MoveNext();
                        }
                    }
                    break;

                case 2:

                    // Busco na tabela da coluna mestre as informa��es recolhidas
                    $sql="SELECT * FROM ".PROGRAMACAO_TIPO;

                    $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                    if (!$resultado2) {
                        // Deu revestrez!
                        erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                        $xls_erro=1;
                    }
                    else {
                        while (!$resultado2->EOF) {
                            $campo=$resultado2->fields;
                            if ($mostra["P" . $campo['MID']]){
                                if ($tbinfo['tipo'] != 1) {
                                    $xnomes[] = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]], 0, $max_desc);

                                    foreach ($dados as $k => $dcol) {
                                        if (! $dcol["P".$campo['MID']]) {
                                            $dcol["P".$campo['MID']] = 0;
                                        }

                                        $barras[$k]['dados'][] = (float) $dcol["P".$campo['MID']];
                                    }
                                }
                                else {
                                    foreach ($dados as $k => $dcol) {
                                        if (! $dcol["P" . $campo['MID']]) {
                                            $dcol["P" . $campo['MID']] = 0;
                                        }

                                        if (! in_array($col_dados, $AG_colMedias)) {
                                            $barras[$campo['MID']]['dados'][$k-1] = (float) $dcol["P".$campo['MID']];
                                        }
                                        else {
                                            $barras[0]['dados'][$k-1] += (float) $dcol["P".$campo['MID']];
                                        }
                                        $total[$k-1]++;
                                    }
                                }


                            }
                            $resultado2->MoveNext();
                        }
                    }

                    $sql="SELECT * FROM ".TIPOS_SERVICOS;

                    $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                    if (!$resultado2) {
                        // Deu revestrez!
                        erromsg("Ocorreu um erro durante o processamento da coluna mestre: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                        $xls_erro=1;
                    }
                    else {
                        while (!$resultado2->EOF) {
                            $campo=$resultado2->fields;
                            if ($mostra[$campo['MID']]){
                                if ($tbinfo['tipo'] != 1) {
                                    if(! $small) {
                                        $xnomes[] = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]], 0, $max_desc);
                                    }
                                    else {
                                        $xnomes[] = substr($campo[$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'][0]], 0, 2);
                                    }

                                    foreach ($dados as $k => $dcol) {
                                        if (! $dcol[$campo['MID']]) {
                                            $dcol[$campo['MID']] = 0;
                                        }

                                        $barras[$k]['dados'][] = (float) $dcol[$campo['MID']];
                                    }
                                }
                                else {
                                    foreach ($dados as $k => $dcol) {
                                        if (! $dcol[$campo['MID']]) {
                                            $dcol[$campo['MID']] = 0;
                                        }
                                        if (! in_array($col_dados, $AG_colMedias)) {
                                            $barras[$campo['MID']]['dados'][$k-1] = (float) $dcol[$campo['MID']];
                                        }
                                        else {
                                            $barras[0]['dados'][$k-1] += (float) $dcol[$campo['MID']];
                                        }
                                        $total[$k-1]++;

                                    }
                                }
                            }
                            $resultado2->MoveNext();
                        }
                    }


                    break;
            }

            // Quando for top 10 mostrar 6 ultimos meses
            if(($filtro_top == 2) and ($ordena_ano)) {
                $mf = (int) $tmp_dataf[1] - 1;
                $mi = ($mf > 5)? $mf - 5 : 0;
            }

            // Fazendo com que todos as colunas ou meses apare�am
            foreach ($barras as $kb => $bar){
                if(($filtro_top == 2) and ($ordena_ano == 1)) {
                    unset($barras[$kb]['dados']);
                }

                foreach ($xnomes as $k => $mes){
                    if(($filtro_top == 2) and ($ordena_ano == 1)) {
                        if($k >= $mi and $k <= $mf) {
                            $barras[$kb]['dados'][] = $bar['dados'][$k];
                        }
                    }
                    else {
                        if(! isset($barras[$kb]['dados'][$k])){
                            $barras[$kb]['dados'][$k] = 0;
                        }
                    }
                }
                if ($tbinfo['tipo'] == 1) {
                    if (in_array($col_dados, $AG_colMedias)) {
                        foreach ($bar['dados'] as $kd => $valor){
                            $barras[$kb]['dados'][$kd] = round($valor / $total[$kd], 2);
                        }
                    }
                }
            }

            if(($filtro_top == 2) and ($ordena_ano)) {
                foreach ($xnomes as $ii => $mtmp) {
                    if($ii >= $mi and $ii <= $mf) {
                        $xnomes_tmp[] = $mtmp;
                    }
                }
                $xnomes = $xnomes_tmp;
            }
        }
    }

    if ($debug) {
        echo "<pre>";
        echo "Nome:<br />";
        var_dump($xnomes);
        echo "Barras:<br />";
        var_dump($barras);
        echo "Dados:<br />";
        var_dump($dados);
        echo "</pre>";
    }


    // Ordena��o para o top 1
    // Somente sen�o for evolu��o anual
    if (($filtro_top == 2) and ($ordena_ano != 1)){
        AG_Sort($barras, $xnomes, (int) $_GET['coltop'], 10, (int) $_GET['colsentido']);
        $descricao_grafico .= " - Top 10";
    }
    elseif ($ordena_ano != 1){
        AG_Sort($barras, $xnomes, (int) $_GET['coltop'], 50, (int) $_GET['colsentido']);
    }

    // Criando o gr�fico
    $graph = new Graph($width, $height, "auto");
    $graph->SetScale("textlin");
    $graph->SetMarginColor('#EFEFEF');
    
    // Cor de preenchimento
    if(! $small) {
        $graph->img->SetMargin(45,55,50,150);
    }
    else {
        $graph->img->SetMargin(30,30,20,100);
    }

    // INCLUINDO FILTROS e periodo
    if (! $small) {
        $txt = new Text($texto_filtro);
        $txt->SetFont(FF_ARIAL,FS_NORMAL,9);
        $txt->SetPos(45, 23, "left", "top");
        $txt->SetColor("gray2");
        $graph->AddText($txt);
    }

    // Legenda
    $graph->legend->SetFillColor('#E0E0E0@0.3');
    $graph->legend->Pos(0.5,0.95,"center","bottom");
    $graph->legend->SetLayout(LEGEND_VERT);
    $graph->legend->SetColumns(10);
    
    
    if(! $small) {
        $graph->legend->SetLineSpacing(6);
        $graph->legend->SetFont(FF_ARIAL,FS_NORMAL,8);
    }
    else {
        $graph->legend->SetLineSpacing(4);
        $graph->legend->SetFont(FF_ARIAL,FS_NORMAL,7);
    }
    
    // Titulo do Gr�fico
    $graph->title->Set($descricao_grafico);
    
    
    if(! $small) {
        $graph->title->SetMargin(10);
        $graph->title->SetFont(FF_ARIAL,FS_BOLD,12);
    }
    else {
        $graph->title->SetFont(FF_ARIAL,FS_BOLD,10);
    }

    // Create the bar plot
    $i = 0;
    $ib = 0;
    foreach ($barras as $bar) {
        if ($bar['modo'] == "column") {
            if (! $bar['dados']) {
                $bar['dados'] = array(0);
            }
            // Salvando a primeira unidade para colocar no gr�fico
            if ($ib == 0) {
                $unidade = $bar['unidade'];
            }

            $bplot[$ib] = new BarPlot($bar['dados']);
            $bplot[$ib]->SetFillColor($cor[$i]);
            $bplot[$ib]->SetLegend($bar['legenda']);
            $bplot[$ib]->SetWidth(0.7);
            $bplot[$ib]->SetValuePos('center');
            $bplot[$ib]->value->Show();
            if (! $small) {
                $bplot[$ib]->value->SetFont(FF_ARIAL,FS_NORMAL,7);
            }
            else {
                $bplot[$ib]->value->SetFont(FF_ARIAL,FS_NORMAL,6);
            }
            $bplot[$ib]->value->SetColor('black');
            $bplot[$ib]->value->SetAngle(45);

            $ib++;
            $i++;
        }
    }

    // Agrupando colunas
    if (count($bplot) > 1) {
        $bgroup = new GroupBarPlot($bplot);
        $graph->Add($bgroup);
    }
    elseif($bplot) {
        $graph->Add($bplot[0]);
    }

    // Mostro todos as linhas
    $il = 0;
    foreach ($barras as $bar) {
        if ($bar['modo'] == "line") {
            if (! $bar['dados']) {
                $bar['dados'] = array(0);
            }

            $lplot[$il] = new LinePlot($bar['dados']);
            $lplot[$il]->SetColor($cor[$i]);
            $lplot[$il]->SetWeight(2);
            $lplot[$il]->SetLegend($bar['legenda']);
            $lplot[$il]->SetBarCenter();

            $lplot[$il]->mark->SetType(MARK_CIRCLE);
            $lplot[$il]->mark->SetColor($cor[$i]);
            $lplot[$il]->mark->SetWidth(3);
            $lplot[$il]->mark->SetWeight(4);

            $lplot[$il]->value->Show();
            
            if(! $small) {
                $lplot[$il]->value->SetFont(FF_ARIAL,FS_NORMAL,7);
            }
            else {
                $lplot[$il]->value->SetFont(FF_ARIAL,FS_NORMAL,6);
            }
            
            $lplot[$il]->value->SetAngle(45);
            $lplot[$il]->value->SetColor('black');

            if ($bar['escala'] == 1) {
                $graph->AddY(0,$lplot[$il]);
                $graph->SetYScale(0, "lin");
                $graph->ynaxis[0]->SetColor($cor[$i]);
                
                if(! $small) {
                    $graph->ynaxis[0]->SetFont(FF_ARIAL,FS_NORMAL, 9);
                    $graph->ynaxis[0]->SetFont(FF_ARIAL,FS_NORMAL, 9);
                }
                else {
                    $graph->ynaxis[0]->SetFont(FF_ARIAL,FS_NORMAL, 7);
                    $graph->ynaxis[0]->SetFont(FF_ARIAL,FS_NORMAL, 7);
                }
                
                $graph->ynaxis[0]->SetTitleMargin(30);
                $graph->ynaxis[0]->scale->SetGrace(10);
                $graph->ynaxis[0]->title->Set($bar['unidade']);
                $graph->ynaxis[0]->title->SetColor($cor[$i]);
            }
            else {
                $graph->Add($lplot[$il]);
            }

            $il++;
            $i++;
        }
    }

    if(! $small) {
        $graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL, 9);
        $graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL, 9);
    }
    else {
        $graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL, 7);
        $graph->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL, 7);
    }
    
    $graph->yaxis->title->Set($unidade);
    
    $graph->yaxis->SetTitleMargin(30);
    $graph->yaxis->scale->SetGrace(10);

    $graph->xaxis->SetTickLabels($xnomes);
    $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL, 7);
    $graph->xaxis->SetLabelAngle(45);



    // Display the graph
    if (! $debug) {
        $graph->Stroke();
    }
}




?>
