<?
/**
* Cria, Abre ou Anexa um gr�fico do Manusis
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 


if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/delcascata.php")) die ($ling['bd01']);

// ID e OP do ManuSis
$id_man = (int) $_GET['id_man'];
$op_man = (int) $_GET['op_man'];


echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">
<div id=\"div_formulario_corpo\">";
// Adicionando Gr�ficos
if ($_POST['evi_anexar']) {
    $arquivo = isset($_FILES['uuimg']) ? $_FILES['uuimg'] : FALSE;
    $arquivo_final=strtolower($arquivo['name']);
    $arquivo_final=str_replace(" ","_",$arquivo_final);
    $arquivo_final=explode(".",$arquivo_final);
    if ($arquivo_final[1] != "xml") {
        erromsg("Formato de arquivo Inv�lido");
    }
    elseif ($arquivo['size'] > $manusis['upload']['tamanho']) {
        erromsg($ling['err03']." ".de_bytes($manusis['upload']['tamanho']));
    }
    else {
        $grafico_dir = "../../".$manusis['dir']['graficos']."/".$arquivo_final[0].".".$arquivo_final[1];
        if (!move_uploaded_file($arquivo['tmp_name'], $grafico_dir)) {
            erromsg($ling['err05']."<strong> $grafico_dir (".$arquivo['tmp_name'].")</strong>" );
        }
    }
}
if ($_POST['evi_novo']) {
    $nome=strtolower(LimpaTexto($_POST['nome_arq']));
    $nome=explode(".",$nome);
    $nome=$nome[0].".xml";
    $c=array('�','�','�','�','�','�','�','�','�','�');
    $nome=str_replace("","_",$nome);
    $nome=str_replace($c,"_",$nome);
    $titulo=LimpaTexto($_POST['titulo_arq']);
    $grupo=LimpaTexto($_POST['grupo_arq']);

    if ($nome == "" || $titulo == "" || $grupo == "") {
        erromsg("Favor preencher os campos Nome, Titulo e Grupo do Gr�fico");
    }
    if (file_exists("../../".$manusis['dir']['graficos']."/$nome")){
        erromsg("J� existe um arquivo com esse nome");
    }
    else {
        // Abrindo um documento xml
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;

        // Cria um n� principal chamado <grafico>
        $xml_grafico = $xml->createElement('grafico');
        $xml_grafico = $xml->appendChild($xml_grafico);

        // dentro do <grafico> coloco a <descricao> e <data_criacao>
        $xml_descricao = $xml->createElement('descricao',utf8_encode($titulo));
        $xml_descricao = $xml_grafico->appendChild($xml_descricao);

        $xml_data = $xml->createElement('data_criacao',utf8_encode(date('d/m/Y')));
        $xml_data = $xml_grafico->appendChild($xml_data);

        $xml_grupo = $xml->createElement('grupo', $grupo);
        $xml_grupo = $xml_grafico->appendChild($xml_grupo);

        if ($xml -> save("../../".$manusis['dir']['graficos']."/$nome")){
            echo "<br />".$ling['doc_graf_criado']."<br /> <br />";
        }
        else {
            erromsg("".$ling['erro_grava_doc']."");
        }
    }
}

echo "<form method=\"POST\" action=\"\" enctype=\"multipart/form-data\">
<fieldset><legend>".$ling['anexar_graf']."</legend>
<input type=\"file\" id=\"anexar\" name=\"uuimg\" size\"40\" class=\"campo_text\" />&nbsp;
<input type=\"submit\" name=\"evi_anexar\" class=\"botao\" value=\"Adicionar Gr�fico\" />
</fieldset>
<fieldset><legend>".$ling['novo_graf']."</legend>
<label for=\"nome_arq\" class=\"campo_label\">".$ling['nome_do_arq']."</label>
<input type=\"text\" name=\"nome_arq\" id=\"nome_arq\" class=\"campo_text\" size=\"10\" maxlenght=\"12\" />
<br clear=\"all\" />
<label for=\"titulo_arq\" class=\"campo_label\">".$ling['titu_graf']."</label>
<input type=\"text\" name=\"titulo_arq\" id=\"titulo_arq\" class=\"campo_text\" size=\"20\" />
<br clear=\"all\" />
<label for=\"grupo_arq\" class=\"campo_label\">".$ling['grupo_graf']."</label>";

FormSelectD("DESCRICAO", "", GRUPO_GRAFICO, "", "grupo_arq", "grupo_arq", "MID", "");

echo "<br clear=\"all\" />
<input type=\"submit\" name=\"evi_novo\" class=\"botao\" value=\"Novo Gr�fico\" />
</fieldset>
<br clear=\"all\" />
<fieldset><legend>".$ling['abrir_grafi']."</legend>";
$dir="../../".$manusis['dir']['graficos'];
if (!is_dir("../../".$manusis['dir']['graficos'])) {
    erromsg("Esse diretorio n�o foi configurado corretamente");
}
echo "\n<div id=\"lt_tabela\">
<table width=\"100%\">
<tr><th width=\"1%\">&nbsp;</th><th width=\"20%\">Gr�fico</th><th width=\"50%\">".$ling['agenda_titulo']."</th><th width=\"9%\">&nbsp;</th></tr>";
$od = opendir($dir);
$graficos = array();

while (false !== ($arq = readdir($od))) {
    $dd=explode(".",$arq);
    if (($dd[1] == "xml")) {
        $xml = new DOMDocument();
        $xml -> load("$dir/$arq");

        $titulo = $xml -> getElementsByTagName('descricao');
        $titulo = utf8_decode($titulo -> item(0) -> nodeValue);

        $grupo = $xml -> getElementsByTagName('grupo');
        $grupo = (int) $grupo -> item(0) -> nodeValue;

        $grupos[$grupo][] = array($arq, $titulo);
    }
}

closedir($od);

$grupo_ant = 0;

foreach ($grupos as $grupo => $graficos) {
    if ($grupo != $grupo_ant) {
        echo "<tr class=\"cor3\">
        <td align=\"left\" colspan=\"4\">
        <img src=\"../../imagens/icones/mais.gif\" onclick=\"abre_div('grupo_$grupo')\" />
        <strong>" . VoltaValor(GRUPO_GRAFICO, "DESCRICAO", "MID", $grupo, 0) . "</strong>
        </td>
        </tr>
        <tr>
        <td colspan=\"4\" style=\"padding:0;\">
        <div id=\"grupo_$grupo\" style=\"display:block;padding:0; width:100%\">
        <table width=\"100%\" style=\"border:0;padding:0;\">";
    }
    
    foreach ($graficos as $i => $grafico) {
        $arq = $grafico[0];
        $titulo = $grafico[1];
        
        echo "<tr class=\"cor1\">
        <td align=\"left\" width=\"1%\">
            <a href=\"$dir/$arq\" target=\"_blank\"><img src=\"../../imagens/icones/22x22/graficos.png\" border=\"0\" hspace=\"5\" alt=\" \" align=\"middle\" \></a>
        </td>
        <td align=\"left\" width=\"20%\">
            <a href=\"../../manusis.php?id=$id_man&op=$op_man&arq=$arq\" target=\"_parent\">$arq</a>
        </td>
        <td align=\"left\" width=\"50%\">
            <div id=\"graf_$i\"><input type=\"text\" size=\"55\" id=\"grafi_$i\" value=\"$titulo\" class=\"campo_text\" /></div>
        </td>
        <td align=\"center\" width=\"9%\">
            <a href=\"javascript:atualiza_area('graf_$i','parametros.php?id_man=$id_man&op_man=$op_man&op=7&arq=$arq&i=$i&titulo=' + document.getElementById('grafi_$i').value)\"> 
                <img src=\"../../imagens/icones/22x22/editar.png\" border=0 align=\"left\" alt=\"Editar\" title=\"Editar\" />
            </a>
            <a href=\"../../modulos/relatorios/arquivos.php?id_man=$id_man&op_man=$op_man&deleta=graficos&deletaarq=$arq&oq=1\" onclick=\"return confirma(this, 'VOC� TEM CERTEZA QUE DESEJA REMOVER ESSE ARQUIVO?')\">
                <img src=\"../../imagens/icones/22x22/del.png\" border=0 />
            </a>
        </td>
        </tr>\n";
    }
    
    if ($grupo != $grupo_ant) {
        echo "</table>
        </div>
        </td>
        </tr>\n";   
        $grupo_ant = $grupo;
    }
}

echo "</table></div>
</fieldset>";

echo "</div></body></html>";

?>
