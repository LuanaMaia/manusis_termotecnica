<?
/**
* Fun��es  do assistente de gr�ficos
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 

/**
 * Retorna o node xml de uma tabela do gr�fico, util para buscar configura��es de filtros e colunas
 * @param object $xml
 * @param integer $id
 * @return object
 */

function AG_PegaTabela($xml,$id){
	global $manusis;
	$params = $xml->getElementsByTagName('tabela');
	foreach ($params as $param) {
		if ($param -> getAttribute('id') == $id){
			return $param;
			break;
		}
	}
}

/**
 * Retorna todas informa��es de uma tabela num array
 * @param object $xml
 * @param integer $id
 * @return array
 */
function AG_InfoTabela($xml,$id){
	global $manusis;
	
	// Usado para relat�rios gerenciais
	$grupo=$xml->getElementsByTagName('grupo');
	$tmp['grupo']=$grupo -> item(0) -> nodeValue;
	
	$params = $xml->getElementsByTagName('tabela');
	foreach ($params as $param) {
		if ($param -> getAttribute('id') == $id){
			$tmp['tipo']=$param -> getAttribute('tipo');
			if ($tmp['tipo'] == 1) {
				$tmp['tipo_nome']="Evolu��o anual";
			}
			if ($tmp['tipo'] == 2) {
				$tmp['tipo_nome']="Per�odo";
			}
			$tmp['id']=$id;

			$titulo=$param->getElementsByTagName('titulo');
			$tmp['titulo']=utf8_decode($titulo -> item(0) -> nodeValue);

			

			$colmestre=$param->getElementsByTagName('colmestre');

			if ($colmestre -> item(0) ->  nodeValue) {
				$tmp['coluna_mestre']=$colmestre-> item(0) -> getAttribute('db_tb');
				$tmp['coluna_mestre_tipo']=$colmestre-> item(0) -> getAttribute('tipo');
				$tmp['coluna_mestre_descricao']=utf8_decode($colmestre-> item(0) ->  nodeValue);
			}
			$cols = $param->getElementsByTagName('col');
			foreach ($cols as $col) {
				$tmp['coluna_dados'][$col-> getAttribute('tipo')]=utf8_decode($col -> nodeValue);
				$tmp['coluna_dados_modo'][$col-> getAttribute('tipo')]=$col-> getAttribute('modo');
				$tmp['coluna_dados_escala'][$col-> getAttribute('tipo')]=(int) $col-> getAttribute('escala');
			}
		}
	}
	if ($tmp) {
		return $tmp;
	}
	else {
		return 0;
	}
}

/**
 * Deleta uma tabela do gr�fico
 * @param object $xml
 * @param integer $alvo
 */
function AG_DelTabela($xml,$alvo){
	global $manusis;

	$tabela=$xml->documentElement;
	$deleta=$tabela -> getElementsByTagName('tabela')->item($alvo);
	if ($tabela ->removeChild($deleta)){
		$xml -> save($xml ->documentURI);
	}
}
/**
 * Atualiza  colunas de dados da tabela de gr�fico
 * @param object $xml
 * @param object $xml_tabela
 * @param array $cols
 */
function AG_AtualizaColTabela($xml,$xml_tabela,$cols){
	global $manusis,$AG_colDados;
	$params = $xml_tabela->getElementsByTagName('col');
	foreach ($params as $param) {
		$xml_tabela ->removeChild($param);
	}

	$params = $xml_tabela->getElementsByTagName('col');
	foreach ($params as $param) {
		$xml_tabela ->removeChild($param);
	}
	if ($cols) {
		foreach ($cols as $param) {
			$col=$xml->createElement('col',utf8_encode(html_entity_decode($AG_colDados[$param]['DESCRICAO'])));
			$col -> setAttribute('tipo',$param);
			$col -> setAttribute('modo','column');
			$col -> setAttribute('escala','0');
			$col = $xml_tabela->appendChild($col);
		}
	}
}

/**
 * Atualiza  colunas de dados da tabela de gr�fico
 * @param object $xml
 * @param object $xml_tabela
 * @param array $cols
 */
function AG_AtualizaColTabelaGer($xml, $modo, $escala){
	global $manusis, $AG_colDados;
	$params = $xml->getElementsByTagName('col');
	foreach ($params as $param) {
		if ($modo[$param->getAttribute('tipo')]) {
			$param -> setAttribute('modo', $modo[$param->getAttribute('tipo')]);
		}
		if ($escala[$param->getAttribute('tipo')]) {
			$valor = ((int) $escala[$param->getAttribute('tipo')] == 1)? 0 : 1;
			$param -> setAttribute('escala', $valor);
		}
	}
}

/**
 * Enter description here...
 *
 * @param object $xml
 * @param int $grupo
 */
function AG_AtualizaGrupoTabelaGer($xml, $grupo){
	global $manusis, $AG_colDados;

	$grupo = ($grupo == 0)? 1 : $grupo;

	if ($xml->getElementsByTagName('grupo')->item(0)->nodeValue != "") {
		$tabela_grupo = $xml->getElementsByTagName('grupo')->item(0)->nodeValue = $grupo;
	}
	else {
		$tabela_grupo = $xml->createElement('grupo', $grupo);
		$tabela_grupo = $xml->appendChild($tabela_grupo);
	}
}


/**
 * Adiciona uma tabela no gr�fico
 * @param object $xml
 * @param string $titulo
 * @param integer$tipo
 */
function AG_AddTabela($xml, $titulo, $tipo){
	global $manusis;
	$xml->formatOutput = true;
	$id=(int)AG_GeraIDTabela($xml);

	$grafico=$xml -> getElementsByTagName('grafico')->item(0);

	$tabela=$xml -> createElement('tabela');
	$tabela -> setAttribute('id',$id);
	$tabela -> setAttribute('tipo',$tipo);
	$tabela=$grafico-> appendChild($tabela);

	$tabela_titulo = $xml->createElement('titulo',utf8_encode($titulo));
	$tabela_titulo = $tabela->appendChild($tabela_titulo);

//	$tabela_grupo = $xml->createElement('grupo', 1);
//	$tabela_grupo = $tabela->appendChild($tabela_grupo);

	$xml -> save($xml ->documentURI);

}


/**
 * Retorna um array com as informa��es de filtro de uma tabela
 * @param object $xml
 * @param integer $id
 * @param integer $node
 * @return array
 */
function AG_InfoFiltro($xml,$id,$node){
	global $manusis;
	$tabela=AG_PegaTabela($xml,$id);
	$params = $tabela->getElementsByTagName('filtro');
	$tmp['condicao']=$params -> item($node) -> getAttribute('condicao');
	$campo=$params-> item($node) ->getElementsByTagName('campo');
	$tmp['campo']=$campo -> item(0) -> nodeValue;
	$valor=$params-> item($node) -> getElementsByTagName('valor');
	$tmp['valor']=$valor -> item(0) -> nodeValue;
	$ope=$params-> item($node) -> getElementsByTagName('operador');
	$tmp['operador']=$ope -> item(0) -> nodeValue;
	return $tmp;
}


function AG_TrocaCondicao($xml,$tb,$node,$operador){
	global $manusis;
	$xml->formatOutput = true;
	$tabela=AG_PegaTabela($xml,$tb);
	$filtro=$tabela -> getElementsByTagName('filtro')->item($node);
	$ope_antigo=$filtro -> getElementsByTagName('operador') -> item(0);
	$ope_novo = $xml->createElement('operador',utf8_encode("$operador"));
	$ope_novo = $filtro->replaceChild($ope_novo,$ope_antigo);



	$xml -> save($xml ->documentURI);




}




/**
 * Retorna um array com as informa��es de filtro de uma tabela
 * @param object $xml
 * @param integer $tb
 * @param string $campo
 * @param integer $condicao
 * @param string $valor
 * @return array
 */
function AG_AddFiltro($xml, $tb, $campo, $condicao, $valor){
	global $manusis;

	$xml->formatOutput = true;
	$tabela=AG_PegaTabela($xml,$tb);
	$filtro=$xml -> createElement('filtro');
	$filtro -> setAttribute('condicao',$condicao);
	$filtro=$tabela-> appendChild($filtro);

	$filtro_campo = $xml->createElement('campo',utf8_encode($campo));
	$filtro_campo = $filtro->appendChild($filtro_campo);

	$filtro_valor = $xml->createElement('valor',utf8_encode($valor));
	$filtro_valor = $filtro->appendChild($filtro_valor);

	$filtro_ope = $xml->createElement('operador',utf8_encode("1"));
	$filtro_ope = $filtro->appendChild($filtro_ope);

	$xml -> save($xml ->documentURI);
}
/**
 * Deleta um filtro de uma tabela do gr�fico
 * @param object $xml
 * @param integer $tb
 * @param integer $alvo
 */
function AG_DelFiltro($xml,$tb, $alvo){
	global $manusis;
	// $xml = Parse DOM do arquivo XML
	// $alvo = posi��o do NODE que ser� excluido
	$xml->formatOutput = true;
	$tabela=AG_PegaTabela($xml,$tb);
	$deleta=$tabela -> getElementsByTagName('filtro') -> item($alvo);
	if ($tabela ->removeChild($deleta)){
		$xml -> save($xml ->documentURI);
	}
}
/**
 * Gera um indentificador unico e sequencial para tabela do gr�fico
 * @param object $xml
 * @return  integer
 */
function AG_GeraIDTabela($xml){
	global $manusis;
	// $xml = Parse DOM do arquivo XML
	$params = $xml->getElementsByTagName('tabela');
	$i=0;
	foreach ($params as $param) {
		$tmp=$param -> getAttribute('id');
		$i++;
	}

	if ($tmp == 0){
		return $tmp=1;
	}
	else {
		$tmp++;
		return $tmp;
	}
}
/**
 * Exporta uma gr�fico para o formato XLS (MS Excel)
 * @param string $titulo
 * @param string $plan
 * @return  string
 */
function  AG_ExportaXLS($titulo,$plan){
	global $manusis;
	$doc.="<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content=\"Manusis Web\">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author> Manusistem</o:Author>
  <o:LastAuthor> Manusistem</o:LastAuthor>
  <o:LastPrinted>2006-06-17T14:07:58Z</o:LastPrinted>
  <o:Created>2006-06-07T17:44:39Z</o:Created>
  <o:LastSaved>2006-06-17T14:09:43Z</o:LastSaved>
  <o:Company>Manusistem</o:Company>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
  <o:LocationOfComponents HRef=\"file:///D:\\\" />
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:\"\,\";
	mso-displayed-thousand-separator:\"\.\";}
@page
	{margin:.39in .2in .39in .2in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;
	mso-page-orientation:landscape;
	mso-horizontal-page-align:center;}
.font7
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	mso-number-format:\"\@\";}
.xl25
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	border:.5pt solid windowtext;
	background:gray;
	mso-pattern:auto none;}
.xl26
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	border:.5pt solid windowtext;}
.xl27
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	border:.5pt solid windowtext;
	background:gray;
	mso-pattern:auto none;}
.xl28
	{mso-style-parent:style0;
	font-size:8.0pt;
	text-align:center;
	border:.5pt solid windowtext;}
.xl29
	{mso-style-parent:style0;
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;}
.xl30
	{mso-style-parent:style0;
	font-size:14.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:\"\@\";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;}
.xl31
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-weight:700;
	mso-number-format:\"\@\";
	text-align:right;
	vertical-align:top;
	border:.5pt solid windowtext;
	white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Plan1</x:Name>
    <x:WorksheetOptions>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>300</x:HorizontalResolution>
      <x:VerticalResolution>300</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:TopRowVisible>2</x:TopRowVisible>
     <x:Panes>
      <x:Pane>
       <x:Number>3</x:Number>
       <x:ActiveRow>30</x:ActiveRow>
       <x:ActiveCol>3</x:ActiveCol>
      </x:Pane>
     </x:Panes>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
    <x:Sorting>
     <x:Sort>$ordenado</x:Sort>
     <x:Descending/>
    </x:Sorting>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>8580</x:WindowHeight>
  <x:WindowWidth>11340</x:WindowWidth>
  <x:WindowTopX>480</x:WindowTopX>
  <x:WindowTopY>45</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext=\"edit\" spidmax=\"1028\"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext=\"edit\">
  <o:idmap v:ext=\"edit\" data=\"1\"/>
 </o:shapelayout></xml><![endif]-->
</head><body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 width=987 style='border-collapse: collapse; table-layout:fixed;width:740pt'>
 <col width=120 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'>
 <col width=99 style='mso-width-source:userset;mso-width-alt:3620;width:74pt'>
 <col width=64 span=12 style='width:48pt'>
 <tr height=24 style='mso-height-source:userset;height:18.0pt'>
  <td colspan=2 rowspan=2 height=68 class=xl29 width=219 style='height:51.0pt;  width:164pt'><img src=\"".$manusis['url']."temas/padrao/imagens/logoempresa.png\" border=0 vspace=2 hspace=2 align=center></td>
  <td colspan=12 class=xl30 width=768 style='border-left:none;width:576pt'>$titulo</td>
 </tr>
 <tr class=xl24 height=44 style='mso-height-source:userset;height:33.0pt'>
  <td colspan=12 height=44 class=xl31 width=768 style='height:33.0pt;  border-left:none;width:576pt'>FILTROS:<font class=font7><br>
  $filtros
  </font></td></tr>
 <tr height=14 style='mso-height-source:userset;height:10.5pt'>
  <td height=14 colspan=14 style='height:10.5pt;mso-ignore:colspan'></td>
 </tr>
$plan
$graf
 <tr height=17 style='height:12.75pt'>
  <td colspan=14 height=17 class=xl31 style='border-right:.5pt solid black; height:12.75pt'>EMITIDO POR ".$_SESSION['ManuSess']['user']['NOME']." ".date("d/m/Y H:i")."<br />
</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=120 style='width:90pt'></td>
  <td width=99 style='width:74pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>
</body>
</html>";
	header("Content-type: application/msexcel");
	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header("Content-disposition: inline; filename=excel.xls");
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header("Pragma: public");
	echo $doc;
}
function GetLetter($num){
	if ($num == 1) return 'A';
	if ($num == 2) return 'B';
	if ($num == 3) return 'C';
	if ($num == 4) return 'D';
	if ($num == 5) return 'E';
	if ($num == 6) return 'F';
	if ($num == 7) return 'G';
	if ($num == 8) return 'H';
	if ($num == 9) return 'I';
	if ($num == 10) return 'J';
	if ($num == 11) return 'K';
	if ($num == 12) return 'L';
	if ($num == 13) return 'M';
	if ($num == 14) return 'N';
	if ($num == 15) return 'O';
	if ($num == 16) return 'P';
	if ($num == 17) return 'Q';
	if ($num == 18) return 'R';
	if ($num == 19) return 'S';
	if ($num == 20) return 'T';
	if ($num == 21) return 'U';
	if ($num == 22) return 'V';
	if ($num == 23) return 'W';
	if ($num == 24) return 'X';
	if ($num == 25) return 'Y';
	if ($num == 26) return 'Z';
	if ($num == 27) return 'AA';
	if ($num == 28) return 'AB';
	if ($num == 29) return 'AC';
	if ($num == 30) return 'AD';
	if ($num == 31) return 'AE';
	if ($num == 32) return 'AF';
	if ($num == 33) return 'AG';
	if ($num == 34) return 'AH';
	if ($num == 35) return 'AI';
	if ($num == 36) return 'AJ';
	if ($num == 37) return 'AK';
	if ($num == 38) return 'AL';
	if ($num == 39) return 'AM';
	if ($num == 40) return 'AN';
	if ($num == 41) return 'AO';
	if ($num == 42) return 'AP';
	if ($num == 43) return 'AQ';
	if ($num == 44) return 'AR';
	if ($num == 45) return 'AS';
	if ($num == 46) return 'AT';
	if ($num == 47) return 'AU';
	if ($num == 48) return 'AV';
	if ($num == 49) return 'AW';
	if ($num == 50) return 'AX';
}

/**
 * Ordena um array do maior para o menor.<br/> OBS: s� funciona com arrays no padr�o dos Gr�ficos gerenciais!!!!!!
 *
 * @author Felipe Matos Malinoski
 * @version 1.0
 * 
 * @param array $cols Array contendo os dados
 * @param array $labels Array com os labels do gr�fico
 * @param int $ordk Coluna base para ordena��o. Default: 0
 * @param int $top Caso queira fazer TOP, n�mero de dados. Default: 0
 */

function AG_Sort(&$cols, &$labels, $ordk = 0, $top = 0, $sentido = 0){
    if (count($cols[$ordk]['dados']) == 0) {
    	return;
    }
    
	foreach ($cols[$ordk]['dados'] as $k => $v) {
		$model["$k"] = $v;
	}
    
	if (($sentido == 0) || ($sentido == 1)) {
		arsort($model);
	}
	else {
	    asort($model);
	}
	

	$colsN = array();
	foreach ($cols as $i => $col) {
		$colsN[$i] = $col;
		$colsN[$i]['dados'] = array();
		$c = 0;
		foreach ($model as $k => $v) {
			if (($top > 0) && ($c >= $top)) {
				break;
			}
			$colsN[$i]['dados'][] = $col['dados'][$k];
			$c++;
		}
	}

	$cols = $colsN;
	$c = 0;
	$labelsN = array();
	foreach ($model as $k => $v) {
		if (($top > 0) && ($c >= $top)) {
			break;
		}
		$labelsN[] = $labels[$k];
		$c++;
	}

	$labels = $labelsN;
}

?>