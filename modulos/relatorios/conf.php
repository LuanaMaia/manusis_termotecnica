<?
/**
* Configura��o do Assitente de Gr�ficos
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 

$AG_colMestre[AREAS]['DESCRICAO']=$tdb[AREAS]['DESC']; // DESCRICAO DO CAMPO
$AG_colMestre[AREAS]['CAMPO_ORDEM']="MID_AREA"; // CAMPO NA TABELA ORDEM
$AG_colMestre[AREAS]['TIPO']=1; // PARA USAR EM CASOS ESPECIFICOS
$AG_colMestre[AREAS]['CAMPO'][0]="COD"; // CAMPO PARA APRESENTA��O EX: COD DESCRICAO
$AG_colMestre[AREAS]['CAMPO'][1]="DESCRICAO"; // CAMPO PARA APRESENTA��O EX: COD DESCRICAO

$AG_colMestre[SETORES]['DESCRICAO']=$tdb[SETORES]['DESC'];
$AG_colMestre[SETORES]['CAMPO_ORDEM']="MID_SETOR";
$AG_colMestre[SETORES]['TIPO']=1;
$AG_colMestre[SETORES]['CAMPO'][0]="COD";
$AG_colMestre[SETORES]['CAMPO'][1]="DESCRICAO";

$AG_colMestre[MAQUINAS]['DESCRICAO']=$tdb[MAQUINAS]['DESC'];
$AG_colMestre[MAQUINAS]['CAMPO_ORDEM']="MID_MAQUINA";
$AG_colMestre[MAQUINAS]['TIPO']=1;
$AG_colMestre[MAQUINAS]['CAMPO'][0]="COD";
$AG_colMestre[MAQUINAS]['CAMPO'][1]="DESCRICAO";

$AG_colMestre[CENTRO_DE_CUSTO]['DESCRICAO']=$tdb[CENTRO_DE_CUSTO]['DESC'];
$AG_colMestre[CENTRO_DE_CUSTO]['CAMPO_ORDEM']="CENTRO_DE_CUSTO";
$AG_colMestre[CENTRO_DE_CUSTO]['TIPO']=1;
$AG_colMestre[CENTRO_DE_CUSTO]['CAMPO'][0]="COD";
$AG_colMestre[CENTRO_DE_CUSTO]['CAMPO'][1]="DESCRICAO";

$AG_colMestre[NATUREZA_SERVICOS]['DESCRICAO']=$tdb[NATUREZA_SERVICOS]['DESC'];
$AG_colMestre[NATUREZA_SERVICOS]['CAMPO_ORDEM']="NATUREZA";
$AG_colMestre[NATUREZA_SERVICOS]['TIPO']=1;
$AG_colMestre[NATUREZA_SERVICOS]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[TIPOS_SERVICOS]['DESCRICAO']=$tdb[TIPOS_SERVICOS]['DESC'];
$AG_colMestre[TIPOS_SERVICOS]['CAMPO_ORDEM']="TIPO_SERVICO";
$AG_colMestre[TIPOS_SERVICOS]['TIPO']=2;
$AG_colMestre[TIPOS_SERVICOS]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[CAUSA]['DESCRICAO']=$tdb[CAUSA]['DESC'];
$AG_colMestre[CAUSA]['CAMPO_ORDEM']="CAUSA";
$AG_colMestre[CAUSA]['TIPO']=1;
$AG_colMestre[CAUSA]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[DEFEITO]['DESCRICAO']=$tdb[DEFEITO]['DESC'];
$AG_colMestre[DEFEITO]['CAMPO_ORDEM']="DEFEITO";
$AG_colMestre[DEFEITO]['TIPO']=1;
$AG_colMestre[DEFEITO]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[SOLUCAO]['DESCRICAO']=$tdb[SOLUCAO]['DESC'];
$AG_colMestre[SOLUCAO]['CAMPO_ORDEM']="SOLUCAO";
$AG_colMestre[SOLUCAO]['TIPO']=1;
$AG_colMestre[SOLUCAO]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[FUNCIONARIOS]['DESCRICAO']=$tdb[FUNCIONARIOS]['DESC'];
$AG_colMestre[FUNCIONARIOS]['CAMPO_ORDEM']="MID_FUNCIONARIO";
$AG_colMestre[FUNCIONARIOS]['TIPO']=1;
$AG_colMestre[FUNCIONARIOS]['CAMPO'][0]="NOME";

$AG_colMestre[EQUIPES]['DESCRICAO']=$tdb[EQUIPES]['DESC'];
$AG_colMestre[EQUIPES]['CAMPO_ORDEM']="MID_EQUIPE";
$AG_colMestre[EQUIPES]['TIPO']=1;
$AG_colMestre[EQUIPES]['CAMPO'][0]="DESCRICAO";

$AG_colMestre[MATERIAIS]['DESCRICAO']=$tdb[MATERIAIS]['DESC'];
$AG_colMestre[MATERIAIS]['CAMPO_ORDEM']="MID_MATERIAL";
$AG_colMestre[MATERIAIS]['TIPO']=1;
$AG_colMestre[MATERIAIS]['CAMPO'][0]="COD";
$AG_colMestre[MATERIAIS]['CAMPO'][1]="DESCRICAO";

// COLUNAS DE DADOS / INFORMA��ES
// USAR COMO CHAVE 3 DIGITOS DE CADA PALAVRA DA DESCRICAO
// EX: Quantidade de Interven��es -> qtd_int
// EX: Parada -> prd
// USAR SEMPRE CARACTERES MINUSCULOS.

$AG_colDados['ord_abr']['DESCRICAO']="Ordens Abertas"; // DESCRI��O DA COLUNA
$AG_colDados['ord_abr']['FORMATO']="decimal"; //FORMATO DE COLUNA
$AG_colDados['ord_abr']['TIPO']=1; // TIPO DA COLUNA MESTRE

$AG_colDados['ord_fec']['DESCRICAO']="Ordens Fechadas";
$AG_colDados['ord_fec']['FORMATO']="decimal";
$AG_colDados['ord_fec']['TIPO']=1;

$AG_colDados['sol_pen']['DESCRICAO']="Solicita��es Pendentes";
$AG_colDados['sol_pen']['FORMATO']="decimal";
$AG_colDados['sol_pen']['TIPO']=1;
$AG_colDados['sol_pen']['SEMOS']=1;

$AG_colDados['sol_ace']['DESCRICAO']="Solicita��es Aceitas";
$AG_colDados['sol_ace']['FORMATO']="decimal";
$AG_colDados['sol_ace']['TIPO']=1;
$AG_colDados['sol_ace']['SEMOS']=1;

$AG_colDados['sol_rej']['DESCRICAO']="Solicita��es Rejeitadas";
$AG_colDados['sol_rej']['FORMATO']="decimal";
$AG_colDados['sol_rej']['TIPO']=1;
$AG_colDados['sol_rej']['SEMOS']=1;


//  COLUNAS DE TEMPOS E INDICADORES
$AG_colDados['qtd_int']['DESCRICAO']="Quantidade Interver��es";
$AG_colDados['qtd_int']['FORMATO']="int";
$AG_colDados['qtd_int']['TIPO']=1;

$AG_colDados['tpo_pre']['DESCRICAO']="Tempo de M.O. Previsto";
$AG_colDados['tpo_pre']['FORMATO']="decimal";
$AG_colDados['tpo_pre']['TIPO']=1;

$AG_colDados['tpo_srv']['DESCRICAO']="Tempo de Servi�o";
$AG_colDados['tpo_srv']['FORMATO']="decimal";
$AG_colDados['tpo_srv']['TIPO']=1;

$AG_colDados['tpo_mao']['DESCRICAO']="Tempo de Servi�o HH";
$AG_colDados['tpo_mao']['FORMATO']="decimal";
$AG_colDados['tpo_mao']['TIPO']=1;

$AG_colDados['tpo_prd']['DESCRICAO']="Tempo de Parada";
$AG_colDados['tpo_prd']['FORMATO']="decimal";
$AG_colDados['tpo_prd']['TIPO']=1;

$AG_colDados['tpo_ope']['DESCRICAO']="Tempo de Opera��o Mensal";
$AG_colDados['tpo_ope']['FORMATO']="decimal";
$AG_colDados['tpo_ope']['UNIDADE']="";
$AG_colDados['tpo_ope']['TIPO']=1;
$AG_colDados['tpo_ope']['SEMOS']=1;

$AG_colDados['dis_ope']['DESCRICAO']="Disponibilidade por Tempo de Opera��o";
$AG_colDados['dis_ope']['FORMATO']="decimal";
$AG_colDados['dis_ope']['UNIDADE']="(%)";
$AG_colDados['dis_ope']['TIPO']=1;

$AG_colDados['dis_cal']['DESCRICAO']="Disponibilidade por Tempo Calend�rio";
$AG_colDados['dis_cal']['FORMATO']="decimal";
$AG_colDados['dis_cal']['UNIDADE']="(%)";
$AG_colDados['dis_cal']['TIPO']=1;

$AG_colDados['mtbf']['DESCRICAO']="MTBF por Tempo de Opera��o";
$AG_colDados['mtbf']['FORMATO']="decimal";
$AG_colDados['mtbf']['TIPO']=1;

$AG_colDados['mtbf2']['DESCRICAO']="MTBF por Tempo Calend�rio";
$AG_colDados['mtbf2']['FORMATO']="decimal";
$AG_colDados['mtbf2']['TIPO']=1;

$AG_colDados['tpo_mpr']['DESCRICAO']="Tempo M�dio Para Reparo (MTTR)";
$AG_colDados['tpo_mpr']['FORMATO']="decimal";
$AG_colDados['tpo_mpr']['TIPO']=1;

$AG_colDados['tpo_mad']['DESCRICAO']="Tempo de M�o de obra Dispon�vel ";
$AG_colDados['tpo_mad']['FORMATO']="decimal";
$AG_colDados['tpo_mad']['TIPO']=1;
$AG_colDados['tpo_mad']['SEMOS']=1;


// COLUNAS DE CUSTO

$AG_colDados['cus_mao']['DESCRICAO']="Custo de M�o de Obra";
$AG_colDados['cus_mao']['FORMATO']="decimal";
$AG_colDados['cus_mao']['TIPO']=1;

$AG_colDados['cus_mex']['DESCRICAO']="Custo Extra de M�o de Obra";
$AG_colDados['cus_mex']['FORMATO']="decimal";
$AG_colDados['cus_mex']['TIPO']=1;

$AG_colDados['con_mat']['DESCRICAO']="Consumo de Material";
$AG_colDados['con_mat']['FORMATO']="int";
$AG_colDados['con_mat']['TIPO']=1;

$AG_colDados['con_pre']['DESCRICAO']="Consumo de Material Previsto";
$AG_colDados['con_pre']['FORMATO']="int";
$AG_colDados['con_pre']['TIPO']=1;

$AG_colDados['con_lub']['DESCRICAO']="Consumo em Lubrifica��o";
$AG_colDados['con_lub']['FORMATO']="int";
$AG_colDados['con_lub']['TIPO']=1;

$AG_colDados['cus_mat']['DESCRICAO']="Custo de Material";
$AG_colDados['cus_mat']['FORMATO']="decimal";
$AG_colDados['cus_mat']['TIPO']=1;

$AG_colDados['out_cus']['DESCRICAO']="Outros Custos";
$AG_colDados['out_cus']['FORMATO']="decimal";
$AG_colDados['out_cus']['TIPO']=1;



/**
 * Processa o resultado de uma consulta organizando as colunas.
 * Retorna uma matriz com a Coluna Mestre e os valores de cada Coluna de Dados
 * @param object $dba_resultado 
 * @param object $xml
 * @param integer $tbid
 * @param string $campo_data
 * @param string $param_adicionais
 * @param string $datai 
 * @param string $dataf
 * @return array
 */
function AG_ColunaDados($dba_resultado,$xml,$tbid,$campo_data,$datai,$dataf,$param_adicionais="") {
    global $tdb, $dba, $AG_colMestre, $AG_colDados;
    $tbinfo=AG_InfoTabela($xml,$tbid);
    $col_mestre=$AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO_ORDEM'];
    $tipo_tabela=$tbinfo['tipo'];
    $pos=0;
    foreach ($tbinfo['coluna_dados'] as $tipo_coluna => $col) {
        $i = 0;

        while ((! $dba_resultado->EOF) or ($AG_colDados[$tipo_coluna]['SEMOS'] == 1 and $i == 0)) {
            $campo=$dba_resultado->fields;
            
            $mes=(int)$dba_resultado->UserDate($campo[$campo_data],'m');
            
            if ($col_mestre == "TIPO_SERVICO"){
                if ($campo['TIPO'] == 1) $campo[$col_mestre]="P1";
                if ($campo['TIPO'] == 2) $campo[$col_mestre]="P2";
            }
            
            switch ($tipo_coluna) {
                
                // QTD DE INTERVEN��ES
                case 'qtd_int' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        if ($tipo_tabela == 1){
                            $dados[$mes][$campo[$col_mestre]]++;
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]++;
                        }
                    }
                    break;
                    
                    // TEMPO DE SERVI�O
                case 'tpo_srv' :
                    if ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_FUNCIONARIO']]=1;
                            $tmp_mo = (VoltaTime($obj_campo['HORA_FINAL'],$tmp->UserDate($obj_campo['DATA_FINAL'],'d/m/Y')) - VoltaTime($obj_campo['HORA_INICIO'],$tmp->UserDate($obj_campo['DATA_INICIO'],'d/m/Y')))/(60*60);
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_FUNCIONARIO']] += $tmp_mo;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_FUNCIONARIO']] += $tmp_mo;
                            }
                            $tmp->MoveNext();
                        }
                    }
                    elseif ($col_mestre == "MID_EQUIPE"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $mid_equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);
                            $campos_mestres[$mid_equip]=1;
                            $tmp_mo = (VoltaTime($obj_campo['HORA_FINAL'],$tmp->UserDate($obj_campo['DATA_FINAL'],'d/m/Y')) - VoltaTime($obj_campo['HORA_INICIO'],$tmp->UserDate($obj_campo['DATA_INICIO'],'d/m/Y')))/(60*60);
                            if ($tipo_tabela == 1){
                                $dados[$mes][$mid_equip] += $tmp_mo;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$mid_equip] += $tmp_mo;
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        if ($campo[$col_mestre]) {
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += $campo['TEMPO_TOTAL'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += $campo['TEMPO_TOTAL'];
                            }
                        }
                    }
                    break;
                    
                    // TEMPO DE SERVI�O
                case 'tpo_pre' :
                    if ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute("SELECT * FROM ".ORDEM_MO_ALOC." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_FUNCIONARIO']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_FUNCIONARIO']] += $obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_FUNCIONARIO']] += $obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    elseif ($col_mestre == "MID_EQUIPE"){
                        $tmp=$dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute("SELECT * FROM ".ORDEM_MO_ALOC." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $mid_equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);
                            $campos_mestres[$mid_equip]=1;
                            
                            if ($tipo_tabela == 1){
                                $dados[$mes][$mid_equip] += $obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$mid_equip] += $obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $tmp=$dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute("SELECT * FROM ".ORDEM_MO_ALOC." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            
                            $campos_mestres[$campo[$col_mestre]]=1;
                            
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    
                    break;
                    
                    // TEMPO DE PARADA
                case 'tpo_prd' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MAQ_PARADA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    break;
                    
                    // TEMPO DE MO INDIVIDUAL
                case 'tpo_mao' :
                    if ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_FUNCIONARIO']]=1;
                            $tmp_mo = (VoltaTime($obj_campo['HORA_FINAL'],$tmp->UserDate($obj_campo['DATA_FINAL'],'d/m/Y')) - VoltaTime($obj_campo['HORA_INICIO'],$tmp->UserDate($obj_campo['DATA_INICIO'],'d/m/Y')))/(60*60);
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_FUNCIONARIO']] += $tmp_mo;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_FUNCIONARIO']] += $tmp_mo;
                            }
                            $tmp->MoveNext();
                        }
                    }
                    elseif ($col_mestre == "MID_EQUIPE"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $mid_equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);
                            $campos_mestres[$mid_equip]=1;
                            $tmp_mo = (VoltaTime($obj_campo['HORA_FINAL'],$tmp->UserDate($obj_campo['DATA_FINAL'],'d/m/Y')) - VoltaTime($obj_campo['HORA_INICIO'],$tmp->UserDate($obj_campo['DATA_INICIO'],'d/m/Y')))/(60*60);
                            if ($tipo_tabela == 1){
                                $dados[$mes][$mid_equip] += $tmp_mo;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$mid_equip] += $tmp_mo;
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $filtro_sql");
                        
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$mid_equip]=1;
                            $tmp_mo = (VoltaTime($obj_campo['HORA_FINAL'],$tmp->UserDate($obj_campo['DATA_FINAL'],'d/m/Y')) - VoltaTime($obj_campo['HORA_INICIO'],$tmp->UserDate($obj_campo['DATA_INICIO'],'d/m/Y')))/(60*60);
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += $tmp_mo;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += $tmp_mo;
                            }
                            $tmp->MoveNext();
                        }
                    }

                    break;
                    // CUSTO DE MAO DE OBRA
                case 'cus_mao' :
                    if ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_FUNCIONARIO']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_FUNCIONARIO']]+=$obj_campo['CUSTO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_FUNCIONARIO']]+=$obj_campo['CUSTO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    elseif ($col_mestre == "MID_EQUIPE"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $mid_equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);
                            $campos_mestres[$mid_equip]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$mid_equip]+=$obj_campo['CUSTO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$mid_equip]+=$obj_campo['CUSTO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['CUSTO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['CUSTO'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;

                    // CUSTO DE MAO DE OBRA EXTRA
                case 'cus_mex' :
                    if ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_FUNCIONARIO']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_FUNCIONARIO']]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_FUNCIONARIO']]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    elseif ($col_mestre == "MID_FUNCIONARIO"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $mid_equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);
                            $campos_mestres[$mid_equip]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$mid_equip]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$mid_equip]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['CUSTO_EXTRA'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;


                    // CUSTO MATERIAIS
                case 'cus_mat' :
                    if ($col_mestre == "MID_MATERIAL"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_MATERIAL']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_MATERIAL']]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_MATERIAL']]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;

                    // CONSUMO MATERIAL
                case 'con_mat' :
                    if ($col_mestre == "MID_MATERIAL"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_MATERIAL']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_MATERIAL']]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_MATERIAL']]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['CUSTO_TOTAL'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;
                    
                    // CONSUMO MATERIAL PREVISTO
                case 'con_pre' :
                    if ($col_mestre == "MID_MATERIAL"){
                        $tmp=$dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute("SELECT * FROM ".ORDEM_MAT_PREVISTO." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_MATERIAL']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_MATERIAL']]+=$obj_campo['QUANTIDADE'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_MATERIAL']]+=$obj_campo['QUANTIDADE'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute("SELECT * FROM ".ORDEM_MAT_PREVISTO." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['QUANTIDADE'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['QUANTIDADE'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;
                    
                    // CONSUMO EM LUBRIFICACAO
                case 'con_lub' :
                    if ($col_mestre == "MID_MATERIAL"){
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_LUB." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo['MID_MATERIAL']]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo['MID_MATERIAL']]+=$obj_campo['QUANTIDADE'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo['MID_MATERIAL']]+=$obj_campo['QUANTIDADE'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    else {
                        $campos_mestres[$campo[$col_mestre]]=1;
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_LUB." WHERE MID_ORDEM = '".$campo['MID']."'");
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            $campos_mestres[$obj_campo[$col_mestre]]=1;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$obj_campo[$col_mestre]]+=$obj_campo['QUANTIDADE'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$obj_campo[$col_mestre]]+=$obj_campo['QUANTIDADE'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    break;

                    // OUTROS CUSTOS
                case 'out_cus' :

                    $campos_mestres[$campo[$col_mestre]]=1;
                    $tmp=$dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_CUSTOS." WHERE MID_ORDEM = '".$campo['MID']."'");
                    while (!$tmp->EOF) {
                        $obj_campo=$tmp->fields;
                        $campos_mestres[$campo[$col_mestre]]=1;
                        if ($tipo_tabela == 1){
                            $dados[$mes][$campo[$col_mestre]]+=$obj_campo['CUSTO'];
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]+=$obj_campo['CUSTO'];
                        }
                        $tmp->MoveNext();
                    }
                    break;



                    // TEMPO DE M�O DE OBRA DISPONIVEL
                case 'tpo_mad' :
                    $tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute("SELECT MID, EQUIPE FROM ".FUNCIONARIOS." WHERE SITUACAO = 1");
                    while (!$tmp->EOF) {
                        $obj_campo=$tmp->fields;
                        if ($col_mestre == 'MID_FUNCIONARIO') {
                            $campos_mestres[$obj_campo['MID']]=1;
                        }
                        elseif ($col_mestre == 'MID_EQUIPE') {
                            $campos_mestres[$obj_campo['EQUIPE']]=1;
                        }
                        $tmp->MoveNext();
                    }

                    break;
                    
                    // ORDENS ABERTAS
                case 'ord_abr' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if (($campo[$col_mestre]) and ($campo['STATUS'] == 1)) {
                        if ($tipo_tabela == 1){
                            if ((int)$dba_resultado -> UserDate($campo['DATA_PROG'],'m') == $mes){
                                $dados[$mes][$campo[$col_mestre]]++;
                            }
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]++;
                        }
                    }
                    break;
                    
                    // ORDENS FECHADAS
                case 'ord_fec' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if (($campo[$col_mestre]) and ($campo['STATUS'] == 2)) {
                        if ($tipo_tabela == 1){
                            if ($dba_resultado ->UserDate($campo['DATA_FINAL'],'m') == $mes){
                                $dados[$mes][$campo[$col_mestre]]++;
                            }
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]++;
                        }
                    }
                    break;
                    
                    // TEMPO MEDIO PARA REPARO
                case 'tpo_mpr' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MAQ_PARADA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        if ($tmp->EOF) {
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += 0;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += 0;
                            }
                        }
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                        
                        if ($tipo_tabela == 1){
                            $mttr[$mes][$campo[$col_mestre]]++;
                        }
                        if ($tipo_tabela == 2) {
                            $mttr[$pos][$campo[$col_mestre]]++;
                        }
                    }
                    $pos_mttr=$pos;
                    break;

                case 'mtbf' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        if ($tipo_tabela == 1) {
                            $dados[$mes][$campo[$col_mestre]]++;
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]++;
                        }
                    }
                    $pos_mtbf= $pos;
                    $mtbf= 1;
                    break;

                case 'mtbf2' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        if ($tipo_tabela == 1) {
                            $dados[$mes][$campo[$col_mestre]]++;
                        }
                        if ($tipo_tabela == 2) {
                            $dados[$pos][$campo[$col_mestre]]++;
                        }

                    }
                    $pos_mtbf2= $pos;
                    $mtbf2= 1;
                    break;

                case 'dis_ope' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MAQ_PARADA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        // Sem nenhum registro
                        if($tmp->EOF) {
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += 0;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += 0;
                            }
                        }
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]] += $obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }
                    }
                    $pos_disoep=$pos;
                    $disope = 1;
                    break;

                case 'dis_cal' :
                    $campos_mestres[$campo[$col_mestre]]=1;
                    if ($campo[$col_mestre]) {
                        $tmp=$dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MAQ_PARADA." WHERE MID_ORDEM = '".$campo['MID']."'");
                        // Sem nenhum registro
                        if($tmp->EOF) {
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=0;
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=0;
                            }
                        }
                        while (!$tmp->EOF) {
                            $obj_campo=$tmp->fields;
                            if ($tipo_tabela == 1){
                                $dados[$mes][$campo[$col_mestre]]+=$obj_campo['TEMPO'];
                            }
                            if ($tipo_tabela == 2) {
                                $dados[$pos][$campo[$col_mestre]]+=$obj_campo['TEMPO'];
                            }
                            $tmp->MoveNext();
                        }

                    }
                    $pos_discal=$pos;
                    $discal = 1;
                    break;

                case 'sol_ace' :
                    $ace_pos=$pos;
                    $sol_ace=1;
                    break;
                case 'sol_rej' :
                    $rej_pos=$pos;
                    $sol_rej=1;
                    break;
                case 'sol_pen' :
                    $pen_pos=$pos;
                    $sol_pen=1;
                    break;
                case 'tpo_ope' :
                    $tpo_pos=$pos;
                    $tpo_ope=1;
                    break;
            }
            $dba_resultado->MoveNext();
            $i ++;
        }
        /*

        Seria muito pesado calcular disponibilidade no loop acima dentro do siwcht
        Por isso algumas colunas devem ser tratadas por fora, infelizmente isso vai consumir
        muito codigo mas por outro lado vai aumentar a velocidade das previsualizacoes.

        Exemplo de colunas que deveram ser tratadas separadamente ou duas vezes durante e apos o loop

        MTTR
        MTBF
        DISPONIBILIDADE DE MAO DE OBRA

        */

        // CALCULANDO DISPONIVILIDADE DE MAO DE OBRA!
        if ($tipo_coluna == "tpo_mad"){
            if ($campos_mestres) foreach ($campos_mestres as $col_tpo_mad => $valor){
                if ($col_mestre == 'MID_FUNCIONARIO') {
                    if ($tipo_tabela == 1){
                        $d=explode("-",$datai);
                        $tbano=$d[0];
                        // SIM! FIQUEI COM PREGUI�A DE FAZER AKELES LOOP DO KCT
                        $dados[1][$col_tpo_mad]=TurnoTotal("01/01/$tbano","31/01/$tbano",$col_tpo_mad);
                        $dados[2][$col_tpo_mad]=TurnoTotal("01/02/$tbano","28/02/$tbano",$col_tpo_mad);
                        $dados[3][$col_tpo_mad]=TurnoTotal("01/03/$tbano","31/03/$tbano",$col_tpo_mad);
                        $dados[4][$col_tpo_mad]=TurnoTotal("01/04/$tbano","30/04/$tbano",$col_tpo_mad);
                        $dados[5][$col_tpo_mad]=TurnoTotal("01/05/$tbano","31/05/$tbano",$col_tpo_mad);
                        $dados[6][$col_tpo_mad]=TurnoTotal("01/06/$tbano","30/06/$tbano",$col_tpo_mad);
                        $dados[7][$col_tpo_mad]=TurnoTotal("01/07/$tbano","31/07/$tbano",$col_tpo_mad);
                        $dados[8][$col_tpo_mad]=TurnoTotal("01/08/$tbano","31/08/$tbano",$col_tpo_mad);
                        $dados[9][$col_tpo_mad]=TurnoTotal("01/09/$tbano","30/09/$tbano",$col_tpo_mad);
                        $dados[10][$col_tpo_mad]=TurnoTotal("01/10/$tbano","31/10/$tbano",$col_tpo_mad);
                        $dados[11][$col_tpo_mad]=TurnoTotal("01/11/$tbano","30/11/$tbano",$col_tpo_mad);
                        $dados[12][$col_tpo_mad]=TurnoTotal("01/12/$tbano","31/12/$tbano",$col_tpo_mad);
                    }
                    if ($tipo_tabela == 2){
                        $d=explode("-",$datai);
                        $datai2=$d[2]."/".$d[1]."/".$d[0];
                        $d=explode("-",$dataf);
                        $dataf2=$d[2]."/".$d[1]."/".$d[0];
                        $dados[$pos][$col_tpo_mad]=TurnoTotal($datai2,$dataf2,$col_tpo_mad);
                    }
                }
                elseif ($col_mestre == 'MID_EQUIPE') {
                    if ($tipo_tabela == 1){
                        $d=explode("-",$datai);
                        $tbano=$d[0];
                        // SIM! FIQUEI COM PREGUI�A DE FAZER AKELES LOOP DO KCT
                        $dados[1][$col_tpo_mad]=TurnoTotalEquipe("01/01/$tbano","31/01/$tbano",$col_tpo_mad);
                        $dados[2][$col_tpo_mad]=TurnoTotalEquipe("01/02/$tbano","28/02/$tbano",$col_tpo_mad);
                        $dados[3][$col_tpo_mad]=TurnoTotalEquipe("01/03/$tbano","31/03/$tbano",$col_tpo_mad);
                        $dados[4][$col_tpo_mad]=TurnoTotalEquipe("01/04/$tbano","30/04/$tbano",$col_tpo_mad);
                        $dados[5][$col_tpo_mad]=TurnoTotalEquipe("01/05/$tbano","31/05/$tbano",$col_tpo_mad);
                        $dados[6][$col_tpo_mad]=TurnoTotalEquipe("01/06/$tbano","30/06/$tbano",$col_tpo_mad);
                        $dados[7][$col_tpo_mad]=TurnoTotalEquipe("01/07/$tbano","31/07/$tbano",$col_tpo_mad);
                        $dados[8][$col_tpo_mad]=TurnoTotalEquipe("01/08/$tbano","31/08/$tbano",$col_tpo_mad);
                        $dados[9][$col_tpo_mad]=TurnoTotalEquipe("01/09/$tbano","30/09/$tbano",$col_tpo_mad);
                        $dados[10][$col_tpo_mad]=TurnoTotalEquipe("01/10/$tbano","31/10/$tbano",$col_tpo_mad);
                        $dados[11][$col_tpo_mad]=TurnoTotalEquipe("01/11/$tbano","30/11/$tbano",$col_tpo_mad);
                        $dados[12][$col_tpo_mad]=TurnoTotalEquipe("01/12/$tbano","31/12/$tbano",$col_tpo_mad);
                    }
                    if ($tipo_tabela == 2){
                        $d=explode("-",$datai);
                        $datai2=$d[2]."/".$d[1]."/".$d[0];
                        $d=explode("-",$dataf);
                        $dataf2=$d[2]."/".$d[1]."/".$d[0];
                        $dados[$pos][$col_tpo_mad]=TurnoTotalEquipe($datai2,$dataf2,$col_tpo_mad);
                    }
                }
            }
        }
        
        $pos++;
        $dba_resultado->MoveFirst();
    }

    // CALCULANDO MTTR
    if ($mttr){
        if ($tipo_tabela == 1){
            foreach ($dados as $mes => $valor){
                foreach ($valor as $col => $valor_mes){
                    if ($valor_mes=! 0) {
                        $dados_mttr[$mes][$col]=round((float)$dados[$mes][$col] / $mttr[$mes][$col],2);
                    }
                }
            }
            $dados=$dados_mttr;
        }
        if ($tipo_tabela == 2) {
            foreach ($dados[$pos_mttr] as $col => $valor){
                if ($valor=! 0) {
                    $dados_mttr[$col]=round((float)$dados[$pos_mttr][$col] / $mttr[$pos_mttr][$col],2);
                }
            }
            $dados[$pos_mttr]=$dados_mttr;
        }
    }

    // CALCULANDO A DISPONIBILIDADE
    if ($disope){
        if ($tipo_tabela == 1){
            $anoi=explode("-",$datai);
            $anoi=$anoi[0];
            $anof=explode("-",$dataf);
            $anof=$anof[0];
            for ($i=1; $i <= 12; $i++){
                if ($dados[$i]) {
                    foreach ($dados[$i] as $col => $valor_mes){
                        // SOMA A DISPONIBILIDADE DA AREA
                        if ($col_mestre == "MID_AREA") {
                            $disope_setor=ListaSetor($col);
                            for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                                $disope_maq=ListaMaquina($disope_setor[$is]['mid']);
                                for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                    $va_mes+=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$disope_maq[$im]['mid']);
                                }
                            }
                        }
                        // SOMA A DISPONIBILIDADE DO SETOR
                        elseif ($col_mestre == "MID_SETOR") {
                            $disope_maq=ListaMaquina($col);
                            for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                $va_mes+=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$disope_maq[$im]['mid']);
                            }
                        }
                        // DISPONIBILIDADE DA MAQUINA
                        else {
                            $va_mes=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$col);
                        }
                        if ($va_mes != 0) {
                            $va=$va_mes - $valor_mes;
                            $dados_disope[$i][$col]=round(($va/$va_mes) * 100,2);
                            $campos_mestres[$col]=1;
                            if ((int)$dados_disope[$i][$col] == 0) {
                                $dados_disope[$i][$col]=100;
                            }
                            $va_mes=0;
                        }
                        else {
                            $campos_mestres[$col]=1;
                            //$dados_disope[$i][$col]="s/d";
                        }

                    }
                }
            }
            $dados=$dados_disope;
        }
        if ($tipo_tabela == 2) {
            foreach ($dados[$pos_disoep] as $col => $valor){
                $anoi=explode("-",$datai);
                $anoi=$anoi[0];
                $anof=explode("-",$dataf);
                $anof=$anof[0];
                $mesi=explode("-",$datai);
                $mesi=$mesi[1];
                $mesf=explode("-",$dataf);
                $mesf=$mesf[1];
                if ($valor=! 0) {
                    // SOMA A DISPONIBILIDADE DA AREA
                    if ($col_mestre == "MID_AREA") {
                        $disope_setor=ListaSetor($col);
                        for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                            $disope_maq=ListaMaquina($disope_setor[$is]['mid']);
                            for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                $va_mes+=TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                            }
                        }
                    }
                    // SOMA A DISPONIBILIDADE DO SETOR
                    elseif ($col_mestre == "MID_SETOR") {
                        $disope_maq=ListaMaquina($col);
                        for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                            $va_mes+=TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                        }//echo "+++".$va_mes;
                    }
                    // DISPONIBILIDADE DA MAQUINA
                    else {
                        $va_mes=TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$col);
                    }

                    if ((int)$va_mes != 0) {
                    //  echo "***".$dados[$pos_disoep][$col]; 
                        $va=$va_mes - $dados[$pos_disoep][$col];
                        //echo "mes $mesi/$anoi $mesf/$anof va $va_mes ".$dados[$pos_disoep][$col]." ($col)<br>";
                        $dados_disope[$col]=round(($va/$va_mes) * 100,2);
                        $campos_mestres[$col]=1;
                        $va_mes=0;
                        if ((int)$dados_disope[$col] == 0) {
                            $dados_disope[$col]=100;
                        }
                    }
                    else {
                        $dados_disope[$col]="n/a";
                    }
                }
            }
            //echo $pos_disoep;
            $dados[$pos_disoep]=$dados_disope;
        }
    }
    // Disponibilidade por Tempo Calend�rio
    if ($discal){
        if ($tipo_tabela == 1){
            for ($i=1; $i <= 12; $i++){
                if ($dados[$i]) {
                    foreach ($dados[$i] as $col => $valor_mes){
                        if ($col_mestre == "MID_AREA") {
                            $tmp_mul=count(ListaSetor($col));
                        }
                        if ($col_mestre == "MID_SETOR") {
                            $tmp_mul=count(ListaMaquina($col));
                        }
                        else {
                            $tmp_mul=1;
                        }
                        
                        $va_mes=0;
                        
                        // Tratando as datas
                        $data_inicio = explode("-", $datai);
                        $data_fim    = explode("-", $dataf);
                        $data_inicial = mktime(0,0,0,$i,1, $data_inicio[0]);
                        $data_final = mktime(23,59,59,$i+1, 1, $data_fim[0]);
                        // Calculando tempo em horas
                        $tempo_unix = $data_final - $data_inicial;
                        $periodo = round($tempo_unix /(60*60),2);
                        // Itens a considerar
                        $va_mes = $periodo * $tmp_mul;
                        // Tempo total - Tempo de parada
                        $va = $va_mes - $valor_mes;
                        // Calculando a porcentagem
                        $dados_discal[$i][$col]=round(($va / $va_mes) * 100, 3);
                        
                        // Caso zere mostrar 100%
                        if ((int)$dados_discal[$i][$col] == 0) {
                            $dados_discal[$i][$col]=100;
                        }
                        
                        $campos_mestres[$col]=1;
                    }
                }

            }
            $dados=$dados_discal;
        }
        if ($tipo_tabela == 2) {
            foreach ($dados[$pos_discal] as $col => $valor){

                if ($col_mestre == "MID_AREA") {
                    $tmp_mul=count(ListaSetor($col));
                }
                if ($col_mestre == "MID_SETOR") {
                    //$tmp_mul=count(ListaMaquina($col));
                    $sql = "SELECT MID FROM ".MAQUINAS." WHERE MID_SETOR = '$col' AND STATUS = 1 ";
                    $tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
                    $tmp_mul=count($tmp -> getrows());
                }
                else {
                    $tmp_mul=1;
                }
                //if($col == 7)echo "<br>".$tmp_mul."<br>";
                $data_inicio = explode("-",$datai);
                $data_fim = explode("-",$dataf);
                $data_inicial = mktime(0,0,0,$data_inicio[1], $data_inicio[2], $data_inicio[0]);
                //echo $data_fim[2];
                $data_final = mktime(23,59,59,$data_fim[1], $data_fim[2]+1, $data_fim[0]);
                $tempo_unix = $data_final - $data_inicial;

                //echo "-".$dia = date('d',$tempo_unix);

                $periodo = round($tempo_unix /(60*60),2);
                $va_mes=$periodo*$tmp_mul;
                if($va_mes != 0){
                    $va=$va_mes - (int)$valor;
                    $dados_discal[$col]=round(($va/$va_mes) * 100,3);
                }
                else (int)$dados_discal[$col] = 0;
                $campos_mestres[$col]=1;
                if ((int)$dados_discal[$col] == 0) {
                    $dados_discal[$col]=100;
                }
                $va_mes=0;
            }
            $dados[$pos_discal]=$dados_discal;
        }
    }


    // TEMPO DE OPERACAO
    if ($tpo_ope) {
        if ($tipo_tabela == 1){

            $anoi=explode("-",$datai);
            $anoi=$anoi[0];
            $anof=explode("-",$dataf);
            $anof=$anof[0];
            for ($i=1; $i <= 12; $i++){
                if ($col_mestre == "MID_AREA") {
                    $r = $dba[0] -> Execute("SELECT MID FROM ".AREAS);
                    while (!$r -> EOF) {
                        $col = $r -> fields['MID'];
                        $disope_setor=ListaSetor($col);
                        for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                            $disope_maq=ListaMaquina($disope_setor[$is]['mid']);
                            for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                $tmp_tpo+=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anof,$disope_maq[$im]['mid'])/60;
                            }
                        }
                        if ($tmp_tpo != 0) {

                            $campos_mestres[$col]=1;
                            $dados[$i][$col]=$tmp_tpo;
                        }
                        unset ($tmp_tpo);
                        $r -> MoveNext();
                    }

                }
                // SOMA A DISPONIBILIDADE DO SETOR
                elseif ($col_mestre == "MID_SETOR") {
                    $r = $dba[0] -> Execute("SELECT MID FROM ".SETORES);
                    while (!$r -> EOF) {
                        $col = $r -> fields['MID'];
                        $disope_maq=ListaMaquina($col);
                        for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                            $tmp_tpo+=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anof,$disope_maq[$im]['mid'])/60;
                        }
                        if ($tmp_tpo != 0) {
                            $campos_mestres[$col]=1;
                            $dados[$i][$col]=$tmp_tpo;
                        }
                        unset ($tmp_tpo);
                        $r -> MoveNext();
                    }
                }
                // DISPONIBILIDADE DA MAQUINA
                else {
                    $r = $dba[0] -> Execute("SELECT MID, STATUS FROM ".MAQUINAS." WHERE STATUS = 1");
                    while (!$r -> EOF) {
                        $col = $r -> fields['MID'];
                        $tmp_tpo=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anof,$col)/60;

                        if ($tmp_tpo != 0) {
                            $campos_mestres[$col]=1;
                            $dados[$i][$col]=$tmp_tpo;
                        }
                        unset ($tmp_tpo);
                        $r -> MoveNext();
                    }
                }
            }
        }
        if ($tipo_tabela == 2) {
            $anoi=explode("-",$datai);
            $anoi=$anoi[0];
            $anof=explode("-",$dataf);
            $anof=$anof[0];
            $mesi=explode("-",$datai);
            $mesi=$mesi[1];
            $mesf=explode("-",$dataf);
            $mesf=$mesf[1];

            // SOMA A DISPONIBILIDADE DA AREA
            if ($col_mestre == "MID_AREA") {
                $r = $dba[0] -> Execute("SELECT MID FROM ".AREAS);
                while (!$r -> EOF) {
                    $col = $r -> fields['MID'];
                    
                    $disope_setor=ListaSetor($col);
                    for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                        $disope_maq=ListaMaquina($disope_setor[$is]['mid']);
                        for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                            $tmp_tpo+=TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                        }
                    }
                    if ($tmp_tpo != 0) {
                        $campos_mestres[$col]=1;
                        $dados[$tpo_pos][$col]=$tmp_tpo;
                    }
                    unset ($tmp_tpo);
                    $r -> MoveNext();
                }

            }
            // SOMA A DISPONIBILIDADE DO SETOR
            elseif ($col_mestre == "MID_SETOR") {
                $r = $dba[0] -> Execute("SELECT MID FROM ".SETORES);
                while (!$r -> EOF) {
                    $col = $r -> fields['MID'];
                    
                    $disope_maq=ListaMaquina($col);
                    for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                        $tmp_tpo+=TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                    }
                    if ($tmp_tpo != 0) {
                        $campos_mestres[$col]=1;
                        $dados[$tpo_pos][$col]=$tmp_tpo;
                    }
                    unset ($tmp_tpo);
                    $r -> MoveNext();
                }
            }
            // DISPONIBILIDADE DA MAQUINA
            else {
                $r = $dba[0] -> Execute("SELECT MID FROM ".MAQUINAS." WHERE STATUS = 1");
                while (!$r -> EOF) {
                    $col = $r -> fields['MID'];
                    
                    $tmp_tpo = TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$col);
                    if ($tmp_tpo != 0) {
                        $campos_mestres[$col]=1;
                        $dados[$tpo_pos][$col]=$tmp_tpo;
                    }
                    
                    $r -> MoveNext();
                }
            }
        }
    }
    
    // MTBF POR TEMPO DE OPERA��O
    if ($mtbf){
        if ($tipo_tabela == 1){
            $anoi=explode("-",$datai);
            $anoi=$anoi[0];
            $anof=explode("-",$dataf);
            $anof=$anof[0];
            for ($i=1; $i <= 12; $i++){
                if ($dados[$i]) {
                    foreach ($dados[$i] as $col => $valor_mes){
                        // Quant. OS
                        if ($valor_mes == 0){
                            $valor_mes = 1;
                        }
                        // Tempo de Opera��o
                        $va_mes = 0;
                        
                        // SOMA A DISPONIBILIDADE DA AREA
                        if ($col_mestre == "MID_AREA") {
                            $disope_setor = ListaSetor($col);
                            for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                                $disope_maq = ListaMaquina($disope_setor[$is]['mid']);
                                for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                    $va_mes += TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$disope_maq[$im]['mid'])/60;
                                }
                            }
                        }
                        // SOMA A DISPONIBILIDADE DO SETOR
                        elseif ($col_mestre == "MID_SETOR") {
                            $disope_maq = ListaMaquina($col);
                            for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                                $va_mes += TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$disope_maq[$im]['mid'])/60;
                            }
                        }
                        // DISPONIBILIDADE DA MAQUINA
                        else {
                            $va_mes=TempoMaqOperacao((int)$i,$anoi,(int)$i,$anoi,$col)/60;
                        }
                        
                        if ($va_mes != 0) {
                            $dados_disope[$i][$col] = round($va_mes/$valor_mes,2);
                            $campos_mestres[$col]=1;
                        }
                        else {
                            $dados_disope[$i][$col] = 0;
                        }
                    }
                }
            }
            $dados=$dados_disope;
            unset($dados_disope);
        }
        if ($tipo_tabela == 2) {
            //var_dump($dados[$pos_mtbf]);
            foreach ($dados[$pos_mtbf] as $col => $valor_mes){
                // Quant. OS
                if ($valor_mes == 0){
                    $valor_mes = 1;
                }
                // Tempo de Opera��o
                $va_mes = 0;
                
                //echo "$valor_mes <br>";
                $anoi=explode("-",$datai);
                $anoi=$anoi[0];
                $anof=explode("-",$dataf);
                $anof=$anof[0];
                $mesi=explode("-",$datai);
                $mesi=$mesi[1];
                $mesf=explode("-",$dataf);
                $mesf=$mesf[1];
            
                // SOMA A DISPONIBILIDADE DA AREA
                if ($col_mestre == "MID_AREA") {
                    $disope_setor = ListaSetor($col);
                    for ($is=0; $disope_setor[$is]['nome'] != ""; $is++) {
                        $disope_maq = ListaMaquina($disope_setor[$is]['mid']);
                        for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                            $va_mes += TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                        }
                    }
                }
                // SOMA A DISPONIBILIDADE DO SETOR
                elseif ($col_mestre == "MID_SETOR") {
                    $disope_maq = ListaMaquina($col);
                    for ($im=0; $disope_maq[$im]['nome'] != ""; $im++) {
                        $va_mes += TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$disope_maq[$im]['mid']);
                    }
                }
                // DISPONIBILIDADE DA MAQUINA
                else {
                    $va_mes = TempoMaqOperacao((int)$mesi,$anoi,(int)$mesf,$anof,$col);
                }

                if ((int)$va_mes != 0) {
                    $dados_disope[$col]=round($va_mes/$valor_mes,2);
                    $campos_mestres[$col]=1;
                }
                else {
                    $dados_disope[$col] = 0;
                }
            }
            //echo $pos_disoep;
            $dados[$pos_mtbf]=$dados_disope;
        }
    }
    
    // MTBF por tempo calend�rio
    if ($mtbf2){
        if ($tipo_tabela == 1){
            $data_inicio = explode("-",$datai);
            $data_fim = explode("-",$dataf);
            for ($i=1; $i <= 12; $i++){
                if ($dados[$i]) {
                    foreach ($dados[$i] as $col => $valor_mes){
                        if ($col_mestre == "MID_AREA") {
                            $tmp_mul=count(ListaSetor($col));
                        }
                        if ($col_mestre == "MID_SETOR") {
                            $tmp_mul=count(ListaMaquina($col));
                        }
                        else {
                            $tmp_mul=1;
                        }
                        
                        // Para n�o dar erro na divis�o
                        if ($valor_mes == 0) {
                            $valor_mes = 1;
                        }
                        
                        $va_mes = 0;
                        
                        $data_inicial = mktime(0,0,0, $i, 1, $data_inicio[0]);
                        $data_final = mktime(23,59,59, $i + 1, 0, $data_fim[0]);
                        $tempo_unix = $data_final - $data_inicial;
                        $periodo = round($tempo_unix /(60*60), 2);
                        
                        $va_mes = $periodo * $tmp_mul;

                        $dados_discal[$i][$col] = round($va_mes/$valor_mes, 2);
                        $campos_mestres[$col]=1;
                    }
                }

            }
            $dados=$dados_discal;
        }
        if ($tipo_tabela == 2) {
            foreach ($dados[$pos_mtbf2] as $col => $valor){
                if ($col_mestre == "MID_AREA") {
                    $tmp_mul=count(ListaSetor($col));
                }
                if ($col_mestre == "MID_SETOR") {
                    $tmp_mul=count(ListaMaquina($col));
                }
                else {
                    $tmp_mul=1;
                }
                
                // Para n�o dar erro na divis�o
                if ($valor == 0) {
                    $valor = 1;
                }
                
                $va_mes=0;

                // Tratatando as datas
                $data_inicio  = explode("-", $datai);
                $data_fim     = explode("-", $dataf);
                // Recuperando o mktime
                $data_inicial = mktime(0,0,0, $data_inicio[1], $data_inicio[2], $data_inicio[0]);
                $data_final   = mktime(23,59,59, $data_fim[1], $data_fim[2], $data_fim[0]);
                // Calculando o total em horas
                $tempo_unix   = $data_final - $data_inicial;
                $periodo      = round($tempo_unix /(60*60),2);
                // Itens a considerar
                $va_mes       = $periodo * $tmp_mul;
                // Calculando o MTBF
                $dados_discal[$col]=round($va_mes/$valor,2);
                $campos_mestres[$col]=1;
            }
            $dados[$pos_mtbf2]=$dados_discal;
        }
    }


    // SOLICITACOES ACEITAS
    if ($sol_ace){
        $tmp=$dba[$tdb[SOLICITACOES]['dba']] -> Execute("SELECT * FROM ".SOLICITACOES." WHERE DATA >= '$datai' AND DATA <= '$dataf' AND STATUS = 1");
        while (!$tmp->EOF) {
            $obj_campo=$tmp->fields;

            if ($col_mestre == "MID_AREA") {
                $setor = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $valor_mestre = VoltaValor (SETORES, 'MID_AREA', 'MID', $setor);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$ace_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "MID_SETOR") {
                $valor_mestre = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$ace_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "CENTRO_DE_CUSTO") {
                $valor_mestre = VoltaValor (MAQUINAS, 'CENTRO_DE_CUSTO', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$ace_pos][$valor_mestre]++;
                }
            }

            else {
                $campos_mestres[$obj_campo[$col_mestre]]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$obj_campo[$col_mestre]]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$ace_pos][$obj_campo[$col_mestre]]++;
                }
            }

            $tmp->MoveNext();
        }
    }
    if ($sol_pen){
        $tmp=$dba[$tdb[SOLICITACOES]['dba']] -> Execute("SELECT * FROM ".SOLICITACOES." WHERE DATA >= '$datai' AND DATA <= '$dataf' AND STATUS = 0");
        while (!$tmp->EOF) {
            $obj_campo=$tmp->fields;

            if ($col_mestre == "MID_AREA") {
                $setor = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $valor_mestre = VoltaValor (SETORES, 'MID_AREA', 'MID', $setor);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$pen_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "MID_SETOR") {
                $valor_mestre = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$pen_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "CENTRO_DE_CUSTO") {
                $valor_mestre = VoltaValor (MAQUINAS, 'CENTRO_DE_CUSTO', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$pen_pos][$valor_mestre]++;
                }
            }

            else {
                $campos_mestres[$obj_campo[$col_mestre]]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$obj_campo[$col_mestre]]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$pen_pos][$obj_campo[$col_mestre]]++;
                }
            }

            $tmp->MoveNext();
        }
    }

    if ($sol_rej){
        $tmp=$dba[$tdb[SOLICITACOES]['dba']] -> Execute("SELECT * FROM ".SOLICITACOES." WHERE DATA >= '$datai' AND DATA <= '$dataf' AND STATUS = 2");
        while (!$tmp->EOF) {
            $obj_campo=$tmp->fields;

            if ($col_mestre == "MID_AREA") {
                $setor = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $valor_mestre = VoltaValor (SETORES, 'MID_AREA', 'MID', $setor);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$rej_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "MID_SETOR") {
                $valor_mestre = VoltaValor (MAQUINAS, 'MID_SETOR', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$rej_pos][$valor_mestre]++;
                }
            }

            elseif ($col_mestre == "CENTRO_DE_CUSTO") {
                $valor_mestre = VoltaValor (MAQUINAS, 'CENTRO_DE_CUSTO', 'MID', $obj_campo['MID_MAQUINA']);
                $campos_mestres[$valor_mestre]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$valor_mestre]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$rej_pos][$valor_mestre]++;
                }
            }

            else {
                $campos_mestres[$obj_campo[$col_mestre]]=1;
                $mes=(int)$tmp->UserDate($obj_campo['DATA'],'m');
                if ($tipo_tabela == 1){
                    $dados[$mes][$obj_campo[$col_mestre]]++;
                }
                if ($tipo_tabela == 2) {
                    $dados[$rej_pos][$obj_campo[$col_mestre]]++;
                }
            }

            $tmp->MoveNext();
        }
    }

    $tmp_ex['dados']=$dados;
    $tmp_ex['campos']=$campos_mestres;
    return $tmp_ex;
}

// FILTROS
$AG_filtros['MID_EMPRESA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_EMPRESA'];
$AG_filtros['MID_AREA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_AREA'];
$AG_filtros['MID_SETOR']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_SETOR'];
$AG_filtros['CENTRO_DE_CUSTO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['CENTRO_DE_CUSTO'];
$AG_filtros['MID_MAQUINA_FAMILIA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_MAQUINA_FAMILIA'];
$AG_filtros['MID_MAQUINA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_MAQUINA'];
$AG_filtros['MID_CONJUNTO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_CONJUNTO'];
$AG_filtros['MID_EQUIPAMENTO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['MID_EQUIPAMENTO'];
$AG_filtros['NATUREZA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['NATUREZA'];
$AG_filtros['STATUS']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['STATUS'];
$AG_filtros['TIPO_SERVICO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['TIPO_SERVICO'];
$AG_filtros['TIPO']['DESCRICAO']="Origem";
$AG_filtros['CAUSA']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['CAUSA'];
$AG_filtros['DEFEITO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['DEFEITO'];
$AG_filtros['SOLUCAO']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['SOLUCAO'];
$AG_filtros['RESPONSAVEL']['DESCRICAO']=$tdb[ORDEM_PLANEJADO]['RESPONSAVEL'];

// FILTROS L�GICOS PARA O VALOR SELECIONADO USE %%VALOR%%
$AG_filtros['TEM_PARADA']['DESCRICAO'] = "Com parada de " . $tdb[ORDEM_PLANEJADO]['MID_MAQUINA'];
$AG_filtros['TEM_PARADA']['COND'][1] = "(SELECT COUNT(MID) FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = O.MID) > 0";
$AG_filtros['TEM_PARADA']['COND'][2] = "(SELECT COUNT(MID) FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = O.MID) = 0";

$AG_filtros['TEM_PENDENCIA1']['DESCRICAO'] = "Com pend&ecirc;ncias geradas";
$AG_filtros['TEM_PENDENCIA1']['COND'][1] = "(SELECT COUNT(MID) FROM " . PENDENCIAS . " WHERE MID_ORDEM = O.MID) > 0";
$AG_filtros['TEM_PENDENCIA1']['COND'][2] = "(SELECT COUNT(MID) FROM " . PENDENCIAS . " WHERE MID_ORDEM = O.MID) = 0";

$AG_filtros['TEM_PENDENCIA2']['DESCRICAO'] = "Com pend&ecirc;ncias anexas";
$AG_filtros['TEM_PENDENCIA2']['COND'][1] = "(SELECT COUNT(MID) FROM " . PENDENCIAS . " WHERE MID_ORDEM_EXC = O.MID) > 0";
$AG_filtros['TEM_PENDENCIA2']['COND'][2] = "(SELECT COUNT(MID) FROM " . PENDENCIAS . " WHERE MID_ORDEM_EXC = O.MID) = 0";

$AG_filtros['TEM_REPROG']['DESCRICAO'] = "Com reprograma&ccedil;&otilde;es";
$AG_filtros['TEM_REPROG']['COND'][1] = "(SELECT COUNT(MID) FROM " . ORDEM_REPROGRAMA . " WHERE MID_ORDEM = O.MID) > 0";
$AG_filtros['TEM_REPROG']['COND'][2] = "(SELECT COUNT(MID) FROM " . ORDEM_REPROGRAMA . " WHERE MID_ORDEM = O.MID) = 0";

$AG_filtros['MID_EQUIPE']['DESCRICAO'] = $tdb[EQUIPES]['DESC'];
$AG_filtros['MID_EQUIPE']['COND'] = "(SELECT COUNT(MO.MID) FROM " . ORDEM_MADODEOBRA . " MO, " . FUNCIONARIOS . " F WHERE MO.MID_FUNCIONARIO = F.MID AND F.EQUIPE %%COND%% %%VALOR%% AND MO.MID_ORDEM = O.MID) > 0";

// CONDI��ES & OPERADORES
$AG_cond[1]="Igual";
$AG_cond_sinal[1]="=";

$AG_cond[2]="Diferente";
$AG_cond_sinal[2]="!=";

$AG_oper[1]="E";
$AG_oper_sinal[1]="and";

$AG_oper[2]="Ou";
$AG_oper_sinal[2]="or";

?>
