<?
/** 
* Edita tabelas de um gafico
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/delcascata.php")) die ($ling['bd01']);
elseif (!require("funcoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("conf.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">
<div id=\"div_formulario_corpo\">";

$tb=(int)$_GET['tb'];
$arq=urldecode($_GET['arq']);
if ($arq == "") erromsg("Selecione um arquivo primeiro.");
if ($tb == 0) erromsg("Selecione uma tabela primeiro.");
else {
    $dir="../../".$manusis['dir']['graficos'];
    $xml = new DOMDocument();
    $xml->formatOutput = true;
    $xml -> load("$dir/$arq");
    $descricao_grafico=$xml -> getElementsByTagName('descricao');
    $descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
    if ($_POST['env_form']){
        $tb_titulo=LimpaTexto($_POST['tabela_titulo']);
        $tb_col_mestre=LimpaTexto($_POST['coluna_mestre']);
        $tb_col_dados=$_POST['coluna_dados'];

        // Pega o objeto DOM referente a tabela atual
        $tabela=AG_PegaTabela($xml,$tb);

        // Atualiza o Titulo da tabela
        $titulo_antigo=$tabela->getElementsByTagName('titulo')->item(0);
        $titulo_novo = $xml->createElement('titulo',utf8_encode($tb_titulo));
        $titulo_novo = $tabela->replaceChild($titulo_novo,$titulo_antigo);

        // Atualiza a Coluna Mestre
        $col_antigo=$tabela->getElementsByTagName('colmestre')->item(0);
        $col_novo=$xml->createElement('colmestre',utf8_encode(html_entity_decode($AG_colMestre[$tb_col_mestre]['DESCRICAO'])));
        $col_novo -> setAttribute('tipo',$AG_colMestre[$tb_col_mestre]['TIPO']);
        $col_novo -> setAttribute('db_tb',$tb_col_mestre);
        if ($col_antigo) $col_novo = $tabela->replaceChild($col_novo,$col_antigo);
        else $col_novo = $tabela->appendChild($col_novo);


        AG_AtualizaColTabela($xml,$tabela,$tb_col_dados);


        // Salva Documento
        $xml -> save($xml ->documentURI);
    }




    $tabela_dados=AG_InfoTabela($xml,$tb);

    echo "<h2> $descricao_grafico </h2>
<form method=\"POST\" action=\"\" enctype=\"multipart/form-data\">
<fieldset><legend>Informa��es da Tabela</legend>
<label for=\"tabela_titulo\">T�tulo da tabela:</label>
<input class=\"campo_text\" type=\"text\" id=\"tabela_titulo\" name=\"tabela_titulo\" size=\"50\" maxlenght=\"75\" value=\"".$tabela_dados['titulo']."\" />
<br clear=\"all\" />
<label for=\"tabela_tipo\">Tipo:</label>
<input disabled=\"disabled\" class=\"campo_text\" type=\"text\" id=\"tabela_tipo\" size=\"25\" maxlenght=\"25\" value=\"".$tabela_dados['tipo_nome']."\" />
</fieldset>";

    echo "<fieldset><legend>P�rametros para Coluna Mestre</legend>
    <label for=\"coluna_mestre\">Coluna Mestre:</label>
    <select name=\"coluna_mestre\" id=\"coluna_mestre\" class=\"campo_select\">";
    foreach ($AG_colMestre as $valor => $des) {
        if ($tabela_dados['coluna_mestre'] == $valor) echo "<option selected=\"selected\" value=\"$valor\">".$des['DESCRICAO']."</option>\n";
        else echo "<option value=\"$valor\">".$des['DESCRICAO']."</option>\n";
    }
    echo "</select></fieldset>";

    echo "<fieldset><legend>Colunas de Dados</legend>";

    If ($tabela_dados['tipo'] == 2) {
        echo "
    <table width=\"100%\" cellspacing=\"3\"  border=\"0\">
    <tr>
    <td align=\"center\" width=\"45%\" style=\"font-size:10px\">
    Colunas Dispon�veis<br />   
    <select class=\"campo_text\" multiple=\"multiple\" size=\"10\" style=\"width:200px\" name=\"coluna_dados_fonte\" id=\"coluna_dados_fonte\" >";
        foreach ($AG_colDados as $valor => $des) {
            if (!$tabela_dados['coluna_dados'][$valor]) echo "<option value=\"$valor\">".$des['DESCRICAO']."</option>\n";
        }
        echo "</select></td>
    <td align=\"center\">
<input class=\"botao\" type=\"button\" name=\"insere\" value=\"Adicionar >>\" onclick=\"AdicionaItemLista(document.getElementById('coluna_dados_fonte'),document.getElementById('coluna_dados'))\"><br />
<input class=\"botao\" type=\"button\" name=\"deleta\" value=\"<< Remover\" onclick=\"AdicionaItemLista(document.getElementById('coluna_dados'),document.getElementById('coluna_dados_fonte'))\">
</td>
<td align=\"center\" width=\"45%\" style=\"font-size:10px\">
Colunas Selecionadas<br />
    
    <select style=\"width:200px\"  class=\"campo_text\" multiple=\"multiple\" size=\"10\" name=\"coluna_dados[]\" id=\"coluna_dados\" >";
        foreach ($AG_colDados as $valor => $des) {
            if ($tabela_dados['coluna_dados'][$valor]) echo "<option  value=\"$valor\">".$des['DESCRICAO']."</option>\n";
        }
        echo "</select></td>
    </tr>
    </table>";
    }
    elseif ($tabela_dados['tipo'] == 1){
        echo "<label for=\"coluna_dados\">Coluna:</label>
            <select class=\"campo_select\" name=\"coluna_dados[]\" id=\"coluna_dados\" >";
        foreach ($AG_colDados as $valor => $des) {
            if ($tabela_dados['coluna_dados'][$valor]) echo "<option selected=\"selected\" value=\"$valor\">".$des['DESCRICAO']."</option>\n";
            else echo "<option  value=\"$valor\">".$des['DESCRICAO']."</option>\n";
        }
        echo "</select>";
    }
    echo "</fieldset><br clear=\"all\" />";
    if ($tabela_dados['tipo'] == 2) echo "<input type=\"submit\" value=\"Atualizar Tabela\" name=\"env_form\" class=\"botao\" onclick=\"SelecionaTodosLista(document.getElementById('coluna_dados'))\" /></form>";
    if ($tabela_dados['tipo'] == 1) echo "<input type=\"submit\" value=\"Atualizar Tabela\" name=\"env_form\" class=\"botao\" /></form>";
}
echo "</div></body></html>";

?>
