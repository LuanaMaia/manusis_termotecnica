<?
/**
* Define os filtros de uma tabela do gr�fico
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 

if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/delcascata.php")) die ($ling['bd01']);
elseif (!require("../../lib/forms.php")) die ($ling['bd01']);
elseif (!require("funcoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require("conf.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">
<div id=\"div_formulario_corpo\">";

$tb=(int)$_GET['tb'];
$arq=urldecode($_GET['arq']);
$arq_envia=urlencode($_GET['arq']);

if ($arq == "") {
	erromsg("Selecione um arquivo primeiro.");
}
elseif ($tb == 0) {
	erromsg("Selecione uma tabela primeiro.");
}
else {
	$dir="../../".$manusis['dir']['graficos'];
	$xml = new DOMDocument();
	$xml->formatOutput = true;
	$xml -> load("$dir/$arq");
	$descricao_grafico=$xml -> getElementsByTagName('descricao');
	$descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
	
	
	// Atualizando as colunas
	if ($_POST['env_form'] != "") {
	    $tipo = $_POST['tipo'];
	    $esc  = $_POST['esc'];
	    $grupo = (int) $_POST['grupo'];
	    
	    AG_AtualizaColTabelaGer($xml, $tipo, $esc);
	    AG_AtualizaGrupoTabelaGer($xml, $grupo);
	    
	    // Salva Documento
		$xml -> save($xml ->documentURI);
	}
	
	$tabela = AG_InfoTabela($xml,$tb);
	
	echo "<h2 style=\"font-size:15px\">$descricao_grafico</h2>
    <form method=\"POST\" action=\"\">
    <fieldset><legend>" . $tdb[GRUPO_GRAFICO]['DESC'] . "</legend>
    <label for=\"grupo\">Grupo:</label>";
	
	FormSelectD("DESCRICAO", "", GRUPO_GRAFICO, $tabela['grupo'], "grupo", "grupo", "MID", "GRUPO_GRAFICO");
	
    echo "</fieldset>
	<fieldset><legend>Colunas de Dados</legend>";
	echo "
		<div id=\"lt_tabela\"><table id=\"lt_tabela\">
		<tr>
		<th>Coluna</th>
		<th>Tipo</th>
		<th>Escala</th>
		</tr>";
	
    $i=0;
    // Verifica se ja existem colunas
    if(is_array($tabela['coluna_dados'])) {
    	foreach ($tabela['coluna_dados'] as $tpo => $col) {
    	    if ($tabela['coluna_dados_modo'][$tpo] == "column") {
    	    	$tipo1 = "checked=\"checked\"";
    	    	$tipo2 = "";
    	    }
    	    elseif ($tabela['coluna_dados_modo'][$tpo] == "line") {
    	        $tipo1 = "";
    	        $tipo2 = "checked=\"checked\"";
    	    }
    	    
    	    if ((int) $tabela['coluna_dados_escala'][$tpo] == 0) {
    	    	$esc1 = "checked=\"checked\"";
    	    	$esc2 = "";
    	    }
    	    elseif ((int) $tabela['coluna_dados_escala'][$tpo] == 1) {
    	        $esc1 = "";
    	        $esc2 = "checked=\"checked\"";
    	    }
    	    
    		echo "<tr class=\"cor1\">
    		<td align=\"left\">".$col."</td>
    		<td align=\"left\">
    		  <input type=\"radio\" name=\"tipo[$tpo]\" value=\"column\" $tipo1 /> Coluna
    		  <input type=\"radio\" name=\"tipo[$tpo]\" value=\"line\" $tipo2 /> Linha
    		</td>
    		<td align=\"left\">
    		  <input type=\"radio\" name=\"esc[$tpo]\" value=\"1\" $esc1 /> Normal
    		  <input type=\"radio\" name=\"esc[$tpo]\" value=\"2\" $esc2 /> Idependente
    		</td>
    		</tr>";
    		
    		$i++;
    	}
    }

	echo "</table></div>";
	echo "</fieldset><br clear=\"all\">
	<input type=\"submit\" value=\"Atualizar Tabela\" name=\"env_form\" class=\"botao\" />
	</form>";
}
echo "</div></body></html>";

?>