<?
/**
* Plano padr�o e suas atividades
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package planejamento
*/
if (($exe == 1) or ($exe == "")) {
    $campos = array();
    $campos[] = "MID_EMPRESA";
    $campos[] = "DESCRICAO";
    $campos[] = "TIPO";
    $campos[] = "MID_MAQUINA";
    $campos[] = "MAQUINA_FAMILIA";
    $campos[] = "EQUIPAMENTO_FAMILIA";
    $campos[] = "OBSERVACAO";
    
    ListaTabela (PLANO_PADRAO,"MID",$campos,"PLANOPADRAO","","",1,1,1,1,0,1,1,1,1,1,"PP");
}
if  ((($exe == 11) or ($exe == 111)) and ($oq != "")) {
    echo "<div id=\"mod_menu\">
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['voltar']."\" />
<span>".$ling['voltar']."</span>
</a>
</div>";

    if(VoltaPermissao($id, $op) != 3){
        echo "<div>
<a href=\"manusis.php?id=$id&op=$op&exe=111&oq=$oq\" onclick=\"return confirma(this, '".$ling['conf_copiar_plano']."')\">
<img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" alt=\"".$ling['copiar_plano']."\" />
<span>".$ling['copiar_plano']."</span>
</a>
</div>";
    }

    echo "<div>
	<h3>".$ling['plano'].": ".htmlentities(VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$oq,$tdb[PLANO_PADRAO]['dba']))."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";

}
if  ($exe == 11) {
    $tipo=VoltaValor(PLANO_PADRAO,"TIPO","MID",$oq,$tdb[PLANO_PADRAO]['dba']);
    if ($tipo == 1) {
        $campos = array();
        $campos[] = "NUMERO";
        $campos[] = "MID_CONJUNTO";
        $campos[] = "PARTE";
        $campos[] = "TAREFA";
        $campos[] = "INSTRUCAO_DE_TRABALHO";
        $campos[] = "PERIODICIDADE";
        $campos[] = "FREQUENCIA";
        $campos[] = "ESPECIALIDADE";
        $campos[] = "TEMPO_PREVISTO";
        $campos[] = "QUANTIDADE_MO";
        $campos[] = "MAQUINA_PARADA";      
        $campos[] = "MID_MATERIAL";
        $campos[] = "QUANTIDADE";
        $campos[] = "CONTADOR";
        $campos[] = "DISPARO";
        
        ListaTabela (ATIVIDADES,"MID",$campos,"ATIVIDADE2","MID_PLANO_PADRAO",$oq,1,1,1,1,1,1,1,1,1,1,"PREVATIV");
    }
    else {
        $campos = array();
        $campos[] = "NUMERO";
        $campos[] = "PARTE";
        $campos[] = "TAREFA";
        $campos[] = "INSTRUCAO_DE_TRABALHO";
        $campos[] = "PERIODICIDADE";
        $campos[] = "FREQUENCIA";
        $campos[] = "ESPECIALIDADE";
        $campos[] = "TEMPO_PREVISTO";
        $campos[] = "QUANTIDADE_MO";
        $campos[] = "MAQUINA_PARADA";      
        $campos[] = "MID_MATERIAL";
        $campos[] = "QUANTIDADE";
        $campos[] = "CONTADOR";
        $campos[] = "DISPARO";

        ListaTabela (ATIVIDADES,"MID",$campos,"ATIVIDADE","MID_PLANO_PADRAO",$oq,1,1,1,1,1,1,1,1,1,1,"PREVATIV");
    }

    if (VoltaPermissao($id, $op) != 3) {
        // caso tenha atividade com status=2, mostra bot�o de atualizar
        $sql="SELECT COUNT(*) AS TOTAL FROM ".ATIVIDADES." A, ".PROGRAMACAO." P WHERE A.STATUS=2 AND A.MID_PLANO_PADRAO = '$oq' AND P.MID_PLANO = A.MID_PLANO_PADRAO AND P.STATUS != 2";
        $tmp2=$dba[$tdb[ATIVIDADES]['dba']] ->Execute($sql);
        // se tem atividade modificada
        $num = $tmp2->fields['TOTAL'];
        if ($num) {
            $msg = "{$ling['prog_atv_modificadas']}<br />
    		<a href=\"javascript:janela_varvore('modulos/planejamento/atualiza_ativ_editada.php?tipo=1&plano=$oq','atualiza_programacao')\">
    		{$ling['clique_aqui']}</a> {$ling['prog_atualizar']}";
            blocomsg($msg,4);
        }
    }
}
echo "</div>";

if (($exe == 111) and ($oq != "") and ($_GET['confirma'] == 1)) {
    $r=$dba[$tdb[PLANO_PADRAO]['dba']] -> Execute("SELECT * FROM ".PLANO_PADRAO." WHERE MID = '$oq'");
    if (!$r) erromsg($dba[$tdb[PLANO_PADRAO]['dba']] -> ErrorMsg());
    else {
        $pla=$r->fields;
        $pla['MID'] = GeraMid(PLANO_PADRAO,"MID",$tdb[PLANO_PADRAO]['dba']);
        $pla['DESCRICAO'] .= " (C�PIA)";

        // Criando o novo
        $sql = "INSERT INTO ".PLANO_PADRAO." (" . implode(", ", array_keys($pla)) . ") VALUES ('" . implode("', '", array_values($pla)) . "')";
        if (! $dba[$tdb[PLANO_PADRAO]['dba']] -> Execute ($sql)) {
            erromsg("".$dba[$tdb[PLANO_PADRAO]['dba']] -> ErrorMsg());
        }
        
        $r2=$dba[$tdb[ATIVIDADES]['dba']] -> Execute("SELECT * FROM ".ATIVIDADES." WHERE MID_PLANO_PADRAO = '$oq' ");
        $ip=0;
        while (!$r2->EOF) {
            $at=$r2->fields;
            $at['MID'] = GeraMid(ATIVIDADES,"MID",$tdb[ATIVIDADES]['dba']);
            $at['MID_PLANO_PADRAO'] = $pla['MID'];
    
            // Criando o novo
            $sql = "INSERT INTO ".ATIVIDADES." (" . implode(", ", array_keys($at)) . ") VALUES ('" . implode("', '", array_values($at)) . "')";
            if (! $dba[$tdb[PLANO_PADRAO]['dba']] -> Execute ($sql)) {
                erromsg("".$dba[$tdb[PLANO_PADRAO]['dba']] -> ErrorMsg());
            }
            $r2->MoveNext();
            $ip++;
        }
        echo "<h3>".$ling['copiar_plano']."</h3>
        </div>
        </div>
        <br clear=\"all\" />
        <div><br /><br /><p align=\"center\"></h2><br /> <br /><a href=\"manusis.php?id=$id&op=$op&exe=11&oq={$pla['MID']}\">".$ling['acessar_plano_copiado']."</a><br /> <br /><br /> <br /></p></div>";
    }
}
?>