<?
/**
* Visualiza��o dos graficos do monitoramento (preditiva)
*
* @author  Fernando Cosentino
* @version 0.1 
* @package planejamento
* @subpackage preditiva
*/
ob_start();

function diefile($msg) {
	file_put_contents('die.txt',$msg);	
}

//if (!$noinclude) {
	if (!include("../../lib/mfuncoes.php")) diefile ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
	if (!include("../../conf/manusis.conf.php")) diefile ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
	if (!include("../../lib/idiomas/".$manusis['idioma'][0].".php")) diefile ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
	if (!include("../../lib/adodb/adodb.inc.php")) diefile ($ling['bd01']);
	if (!include("../../lib/bd.php")) diefile ($ling['bd01']);
	
	if (!include("../../modulos/relatorios/funcoes.php")) diefile ($ling['bd01']);
	elseif (!include("../../modulos/relatorios/conf.php")) diefile ($ling['bd01']);
	
	include("../../lib/swfcharts/charts.php");
//}

// transi��es dos graficos

// troca , por . ?
$trocavirg = 1;
$troca1 = ',';
$troca2 = '.';
// caracteres em volta dos valores
$carac1 = '';
$carac2 = '';

// GET
$ponto = (int)$_GET['ponto'];
$last = (int)$_GET['last'];

// Valores do ponto
$grand = VoltaValor(PONTOS_PREDITIVA,'GRANDEZA','MID',$ponto,0);
$desc = VoltaValor(PONTOS_PREDITIVA,'PONTO','MID',$ponto,0);
$maq_mid = (int)VoltaValor(PONTOS_PREDITIVA,'MID_MAQUINA','MID',$ponto,0);
$maq_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$maq_mid,0);

$max = VoltaValor(PONTOS_PREDITIVA,'VALOR_MAXIMO','MID',$ponto,0);
$min = VoltaValor(PONTOS_PREDITIVA,'VALOR_MINIMO','MID',$ponto,0);

// calcula 10% de margem
$tenperc = round( ($max - $min) / 10 );

function FlipArray($arr) {
	$cols = count($arr[0]);
	$rows = count($arr);

	for ($i=0; $i<$cols; $i++) {
		for ($j=0; $j<$rows; $j++) {
			$arr2[$i][$j] = $arr[$j][$i];
		}
	}
	return $arr2;
}
function mktimeData ($data) {
	//$resultado -> UserDate($cc_r[$campos[$i]],'d/m/Y');
	list($nossadata,$hora) = explode(' ',$data,2);
	list($a,$m,$d) = explode('-',$nossadata,3);
	list($h,$j,$s) = explode(':',$hora,3);
	$tmp = mktime($h,$j,$s,$m,$d,$a);
	echo "[/ $nossadata = $d $m $a = $tmp /]\r\n";
	return $tmp;
}

$i=$j=0;
$itens=array();
$sql = "SELECT * FROM ".LANC_PREDITIVA." WHERE MID_PONTO = '$ponto' ORDER BY DATA DESC";
//echo "[$sql]\r\n";
$tmp=$dba[0] ->Execute($sql);
while (!$tmp->EOF) {
	$campo = $tmp->fields;
	$i++;
	if ($i > $last) { // come�a a contar os registros
		//echo "($i,$last)";
		$j++;
		if ($j <= 10) { // ainda n�o encheu tudo
			$itens[mktimeData($campo['DATA'])] = $campo;
		}
		else break; // se ja acabou, termina logo pra nao gastar tempo de processador
	}
	$tmp->MoveNext();
}

$itens_keys = array_keys($itens);
sort($itens_keys);

$chartd = $chartt = array();
$chartd[0][0]='';
$chartd[1][0]=utf8_encode("$desc - $grand");
$chartd[2][0]=utf8_encode("M�ximo");
$chartd[3][0]=utf8_encode("M�nimo");

$chartt[0][0]=null;
$chartt[1][0]=null;
$chartt[2][0]=null;
$chartt[3][0]=null;


$qtd=0;
foreach ($itens_keys as $eitem_key) {
	$eitem = $itens[$eitem_key];
//	$chartd[0][] = NossaData($eitem['DATA']);
	$chartd[0][] = date("d/m H",$eitem_key)."h";
	$chartd[1][] = $eitem['VALOR'];
	$chartd[2][] = $max;
	$chartd[3][] = $min;

	//$chartt[0][] = null;
	list($t1,$t2) = explode('.',$eitem['VALOR'],2);
	if ($t2[2] == '0') {
		$t2 = substr($t2,0,2);
		if ($t2[1] == '0') $t2 = $t2[0];
	}

	$chartt[1][] = "$t1.$t2";

	$chartt[2][] = '';
	$chartt[3][] = '';

	$qtd++;
}


$chart['chart_data']=$chartd;
$chart['chart_value_text']=$chartt;

########
// configura��es de apar�ncia do gr�fico
$chart['chart_type'] = 'line';
$chart['legend_label']['size']=8;
$chart['axis_category']['size']=8;
//$chart['axis_category']['margin']=true;
//$chart['axis_category']['orientation']='vertical_up';
$chart['axis_value']['min']=$min-$tenperc;
$chart['axis_value']['max']=$max+$tenperc;
$chart['axis_value']['size']=8;
$chart['axis_value']['suffix']="     ";
$chart['axis_value']['steps']=6;

$chart['chart_value']['position']='above';
$chart['chart_value']['size']=8;

// cores das linhas
$chart['series_color'] = array (
	"0000FF", // valor
	"CC3300", // m�x
	"993366"  // min
);

// espessuras das linhas em %
$chart['series_explode'] = array ( 100, 70, 70 );
$chart['chart_pref']['point_shape'] = 'none';


// textos no grafico
$titulo = utf8_encode("Monitoramento: ".$desc);
/*$chart['draw'][] = array (
		'type'       => "text",
		'x'          => 30, 
		'y'          => 10, 
		'width'      => 700,  
		'h_align'    => "center", 
		'text'       => $titulo,  
		'size'       => 16, 
		'color'      => "000066", 
);*/

// conta quantas entradas tem na primeira linha
// se s� tem a linha das descri��es, mostra que n�o tem coluna

if ((count($chartd) < 2) or (count($chartd[0]) < 2))
	$chart['draw'][] = array (
			'type'       => "text",
			'x'          => 16, 
			'y'          => 70, 
			'width'      => 500,  
			'h_align'    => "center", 
			'text'       => 'Nenhum dado encontrado',  
			'size'       => 16, 
			'color'      => "000066", 
	);

###
// este php ser� chamado por um swf, e portanto n�o se pode usar echo para visualizar nada
// assim, usa-se o arquivo temp.txt em modulos/principal
// d� print->temp.txt com o conte�do de $chart, celulas entre colchetes

$chartstring = "<echo>\r\n{".ob_get_contents()."}\r\n</echo>\r\n"
	.$savestr.
	"tbinfo['coluna_mestre_tipo']={$tbinfo['coluna_mestre_tipo']}\r\n
i1=$i1\r\n
i2=$i2\r\n";
foreach($chartd as $chartline) {
	foreach ($chartline as $chartitem) $chartstring .= "[$chartitem] ";
	$chartstring .= "\r\n";
}
file_put_contents('temp.txt',$chartstring);

ob_end_clean();

if ($_GET['print']) {
	$chartdflip = $chartd;
	unset($chartdflip[2]);
	unset($chartdflip[3]);
	$chartdflip = FlipArray($chartdflip);
	
	$tdstyle="style=\"font-size:10px\"";
	// primeira linha: PONTO GRANDEZA
	echo "<html><head><link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body>";
	echo "<table id=\"lt_tabela\">";
	foreach ($chartdflip as $ck => $chartl) {
		if ($ck == 0) echo "<tr>
			<th $tdstyle>Data / Hora</th><th $tdstyle>Valor</th></tr>";
		else {
			if (($chartl[1] > $max) or ($chartl[1] < $min)) $value = "<font color=\"#FF0000\"><strong>{$chartl[1]}</strong></font>";
			else $value = $chartl[1];
			if (($trcor) == 'cor1') $trcor = 'cor2'; else $trcor = 'cor1';
			echo "<tr class=\"$trcor\"><td $tdstyle>{$chartl[0]}</td><td $tdstyle>$value</td></tr>";
		}
	}
	echo "</table>";
	echo "</body></html>";
}
elseif ($_GET['excel']) {
	
	// primeira linha: PONTO [data] [data] [data] [data] ... [data]
	$doc .="
	<tr height=17 style='height:12.75pt'>";
	foreach ($chartd[0] as $ck => $chartl) {
		if ($ck == 0) $doc .= "<td class=xl26 height=17 style='height:12.75pt'>DATA</td>";
		else $doc .= "<td class=xl26>$chartl</td>";
	}
	$doc .= "</tr>";

	// segunda linha: GRANDEZA [valor] [valor] [valor] [valor] ... [valor]
	$doc.= " <tr height=17 style='height:12.75pt'>";
	foreach ($chartd[1] as $ck => $chartl) {
		if ($ck == 0) $doc .= "<td class=xl26 height=17 style='height:12.75pt'>$grand</td>";
		else $doc .= "<td class=xl26 align=right x:num=\"".$chartl."\">".str_replace('.',',',$chartl)."</td>";
	}
	$doc.= "</tr>";

	$qtdlt = QtdLetra($qtd);
	$graf="<tr height=17 style='height:12.75pt'>
  <td height=17 colspan=14 style='height:12.75pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=17 style='height:12.75pt'>
  <td height=17 style='height:12.75pt' align=left valign=top><!--[if gte vml 1]>
  <v:shapetype id=\"_x0000_t201\" coordsize=\"21600,21600\" o:spt=\"201\" path=\"m,l,21600r21600,l21600,xe\">
   <v:stroke joinstyle=\"miter\"/>
   <v:path shadowok=\"f\" o:extrusionok=\"f\" strokeok=\"f\" fillok=\"f\"
    o:connecttype=\"rect\"/>
   <o:lock v:ext=\"edit\" shapetype=\"t\"/>
  </v:shapetype><v:shape id=\"_x0000_s1026\" type=\"#_x0000_t201\" style='position:absolute;
   margin-left:1.5pt;margin-top:.75pt;width:738pt;height:268.5pt;z-index:1'
   fillcolor=\"window [78]\" strokecolor=\"windowText [77]\" o:insetmode=\"auto\">
   <v:fill color2=\"windowText [77]\"/>
   <o:lock v:ext=\"edit\" rotation=\"t\" text=\"t\"/>
<x:ClientData ObjectType=\"Chart\">
    <x:WebChart>
     <x:Scaling>
      <x:ScaleID>0</x:ScaleID>
      <x:Orientation>MinMax</x:Orientation>
     </x:Scaling>
     <x:Scaling>
      <x:ScaleID>1</x:ScaleID>
      <x:Orientation>MinMax</x:Orientation>
     </x:Scaling>
     <x:Chart>
      <x:Name>Gr�fico</x:Name>
      <x:Title>
       <x:Caption>
        <x:DataSource>-1</x:DataSource>
        <x:Data>&quot;<x:B>MONITORAMENTO: $desc - $grand</x:B>&quot;</x:Data>
       </x:Caption>
       <x:Font>
        <x:FontName>Arial</x:FontName>
        <x:Size>12</x:Size>
        <x:B/>
        <x:AutoScale/>
       </x:Font>
       <x:Border>
        <x:ColorIndex>None</x:ColorIndex>
       </x:Border>
       <x:Interior>
        <x:ColorIndex>None</x:ColorIndex>
       </x:Interior>
      </x:Title>
      <x:Options>
       <x:SizeWithWindow/>
      </x:Options>
      <x:PageSetup>
       <x:ChartSize>FullPage</x:ChartSize>
       <x:Header Margin=\"0.49212598499999999\"/>
       <x:Footer Margin=\"0.49212598499999999\"/>
       <x:PageMargins Bottom=\"0.984251969\" Left=\"0.78740157499999996\"
        Right=\"0.78740157499999996\" Top=\"0.984251969\"/>
      </x:PageSetup>
      <x:Font>
       <x:FontName>Arial</x:FontName>
       <x:Size>12</x:Size>
       <x:AutoScale/>
      </x:Font>
      <x:Left>0</x:Left>
      <x:Top>0</x:Top>
      <x:Width>14774.9853515625</x:Width>
      <x:Height>5384.99267578125</x:Height>
      <x:ChartGrowth>
       <x:HorzGrowth>1</x:HorzGrowth>
       <x:VertGrowth>1</x:VertGrowth>
      </x:ChartGrowth>
      <x:PlotArea>
       <x:Border>
        <x:ColorIndex>15</x:ColorIndex>
        <x:LineStyle>Solid</x:LineStyle>
        <x:Weight>Narrow</x:Weight>
       </x:Border>
       <x:Interior>
        <x:ColorIndex>14</x:ColorIndex>
        <x:BGColorIndex>Neutral</x:BGColorIndex>
       </x:Interior>
       <x:Font>
        <x:FontName>Arial</x:FontName>
        <x:Size>12</x:Size>
        <x:AutoScale/>
       </x:Font>
       <x:Graph>
        <x:Type>Line</x:Type>
        <x:SubType>Standard</x:SubType>
        <x:ScaleID>0</x:ScaleID>
        <x:ScaleID>1</x:ScaleID>
        <x:Series>
         <x:Index>0</x:Index>
         <x:Caption>
          <x:DataSource>0</x:DataSource>
          <x:Data>Plan1!\$A\$5</x:Data>
         </x:Caption>
         <x:Name>INTERVALO</x:Name>
         <x:Category>
          <x:DataSource>0</x:DataSource>
          <x:Data>Plan1!\$B\$4:\$$qtdlt\$4</x:Data>
         </x:Category>
         <x:Value>
          <x:DataSource>0</x:DataSource>
          <x:Data>Plan1!\$B\$5:\$$qtdlt\$5</x:Data>
         </x:Value>
        </x:Series>
        <x:DataLabels>
         <x:Border>
          <x:ColorIndex>None</x:ColorIndex>
         </x:Border>
         <x:Font>
          <x:FontName>Arial</x:FontName>
          <x:Size>12</x:Size>
          <x:AutoScale/>
         </x:Font>
         <x:Interior>
          <x:ColorIndex>None</x:ColorIndex>
         </x:Interior>
         <x:Number>
          <x:SourceLinked/>
          <x:BuiltInFormat>0</x:BuiltInFormat>
         </x:Number>
         <x:ShowValue/>
        </x:DataLabels>
        <x:PlotVisible/>
       </x:Graph>
       <x:Axis>
        <x:Placement>Bottom</x:Placement>
        <x:AxisID>0</x:AxisID>
        <x:ScaleID>0</x:ScaleID>
        <x:CrossingAxis>1</x:CrossingAxis>
        <x:Font>
         <x:FontName>Arial</x:FontName>
         <x:Size>8</x:Size>
         <x:AutoScale/>
        </x:Font>
        <x:Number>
         <x:SourceLinked/>
         <x:BuiltInFormat>0</x:BuiltInFormat>
        </x:Number>
        <x:Type>Automatic</x:Type>
       </x:Axis>
       <x:Axis>
        <x:Placement>Left</x:Placement>
        <x:AxisID>1</x:AxisID>
        <x:ScaleID>1</x:ScaleID>
        <x:MajorGridlines/>
        <x:CrossingAxis>0</x:CrossingAxis>
        <x:CrossesAt>Minimum</x:CrossesAt>
        <x:CrossBetween>Between</x:CrossBetween>
        <x:Font>
         <x:FontName>Arial</x:FontName>
         <x:Size>12</x:Size>
         <x:AutoScale/>
        </x:Font>
        <x:Number>
         <x:SourceLinked/>
         <x:BuiltInFormat>0</x:BuiltInFormat>
        </x:Number>
        <x:Type>Value</x:Type>
       </x:Axis>
      </x:PlotArea>
      <x:Legend>
       <x:Placement>Bottom</x:Placement>
       <x:Font>
        <x:FontName>Arial</x:FontName>
        <x:Size>8</x:Size>
        <x:AutoScale/>
       </x:Font>
      </x:Legend>
     </x:Chart>
    </x:WebChart>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:2px;margin-top:1px;width:985px;
  height:359px'><![endif]><![if !excel]><![endif]><![if !vml]></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=17 width=120 style='height:12.75pt;width:90pt'></td>
   </tr>
  </table>
  </span></td>
  <td colspan=13 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=374 style='height:280.5pt;mso-xlrowspan:22'>
  <td height=374 colspan=14 style='height:280.5pt;mso-ignore:colspan'></td>
 </tr>";
	//$filtro="$filtro<br>Per�odo de: ".$_GET['datai']." a ".$_GET['dataf'];
	$filtro = "$maq_desc - $desc";
	gera_grafico("MONITORAMENTO",$filtro,"",$doc,$graf);
	
}
else SendChartData($chart);

function QtdLetra($qtd) {
	if ($qtd == 0) return 'B';
	elseif ($qtd == 1) return 'B';
	elseif ($qtd == 2) return 'C';
	elseif ($qtd == 3) return 'D';
	elseif ($qtd == 4) return 'E';
	elseif ($qtd == 5) return 'F';
	elseif ($qtd == 6) return 'G';
	elseif ($qtd == 7) return 'H';
	elseif ($qtd == 8) return 'I';
	elseif ($qtd == 9) return 'J';
	else return 'K'; // 10
}

?>
