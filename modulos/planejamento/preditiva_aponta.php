<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: M�dulo Planejamento, preditiva
 *
 * exe = 3: mostrando SELECT de maquinas e equipamentos para o usuario escolher
 * 	- n�o possui entradas previas
 *
 * exe = 4: mostrando a lista de apontamentos
 * 	- um OU outro:
 * 		GET['maq'] = MID_MAQUINA
 * 		GET['equip'] = MID_EQUIPAMENTO
 *
 */

$phpself = "manusis.php?id=$id&op=$op&exe=4&maq=$maq&equip=$equip";

$maq = (int)$_GET['maq'];
$equip = (int)$_GET['equip'];




if ($exe == 3) {
    /**
     * Mostra lista de maquinas/equipamentos para o usu�rio escolher. 
     * "onchange" atualiza tela com os pontos daquela maquina/equipamento
     */
    echo "<div id=\"mod_menu\">
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"".$ling['preditiva']."\" />
<span>".$ling['preditiva']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/preditiva_aponta.png\" border=\"0\" alt=\"".$ling['preditiva_aponta']."\" />
<span>".$ling['preditiva_aponta']."</span>
</a>
</div>

<div>
	<h3>".$ling['preditiva_aponta']."</h3>
</div>
</div>
<br clear=\"all\" />
<div id=\"formularioos\">";

    echo "<br />
	<fieldset>
	<legend>".$ling['pontos_preditiva']."</legend>
	<label class=\"campo_label\" for=\"maq\">{$tdb[MAQUINAS]['DESC']}</label>";

    $sql = "SELECT MID_MAQUINA FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA != '0' GROUP BY MID_MAQUINA";
    $tmp=$dba[0] ->Execute($sql);
    $sqlcond="MID = '-1'";
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        AddStr($sqlcond,' OR ',"MID = '{$campo['MID_MAQUINA']}'");
        $tmp->MoveNext();
    }
    if ($sqlcond) $sqlcond = "WHERE ($sqlcond)";

    FormSelectD('COD', 'DESCRICAO', MAQUINAS, $_GET['maq'], 'maq', 'maq', 'MID', '', '', '', $sqlcond);

    echo "<input type=\"button\" class=\"botao\" value=\"Ver\" id=\"ver_pontos\" name=\"ver_pontos\" onclick=\"if(document.getElementById('maq').value != 0){location.href='manusis.php?id=$id&op=$op&exe=4&maq=' + document.getElementById('maq').value}\" />
	<br clear=\"all\" />";

}
if (($exe == 4) and (($maq) or ($equip))) {
    /**
     * J� escolheu uma maquina ou equipamento
     * Mostra:
     * - lista de pontos de preditiva (monitoramento) com formul�rio para preenchimento
     */
    if ($maq)   $title = VoltaValor(MAQUINAS,'DESCRICAO','MID',$maq,0);
    if ($equip)   $title = VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$equip,0);

    echo "<div id=\"mod_menu\">
	<div>
    <a href=\"manusis.php?id=$id&op=$op&exe=3\">
    <img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['voltar']."\" />
    <span>".$ling['voltar']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=4&maq=$maq&equip=$equip\">
    <img src=\"imagens/icones/22x22/preditiva_aponta.png\" border=\"0\" alt=\"".$ling['preditiva_aponta']."\" />
    <span>".$ling['preditiva_aponta']."</span>
    </a>
    </div>
    
    <div>
    	<h3>".$ling['pontos_preditiva_aponta'].":<br />$title</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";

    // Salvando
    if ($_POST['env']) {
        $cc = $_POST['cc'];
        foreach ($cc as $ponto => $campo) {
            // adiciona
            $datasql = DataSQL($_POST['data_' . $ponto]).' '.$campo['hora'];
            $valor = str_replace(',','.',$campo['valor']);

            $nmid = (int)VoltaValor(LANC_PREDITIVA,'MID',"MID_PONTO = '$ponto' AND DATA",$datasql,0);
            if (!$nmid){
                $nmid = GeraMid(LANC_PREDITIVA,'MID',0);
                $sql = "INSERT INTO ".LANC_PREDITIVA." (MID_PONTO,DATA,VALOR,MID) VALUES ('$ponto','$datasql','$valor','$nmid')";
            }
            else $sql = "UPDATE ".LANC_PREDITIVA." SET VALOR = '$valor' WHERE MID = '$nmid'";
            $dba[0] ->Execute($sql);
        }
        blocomsg($ling['cadastro_sucesso'],3);
    }

    // Buscando
    $sql = "SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA = '$maq'";
    $tmp=$dba[0] ->Execute($sql);

    $i=0;
    $trc = 'cor1';
    echo "<div id=\"lt_tabela\">
	<form method=\"POST\" action=\"$phpself\">
	<table width=\"100%\" id=\"lt_tabela_\">
	<tr><th>{$ling['pontos_preditiva']}</th><th>{$ling['data']}</th><th>{$ling['hora']}</th><th>{$ling['valor']}</th></tr>";
    
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        $i++;
        $emid = $campo['MID'];
        echo "<tr class=\"$trc\">
		<td>{$campo['PONTO']} - {$campo['GRANDEZA']}</td>
		<td width=160>";

        FormData('',"data_" . $emid,'','campo_label');

        echo "</td>
		<td width=120>
		<input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"cc[$emid][hora]\" class=\"campo_text_ob\" name=\"cc[$emid][hora]\" size=\"8\" maxlength=\"8\"  value=\"\"/>
		</td><td width=200>
		<input type=\"text\" class=\"campo_text_ob\" name=\"cc[$emid][valor]\" id=\"cc[$emid][valor]\" value=\"0\" />
		</td></tr>";

        if ($trc == 'cor1') $trc = 'cor2';
        else $trc = 'cor1';

        $tmp->MoveNext();
    }
    
    echo "</table>
	<br clear=\"all\" />
	<center>
	<input type=\"submit\" name=\"env\" value=\"Salvar\" class=\"botao\" />
	</center>
	
	</form>
	</div>";
}



?>