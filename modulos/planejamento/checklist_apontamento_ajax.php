<?
/**
* Modulo planejamento, checklist (ajax)
* 
* @author  Fernando Cosentino
* @version  3.0
* @package principal
* @subpackage resumo
*/

// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/bd.php")) die ($ling['bd01']);

$ajax = $_GET['ajax'];
$mid_lanca = (int)$_GET['mid_lanca_checklist'];
$phpajax = 'modulos/planejamento/checklist_apontamento_ajax.php';

if ($ajax == 'add') {
	$mid_tarefa = $_GET['tarefa'];
	$f_obs = $_GET['obs'];
	$f_data = $_GET['data'];
	$mid_lanca_checklist = $mid_lanca;
	$mid_maq = VoltaValor(LANCA_CHECKLIST,'MID_MAQUINA','MID',$mid_lanca_checklist,0);
	$f_descricao = VoltaValor(PLANO_CHECKLIST_ATIVIDADES,'TAREFA','MID',$mid_tarefa,0);
	list($edia, $emes, $eano) = explode('/',$f_data,3);
	$descricao = $f_descricao." - ".$f_obs;
	
	$mid_pendencia = GeraMid(PENDENCIAS,'MID',0);
	
	// verifica se ta tudo ok
	$erromsg = '';
	if (!$mid_lanca_checklist) AddStr($erromsg,'<br>',$ling['prog_lancamento']);
	elseif (!$mid_maq) AddStr($erromsg,'<br>',$tdb[MAQUINAS]['DESC'].$ling['prog_nao_encontrado']);
	elseif (!$f_descricao) AddStr($erromsg,'<br>',$ling['prog_tarefa_descrita']);
	elseif (!$f_data) AddStr($erromsg,'<br>',$ling['data_preenche']);
	elseif (!checkdate($emes,$edia,$eano)) AddStr($erromsg,'<br>',$ling['data_formato_invalida']);
	else {
		// cadastra
		$sql = "INSERT INTO ".PENDENCIAS." (`MID_ORDEM`,`MID_MAQUINA`,`MID_CONJUNTO`,`DESCRICAO`,`DATA`,`MID_ORDEM_EXC`,`MID_LANCA_CHECKLIST`,MID) VALUES ('0', '$mid_maq', '0', '$descricao', '".DataSQL($f_data)."', '0', '$mid_lanca_checklist','$mid_pendencia')";
		$dba[0] ->Execute($sql);
	}
	if ($erromsg) erromsg($erromsg);
}
if ($ajax == 'del') {
	$mid_pendencia = (int)$_GET['mid_pendencia'];
	$sql = "DELETE FROM ".PENDENCIAS." WHERE MID='$mid_pendencia' LIMIT 1";
	$dba[0] ->Execute($sql);
}

if ($ajax) {
	// pega as pendencias com este checklist
	echo "<table><tr><th>{$tdb[PENDENCIAS]['DESC']}</th><th>{$tdb[PENDENCIAS]['DATA']}</th><th></th></tr>";
	$sql = "SELECT * FROM ".PENDENCIAS." WHERE MID_LANCA_CHECKLIST='$mid_lanca' ORDER BY NUMERO ASC";
	$tdclass="cor1";
	$tmp2=$dba[0] ->Execute($sql);
	while (!$tmp2->EOF) {
		$campo2 = $tmp2->fields;
		echo "<tr class=\"$tdclass\"><td>{$campo2['NUMERO']} - {$campo2['DESCRICAO']}</td>
		<td>".NossaData($campo2['DATA'])."</td>
		<td><a href=\"javascript:atualiza_area2('lanca$mid_lanca','$phpajax?ajax=del&mid_pendencia={$campo2['MID']}&mid_lanca_checklist=$mid_lanca')\" onclick=\"return confirm('{$ling['confirma_remover']}')\">
		<img src=\"imagens/icones/22x22/del.png\" border=0></a></td></tr>\n";
		if ($tdclass == 'cor1') $tdclass = 'cor2';
		else $tdclass = 'cor1';
		$tmp2->MoveNext();
	} // fim de cada atividade
	echo "</td></tr></table>";
}




?>
