<?php

// Recuperando valores
if ((int)$_GET['modo_visao2'] != 0) {
    $_SESSION[ManuSess]['lt']['os']['modo_visao2'] = (int)$_GET['modo_visao2'];
}
elseif ($_SESSION[ManuSess]['lt']['os']['modo_visao2'] == 0) {
    $_SESSION[ManuSess]['lt']['os']['modo_visao2'] = 1;
}
$modo_visao = $_SESSION[ManuSess]['lt']['os']['modo_visao2'];


if ((int)$_GET['agregador'] != 0) {
    $_SESSION[ManuSess]['lt']['os']['agregador'] = (int)$_GET['agregador'];
}
elseif ($_SESSION[ManuSess]['lt']['os']['agregador'] == 0) {
    $_SESSION[ManuSess]['lt']['os']['agregador'] = 1;
}
$agregador = $_SESSION[ManuSess]['lt']['os']['agregador'];


// CALCULANDO AS DATAS
// Semana
if ($modo_visao == 1) {
    $data_inicial = DataSQL(VoltaDataSeg(date('W'), date('o')));
    $data_final   = DataSQL(VoltaDataDom(date('W'), date('o')));
}
// Mes
elseif ($modo_visao == 2) {
    $data_inicial = date('Y') . '-' . date('m') . '-01';
    $data_final   = date('Y') . '-' . date('m') . '-' . date('t');
}
// Periodo
elseif ($modo_visao == 3) {
    if ($_GET['datai_bl'] != '') {
        $_SESSION[ManuSess]['lt']['os']['datai_bl'] = $_GET['datai_bl'];
    }
    elseif ($_SESSION[ManuSess]['lt']['os']['datai_bl'] == '') {
        $_SESSION[ManuSess]['lt']['os']['datai_bl'] = '01/' . date('m') . '/' . date('Y');
    }
    
    if ($_GET['dataf_bl'] != 0) {
        $_SESSION[ManuSess]['lt']['os']['dataf_bl'] = $_GET['dataf_bl'];
    }
    elseif ($_SESSION[ManuSess]['lt']['os']['dataf_bl'] == '') {
        $_SESSION[ManuSess]['lt']['os']['dataf_bl'] = date('t') . '/' . date('m') . '/' . date('Y');
    }
    
    
    $data_inicial = DataSQL($_SESSION[ManuSess]['lt']['os']['datai_bl']);
    $data_final   = DataSQL($_SESSION[ManuSess]['lt']['os']['dataf_bl']);
}

// FILTROS
$sqlvar = "";

if (($filtro_origem != 0) and ($filtro_origem != 5)) $sqlvar.="AND O.TIPO = '$filtro_origem' ";
elseif ($filtro_origem == 5) $sqlvar.="AND O.TIPO = '0' ";

if ($filtro_empresa != 0) $sqlvar.="AND O.MID_EMPRESA = '$filtro_empresa' ";
if ($filtro_area != 0) $sqlvar.="AND O.MID_AREA = '$filtro_area' ";
if ($filtro_setor != 0) $sqlvar.="AND O.MID_SETOR = '$filtro_setor' ";
if (count($filtro_natureza) > 0) $sqlvar .= "AND O.NATUREZA IN (".implode(", ", array_values($filtro_natureza)).") ";

// Filtro por empresa
$filtro_tmp = VoltaFiltroEmpresa(ORDEM, 2, false, 'O');
if($filtro_tmp != '') {
    // MAQUINAS VISIVEIS NAS ROTAS
    $filtro_lub = VoltaFiltroEmpresa(ORDEM_LUB, 2);
    // PERMISSÃO POR EMPRESA NAS OS E EM ROTAS
    $sqlvar .= " AND ($filtro_tmp OR (O.TIPO = 2 AND (SELECT COUNT(MID) FROM " . ORDEM_LUB . " WHERE MID_ORDEM = O.MID AND $filtro_lub) > 0))";
}

// FILTRO POR CENTRO DE CUSTO
if(($_SESSION[ManuSess]['user']['MID'] != 0) and (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'MID', 'USUARIO', (int)$_SESSION[ManuSess]['user']['MID']) != 0)) {
   $sqlvar .= " AND O.CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}')";
}

// FILTRO POR DESCRIÇÃO
if ($desc != '') {
    $desc_sql = LimpaTexto($desc);

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2, false, "M");
    $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

    // Filtra as maquinas
    $sql = "SELECT M.MID, M.COD, M.DESCRICAO FROM ".MAQUINAS." M WHERE (M.COD LIKE '%$desc_sql%' OR M.DESCRICAO LIKE '%$desc_sql%') $fil_emp";
    $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
    $filsql = '';
    while (! $resultado->EOF) {
        $campo = $resultado->fields;
        $filsql .= ($filsql != "")? " OR " : "";
        $filsql .= "O.MID_MAQUINA = '{$campo['MID']}'";
        $resultado->MoveNext();
    }

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(EQUIPAMENTOS, 2);
    $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

    // Filtra equipamentos
    $sql = "SELECT MID FROM ".EQUIPAMENTOS." WHERE (COD LIKE '%$desc_sql%' OR DESCRICAO LIKE '%$desc_sql%')  $fil_emp";
    $resultado=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($sql);
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $filsql .= ($filsql != "")? " OR " : "";
        $filsql .= "O.MID_EQUIPAMENTO = '{$campo['MID']}'";
        $resultado->MoveNext();
    }

    // Busca na descrição do servico
    $filsql .= ($filsql != "")? " OR " : "";
    $filsql .= "O.TEXTO LIKE '%$desc_sql%'";


    $sqlvar .= " AND ($filsql)";
}

// JUNTANDO OS VALORES
$dados = array();

// BUSCANDO INFORMAÇÕES
$sql = "SELECT O.* FROM " . ORDEM . " O WHERE O.STATUS = 1 AND O.DATA_PROG >= '" . $data_inicial . "' AND O.DATA_PROG <= '" . $data_final . "' $sqlvar";
if(! $rs = $dba[$tdb[ORDEM]['dba']] -> Execute($sql)) {
    erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
while(! $rs->EOF) {
    $os = $rs->fields;
    $osmid = $os['MID'];
    
    if ($agregador == 3) {
        if (($rs -> fields['TIPO'] == 1) or ($rs -> fields['TIPO'] == 2)) {
            $ag_valor = "T" . $rs -> fields['TIPO'];
        }
        else {
            $ag_valor = (int) $rs -> fields['TIPO_SERVICO'];
        }
    }
    elseif ($agregador == 4) {
        $ag_valor = (int) $rs -> fields['MID_MAQUINA'];
    }
    
    // BUSCANDO TEMPO PREVISTO
    if (($agregador == 1) or ($agregador == 3) or ($agregador == 4)) {
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            if ($agregador == 1) {
                $ag_valor = (int) $rs_tmp->fields['MID_ESPECIALIDADE'];
            }
            
            // PREVISTO POR DIA
            if (! $dados[$ag_valor]['TP']) {
                $dados[$ag_valor]['TP'] = 0;
            }
            $dados[$ag_valor]['TP'] += $rs_tmp->fields['TEMPO'];

            $rs_tmp->MoveNext();
        }
    }
    elseif($agregador == 2) {
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            $ag_valor = (int) VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $rs_tmp->fields['MID_FUNCIONARIO']);

            // PREVISTO POR DIA
            if (! $dados[$ag_valor]['TP']) {
                $dados[$ag_valor]['TP'] = 0;
            }
            $dados[$ag_valor]['TP'] += $rs_tmp->fields['TEMPO'];

            $rs_tmp->MoveNext();
        }
    }
    
    
    $rs -> MoveNext();
}

// CALCULANDO O BACKLOG
$mki = VoltaTime("00:00:00", NossaData($data_inicial));
$mkf = VoltaTime("23:59:59", NossaData($data_final));
$dias_total = round(($mkf - $mki) / $mktime_1_dia);

// Buscando o previsto
if ($agregador == 1) {
    foreach ($dados as $ag_valor => $dado) {
        $dados[$ag_valor]['DESC'] = VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $ag_valor);
        $dados[$ag_valor]['DIS'] = TurnoTotalEspecialidade(NossaData($data_inicial), NossaData($data_final), $ag_valor) / $dias_total;
    }
}
elseif ($agregador == 2) {
    foreach ($dados as $ag_valor => $dado) {
        $dados[$ag_valor]['DESC'] = VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $ag_valor);
        $dados[$ag_valor]['DIS'] = TurnoTotalEquipe(NossaData($data_inicial), NossaData($data_final), $ag_valor) / $dias_total;
    }
}
elseif ($agregador == 3) {
    foreach ($dados as $ag_valor => $dado) {
        if ($ag_valor == 'T1') {
            $dados[$ag_valor]['DESC'] = VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', 1);
        }
        elseif ($ag_valor == 'T2') {
            $dados[$ag_valor]['DESC'] = VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', 2);
        }
        else {
            $dados[$ag_valor]['DESC'] = VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $ag_valor);
        }
        $dados[$ag_valor]['DIS'] = TurnoTotalGeral(NossaData($data_inicial), NossaData($data_final)) / $dias_total;
    }
}
elseif ($agregador == 4) {
    foreach ($dados as $ag_valor => $dado) {
        if ($ag_valor != 0) {
            $dados[$ag_valor]['DESC'] = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $ag_valor);
        }
        else {
            $dados[$ag_valor]['DESC'] = VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', 2);
        }
        
        $dados[$ag_valor]['DIS'] = TurnoTotalGeral(NossaData($data_inicial), NossaData($data_final)) / $dias_total;
    }
}


foreach ($dados as $ag_valor => $dado) {
    if (($dado['DIS'] == 0) or ($dado['TP'] == 0)) {
        $dados[$ag_valor]['BLOG'] = 0;
    }
    else { 
        $dados[$ag_valor]['BLOG'] = $dado['TP'] / $dado['DIS'];
    }
}

/*
echo "<pre>";
print_r($dados);
echo "</pre>";
*/



echo "
<br clear=\"all\" />


<table width=\"100%\" cellspacing=\"0\" style=\"font-size:10px;border:0px;\">
<tr>

<td class=\"".(($modo_visao == 1)?"aba_sel":"aba")."\" id=\"backlog_sem\">
<img src=\"imagens/icones/22x22/calendario.png\" border=\"0\" align=\"bottom\" /> " . $ling['semana_desc'] . ": " . date('W') . "
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 2)?"aba_sel":"aba")."\" id=\"backlog_mes\">
<img src=\"imagens/icones/22x22/calendario.png\" border=\"0\" align=\"bottom\" /> " . $ling['mes_label'] . ": " . $ling['mes'][date('n')] . "
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 3)?"aba_sel":"aba")."\" id=\"backlog_per\">
<img src=\"imagens/icones/22x22/calendario.png\" border=\"0\" align=\"bottom\" /> " . $ling['rel_desc_periodo'] . "
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"aba_fim\">&nbsp;";

// POR PERIODO
if ($modo_visao == 3) {
        FormData($ling['data'] . ":", "datai_bl", NossaData($data_inicial));
        
        echo " &agrave; ";

        FormData("", "dataf_bl", NossaData($data_final));
        
        echo "<input type=\"button\" value=\"IR\" class=\"botao\" name=\"env_data2\" id=\"env_data2\" />";
}

echo "</td>

</tr>
<tr><td colspan=\"20\" class=\"aba_quadro\" valign=\"top\">";


echo "<table border=\"0\" width=\"100%\" id=\"lt_tabela_\">
<thead>
<tr>
<th>
<label for=\"agregador\">Ver por:</label>
<select class=\"campo_select_ob\" name=\"agregador\" id=\"agregador\">
    <option value=\"1\" " . (($agregador == 1)? "selected=\"selected\"" : "") . ">" . $tdb[ESPECIALIDADES]['DESC'] . "</option>
    <option value=\"2\" " . (($agregador == 2)? "selected=\"selected\"" : "") . ">" . $tdb[EQUIPES]['DESC'] . "</option>
    <option value=\"3\" " . (($agregador == 3)? "selected=\"selected\"" : "") . ">" . $tdb[TIPOS_SERVICOS]['DESC'] . "</option>
    <option value=\"4\" " . (($agregador == 4)? "selected=\"selected\"" : "") . ">" . $tdb[MAQUINAS]['DESC'] . "</option>
</select>
</th>
<th style=\"text-align:right\">" . $ling['ord_carga_prev'] . "</th>
<th style=\"text-align:right\">" . $ling['ord_carga_disp_dia'] . "</th>
<th style=\"text-align:right\">" . $ling['ord_backlog'] . "</th>
</tr>
</thead>
<tbody>";

// Cor da linha
$cor_linha = "cor2";

// Totais
$total_prev = 0;
$total_disp = 0;
$total_blog = 0;

// Mostrando
foreach ($dados as $ag_valor => $dado) {
    // Se estiver em branco nem mostra
    if (($dado['TP'] == 0) and ($dado['DIS'] == 0)) continue;
    
    // Mostrando
    echo "<tr class=\"$cor_linha\">
    <td>" . htmlentities($dado['DESC']) . "</td>
    <td align=\"right\">" . round($dado['TP'], 2) . "</td>
    <td align=\"right\">" . round($dado['DIS'], 2) . "</td>
    <td align=\"right\">" . round($dado['BLOG'], 2) . "</td>
    </tr>";
    
    // Somando os totais
    $total_prev += $dado['TP'];
    $total_disp += $dado['DIS'];
    $total_blog += $dado['BLOG'];
    
    $cor_linha = ($cor_linha == 'cor1')? 'cor2' : 'cor1';
}

// TOTAIS
$cor_linha = ($cor_linha == 'cor1')? 'cor2' : 'cor1';

echo "</tbody>
<tfoot>
<tr class=\"$cor_linha\">
<td><strong>TOTAL</strong></td>
<td align=\"right\"><strong>" . round($total_prev, 2) . "</strong></td>
<td align=\"right\"><strong>" . round($total_disp, 2) . "</strong></td>
<td align=\"right\"><strong>" . round($total_blog, 2) . "</strong></td>
</tr>
</tfoot>
</table>";

echo "</td></tr></table>";


?>
