<?
/**
* Modulo principal, ajax do grafico
* 
* @author  Fernando Cosentino
* @version  3.0
* @package manusis
* @subpackage principal
*/

include("../../lib/swfcharts/charts.php");

$arq = $_GET['arq'];
$modo = $_GET['modo'];


echo "
    <a href=\"javascript:atualiza_area('grafico','modulos/principal/grafico_ajax.php?arq=$arq&modo=3d+column')\">
    <img src=\"imagens/icones/22x22/graf_coluna.png\" border=\"0\" vspace=5 /></a>
    <a href=\"javascript:atualiza_area('grafico','modulos/principal/grafico_ajax.php?arq=$arq&modo=bar')\">
    <img src=\"imagens/icones/22x22/graf_barra.png\" border=\"0\" vspace=5 /></a> 
    <a href=\"javascript:atualiza_area('grafico','modulos/principal/grafico_ajax.php?arq=$arq&modo=3d+pie')\">
    <img src=\"imagens/icones/22x22/graf_pizza.png\" border=\"0\" vspace=5 /></a>
    <a href=\"javascript:atualiza_area('grafico','modulos/principal/grafico_ajax.php?arq=$arq&modo=area')\">
    <img src=\"imagens/icones/22x22/graf_area.png\" border=\"0\" vspace=5 /></a>
    <a href=\"javascript:atualiza_area('grafico','modulos/principal/grafico_ajax.php?arq=$arq&modo=line')\">
    <img src=\"imagens/icones/22x22/graf_linha.png\" border=\"0\" vspace=5 /></a>
    <br>";

echo InsertChart("lib/swfcharts/charts.swf", "lib/swfcharts/charts_library","modulos/principal/ag_graf.php?arq=$arq&modo=$modo", 720, 450, 'BBCCEE' );


?>
