<?
$php_self = 'exportar_ajax.php';
// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../../lib/autent.php")) die ($ling['autent01']);

$id = $_GET['id'];

if ($id == 'crit') {
	$tb = $_GET['tb'];
	$cric = $_GET['cric'];
	if ($tb and $cric) {
		$rtb=VoltaRelacao($tb,$cric);
		if ($rtb) {
			echo "
<select class=\"campo_select\" id=\"cris\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
</select>";
			FormSelect("criv",$rtb['tb'],$criv,$rtb['campo'],"MID",$rtb['dba'],0);
		}
		else {
			echo "
<select class=\"campo_select\" id=\"cris\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
<option class=\"campo_option\" value=\"3\">".$ling['maior']."</option>
<option class=\"campo_option\" value=\"4\">".$ling['menor']."</option>
<option class=\"campo_option\" value=\"5\">".$ling['contem']."</option>
</select>
<input class=\"campo_text\" id=\"criv\" value=\"\" size=\"25\" maxlength=\"65\" />";
		}
		echo "<input type=\"button\" class=\"botao\" value=\"{$ling['adicionar']}\" onclick=\"AdicionaCrit()\">";
	}
	
}
if ($id == 'lista_crits') {
	$modelo_xml = $_SESSION['ManuSess']['exportar_modelo_xml'];
	if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
	$modelo = new SimpleXMLElement($modelo_xml);
	$tb = (string)$modelo->tabela;

	$c1 = $_GET['c1'];
	$cs = $_GET['cs'];
	$c2 = $_GET['c2'];
	if ($c1 and $cs and $c2) {
		$nfiltro = $modelo->addChild('filtro');
		$nfiltro->addAttribute('campo',$c1);
		$nfiltro->addAttribute('condicao',$cs);
		$nfiltro[0] = $c2;
		$_SESSION['ManuSess']['exportar_modelo_xml'] = $modelo->asXML();
	}
	$del = $_GET['del'];
	if (is_numeric($del)) {
		unset($modelo->filtro[(int)$del]);
		$_SESSION['ManuSess']['exportar_modelo_xml'] = $modelo->asXML();
	}
	
	$conds = array(
		1 => $ling['igual'],
		2 => $ling['diferente'],
		3 => $ling['maior'],
		4 => $ling['menor'],
		5 => $ling['contem']
	);
	
	
	echo "<table style=\"border: 1px solid #ABB1C0\" width=\"500\" id=\"lt_tabela\" cellspacing=1>
	<tr>
		<th width=\"30%\">".$ling['criterio']."</th>
		<th width=\"10%\">".$ling['condicao']."</th>
		<th width=\"50%\">".$ling['valor']."</th>
		<th width=\"10%\">".$ling['opcoes']."</th>
	</tr>";
	$i=0;
	foreach ($modelo->filtro as $id => $efiltro) {
		$rtb=VoltaRelacao($tb,$efiltro['campo']);
		if ($rtb) {
			$fil = (int)$efiltro[0];
			$val = htmlentities(VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$fil,$rtb['dba']));
		}
		else $val = (string)$efiltro[0];
		echo "	<tr class=\"cor1\">
		<td>".$tdb[$tb][(string)$efiltro['campo']]."</td>
		<td>{$conds[(int)$efiltro['condicao']]}</td>
		<td>{$val}</td>
		<td><a onclick=\"RemoveCrit($i)\" style=\"cursor: pointer\"><img src=\"../../../imagens/icones/22x22/del.png\"></a></td>
	</tr>";
		$i++;
	}
	if (!$i) echo "<tr><td colspan=4><em>({$ling['nenhum_filtro']})</em></td></tr>";

	echo "</table>";
}

if ($id == 'progresso') {
	$arq_xml = $_GET['m'];
	$modelos_dir = "../../../{$manusis['dir']['emodelos']}";
	$arq_log = "{$modelos_dir}/{$arq_xml}.txt";
	$dados = trim(file_get_contents($arq_log));
	echo "<html>";
	if (substr($dados,0,3) != 'fim') echo "<head>
	<script> setTimeout(\"location.href='{$php_self}?id=progresso&m={$arq_xml}'\",5000) </script>
</head>";
	echo "<body>";
	if ($dados == '0') echo "{$ling['inicializando']}...";
	else {
		list($perc,$atual,$total,$msg) = split('/',$dados,4);
		if ($perc == 'fim') {
			$perc = '100';
			$falta = '0';
		}
		$falta = 100-$perc;
		echo "{$ling['processando_aguarde']}...
	<table width=\"100%\" style=\"border: 1px solid #113377; height:16px\">
		<tr>";
		if ($perc != '0') echo "	<td width=\"$perc\" bgcolor=\"#113377\"></td>";
		if ($perc != '100') echo "	<td width=\"$falta\" bgcolor=\"#FFFFFF\"></td>";
		echo "</tr>
	</table>
{$perc}% {$ling['concluido']}.".($msg? "{$ling['registros']}: $total<br><br><strong>$msg</strong>" : "{$ling['registros']} $atual de $total")."
</body></html>";
	}
}

?>
