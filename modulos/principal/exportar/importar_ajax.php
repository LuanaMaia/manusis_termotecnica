<?
$php_self = 'importar_ajax.php';
// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../../lib/autent.php")) die ($ling['autent01']);

$id = $_GET['id'];

if ($id == 'progresso') {
	$arq_xml = $_GET['m'];
	$modelos_dir = "../../../{$manusis['dir']['imodelos']}";
	$arq_log = "{$modelos_dir}/{$arq_xml}.txt";
	$dados = trim(file_get_contents($arq_log));
	echo "<html>";
	list($fase,$perc,$atual,$total,$msg) = split('/',$dados,5);
	if ($perc != 'fim') echo "<script> setTimeout(\"location.href='{$php_self}?id=progresso&m={$arq_xml}'\",5000) </script>";
	echo "<body>";
	if ($dados == '0') echo "{$ling['inicializando']}...";
	else {
		if ($fase == '1') {
			$perc1 = $perc;
			$falta1 = 100-$perc1;
		}
		if (($fase == '2') and ($perc != 'fim')) {
			$perc1 = '100';
			$falta1 = '0';
			$perc2 = $perc;
			$falta2 = 100-$perc2;
		}
		if (($fase == '3') and ($perc != 'fim')) {
			$perc1 = '100';
			$falta1 = '0';
			$perc2 = '100';
			$falta2 = '0';
			$perc3 = $perc;
			$falta3 = 100-$perc3;
		}
		if ($perc == 'fim') {
			$perc1 = '100';
			$falta1 = '0';
			$perc2 = '100';
			$falta2 = '0';
			$perc3 = '100';
			$falta3 = '0';
		}

		if ($fase != 'erro') {
			echo "{$ling['processando_aguarde']}...<br>
			<br>
			{$ling['expo_arq_origem']}:
		<table width=\"100%\" style=\"border: 1px solid #113377; height:16px\">
			<tr>";
			if ($perc1 != '0') echo "	<td width=\"$perc1\" bgcolor=\"#113377\"></td>";
			if ($perc1 != '100') echo "	<td width=\"$falta1\" bgcolor=\"#FFFFFF\"></td>";
			echo "</tr>
		</table>
	{$perc1}% {$ling['concluido2']}.";
			
		}
		if (($fase == '2') or ($fase == '3') or (($perc == 'fim') and ($fase != 'erro'))) {
			echo "<br>
		<br>
		{$ling['expo_ins_reg']}:
	<table width=\"100%\" style=\"border: 1px solid #113377; height:16px\">
		<tr>";
			if ($perc2 != '0') echo "	<td width=\"$perc2\" bgcolor=\"#113377\"></td>";
			if ($perc2 != '100') echo "	<td width=\"$falta2\" bgcolor=\"#FFFFFF\"></td>";
			echo "</tr>
	</table>
{$perc2}% {$ling['concluido2']}.";
		}

		if (($fase == '3')) {
			echo "<br>
		<br>
		{$ling['expo_analiasndo_sist']}:
	<table width=\"100%\" style=\"border: 1px solid #113377; height:16px\">
		<tr>";
			if ($perc3 != '0') echo "	<td width=\"$perc3\" bgcolor=\"#113377\"></td>";
			if ($perc3 != '100') echo "	<td width=\"$falta3\" bgcolor=\"#FFFFFF\"></td>";
			echo "</tr>
	</table>
{$perc3}% {$ling['concluido2']}.";
		}
		
		echo ($msg? " {$ling['linhas']}: $total<br><br><strong>".nl2br($msg)."</strong>" : "{$ling['registro']} $atual {$ling['de']} $total")."
</body></html>";
	}
}

?>