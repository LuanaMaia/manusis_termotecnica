<?
chdir(dirname(realpath($_SERVER['SCRIPT_FILENAME'])));
/*$is_windows = (strpos(strtoupper($_SERVER["OS"]),'WINDOWS') !== false) ? true : false;
$barra = ($is_windows ? '\\' : '/');*/
$barra = '/';

$time_inicio = time();

// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);

if (!include('importar_eventos.php')) die('N�o foi poss�vel encontrar os eventos de importa��o');

$separador_csv = ';';
$modelos_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['imodelos']}";
$arquivos_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['temporarios']}";
$exportar_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['exportar']}";
$exportar_url = "{$manusis['url']}{$manusis['dir']['exportar']}";

function FormataArquivo($arq) {
    if (strpos($arq,'%ds')) $arq = str_replace('%ds',date('w')+1,$arq);
    if (strpos($arq,'%d')) $arq = str_replace('%d',date('d'),$arq);
    if (strpos($arq,'%m')) $arq = str_replace('%m',date('m'),$arq);
    if (strpos($arq,'%a')) $arq = str_replace('%a',date('Y'),$arq);
    if (strpos($arq,'%s')) $arq = str_replace('%s',date('W'),$arq);
    if (strpos($arq,'%h')) $arq = str_replace('%h',date('H-i-s'),$arq);
    return $arq;
}
function FormataFloat($str, $sep) {
    $tmp='';
    $len = strlen($str);
    for ($i=0; $i<$len; $i++) {
        $char = $str[$i];
        if (is_numeric($char) or ($char == $sep)) $tmp .= $char;
    }
    $tmp = str_replace($sep,'.',$tmp);
    return $tmp;
}
function ForcaFormatoData(&$str, &$fmt) {
    $fmt = (string)$fmt;
    $str = (string)$str;
    if ($fmt == '%Y%m%d') {
        $fmt = '%Y %m %d';
        $str = substr($str,0,4).' '.substr($str,4,2).' '.substr($str,6,2);
    }
    if ($fmt == '%d%m%Y') {
        $fmt = '%d %m %Y';
        $str = substr($str,0,2).' '.substr($str,2,2).' '.substr($str,4,4);
    }
}

$CSVTotal = 0;
function CSVMenorLinha($handle) {
    global $CSVTotal,$separador_csv;
    $menor_linha=0;
    rewind($handle);
    while (($data = fgetcsv($handle,0,$separador_csv)) !== FALSE) {
        if ($data != array('')) {
            $CSVTotal++;
            if ((!$menor_linha) or (count($data) < $menor_linha)) $menor_linha = count($data);
        }
    }
    rewind($handle);
    return $menor_linha;
}

// Variaveis de direcionamento
if ($argv[1]) parse_str($argv[1],$_GET);
$arq_xml=LimpaTexto($_GET['m']);

file_put_contents("{$modelos_dir}/{$arq_xml}.txt",'0');
$modelo_xml = file_get_contents("{$modelos_dir}/{$arq_xml}");
if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
$_SESSION['ManuSess']['importar_modelo_xml'] = $modelo_xml;
$modelo = new SimpleXMLElement($modelo_xml);
$tb = (string)$modelo->tabela;

if (!$tb) die($ling['err10']);

$ignorar_titulos = (int)$modelo->ignorar_titulos;

$campos_info = VoltaCampos($tb);

$tbcampos = array();

$maior_campo_csv=0;
foreach ($modelo->campo as $evalor) {
    $eitem=array(
        'use' => '1',
        'nome' => (string)$evalor['nome'],
        'rel' => (string)$evalor['relacao'],
        'rtb' => (string)$evalor['tabela'],
        'rmid' => (string)$evalor['alvo'],
        'dfmt' => (string)$evalor['data'],
        'sep' => (string)$evalor['separador'],
        'tam' => (string)$evalor['tamanho'],
        'tipo' => $campos_info[(string)$evalor['nome']]['tipo'],
        'campo' => (string)$evalor[0]
    );
    $tbcampos[(string)$evalor['nome']] = $eitem;
    if ((int)$eitem['campo'] > $maior_campo_csv) $maior_campo_csv = (int)$eitem['campo'];
    
    if ($eitem['rtb']) {
        $sql2 = "SELECT {$eitem['rmid']}, {$eitem['rel']} FROM {$eitem['rtb']}";
        $tmp2=$dba[$tdb[$eitem['rtb']]['dba']] -> Execute($sql2);
        while (!$tmp2->EOF) {
            $campo2=$tmp2->fields;
            $relacoes[$eitem['rtb']][$eitem['rel']][$campo2[$eitem['rel']]] = $campo2[$eitem['rmid']];
            $tmp2->MoveNext();
        }
    }
}

### carrega arquivo
$status=1;
$msg='';
$cod_erro='';
$csv_handle = false;
$timest = time();

if ((int)$modelo->ler_unidade) {
    $edir = "{$exportar_dir}{$barra}$tb";
    $eurl = "{$exportar_url}{$barra}$tb";
    $arq_nome_arq=FormataArquivo((string)$modelo->unidade);

    if (!strpos($arq_nome_arq,$barra)) $arq_nome=$edir."{$barra}".$arq_nome_arq;
    else $arq_nome=$arq_nome_arq;

    //die($arq_nome);
    AddStr($msg,"\n","{$ling['impo_arq']}: ".realpath($arq_nome));

    if (!file_exists($arq_nome)) {
        AddStr($msg,"\n","{$ling['err15']} - $arq_nome");
        AddStr($cod_erro,',','no_file');
        $status = 0;
    }
    else {
        $csv_handle = fopen($arq_nome, "r");        
        //$texto_csv = file_get_contents($arq_nome);
        if (feof($csv_handle)) {
            AddStr($msg,"\n",$ling['arq_vazio']);
            AddStr($cod_erro,',','empty_file');
            $status = 0;
        }
    }
}

if ((int)$modelo->ler_ftp) {
    $ftp_host = (string)$modelo->ler_ftp_host;
    $ftp_local = (string)$modelo->ler_ftp_local;
    $ftp_user = (string)$modelo->ler_ftp_user;
    $ftp_pass = base64_decode((string)$modelo->ler_ftp_pass);
    
    $ftp_path = (strpos($ftp_local,'/') === false) ? '' : substr($ftp_local,0,strrpos($ftp_local,'/')+1); 
    $ftp_filename = FormataArquivo((strpos($ftp_local,'/') === false) ? $ftp_local : substr($ftp_local,strrpos($ftp_local,'/')+1));
    
    AddStr($msg,"\n","{$ling['impo_arq_ftp']}: $ftp_filename");
    //die("$ftp_path - $ftp_filename");
    
    if ($ftp_host and $ftp_local) {
        $conn_id = ftp_connect($ftp_host);
        if (!$conn_id) {
            AddStr($msg,"\n",$ling['impo_arq_ftp_nao_servidor']);
            AddStr($cod_erro,',','ftp_no_server');
            $status=0;
        }
        else {
            $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass); 
            if (!$login_result) {
                AddStr($msg,"\n",$ling['impo_arq_ftp_nao_logar']);
                AddStr($cod_erro,',','ftp_no_login');
                $status=0;
            }
            else {
                // logado
                $temp_arq = realpath($arquivos_dir."/exportar.csv");
                ftp_pasv($conn_id, true);
                ftp_chdir($conn_id,$ftp_path);
                $download = ftp_get($conn_id, $temp_arq, $ftp_filename, FTP_BINARY); 
                if (!$download) {
                    AddStr($msg,"\n",$ling['impo_arq_ftp_nao_obter']);
                    AddStr($cod_erro,',','ftp_no_file');
                    $status=0;
                }
                else {
                    //$csvarr = CSV2Array($temp_arq);
                    $csv_handle = fopen($temp_arq, "r");        
                    //$texto_csv = file_get_contents($temp_arq);
                    if (feof($csv_handle)) {
                        AddStr($msg,"\n",$ling['impo_arq_vazio']);
                        AddStr($cod_erro,',','ftp_empty_file');
                        $status = 0;
                    }
                }
            }
            ftp_close($conn_id); 
        }
    }
}


// BANCO DE DADOS
if ((int)$modelo->ler_bd) {
    $bd_tipo = (string)$modelo->ler_bd_tipo;
    $bd_host = (string)$modelo->ler_bd_host;
    $bd_porta = (string)$modelo->ler_bd_porta;
    $bd_base = (string)$modelo->ler_bd_base;
    $bd_sid = (string)$modelo->ler_bd_sid;
    $bd_user = (string)$modelo->ler_bd_user;
    $bd_pass = base64_decode((string)$modelo->ler_bd_pass);
    $bd_tb = (string)$modelo->ler_bd_tb;
    $bd_filtro = (string)$modelo->ler_bd_filtro;
    
    // Drive selecionado
    if ($bd_tipo == 1) {
        $bd_drive = "mysql";
    }
    elseif ($bd_tipo == 2) {
        $bd_drive = "mssql";
    }
    elseif ($bd_tipo == 3) {
        $bd_drive = "oci8";
    }
        
    AddStr($msg,"\n","Conectando com $bd_host na base $bd_base.");
    
    $dba_con = ADONewConnection($bd_drive);
    $dba_con->SetFetchMode(ADODB_FETCH_NUM);
    
    // A CONEXAO PROPRIAMENTE DITA
    if (! $dba_con->Connect($bd_host, $bd_user, $bd_pass, $bd_base)) {
        echo $dba_con->ErrorMsg();
        AddStr($msg,"\n",$ling['err15']);
        AddStr($cod_erro,',',$dba_con->ErrorMsg());
        $status = 0;
    }
    
    // CAMPOS A BUSCAR
    $campos = "";
    $i = 1;
    foreach ($tbcampos as $ctb => $def) {
        $campos .= ($campos != '')? ', ' : '';
        $campos .= $def['campo'];
        $tbcampos[$ctb]['campo'] = $i;
        $i++;
    }
    
    if ($campos == "") {
        AddStr($msg,"\n",$ling['impo_erro_esquema']);
        AddStr($cod_erro,',',$ling['impo_campo_tab_rem']);
        $status = 0;
    }
    
    // SE TUDO CERTO CONTINUA
    if($status) {
        // MONTANDO O SELECT
        $sql = "SELECT $campos FROM $bd_tb";
        
        // ADICIONANDO OS FILTROS
        if ($bd_filtro != '') {
            $sql .= " WHERE $bd_filtro";
        }
        
        // EXECUTANDO
        if (! $rs = $dba_con->Execute($sql)) {
            echo $dba_con->ErrorMsg();
            AddStr($msg, "\n", $dba_con->ErrorMsg());
            AddStr($cod_erro, ',' , "{$ling['impo_erro_bd_destino']} " . $dba_con->ErrorMsg());
            $status = 0;
        }
        
        // SALVA O RESULTADO
        $cvs_dados = array();
        
        // ARQUIVO TEMPORARIO
        $temp_arq = $arquivos_dir."/importar-{$timest}.csv";
        
        $csv_handle = fopen($temp_arq, 'a');
                
        // PASSANDO PELOS REGISTROS ENCONTRADOS
        if ($status) {
            while (! $rs->EOF) {
                // SALVANDO EM CSV
                if(fputcsv($csv_handle, $rs->fields, $separador_csv, "\"") === FALSE) {
                    AddStr($msg, "\n", $ling['impo_erro_csv_tem']);
                    AddStr($cod_erro, ',' , $ling['impo_erro_csv_tem']);
                    $status = 0;
                    break;
                }
                
                $rs->MoveNext();
            }
        }
        
        fclose($temp_arq, "r");
        
        $csv_handle = fopen($temp_arq, "r");        
        //$texto_csv = file_get_contents($temp_arq);
        if (feof($csv_handle)) {
            AddStr($msg,"\n",$ling['impo_erro_arq_tem']);
            AddStr($cod_erro,',',$ling['impo_erro_bd_vazio']);
            $status = 0;
        }

    }
}

$menor_linha = ($csv_handle ? CSVMenorLinha($csv_handle) : 0);

$sqls=array();
if ($maior_campo_csv > $menor_linha) {
    AddStr($msg,"\n","{$ling['impo_arq_csv']} $maior_campo_csv {$ling['impo_arq_csv_complemento']} $menor_linha {$ling['campos']}");
    AddStr($cod_erro,',','less_fields');
    $status = 0;
}

if ($CSVTotal == 0) {
    AddStr($msg,"\n",$ling['impo_arq_vazio']);
    AddStr($cod_erro,',',$ling['impo_arq_vazio']);
    $status = 0;
}

#####################################
## COME�A A TRABALHAR OS DADOS


$arq_buf = "{$exportar_dir}{$barra}log{$barra}{$arq_xml}-{$timest}.buf";
$arq_temp = "{$exportar_dir}{$barra}log{$barra}{$arq_xml}-{$timest}.tmp.csv";
$arq_temp_zip_filename = "{$arq_xml}-{$timest}.zip";
$arq_temp_zip = "{$exportar_dir}{$barra}log{$barra}{$arq_temp_zip_filename}";
$arq_log = "{$modelos_dir}{$barra}{$arq_xml}.txt";


$insercoes_por_vez = 500; // edite aqui

if ($status and $csv_handle) {
    
    file_put_contents($arq_temp,'');
    
    rewind($csv_handle);
    
    if ($ignorar_titulos) fgets($csv_handle); // joga fora uma linha
    
    $linhas_ok = $linhas_nao_ok = 0;

    switch($tb) {
        case LANCA_CONTADOR:
            $num_linhas=0;
            $numero_registros = $CSVTotal;
            $tem_linhas_em_branco=0;
            while (($ecsv = fgetcsv($csv_handle,0,$separador_csv)) !== FALSE) {
                if ($ecsv != array('')) {
                    $num_linhas++;
                    $campos=array();

                    foreach ($tbcampos as $ec=>$ev) {
                        $cid = ((int)$ev['campo']-1); if ($cid < 0) $cid=0;
                        
                        if ($ev['rel'] and $ev['rtb'] and $ev['rmid']) {
                            $cev = $relacoes[$ev['rtb']][$ev['rel']][$ecsv[$cid]];
                            if ($ev['rmid'] == 'MID') $cev = (int)$cev;
                        }
                        elseif ($ev['dfmt']) {
                            ForcaFormatoData($ecsv[$cid],$ev['dfmt']);
                            $darr = parse_string($ecsv[$cid],$ev['dfmt']);
                            $cev = (int)$darr['Y']."-".(int)$darr['m']."-".(int)$darr['d'];
                            if ($cev == '--') $cev = '0000-00-00';
                        }
                        elseif ($ev['sep']) {
                            $cev = FormataFloat($ecsv[$cid],(string)$ev['sep']);
                        }
                        else {
                            $cev = $ecsv[$cid];
                            if ($ev['tam'] and ((int)$ev['tam'] < strlen($cev))) {
                                AddStr($msg,"\n","{$ling['impo_formatacao']} $ec {$ling['impo_formatacao_comple']} ".(int)$ev['tam']." {$ling['impo_na_linha']} $num_linhas {$ling['impo_com_valor']} \"$cev\"");
                                AddStr($cod_erro,',','max_size');
                                $status = 0;
                            }
                        }
                        if ($ev['tipo'] == 'int') {
                            $cev = (int)$cev;
                        }
                        if ($ev['tipo'] == 'decimal') {
                            $cev = str_replace(',','.',$cev);
                        }
                        if ($cev == '') {
                            if ($ev['tipo'] == 'time') $cev = '00:00:00';
                            if ($ev['tipo'] == 'date') $cev = '0000-00-00';
                        }
                        $campos[$ec]=$cev;
                        if (!$status) break;
                    }
                    if ($status) {
                        if (!$campos['MID_CONTADOR']) $campos['MID_CONTADOR'] = VoltaValor(MAQUINAS_CONTADOR,'MID','MID_MAQUINA',$campos['MID_MAQUINA'],0);
                        if (!$campos['DATA']) $campos['DATA'] = date('Y-m-d');
                        
                        if ($campos['MID_CONTADOR']) {
                            $s = false;
                            
                            $s = LancaContador($campos['MID_CONTADOR'],(float)$campos['VALOR'],NossaData($campos['DATA']));
                            if (is_string($s) and strlen($s)) {
                                if (substr($s,0,6) == 'ORDEM:') {
                                    AddStr($msg,"\n","{$ling['impo_cont_disp']} - $s");
                                    $linhas_ok++;
                                }
                                else {
                                    AddStr($msg,"\n","{$ling['impo_nao_lanca_cont']}: $s");
                                    //falha em silencio
                                    $linhas_nao_ok++;
                                }
                            }
                            else {
                                $linhas_ok++;
                            }
                            //AddStr($msg,"\n","Maquina={$campos['MID_MAQUINA']} LancaContador('{$campos['MID_CONTADOR']}','".(float)$campos['VALOR']."','".NossaData($campos['DATA'])."')<br>");
                        }
                        else {
                            $maqcod = VoltaValor(MAQUINAS,'COD','MID',$campos['MID_MAQUINA'],0);
                            AddStr($msg,"\n","{$ling['impo_maq']} $maqcod {$ling['impo_maq_nao_cont']}");
                            $linhas_nao_ok++;
                            // falha em sil�ncio
                        }
                        
                            
                    }
                    
                    if (!$status) break;
                    $perc = round(($num_linhas/$numero_registros)*100);
                    file_put_contents($arq_log,"1/$perc/$num_linhas/$numero_registros/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));
                }
                else {
                    $tem_linhas_em_branco++;
                    $linhas_nao_ok++;
                }
                
            }
            if ($tem_linhas_em_branco) AddStr($msg,"\n","{$ling['impo_arq_possui']} $tem_linhas_em_branco {$ling['impo_arq_linhas_branco']}");
            
            AddStr($msg,"\n","{$ling['impo_conclu_importacao']}: $linhas_ok{$ling['impo_conclu_importacao_comple']}: $linhas_nao_ok");
            

            $tmp_handle = fopen($arq_temp,'w');
            rewind($csv_handle);
            while (!feof($csv_handle)) fwrite($tmp_handle,fgets($csv_handle));
            fclose($csv_handle);    
            fclose($tmp_handle);    
            
            $zip = new ZipArchive;
            $zip_status = $zip->open($arq_temp_zip,ZIPARCHIVE::CREATE);
            if ($zip_status === true) {
                $zip->addFile($arq_temp, "{$arq_xml}.csv");
                $zip->close();
                $texto_csv = addslashes($arq_temp_zip);
                unlink($arq_temp);
            } else {
                AddStr($msg,"\n","{$ling['impo_erro_arq_zip']} ($zip_status)");
                AddStr($cod_erro,',','cant_zip');
                $texto_csv = '';
            }
            $texto_xml = addslashes($modelo->asXML());
            $texto_msg = addslashes($msg);
            
            $lmid = GeraMid(INTEGRADOR,'MID',0);
            $sql = "INSERT INTO ".INTEGRADOR." (TIPO, ESQUEMA, ESQUEMA_ARQUIVO, ULTIMO_EVENTO, CSV, RESULTADO, OBS, MID) VALUES (1, '$texto_xml','$arq_xml',NOW(),'$texto_csv','$num_linhas','$texto_msg','$lmid')";
            $tmp=$dba[$tdb[INTEGRADOR]['dba']] -> Execute($sql);
            if (!$tmp) {
                AddStr($msg,"\n",$ling['impo_nao_arq_bd']);
                AddStr($cod_erro,',','cant_log');
                $status=0;
            }

            file_put_contents($arq_log,"fim/fim/$num_linhas/$numero_registros/$msg");
    
        break;
        
        default:
            $ecampos=$eupdate='';
            $num_linhas=0;
            $numero_registros = $CSVTotal;
            $nmid = GeraMid($tb,'MID',$tdb[$tb]['dba']);
            $asinc = $eventos_mids = array();
            $tem_linhas_em_branco=0;
            $buf_handle = fopen($arq_buf,'w');
            //foreach ($csvarr as $ecsv) {
            $buf_total=0;
            echo "{$ling['impo_lendo_arq']} $numero_registros!!!\n";
            while (($ecsv = fgetcsv($csv_handle,0,$separador_csv)) !== FALSE) {
                if ($ecsv != array('')) {
                    echo ".";
                    $evalores='';
                    $elinha_arr=array(); // array que conter� somente a linha atual
                    $num_linhas++;
                    //foreach ($ecsv as $ek=>$ev) {
                    foreach ($tbcampos as $ec=>$ev) {
                        $cid = ((int)$ev['campo']-1); 
                        if ($cid < 0) $cid=0;
                        
                        // Campos relacinados
                        if ($ev['rel'] and $ev['rtb'] and $ev['rmid']) {
                            // Verifica se o valor ja esta cadastrado
                            $cev = VoltaValor($ev['rtb'], $ev['rmid'], $ev['rel'], $ecsv[$cid]);
                            
                            // Ainda n�o existe cadastro
                            if ($cev == '') {
                                $cev = GeraMid($ev['rtb'], $ev['rmid'], $tdb[$ev['rtb']]['dba']);
                                $ins = "INSERT INTO " . $ev['rtb'] . "(" . $ev['rmid'] . ", " . $ev['rel'] . ") VALUES ($cev, '" . LimpaTexto($ecsv[$cid]) . "')";
                                if (!$dba[$tdb[$ev['rtb']]['dba']] -> Execute($ins)) {
                                    AddStr($msg,"\n","{$ling['impo_nao_tab_rel']} {$ev['rtb']} {$ling['impo_no_bd']}");
                                    AddStr($cod_erro,',','cant_save');
                                    $status=0;
                                }
                            }
                            
                        }
                        elseif ($ev['dfmt']) {
                            $darr = parse_string($ecsv[$cid],(string)$ev['dfmt']);
                            $cev = (int)$darr['Y']."-".(int)$darr['m']."-".(int)$darr['d'];
                            if ($cev == '--') $cev = '0000-00-00';
                        }
                        elseif ($ev['sep']) {
                            $cev = FormataFloat($ecsv[$cid],(string)$ev['sep']);
                        }
                        else {
                            $cev = $ecsv[$cid];
                            if ($ev['tam'] and ((int)$ev['tam'] < strlen($cev))) {
                                AddStr($msg,"\n","{$ling['impo_formatacao']} $ec {$ling['impo_formatacao_comple']} ".(int)$ev['tam']." {$ling['impo_na_linha']} $num_linhas {$ling['impo_com_valor']} \"$cev\"");
                                AddStr($cod_erro,',','max_size');
                                $status = 0;
                            }
                        }
                        if ($ev['tipo'] == 'int') {
                            $cev = (int)$cev;
                        }
                        if ($ev['tipo'] == 'decimal') {
                            $cev = str_replace(',','.',$cev);
                        }
                        if ($cev == '') {
                            if ($ev['tipo'] == 'time') $cev = '00:00:00';
                            if ($ev['tipo'] == 'date') $cev = '0000-00-00';
                        }
            
                        // SALVANDO VALORES PARA TRATAR SINCRONIZAR DEPOIS
                        if ((string)$modelo->sincronizar and ((string)$modelo->sincronizar == $ec)) {
                            $asinc[$cev] = $cev;
                        }
                        
                        $elinha_arr[$ec]=$cev;
                        
                        if (!$status) break;
                    }
                
                    $elinha_arr['MID']=$nmid;
            
                    if ($importar_eventos_tabelas[$tb]) {
                        $eventos_mids[$nmid] = $nmid;
                        Importar_Evento($tb, $elinha_arr);
                    }
                    //usleep(50*1000);
                
                    if (!$ecampos) {
                        $ecampos = join(',',array_keys($elinha_arr));
                        foreach ($elinha_arr as $ec => $ev) {
                            if ($ec != 'MID') AddStr($eupdate,', ',"$ec = VALUES($ec)");
                        }
                    }
                    foreach ($elinha_arr as $ec => $ev) $elinha_arr[$ec] = "'".addslashes($ev)."'";
                    $evalores = join(',',array_values($elinha_arr));
                    
                    $nmid++;
            
                    $sqls[] = "(".str_replace("\n",'<<br>>',$evalores).")\n";
                    
                    if (count($sqls) >= $insercoes_por_vez) {
                        fwrite($buf_handle,join('',$sqls));
                        $sqls=array();
                    }
                    $buf_total++;
                    
                    $linhas_ok++;
            
                    $perc = round(($num_linhas/$numero_registros)*100);
                    file_put_contents($arq_log,"1/$perc/$num_linhas/$numero_registros/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));

                    
                    if (!$status) break;
                }
                else {
                    $tem_linhas_em_branco++;
                    $linhas_nao_ok++;
                }
            }
            if (count($sqls)) fwrite($buf_handle,join('',$sqls));
            fclose($buf_handle);
            if ($tem_linhas_em_branco) AddStr($msg,"\n","{$ling['impo_arq_possui']} $tem_linhas_em_branco {$ling['impo_arq_linhas_branco']}");
            
            // DESCONTANDO OS VALORES RETIRADOS NA SINCRONIZA��O, SE FOR O CASO
            if ($status and (string)$modelo->sincronizar) {
                    /*
                    $tmp=$dba[0] -> Execute("SELECT COUNT(*) FROM $tb WHERE ".(string)$modelo->sincronizar." NOT IN (".join(', ',$asinc).")");
                    if (!$tmp->EOF) {
                        $numero_registros += $tmp->fields['COUNT(*)'];
                        file_put_contents($arq_log,"1/$perc/$num_linhas/$numero_registros/Tempo de processamento at� agora: ".gmdate('H:i:s',time()-$time_inicio));
                        $num_linhas += $tmp->fields['COUNT(*)'];
                    }
                    */
                
            }
        
            $tmp_handle = fopen($arq_temp,'w');
            rewind($csv_handle);
            while (!feof($csv_handle)) fwrite($tmp_handle,fgets($csv_handle));
            fclose($csv_handle);    
            fclose($tmp_handle);    
            
            $zip = new ZipArchive;
            $zip_status = $zip->open($arq_temp_zip,ZIPARCHIVE::CREATE);
            if ($zip_status === true) {
                $zip->addFile($arq_temp, "{$arq_xml}.csv");
                $zip->close();
                $texto_csv = addslashes($arq_temp_zip);
                unlink($arq_temp);
            } else {
                AddStr($msg,"\n","{$ling['impo_erro_arq_zip']} ($zip_status)");
                AddStr($cod_erro,',','cant_zip');
                $texto_csv = '';
            }
            $texto_xml = addslashes($modelo->asXML());
            $texto_msg = addslashes($msg);
        
            
            $lmid = GeraMid(INTEGRADOR,'MID',0);
            $sql = "INSERT INTO ".INTEGRADOR." (TIPO, ESQUEMA, ESQUEMA_ARQUIVO, ULTIMO_EVENTO, CSV, RESULTADO, OBS, MID) VALUES (1, '$texto_xml','$arq_xml',NOW(),'$texto_csv','$num_linhas','$texto_msg','$lmid')";
            $tmp=$dba[$tdb[INTEGRADOR]['dba']] -> Execute($sql);
            if (!$tmp) {
                AddStr($msg,"\n",$ling['impo_nao_arq_bd']);
                AddStr($cod_erro,',','cant_log');
                $status=0;
            }
            
            if ($status) {
                $sql_status=1;
                $dba[$tdb[$tb]['dba']] -> BeginTrans(); 
                $num_linhas2=0;
                $sqls_i=array();
                $buf_handle = fopen($arq_buf,'r');
                while (!feof($buf_handle)) {
                    $elcampos = str_replace('<<br>>',"\n",trim(fgets($buf_handle)));
                    $num_linhas2++;
                    if ($elcampos) $sqls_i[]=$elcampos;
                    if (((count($sqls_i) >= $insercoes_por_vez) or ($num_linhas2 >= $buf_total)) and (count($sqls_i))) {
                        $perc = round(($num_linhas2/$numero_registros)*100);
                        file_put_contents($arq_log,"2/$perc/$num_linhas2/$buf_total/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));

                        $sql_i = "INSERT INTO $tb ($ecampos) VALUES ".join(", ",$sqls_i)." ON DUPLICATE KEY UPDATE $eupdate";

                        $sql_status=$dba[$tdb[$tb]['dba']] -> Execute($sql_i);
                        if (!$sql_status) {
                            file_put_contents("../../../{$manusis['dir']['exportar']}/tmp2.txt",$dba[$tdb[$tb]['dba']]->ErrorMsg()."\n".$sql_i);
                            die($dba[$tdb[$tb]['dba']]->ErrorMsg());
                        }
                        $sqls_i = array();
                    }                
                }
                
                if (count($sqls_i)) die($ling['impo_erro_interno']);
                fclose($buf_handle);
                
                // CASO SEJA SINCRONIZA��O ENTRA AQUI!!!!!
                if ($sql_status and (string)$modelo->sincronizar) {
                    if (! Importar_Evento_Sincronizar($tb, $modelo->sincronizar, $asinc)) {
                        AddStr($msg,"\n","{$ling['impo_erro_sincr_bd']}: ".$dba[$tdb[$tb]['dba']]->ErrorMsg()."\n");
                        AddStr($cod_erro,',','cant_sinc');
                        $status = 0;
                    }
                }
                
                if ($sql_status) {
                    $sql_status=$dba[$tdb[$tb]['dba']] -> CommitTrans();
                    file_put_contents($arq_log,"2/$perc/$num_linhas/$buf_total/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));
                    AddStr($msg,"\n",$ling['impo_dados_sucesso']);
                    if (!$sql_status) {
                        AddStr($msg,"\n","{$ling['impo_nao_tab']}: ".$dba[$tdb[$tb]['dba']]->ErrorMsg());
                        AddStr($cod_erro,',','cant_commit');
                        $status=0;
                    }
                }
                else {
                    AddStr($msg,"\n","{$ling['impo_nao_imp_dados']}: ".$dba[$tdb[$tb]['dba']]->ErrorMsg()."\n<br>$sql_importar");
                    AddStr($cod_erro,',','cant_replace');
                    $dba[$tdb[$tb]['dba']] -> RollbackTrans(); 
                    $status=0;
                }
                
                //// EVENTO AP�S
                if ($importar_eventos_tabelas[$tb]) {
                    $num_mids = count($eventos_mids);
                    $eemid=0;
                    foreach($eventos_mids as $emid) {
                        Importar_Evento_Apos($tb,$emid);
                        $eemid++;
                        $perc = round(($eemid/$num_mids)*100);
                        file_put_contents($arq_log,"3/$perc/$eemid/$num_mids/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));
                    }
                }
            }
            $texto_msg = addslashes($msg);
            $dba[$tdb[$tb]['dba']] -> Execute("UPDATE ".INTEGRADOR." SET OBS = '$texto_msg'".($status?'':", RESULTADO = '0'")." WHERE MID = '$lmid' LIMIT 1");

            file_put_contents($arq_log,(($importar_eventos_tabelas[$tb])? '3' : '2')."/fim/$num_linhas/$buf_total/$msg");
            
        break;
    }
}
else { // nao conseguiu upar
    $texto_xml = addslashes($modelo->asXML());
    $texto_csv = '';
    $texto_msg = addslashes($msg);
    $nmid = GeraMid(INTEGRADOR,'MID',0);
    $sql = "INSERT INTO ".INTEGRADOR." (TIPO, ESQUEMA, ESQUEMA_ARQUIVO, ULTIMO_EVENTO, CSV, RESULTADO, OBS, MID) VALUES (1, '$texto_xml','$arq_xml',NOW(),'$texto_csv','0','$texto_msg','$nmid')";
    $tmp=$dba[$tdb[INTEGRADOR]['dba']] -> Execute($sql);
    if (!$tmp) {
        AddStr($msg,"\n",$ling['impo_nao_arq_bd']);
        $status=0;
    }
    
}


if(is_file($arq_buf)) unlink($arq_buf);
if ($texto_csv) unlink($arq_temp);
if(is_file($temp_arq)) unlink($temp_arq);

if ($status) {
    die("msg=".urlencode($msg));
}
else {
    file_put_contents($arq_log,"erro/fim/0/0/$msg");
    die("erro=".urlencode($msg). "\n");
}

?>
