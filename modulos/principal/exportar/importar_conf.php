<?
// tabelas e campos que s�o chaves
$importar_modelo[MAQUINAS]=array(
    'COD','DESCRICAO',
    'NUMERO_DE_SERIE',
    'NUMERO_DO_PATRIMONIO'
);
$importar_modelo[MAQUINAS_CONJUNTO]=array('TAG','DESCRICAO');
$importar_modelo[MATERIAIS]=array('COD','DESCRICAO');
$importar_modelo[MATERIAIS_UNIDADE]=array('COD', 'DESCRICAO');
$importar_modelo[MATERIAIS_FAMILIA]=array('COD', 'DESCRICAO');
$importar_modelo[CENTRO_DE_CUSTO]=array('COD','DESCRICAO');
$importar_modelo[AREAS]=array('COD','DESCRICAO');
$importar_modelo[SETORES]=array('COD','DESCRICAO');
$importar_modelo[ORDEM_PLANEJADO]=array('NUMERO');
$importar_modelo[FUNCIONARIOS]=array('MATRICULA','NOME');
$importar_modelo[EQUIPAMENTOS]=array('COD','DESCRICAO');
$importar_modelo[MAQUINAS_FAMILIA]=array('COD','DESCRICAO');
$importar_modelo[MAQUINAS_CLASSE]=array('DESCRICAO');
$importar_modelo[MAQUINAS_STATUS]=array('DESCRICAO');
$importar_modelo[FABRICANTES]=array('NOME');
$importar_modelo[FORNECEDORES]=array('NOME');
$importar_modelo[USUARIOS]=array('NOME');
$importar_modelo[ORDEM_PLANEJADO_STATUS]=array('DESCRICAO');

?>
