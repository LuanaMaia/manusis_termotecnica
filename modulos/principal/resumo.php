<?
/** 
* Modulo principal, resumo de informações
* Atualmente gráfico SWF
* 
* @author  Fernando Cosentino
* @version  3.0
* @package manusis
* @subpackage principal
*/


echo "<br />
<div id=\"lt\">\n
<div id=\"lt_cab\">\n
<h3>Programação & Indicadores</h3>
</div><br />

<table style=\"border:1px solid gray\" cellpadding=\"7\" cellspacing=\"0\" width=100%><tr><td valign=top width=\"60%\">
<div id=\"grafico\">";

echo "</div></td><td valign=top WIDTH=40%>";
include("modulos/principal/alertas.php");
echo "<hr width=90% />
<strong><b>{$ling['indicadores']}:</b></strong><br />";

$dir="".$manusis['dir']['graficos'];
if (!is_dir("".$manusis['dir']['graficos'])) erromsg($ling['direto_nao_foi_conf']);
$od=opendir($dir);
$i=0;
while (false !== ($arq = readdir($od))) {
    $dd=explode(".",$arq);
    list($ini,$trash) = explode('_',$dd[0],2);
    if (($dd[1] == "xml") and ($ini=='pi')) {
        $xml = new DOMDocument();
        $xml -> load("$dir/$arq");
        
        $titulo=$xml -> getElementsByTagName('descricao');
        $titulo=$titulo -> item(0) -> nodeValue;
        
        echo "&nbsp;&nbsp;&nbsp;<a class=\"link2\" ondblclick=\"return\" href=\"javascript: atualiza_area2('grafico', 'modulos/principal/grafico_ajax.php?arq=$arq&modo=3d+column')\">
        <img src=\"imagens/icones/22x22/graficos.png\" border=\"0\" /> ".utf8_decode($titulo)."</a><br />";
        
        if ($i == 0) {
            $graf_ini = $arq;
        }
        
        $i++;
    }
}
closedir($od);

echo "</ul></td></tr></table>

</td></tr></table>";

if ($graf_ini != '') {
    echo "<script>
    window.onload = function () {
        atualiza_area2('grafico','modulos/principal/grafico_ajax.php?arq=$graf_ini');
    }
    </script>";
}
?>
