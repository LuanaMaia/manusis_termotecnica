<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}


$busca=LimpaTexto($_GET['busca']);
$campo=$_GET['cc'];

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body>
<div id=\"corpo_relatorio\">
<script> window.resizeTo(500,500)</script>
<div id=\"lt_tabela\">
<table widht=\"100%\" cellpadding=\"3\" cellspacing=\"1\">";
$resultado = $dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = '$busca' ");
if (!$resultado) echo $dba[0] -> ErrorMsg();
$ca=$resultado->fields;
if ($ca['STATUS'] == 1) $st="ABERTO";
if ($ca['STATUS'] == 2) $st="FECHADO";
if ($ca['STATUS'] == 3) $st="CANCELADO";
echo "<tr class=\"white\">
<td><p align=center><b>".$ling['ordem_n']." ".$ca['NUMERO']." ($st)</b></p><p align=left><BR />
{$ling['prazo_m']}:".$resultado -> UserDate($ca['DATA_PROG'],'d/m/Y')." <br>";
if ($ca['STATUS'] == 2) echo "".$ling['data_de_termino'].": ".$resultado -> UserDate($ca['DATA_FINAL'],'d/m/Y')."<BR>";
echo "{$ling['rel_desc_resp']}:".htmlentities(html_entity_decode(VoltaValor(FUNCIONARIOS,"NOME","MID",$ca['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba'])))."<BR>
{$ling['rel_desc_obj2']}: ".htmlentities(html_entity_decode(VoltaValor(MAQUINAS,"DESCRICAO","MID",$ca['MID_MAQUINA'],$cm)))."<br>
{$ling['rel_desc_pos']}: ".htmlentities(html_entity_decode(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$ca['MID_CONJUNTO'],$cc)))."<br><Br>";
echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
echo "<tr><td colspan=\"4\" aling=\"center\"><strong>".$ling['MAO_DE_OBRA_APONTA']."</strong></td></tr>
<tr>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['FUNCIONARIO_M']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['EQUIPE_M']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_INICIO_M']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_FINAL_M']."</strong></span></td>
</tr>";
$obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$ca['MID']."'");
while (!$obj->EOF) {
	$obj_campo = $obj -> fields;
	echo  "<tr>";
	echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(FUNCIONARIOS,"NOME","MID",$obj_campo['MID_FUNCIONARIO'],$tdb[FUNCIONARIOS]['dba'])."</span></td>";
	echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(EQUIPES,"DESCRICAO","MID",$obj_campo['EQUIPE'],$tdb[EQUIPES]['dba'])."</span></td>";
	echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_INICIO'],'d/m/Y')." ".$obj_campo['HORA_INICIO']."</span></td>";
	echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_FINAL'],'d/m/Y')." ".$obj_campo['HORA_FINAL']."</span></td>";
	echo  "</tr>\n";
	$obj->MoveNext();
}
echo "</table><br>";
$obj=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$ca['MID']."'");
$obj_campo = $obj -> fields;
$objl=$i=count($obj -> getrows());
if ($objl != 0) {
	echo "<table id=\"dados_processados\" cellpadding=\"2\" width=\"100%\"><tr>
<tr><td colspan=\"2\" aling=\"center\" style='border: 1px solid black'><strong>".$ling['REPROGRAMACOES']."</strong></td></tr>
					<tr>
<td style='border: 1px solid black'><strong>".$ling['data']."</strong></td>
<td style='border: 1px solid black'><strong>".$ling['motivo']."</strong></td>
</tr>";	
	$tmp=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$ca['MID']."'");
	while (!$tmp->EOF) {
		$obj_campo=$tmp->fields;
		echo "<tr>
<td style='border: 1px solid black'>".$tmp -> UserDate($obj_campo['DATA'],'d/m/Y')."</td>
<td style='border: 1px solid black'>".$obj_campo['MOTIVO']."</td>
</td></tr>";
		$tmp->MoveNext();
	}
	echo "</table><br />";
}

$obj=$dba[0] -> Execute("SELECT MID FROM ".PENDENCIAS." WHERE MID_ORDEM = '".$ca['MID']."' OR MID_ORDEM_EXC = '".$ca['MID']."' ORDER BY NUMERO ASC");
$obj_campo = $obj -> fields;
$objl=$i=count($obj -> getrows());
if ($objl != 0) {
	echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
	echo "<tr><td colspan=\"4\" aling=\"center\"><strong>{$ling['pendencias_m']}</strong></td></tr>
<tr>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['Nordem']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['OBJ_MANUTENCAO']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['POSICAO_M']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DESCRICAO_M']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['ORDEM_ORIGEM']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['ORDEM_EXECUTADA']."</strong></span></td>
</tr>";
	$obj=$dba[0] -> Execute("SELECT * FROM ".PENDENCIAS." WHERE MID_ORDEM = '".$ca['MID']."' OR MID_ORDEM_EXC = '".$ca['MID']."' ORDER BY NUMERO ASC");
	while (!$obj->EOF) {
		$obj_campo = $obj -> fields;
		$ordexec=(int)$obj_campo['MID_ORDEM_EXC'];
		if ($ordexec == 0) $txt .= "<tr>";
		else echo "<tr bgcolor=\"gray\">";
		echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['NUMERO']."</span></td>";
		echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],0)."</span></td>";
		echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0)."</span></td>";
		echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['DESCRICAO']."</span></td>";
		echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(ORDEM_PLANEJADO,"NUMERO","MID",$obj_campo['MID_ORDEM'],0)."</span></td>";
		echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(ORDEM_PLANEJADO,"NUMERO","MID",$obj_campo['MID_ORDEM_EXC'],0)."</span></td>";
		echo "</tr>\n";
		$obj->MoveNext();
	}
	echo "</table>";
}

echo "</td></tr></table><Br><Br> <a href=javascript:print()>  {$ling['imprimir']} </a>
	</form><br />
	</div>
	</body>
	</html>";


?>
