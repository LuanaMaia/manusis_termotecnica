<?
/**
* Configuração Geral
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  configuracao
*/

ini_set('register_globals','Off');
ini_set('safe_mode','On');
setlocale(LC_ALL, 'en_US');

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

session_start();

//$manusis['debug'] = 1;
define('CONF_CARREGADO','1');
define('ManuSess','ManuSess_patiobatel');

// Seta o caminho pro binario do PHP
// essa constante vai estar disponivel apartir do PHP 5.4
if (substr(php_uname(), 0, 7) == "Windows") {
	define('PHP_BINARY', 'php.exe');
} else {
	define('PHP_BINARY', '/usr/bin/php');
}

$manusis['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/termotecnica/';

/**
 * Preencha obrigatoriamente o diretorio principal do Manusis!
 */
$manusis['dir']['principal'] = 'manusis/';
$manusis['dir']['temporarios'] = 'arquivos/temp';
$manusis['dir']['maquinas'] = 'arquivos/maquinas';
$manusis['dir']['equipamentos'] = 'arquivos/equipamentos';
$manusis['dir']['planopadrao'] = 'arquivos/planopadrao';
$manusis['dir']['inspecao'] = 'arquivos/inspecao';
$manusis['dir']['lubrificacao'] = 'arquivos/lubrificacao';
$manusis['dir']['materiais'] = 'arquivos/materiais';
$manusis['dir']['graficos'] = 'arquivos/graficos';
$manusis['dir']['ordens'] = 'arquivos/ordens';
$manusis['dir']['emodelos'] = 'arquivos/modelos_exportar';
$manusis['dir']['imodelos'] = 'arquivos/modelos_importar';
$manusis['dir']['exportar'] = 'arquivos/exportar';

$manusis['caixaalta'] = 1;
$manusis['tema'] = 'padrao';

$manusis['idioma'][0] = 'portuguesbr';

$manusis['db'][0]['driver']="mysqli";
$manusis['db'][0]['host']="192.168.0.100";
$manusis['db'][0]['user']="manusis";
$manusis['db'][0]['senha']="ma20nu3";
$manusis['db'][0]['base']="manusis_termotecnica";


$manusis['debug'] = 0;

// NÚMERO DE EMPRESAS PERMITIDO NA LICENÇA
$manusis['empresas'] = 9;


$manusis['admin']['nome'] = 'Administrador';
$manusis['admin']['email'] = 'suporte@wertsolutions.com.br';
$manusis['admin']['usuario'] = 'administrador';
$manusis['admin']['senha'] = 'fRuhet5u';


/**
 * Esta configuração determina se o tipo de serviço envia o molde para um pátio de setup ou não
 * Caso o mid do tipo de serviço esteja neste array, e o campo TROCA na tabela de tipos de serviço esteja setado como 1
 * ao selecionarmos esse tipo de serviço no apontamento de ordem, será possível selecionar apenas um pátio de setup como
 * destino para a movimentação do componente que será feita ao salvar a ordem 
 */
$manusis['tpo_serv_troca_setup'] = array(8,15,17,11,13);

/**
 * Nesta configuração ficam os mids das máquinas que são tratadas como pátio de setup
 * Uma vez adicionadas aqui, será possível selecionalas para o tipo de serviço de troca no 
 * apontamento de ordem de serviço.
 */
$manusis['patio_setup'] = array(2493,2494,2495,2496,2497,2498,2499,2500,2501);

/**
 * Familia de máquinas que podem ser utilizadas como destino nas ordens de troca
 * 3 -  MDA MOLDADORA
 * 13 - PAT PÁTIOS 
 */
$manusis['familias_destino_troca'] = array(3, 13);


// FAMÍLIAS DE COMPONENTES UTILIZADOS COMO MOLDES.
$manusis['moldes_familia'][] = 36;


$manusis['solicitacao']['informa'] = "";
$manusis['solicitacao']['gravaos'] = 1;

$manusis['ordem']['custo'] = 2;

$manusis['os']['alocacao_recursos'] = 1;
$manusis['os']['impressao_nova'] = 1;
$manusis['os']['parada'] = 2;
$manusis['os']['materiais'] = 3;
$manusis['os']['maodeobra'] = 3;
$manusis['os']['defeito'] = 2;
$manusis['os']['causa'] = 2;
$manusis['os']['solucao'] = 5;
$manusis['os']['pendencia'] = 5;
$manusis['os']['dias_para_alerta'] = 1;

$manusis['upload']['tamanho'] = 15360000;

$manusis['erro']['reportar'] = 0;

$manusis['sess']['expira'] = 0;
$manusis['sess']['tempo'] = 99999500;

$manusis['log']['logon'] = 1;
$manusis['log']['logoff'] = 1;
$manusis['log']['delete'] = 1;
$manusis['log']['insert'] = 1;
$manusis['log']['update'] = 1;
$manusis['log']['erros'] = 1;

$_autotag_setor['status'] = 0;
$_autotag_setor['tabela'] = 'areas';
$_autotag_setor['caracter'] = '.';
$_autotag_setor['casasdecimais'] = 3;
$_autotag_setor['incrementa'] = 1;

$_autotag_maq['status'] = 0;
$_autotag_maq['tabela'] = 'maquinas_familia';
$_autotag_maq['caracter'] = '';
$_autotag_maq['casasdecimais'] = 3;
$_autotag_maq['incrementa'] = 1;

$_autotag_conj['status'] = 0;
$_autotag_conj['tabela'] = 'maquinas';
$_autotag_conj['caracter'] = '.';
$_autotag_conj['casasdecimais'] = 2;
$_autotag_conj['incrementa'] = 1;

$_autotag_equip['status'] = 0;
$_autotag_equip['tabela'] = 'equipamentos_familia';
$_autotag_equip['caracter'] = '.';
$_autotag_equip['casasdecimais'] = 3;
$_autotag_equip['incrementa'] = 1;

$_autotag_fammaq = 0;
$_autotag_famequip = 0;