<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Nota: Arquivo para Formularios
 */
// Variaveis de direcionamento
$st=(int)$_GET['st'];
$id=(int)$_GET['id']; // Modulo
$op=(int)$_GET['op']; // Operação do modulo
$exe=(int)$_GET['exe'];
$act=(int)$_GET['act'];
$form_recb_valor=$_GET['form_recb_valor'];
$atualiza=$_GET['atualiza'];
$foq=(int)$_GET['foq'];

// Funções do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Autentificação
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Formulários
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $fpos = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $$fpos, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);

    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}


if ($id == 1) {
    $tipo=(int)$_GET['tipo'];
    if ($_GET['tipo'] != "") {
        if ($_GET['tipo'] == 1) {
            echo "<label class=\"campo_label \" for=\"plano\">".$ling['plano'].":</label>";
            FormSelectD('DESCRICAO', '', PLANO_PADRAO, '', 'plano', 'plano', 'MID', '', '', "atualiza_area2('obj','progra.php?id=2&tipo=$tipo&m=1&plano=' + this.options[this.selectedIndex].value)");
        }
        if ($_GET['tipo'] == 2) {
            echo "<label class=\"campo_label \" for=\"plano\">".$ling['plano'].":</label>";
            FormSelectD('DESCRICAO', '', PLANO_ROTAS, '', 'plano', 'plano', 'MID', '');
        }
    }
}
elseif ($id == 9) {
    $di=explode("/",$_GET['di']);
    $df=explode("/",$_GET['df']);
    if ($df[0] != "") {
        if (!checkdate ((int)$di[1], (int)$di[0], (int)$di[2])) echo "<br /><strong><font color=\"red\">".$ling['data_inicial_invalida']."</font></strong><br />";
        elseif (!checkdate ((int)$df[1], (int)$df[0], (int)$df[2])) echo "<br /><strong><font color=\"red\">".$ling['data_final_invalida']."</font></strong><br />";
        else {
            $di=mktime(0,0,0,$di[1],$di[0],$di[2]);
            $df=mktime(0,0,0,$df[1],$df[0],$df[2]);
            if ($di > $df) echo "<br /><strong><font color=\"red\">".$ling['data_inicial_maior_data_final']."</font></strong><br />";
        }
    }
}
if ($id == 2) {
    $tipo=(int)$_GET['tipo'];
    $plano=(int)$_GET['plano'];
    $hora=$_GET['hora'];
    $datai=$_GET['datai'];
    $dataf=$_GET['dataf'];

    if (($_GET['tipo'] == 1) and ($_GET['plano'] != 0)) {
        // Filtro por empresa
        $fil_maq = VoltaFiltroEmpresa(MAQUINAS, 2);
        $fil_maq_and   = ($fil_maq != "")? " AND " . $fil_maq : "";
        $fil_maq_where = ($fil_maq != "")? " WHERE " . $fil_maq : "";

        $fil_equip = VoltaFiltroEmpresa(EQUIPAMENTOS, 2);
        $fil_equip_and   = ($fil_equip != "")? " AND " . $fil_equip : "";
        $fil_equip_where = ($fil_equip != "")? " WHERE " . $fil_equip : "";

        // Se tiver filtro por empresa mostrar apenas maquinas dessa empresa
        $emp_plano = (int)VoltaValor(PLANO_PADRAO, "MID_EMPRESA", "MID", $plano, 0);
        if ($emp_plano != 0) {
            $tmp_maq = array();
            // Buscando as maquinas dessa empresa
            $sql = "SELECT M.MID FROM ".MAQUINAS." M, ".SETORES." S, ".AREAS." A WHERE M.STATUS = 1 AND M.MID_SETOR = S.MID AND S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp_plano";
            $tmp = $dba[0] -> Execute($sql);
            while (!$tmp->EOF) {
                $tmp_maq[] = $tmp->fields['MID'];
                $tmp->MoveNext();
            }
            // Adicionando no select
            if (count($tmp_maq) > 0) {
                $fil_maq_and   .= " AND MID IN (" . implode(', ', $tmp_maq) . ")";
                $fil_maq_where .= ($fil_maq_where != "")? " WHERE " : " AND ";
                $fil_maq_where .= "MID IN (" . implode(', ', $tmp_maq) . ")";

                $fil_equip_and   .= " AND MID_MAQUINA IN (" . implode(', ', $tmp_maq) . ")";
                $fil_equip_where .= ($fil_maq_where != "")? " WHERE " : " AND ";
                $fil_equip_where .= "MID_MAQUINA IN (" . implode(', ', $tmp_maq) . ")";
            }
            else {
                $fil_maq_and   .= " AND MID = 0";
                $fil_maq_where .= ($fil_maq_where != "")? " WHERE " : " AND ";
                $fil_maq_where .= "MID = 0";

                $fil_equip_and   .= " AND MID = 0";
                $fil_equip_where .= ($fil_maq_where != "")? " WHERE " : " AND ";
                $fil_equip_where .= "MID = 0";
            }
        }

        $tipop=VoltaValor(PLANO_PADRAO,"TIPO","MID",$plano,0);
        if (($tipop == 1) or ($tipo == 0)) {
            $fam=VoltaValor(PLANO_PADRAO,"MID_MAQUINA","MID",$plano,0);
            $sql="SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." WHERE STATUS = 1 AND MID = '$fam' $fil_maq_and ORDER BY COD ASC";
        }
        if ($tipop == 2) {
            $fam=VoltaValor(PLANO_PADRAO,"MAQUINA_FAMILIA","MID",$plano,0);
            $sql="SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." WHERE STATUS = 1 AND FAMILIA = '$fam' $fil_maq_and ORDER BY COD ASC";
        }
        if ($tipop == 3) {
            $sql="SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." WHERE STATUS = 1 $fil_maq_and ORDER BY COD ASC";
        }
        if ($tipop == 4) {
            $fam=VoltaValor(PLANO_PADRAO,"EQUIPAMENTO_FAMILIA","MID",$plano,0);
            $sql="SELECT COD,DESCRICAO,MID FROM ".EQUIPAMENTOS." WHERE MID_STATUS = 1 AND FAMILIA = '$fam' $fil_equip_and ORDER BY COD ASC";
        }
        if ($tipop == 5) {
            $sql="SELECT COD,DESCRICAO,MID FROM ".EQUIPAMENTOS." WHERE MID_STATUS = 1 $fil_equip_and ORDER BY COD ASC";
        }

        // LISTANDO OS OBJETOS SELECIONADOS
        echo "<fieldset><legend>".$ling['prog_sel_obj']."</legend>";

        $tmp=$dba[0] -> Execute($sql);
        while (!$tmp->EOF) {
            $campo=$tmp->fields;
             
            echo "<input type=\"checkbox\" name=\"cc[".$campo['MID']."]\" id=\"cc[".$campo['MID']."]\" value=\"1\" />
            <label for=\"cc[".$campo['MID']."]\">".htmlentities($campo['COD'])." - ".htmlentities($campo['DESCRICAO'])."</label>
            <br clear=\"all\" />";
             
            $tmp->MoveNext();
        }

        echo "</fieldset>";
    }
}

if($id == 10) {
    // Colotando os dados
    $datai = $_GET['datai'];
    $dataf = $_GET['dataf'];
    $tipo = (int) $_GET['tipo'];
    $plano = (int) $_GET['plano'];
    $progs = $_GET['progs'];
    $mtipo = (int) $_GET['mtipo'];
    $pag_tam = (int) $_GET['pag_tam'];
    $pag_ini = (int) $_GET['pag_ini'];

    echo "<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />\n";
    echo "<center><strong>{$ling['gravando_ordem']} ...</strong><br />\n";

    // Gerando a programação usando paginação
    $pag = grava_programacao_data($datai, $dataf, 0, $tipo, $plano, $progs, $mtipo, $pag_tam, $pag_ini, true);
    
    echo "</center>";
    
    if($pag !== TRUE) {
        $prog_url = implode("&progs[]=", $progs);
        echo "<script>
        self.window.location = 'progra.php?id=10&datai=$datai&dataf=$dataf&tipo=$tipo&plano=$plano&mtipo=$mtipo&pag_tam=$pag_tam&pag_ini=$pag&progs[]=$prog_url';
        </script>\n";
    }
    else {
        echo "<br />
        <br />
        <center><strong>".$ling['prog_ok']."</strong></center>
        <script type=\"text/javascript\" src=\"lib/javascript.js\"></script>
        <script>
        mostraCarregando (false, true);
        atualiza_area_frame('corpo','manusis.php?st=1&id=3&op=5');
        </script>\n";
    }
    exit();
}

if ($id == "") {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
    <body class=\"body_form\">
    <div id=\"formularioos\">";

    if ($_POST['env_form'] != "")  {
        $tipo=(int)$_POST['tipo'];
        $plano=(int)$_POST['plano'];
        $datai=$_POST['datai'];
        $di=explode("/",$datai);
        $datai=$di[2]."-".$di[1]."-".$di[0];
        $dataf=$_POST['dataf'];
        $df=explode("/",$dataf);
        $dataf=$df[2]."-".$df[1]."-".$df[0];



        if (!checkdate ((int)$di[1], (int)$di[0], (int)$di[2])) {
            echo "<br /><strong><font color=\"red\">".$ling['data_inicial_invalida']."</font></strong><br />
                 <br /><p align=center>".$ling['erro_durante_proc_prog_']."<br /> <br /> <a href=\"progra.php?datai=".$_POST['datai']."&dataf=".$_POST['dataf']."&hora=".$_POST['hora']."\"><img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"{$ling['voltar']}\" /></a></p>";
            exit;
        }
        elseif (!checkdate ((int)$df[1], (int)$df[0], (int)$df[2])) {
            echo "<br /><strong><font color=\"red\">".$ling['data_final_invalida']."</font></strong><br />
                 <br /><p align=center>".$ling['erro_durante_proc_prog_']." <br /> <br /> <a href=\"progra.php?datai=".$_POST['datai']."&dataf=".$_POST['dataf']."&hora=".$_POST['hora']."\"><img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"{$ling['voltar']}\" /></a></p>";
            exit;
        }
        else {
            $di=mktime(0,0,0,$di[1],$di[0],$di[2]);
            $df=mktime(0,0,0,$df[1],$df[0],$df[2]);
            if ($di > $df) {
                echo "<br /><strong><font color=\"red\">".$ling['data_inicial_maior_data_final']."</font></strong><br /><br>
                     <br /><p align=center>".$ling['erro_durante_proc_prog_']." <br /> <br /> <a href=\"progra.php?datai=".$_POST['datai']."&dataf=".$_POST['dataf']."&hora=".$_POST['hora']."\"><img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"{$ling['voltar']}\" /></a></p>";
                exit;
            }
        }

        $m=$_POST['tipo_p'];
        $cc=$_POST['cc'];
        $cont=0;
        $mtipo=VoltaValor(PLANO_PADRAO,"TIPO","MID",$plano,0);

        if (($tipo == 1) and (($mtipo == 1) or ($mtipo == 2) or ($mtipo == 3))) {
            $progs = array();
            foreach ($cc as $mid_maq => $ok) {
                if($ok == 1) {
                    $mid=GeraMid(PROGRAMACAO,"MID",0);
                    $sql="INSERT INTO ".PROGRAMACAO." (TIPO,MID_PLANO,DATA_INICIAL,DATA_FINAL,MID_MAQUINA,MID) VALUES ('$tipo','$plano','$datai','$dataf','".$mid_maq."','$mid')";
                    $ins=$dba[0] ->Execute($sql);
                    if ($ins) {
                        $progs[] = $mid;
                        logar(3, "", PROGRAMACAO, "MID", $mid);
                        $cont++;
                      
                    }
                }
            }

            echo "<center>";

            echo "<strong>".$ling['gravando_ordem']." ...</strong><br />";
            

            // Gerando a programação usando paginação
            $pag = grava_programacao_data($datai, $dataf, 0, 1, $plano, $progs, 1, 200, 0, true);
            
            echo "</center>";
            
            if($pag >= 1) {
                $prog_url = implode("&progs[]=", $progs);
                
                echo "<script>
                mostraCarregando (true, true);
                self.window.location = 'progra.php?id=10&datai=$datai&dataf=$dataf&tipo=1&plano=$plano&mtipo=1&pag_tam=200&pag_ini=$pag&progs[]=$prog_url';
                </script>\n";
                exit();
            }
        }
        elseif (($tipo == 1) and (($mtipo == 4) or ($mtipo == 5))) {
            $progs = array();
            foreach ($cc as $mid_maq => $ok) {
                if($ok == 1) {
                    $mid=GeraMid(PROGRAMACAO,"MID",0);
                    $sql="INSERT INTO ".PROGRAMACAO." (TIPO,MID_PLANO,DATA_INICIAL,DATA_FINAL,MID_EQUIPAMENTO,MID) VALUES ('$tipo','$plano','$datai','$dataf','".$mid_maq."','$mid')";
                    $ins=$dba[0] ->Execute($sql);
                    if ($ins) {
                        $progs[] = $mid;
                        logar(3, "", PROGRAMACAO, "MID", $mid);
                        $cont++;
                    }
                }
            }
            
        
            echo "<center>";

            echo "<strong>".$ling['gravando_ordem']." ...</strong><br />";
            

            // Gerando a programação usando paginação
            $pag = grava_programacao_data($datai, $dataf, 0, 1, $plano, $progs, 2, 200, 0, true);
            
            echo "</center>";
            
            if($pag >= 1) {
                $prog_url = implode("&progs[]=", $progs);
                
                echo "<script>
                mostraCarregando (true, true);
                self.window.location = 'progra.php?id=10&datai=$datai&dataf=$dataf&tipo=1&plano=$plano&mtipo=2&pag_tam=200&pag_ini=$pag&progs[]=$prog_url';
                </script>\n";
                exit();
            }
        }

        elseif ($tipo == 2) {
            $mid=GeraMid(PROGRAMACAO,"MID",0);
            $sql="INSERT INTO ".PROGRAMACAO." (TIPO,MID_PLANO,DATA_INICIAL,DATA_FINAL,MID_MAQUINA,MID) VALUES ('$tipo','$plano','$datai','$dataf',0,'$mid')";
            $ins=$dba[0] ->Execute($sql);
            if ($ins) {
                logar(3, "", PROGRAMACAO, "MID", $mid);
                $cont++;
            }
            
            echo "<center>";

            echo "<strong>".$ling['gravando_ordem']." ...</strong><br />";
            

            // Gerando a programação usando paginação
            $pag = grava_programacao_data($datai, $dataf, 0, 2, $plano, $mid, 1, 200, 0, true);
            
            echo "</center>";
            
            if($pag >= 1) {
                echo "<script>
                mostraCarregando (true, true);
                self.window.location = 'progra.php?id=10&datai=$datai&dataf=$dataf&tipo=2&plano=$plano&mtipo=1&pag_tam=200&pag_ini=$pag&progs[]=$mid';
                </script>\n";
                exit();
            }
        }
        if (($cont == 0) or (($tipo != 1) and ($tipo != 2) and ($tipo != 3) and ($tipo != 5))) {
            echo "<br /> <br /> <br /> <br /><p align=center>".$ling['erro_durante_proc_prog_']." -----------. <br /> <br /> <a href=\"progra.php?datai=".$_POST['datai']."&dataf=".$_POST['dataf']."&hora=".$_POST['hora']."\"><img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"{$ling['voltar']}\" /></a></p>";
        }
        else {
            echo "<br /> <br /> <br /> <br /><p align=center>".$ling['prog_ok']."</p><br /> <br /> <br /> <br /> <br /><script>atualiza_area_frame('corpo','manusis.php?st=1&id=3&op=5')</script>";
        }
        unset($_SESSION[ManuSess]['progra']['mo']);
    }
    elseif ($_POST['env_form'] == "") {
        $cc=$_POST['cc'];
        echo "<form name=\"form\" method=\"POST\" action=\"\">
        <fieldset><legend>".$ling['tipo_prog']."</legend>
        <label class=\"campo_label \" for=\"tipo\">".$ling['tipo']."</label>";
        echo "<select name=\"tipo\" id=\"tipo\" class=\"campo_select\" onchange=\"atualiza_area2('mplano','progra.php?&id=1&tipo=' + this.options[this.selectedIndex].value)\">";
        $tmp=$dba[$tdb[PROGRAMACAO_TIPO]['dba']] -> Execute("SELECT DESCRICAO,MID FROM ".PROGRAMACAO_TIPO."     WHERE MID = 1 OR MID = 2 ORDER BY DESCRICAO ASC");
        echo "<option value=\"\">".$ling['select_opcao']."</option>";
        while (!$tmp->EOF) {
            $campo=$tmp->fields;
            if ($cc['TIPO'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['DESCRICAO']."</option>";
            else echo "<option value=\"".$campo['MID']."\">".$campo['DESCRICAO']."</option>";
            $tmp->MoveNext();
        }
        echo "</select><br clear=\"all\" />
        <div id=\"mplano\">";
        echo "<br clear=\"all\" /></div></fieldset>";
        echo "<div id=\"datas\"><fieldset><legend>".$ling['datas']."</legend>";
        FormData($ling['data_inicial'],"datai",$datai,"campo_label");
        echo "<br clear=\"all\" />";
        FormData($ling['data_fim'],"dataf",$dataf,"campo_label");

        echo "</fieldset></div>
        <div id=\"obj\"></div>      
        </div>  
        </div>";
    }
    if (($id == "") and ($_POST['env_form'] == "")) {
        echo "
        </fieldset>
        <div id=\"checkfinal\" align=\"center\"> </div>
        <br /><input type=\"submit\" class=\"botao\" name=\"env_form\" value=\"".$ling['prog_env_form']."\" />";

        echo "
        </form></div>
        </body>
        </html>";
    }
}
?>
