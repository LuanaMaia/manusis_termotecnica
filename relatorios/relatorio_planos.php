<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Relat�rio Planos
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}

$tb_com_cod = array(
	MAQUINAS,
	SETORES,
	AREAS,
	MAQUINAS_FAMILIA
);

function FiltroCheck($campo,$label,$checked=0) {
	if ($checked) $check = "checked=\"checked\"";
	else $check='';
	echo "<input class=\"campo_check\" type=\"checkbox\" $check name=\"filtro[$campo]\" id=\"filtro_$campo\" value=\"$campo\">
	<label for=\"filtro_$campo\" class=\"campo_label\">$label</label>\n";
}

#############################

$alvo = $_GET['alvo'];
$where = '1';

$ajax = (int)$_GET['ajax'];
$emp = (int)$_GET['emp'];
$setor = (int)$_GET['setor'];
$area = (int)$_GET['area'];
$fam = (int)$_GET['fam'];
$obj = (int)$_GET['obj'];
$like = $_GET['like'];
$usa_imagem = (int)$_REQUEST['usa_imagem'];


if ($ajax or $_GET['env']) {
    $condmaq = '';
    $condset = '';
    if ($obj) $condmaq = "MID = '$obj'";
    else {
        if ($fam) {
            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= "FAMILIA = '$fam'";
        }
        if ($setor) {
            $condmaq .= "MID_SETOR = '$setor'";
        }
        elseif ($area) {
            $condset = "MID_AREA = '$area'";
            // gera sequencia de or mid_setor=tal etc
            $setfil = '';
            // Filtro por Empresa
            $fil_emp = VoltaFiltroEmpresa(SETORES, 2);
            $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

            $sql="SELECT MID FROM ".SETORES." WHERE MID_AREA = '$area' $fil_emp";
            if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
                $err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
                erromsg("SQL ERROR .<br>$err<br><br>$sql");
                exit;
            }
            while (!$resultado->EOF) {
                $campo=$resultado -> fields;
                if ($setfil) $setfil .= ' OR ';
                $setfil .= "MID_SETOR = ".$campo['MID'];
                $resultado->MoveNext();
            }
            if ($setfil) $setfil = "($setfil)";
            else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= $setfil;
            
        }
        elseif ($emp) {
            $condarea = "MID_EMPRESA = '$emp'";
            // gera sequencia de or mid_setor=tal etc
            $setfil = '';
            
            // Filtro por Empresa
            $fil_emp = VoltaFiltroEmpresa(AREAS, 2);
            $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";
            
            $sql="SELECT S.MID FROM ".SETORES." S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = '$emp' $fil_emp";
            if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
                $err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
                erromsg("SQL ERROR .<br>$err<br><br>$sql");
                exit;
            }
            while (!$resultado->EOF) {
                $campo=$resultado -> fields;
                if ($setfil) $setfil .= ' OR ';
                $setfil .= "MID_SETOR = ".$campo['MID'];
                $resultado->MoveNext();
            }
            if ($setfil) $setfil = "($setfil)";
            else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= $setfil;
        }
    }
}

if (!$_GET['env']) { // n�o exibindo relatorio

	if ($ajax) {
	
		// Filtros
        echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
        FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
        $where_a = "";
        if($emp) {
            $where_a = "WHERE MID_EMPRESA = $emp";
        }
        FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&setor=$setor&obj=$obj&area=' + this.value)", $where_a);
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
        $where_s = "";
        if($area) {
            $where_s = "WHERE MID_AREA = $area";
        }
        elseif ($emp) {
            $where_s = "WHERE MID_AREA IN (SELECT MID FROM " . AREAS . " WHERE MID_EMPRESA = $emp)";
        }
        FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&obj=$obj&setor=' + this.value)", $where_s);
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&emp=$emp&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
        $where_m = "";
        if ($setor) {
            $where_m = "WHERE MID_SETOR = $setor";
        }
        elseif ($area) {
            $where_m = "WHERE MID_SETOR IN (SELECT MID FROM " . SETORES . " WHERE MID_AREA = $area)";
        }
        elseif ($emp) {
            $list_m = array();
            // Buscando
            $sql = "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S, " . AREAS . " A WHERE M.MID_SETOR = S.MID AND S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp";
            if (!$resultado = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
                $err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
                erromsg("SQL ERROR .<br>$err<br><br>$sql");
                exit;
            }
            while (! $resultado->EOF) {
                $list_m[] = $resultado->fields['MID'];
                $resultado->MoveNext();
            }
            if(count($list_m) > 0) {
                $where_m = "WHERE MID IN (" . implode(', ', $list_m) . ")";
            }
        }
        if($fam) {
            $where_m .= ($where_m == "")? "WHERE " : " AND ";
            $where_m .= "FAMILIA = $fam";
        }
        FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&setor=$setor&obj=' + this.value)", $where_m);
        
	}
	else {
	
		print("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
		<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
		<head>
		 <meta http-equiv=\"pragma\" content=\"no-cache\" />
		<title>{$ling['manusis']}</title>
		<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
		<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n");
		if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
		echo "</head>
		<body>
	
		<div id=\"central_relatorio\">
		<div id=\"cab_relatorio\">
		<h1 />{$ling['rel_planos']}
		</div><div id=\"corpo_relatorio\">
	<form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
	<fieldset>
	<legend>{$ling['rel_desc_col_mostradas']}:</legend>";
		FiltroCheck('NUMERO',$ling['numero'],1);
		FiltroCheck('PARTE',$ling['rel_desc_parte_verificar'],1);
		FiltroCheck('TAREFA',$ling['tarefa_'],1);
		FiltroCheck('INSTRUCAO_DE_TRABALHO',$ling['I_M'], 1);
		echo "<br clear=\"all\" />";
		FiltroCheck('ESPECIALIDADE',$ling['ESP'], 1);
		FiltroCheck('TEMPO_PREVISTO',$ling['tempo_previsto_'], 1);
		FiltroCheck('MID_MATERIAL',$ling['rel_desc_material'], 1);
		FiltroCheck('QUANTIDADE',$ling['qtd'], 1);
		echo "<br clear=\"all\" />";
		FiltroCheck('PERIODICIDADE',$ling['periodicidade_'],1);
		FiltroCheck('FREQUENCIA',$ling['rel_desc_frequencia'],1);
		FiltroCheck('CONTADOR',$tdb[ORDEM_LUB]['CONTADOR'], 1);
		FiltroCheck('DISPARO',$tdb[ORDEM_LUB]['DISPARO'], 1);
		echo "</fieldset>
	
	<fieldset>
	<legend>".$ling['filtros']."</legend>
		<label class=\"campo_label\" for=\"like\">{$ling['descricao']}:</label>
		<input type=\"text\" class=\"campo_text\" name=\"like\" id=\"like\"><br clear=all />
	<div id=\"fil\">";
        
		// Filtros
        echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
        FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&setor=$setor&obj=$obj&area=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&obj=$obj&setor=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=' + this.value)");

        echo "</div>";
		echo "</fieldset>";
	
		echo "<fieldset><legend>".$ling['anexos']."</legend>
	<label class=\"campo_label \" for=\"usa_imagem\">{$ling['incluir_img_anexa']}:</label>
	<input type=\"checkbox\" name=\"usa_imagem\" id=\"usa_imagem\" value=\"1\" ";
		if (!$usa_imagem) echo "checked=\"checked\" ";
		echo "/>
	</fieldset>
	
	<br />
	<input type=\"hidden\" name=\"env\" value=\"1\" />
	
	<input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
	<input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\">
	</form>
	</div>
	</div>
	</body>
	</html>";
	}
}
else { // relatorio
	
	$filtro = $_REQUEST['filtro'];
	$tempoexec_inicial = utime();
	
	// obtem mid_maquina para maquinas preenchendo criterio e monta MID or MID or MID etc
	$condplan = '';
	if ($condmaq) {
		$condmaq = " WHERE $condmaq";
		$sql="SELECT MID FROM ".MAQUINAS . $condmaq;
		//die("$sql<br><br>");
		if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
			$err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
			erromsg("SQL ERROR .<br>$err<br><br>$sql");
			exit;
		}
		while (!$resultado->EOF) {
			$campo=$resultado -> fields;
			if ($condplan) $condplan .= ' OR ';
			$condplan .= "MID_MAQUINA = ".$campo['MID'];
			$resultado->MoveNext();
		}
		if ($condplan) $condplan = "($condplan)";
		//die($condplan);
	}
	
	if ($like) {
		if ($condplan) $condplan .= ' AND ';
		$condplan .= "DESCRICAO LIKE '%$like%'";
	}

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(PLANO_PADRAO, 2);

    // Filtros
    if ($condplan) {
        $condplan = " WHERE $condplan";
        if ($fil_emp != "") {
            $condplan .= " AND " . $fil_emp;
        }
    }
    elseif ($fil_emp != "") {
        $condplan = " WHERE " . $fil_emp;
    }


	$send = '';
	$tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";

    $sql = "SELECT * FROM ".PLANO_PADRAO." $condplan";

	$resultadoplano=$dba[0] -> Execute($sql);
	if(!$resultadoplano) echo "<br><hr />".erromsg($dba[0] -> ErrorMsg())."<hr /><br>";
	else {
		$ii = 0;
		$iim = 0;
		$tmpi = 0;
		while (!$resultadoplano->EOF) { // pra cada plano
			$plano = $resultadoplano -> fields;
			$atvarr=array();
	
			$resultadoat=$dba[0] -> Execute("SELECT * FROM ".ATIVIDADES." WHERE MID_PLANO_PADRAO = '".$plano['MID']."' ORDER BY NUMERO ASC");
			if(!$resultadoat) 	echo "<br><hr />".erromsg($dba[0] -> ErrorMsg())."<hr /><br>";
			else { // tem atividades

				$estamaq = htmlentities(VoltaValor(MAQUINAS,'COD','MID',$plano['MID_MAQUINA'],0).' - '.VoltaValor(MAQUINAS,'DESCRICAO','MID',$plano['MID_MAQUINA'],0));
				$efam_desc = htmlentities(VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$plano['MAQUINA_FAMILIA'],0));
				$tipo_desc = htmlentities(VoltaValor(TIPO_PLANOS,'DESCRICAO','MID',$plano['TIPO'],0));
				if ($plano['TIPO'] == 1) $tipo_desc .= " - $estamaq";
				if ($plano['TIPO'] == 2) $tipo_desc .= " - $efam_desc";
				$send .= "<table border=0 cellspacing=0 width=\"100%\"
				 style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\">
					<thead><tr><th colspan=12 bgcolor=\"#CCCCCC\" $tdstyle align=\"left\"><b>".htmlentities($plano['DESCRICAO'])
				."</b><br />".$tdb[TIPO_PLANOS]['DESC'].": $tipo_desc</th></tr>\n
						<tr>";
				
				if ($filtro['NUMERO']) $send .= "<th $tdstyle>{$ling['Nordem']}</th>";
				if ($filtro['PARTE']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['PARTE']."</th>";
				if ($filtro['TAREFA']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['TAREFA']."</th>";
				if ($filtro['INSTRUCAO_DE_TRABALHO']) $send .= "<th $tdstyle>I.M.</th>";
				if ($filtro['ESPECIALIDADE']) $send .= "<th $tdstyle>{$ling['ESP']}</th>";
				if ($filtro['TEMPO_PREVISTO']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['TEMPO_PREVISTO']."</th>";
				if ($filtro['MID_MATERIAL']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['MID_MATERIAL']."</th>";
				if ($filtro['QUANTIDADE']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['QUANTIDADE']."</th>";
				if ($filtro['PERIODICIDADE']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['PERIODICIDADE']."</th>";
				if ($filtro['FREQUENCIA']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['FREQUENCIA']."</th>";
				if ($filtro['CONTADOR']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['CONTADOR']."</th>";
				if ($filtro['DISPARO']) $send .= "<th $tdstyle>".$tdb[ATIVIDADES]['DISPARO']."</th>";
				
				$send .= "</tr></thead>";
	
				$tmptd = '0:0:0';
				$i = 0;
				while (!$resultadoat->EOF) { // pra cada atividade
					$ativ = $resultadoat -> fields;
					
					$atvarr[$ativ['MID']] = $ativ['MID'];
	
					$espec = VoltaValor(ESPECIALIDADES,'DESCRICAO','MID',$ativ['ESPECIALIDADE'],0);
					$material = VoltaValor(MATERIAIS,'DESCRICAO','MID',$ativ['MID_MATERIAL'],0);
					$unidade = VoltaValor(MATERIAIS,'UNIDADE','MID',$ativ['MID_MATERIAL'],0);
					$quant = $ativ['QUANTIDADE'].' '.VoltaValor(MATERIAIS_UNIDADE,'DESCRICAO','MID',$unidade,0);
					$period = VoltaValor(PROGRAMACAO_PERIODICIDADE,'DESCRICAO','MID',$ativ['PERIODICIDADE'],0);
					$freq = $ativ['FREQUENCIA'];
					$contador = VoltaValor(MAQUINAS_CONTADOR,"DESCRICAO","MID",$ativ['CONTADOR'],0);
					$disparo = $ativ['DISPARO'];
	
					$send .= "<tr>";
					
					if ($filtro['NUMERO']) $send .= "<td align=\"left\" $tdstyle>".$ativ['NUMERO']."</td>";
					if ($filtro['PARTE']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['PARTE'])."</td>";
					if ($filtro['TAREFA']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['TAREFA'])."</td>";
					if ($filtro['INSTRUCAO_DE_TRABALHO']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['INSTRUCAO_DE_TRABALHO'])."</td>";
					if ($filtro['ESPECIALIDADE']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($espec)."</td>";
					if ($filtro['TEMPO_PREVISTO']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['TEMPO_PREVISTO'])."</td>";
					if ($filtro['MID_MATERIAL']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($material)."</td>";
					if ($filtro['QUANTIDADE']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($quant)."</td>";
					if ($filtro['PERIODICIDADE']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($period)."</td>";
					if ($filtro['FREQUENCIA']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($freq)."</td>";
					if ($filtro['CONTADOR']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($contador)."</td>";
					if ($filtro['DISPARO']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($disparo)."</td>";
					
					$send .= "</tr>\n";
					$tmptd = SomaHora(array($tmptd,$ativ['TEMPO_PREVISTO']));
					$resultadoat->MoveNext();
					$i++;
				} // fim pra cada atividade
				$send .= "<tr><td colspan=12 bgcolor=\"#E7E7E7\" $tdstyle>&nbsp;{$ling['rel_desc_qtd_atv']}: $i -
						 {$tdb[ATIVIDADES]['TEMPO_PREVISTO']}o: $tmptd</td></tr></table><br />";
				$ii += $i;
				$tmpi = SomaHora(array($tmpi,$tmptd));
			}
			
			#############
			if ($usa_imagem) {
				// anexos �s atividades deste plano
				$nums = array(); // $nums[numero da atividade] = $i da instru��o;
				if (count($atvarr) > 0) { // tem atividade
					//die('Esta p�gina se encontra em desenvolvimento, por favor aguarde<br><br>');
					foreach ($atvarr as $eatv) {
						$nums[$eatv] = 0; // $i da atividade = 0;
						//echo "[$eatv]";
						// anexos a esta atividade
						$arquivos = ListaImagensAtividade($eatv); // lista na pasta planopadrao pelo mid da atividade
						//print_r($arquivos);
						$dir_i=$manusis['dir']['planopadrao']."/$eatv";
						$j=0;
						if ($arquivos) foreach ($arquivos as $earq) {
							$j++;
							//echo "[$earq]";
							
							// verifica se ANTES dessa foto,
							// a quantidade de fotos que j� foram colocadas � multiplo de 5
							// se for, inicia nova linha
							$album_cnt_last = $album_cnt / 5;
							if ($album_cnt_last == round($album_cnt_last)) {
								// � multiplo de 5
								if ($album_cnt) $album .= "<tr>";
							}
					
							// aumenta para contar foto atual
							$album_cnt++;
							
							// coloca esta imagem:
							$numero_atv = VoltaValor(ATIVIDADES,'NUMERO','MID',$eatv,0);
							$nums[$eatv]++; // $i da atividade ++;
							$album .= "<td align=center><center>
							<table><tr style=\"page-break-inside:avoid\">
							<td style=\"font-size:10px\">
							{$ling['atividade_n']} $numero_atv, {$ling['atividade']} {$nums[$eatv]}:
							<br>
							<img src=\"{$manusis['url']}i.php?i=$dir_i/$earq&s=120\" border=1 hspace=5 align=\"middle\" \>
							</td></tr></table>
							</center></td>";
							
							// verifica se DEPOIS dessa foto,
							// a quantidade de fotos que j� foram colocadas � multiplo de 5
							// se for, fecha essa linha linha
							// (duas checagens s�o necess�rias porque esse loop pode n�o repetir)
							$album_cnt_last = $album_cnt / 5;
							if ($album_cnt_last == round($album_cnt_last)) {
								// � multiplo de 5
								if ($album_cnt) $album .= "</tr>";
							}
							
						}
					}
				}
				//die($album);
				if ($album) $send .= "<br><table style=\"border: 1px solid black; font-size:10px\" width=100%>
			  <thead>
			  <tr>
			    <td valign=top align=left colspan=5>
				<strong>{$ling['anexos']}:</strong>
			    </td>
			   </tr></thead>
				$album
				</table><br><br><br>";
				unset($album);
				unset($album_cnt);
				unset($album_cnt_last);
			}
			#############
			
	
			$resultadoplano->MoveNext();
			$iim++;
		} // fim pra cada plano
	}
	
	if (!$iim) $send = "<font size=\"2\">{$ling['rel_desc_sem_registros']}</font><br />";
	$send .= "{$ling['rel_desc_qtd_atv_total']}: $ii<br />{$ling['rel_desc_tmp_total_prev']}: $tmpi";
	
	$filtro = '';
	if ($fam) $filtro = $tdb[MAQUINAS_FAMILIA]['DESC'].' '.VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$fam,0);
	
	$filtro = '';
	if ($area) $filtro .= $tdb[AREAS]['DESC'].': '.VoltaValor(AREAS,"DESCRICAO","MID",$area,$tdb[AREAS]['dba']);
	if ($setor) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[SETORES]['DESC'].': '.VoltaValor(SETORES,"DESCRICAO","MID",$setor,$tdb[SETORES]['dba']);
	}
	if ($fam) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS_FAMILIA]['DESC'].': '.VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$fam,$tdb[MAQUINAS_FAMILIA]['dba']);
	}
	if ($obj) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS]['DESC'].': '.VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,$tdb[MAQUINAS]['dba']);
	}
	if ($like) {
		if ($filtro) $filtro .= '; ';
		$filtro .= "{$ling['descricao_com']} $like";
	}
	if (!$filtro) $filtro = $ling['nenhum'];
	
	$tempoexec_final = utime();
	$tempoexec = round($tempoexec_final - $tempoexec_inicial,4);
	
	if ($_GET['word']) exportar_word($ling['rel_planos'],$filtro,$iim,$send,1);
	else relatorio_padrao($ling['rel_planos'],$filtro,$iim,$send,1,$tempoexec);

} // fim OK

##################################

function ListaImagensAtividade($mid_atv) {
	global $manusis;
	
	$dir="../".$manusis['dir']['planopadrao']."/$mid_atv";

	$arquivos = array();
	$tem_arquivos = false;
	
	if (file_exists($dir)) {
		$od = opendir($dir);
		$od2 = opendir($dir);
		$cont_verarq = 0;
		while (false !== ($arq_v = readdir($od2))) {
			$cont_verarq++;
		}
		if ($cont_verarq > 2){
			$i=0;
			while (false !== ($arq = readdir($od))) { //mostra
				$dd=explode(".",$arq);
				if (($dd[1] == "jpg") or ($dd[1] == "png")) {
					$arquivos[] = $arq;
					if (!$tem_arquivos) $tem_arquivos = true;
				}
			}
		}
	}
	if (!$tem_arquivos) return false;
	else return $arquivos;
}

?>
