<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Relatório ponto de compra
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";


#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
#############################

$alvo = $_GET['alvo'];


# OK !!!
if ($alvo == 'ok') {
	$tempoexec_inicial = utime();
	$tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";
    
    $where = ' WHERE ';
    
    $whereMaterial = '';
    
    if(!empty($_GET['mat']))
    {
       $mat = LimpaTexto($_GET['mat']);
       
       $whereMaterial = " AND (M.DESCRICAO LIKE('%{$mat}%') OR M.COD LIKE('%{$mat}%') OR M.COMPLEMENTO LIKE('%{$mat}%') )";
    }
    
    if (!empty($_GET['fam'])) $whereMaterial .= " AND FAMILIA = '".LimpaTexto($_GET['fam'])."'";
    
    
    if(isset($_GET['mid_empresas']))
    {
       $midsEmpresa = implode($_GET['mid_empresas'], ', ');
       
       $where .= "MID IN ({$midsEmpresa})";
    }
    
    $whereAlmoaxrifado = '';
    
    if(isset($_GET['mid_almoxarifado']))
    { 
       $whereAlmoaxrifado = ' AND A.MID IN (' . implode($_GET['mid_almoxarifado'], ', '). ')';
    }
    
    $where  = ($where == ' WHERE ')?';':$where;
    
    $sql  = 'SELECT MID, COD, NOME FROM ' . EMPRESAS . $where;
    
   $result = $dba[$tdb[EMPRESAS]['dba']] -> Execute($sql);
      
   $registrosEncontrados = 0;
   if(!$result->EOF)
   {
      while(!$result->EOF)
      {
          $campo = $result->fields;
          
          $sql = 'SELECT 
                    A.MID,
                    A.DESCRICAO 
                  FROM
                    '.ALMOXARIFADO.' A                   
                  WHERE 
                  A.MID_EMPRESA = ' . $campo['MID'] . $whereAlmoaxrifado;
                  
                  $resultAlmoxarifado = $dba[$tdb[ALMOXARIFADO]['dba']] -> Execute($sql);
                  
                  $i = 1;
                  $dadosAlmoxarifado = array();
                  $midsAlmoxarifado = array();
                  if(!$resultAlmoxarifado->EOF)
                  {
                     while(!$resultAlmoxarifado->EOF)
                     {
                         $dadosAlmoxarifado[] =  $resultAlmoxarifado->fields;
                         $midsAlmoxarifado[] = $resultAlmoxarifado->fields['MID'];
                         $i++;
                         $resultAlmoxarifado->MoveNext();
                     }
                  }
                  
                  $totalAlmoxarifado = count($dadosAlmoxarifado);
                  
                  
                  if($totalAlmoxarifado == 0){
                     $result->MoveNext();
                    continue;
                  }
                  
                  $head ="                  
                  <table id=\"dados_processados\" cellspacing=\"0\" cellpadding=\"0\"
	 style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">
     <div align=\"left\" ><strong><font size=\"3\">".$campo['COD'].'-'.$campo['NOME']."</font></strong></div>
     ";
	$head .= "<thead>";
    $head .= "<tr>
                <th rowspan=\"2\" $tdstyle>{$ling['cod']}</th>
                <th rowspan=\"2\" $tdstyle>{$ling['rel_desc_material']}</th>
                <th rowspan=\"2\" $tdstyle>{$ling['rel_desc_uni']}</th>";
                
                foreach($dadosAlmoxarifado as $data){
                   $head .= "<th $tdstyle colspan=\"4\">{$data['DESCRICAO']}</th>\n"; 
                } 
                  
          $head .="<th rowspan=\"2\" $tdstyle>{$ling['custo_total']}</th>
          </tr>";
          
          
               $head .= "<tr>";
               
               foreach($dadosAlmoxarifado as $data){
                  $head .= "<th $tdstyle>{$ling['rel_desc_est_minimo']}</th>\n
                 <th $tdstyle>{$ling['rel_desc_custo_uni']}</th>\n
                 <th $tdstyle>{$ling['rel_desc_est_atual']}</th>\n
                 <th $tdstyle>{$ling['necessidade']}</th>\n";
          }        
                 $head .="</tr>";  
          
          $head .= "</thead>";
          
             $midsAlmoxarifado = implode($midsAlmoxarifado, ', ');
             
             $sql = "SELECT 
                       M.COD, 
                       M.DESCRICAO, 
                       M.ESTOQUE_MINIMO, 
                       MU.COD AS UNIDADE,  
                       MA.CUSTO_UNITARIO, 
                       MA.ESTOQUE_ATUAL,
                       MA.MID_ALMOXARIFADO,
                       M.CUSTO_UNITARIO as  CUSTO_UNITARIO_MATERIAL 
                     FROM ".MATERIAIS." M, ".
                       MATERIAIS_ALMOXARIFADO." MA,".
                       MATERIAIS_UNIDADE. " MU 
                    WHERE 
                        MA.MID_MATERIAL = M.MID 
                    AND M.UNIDADE = MU.MID    
                    AND MA.ESTOQUE_ATUAL <= M.ESTOQUE_MINIMO 
                    AND MA.MID_ALMOXARIFADO  IN ({$midsAlmoxarifado})
                    {$whereMaterial}
                    ORDER BY M.COD ASC";  
             
                   $resultMateriais = $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> Execute($sql);
                   
                   $populateMaterirais = array();
                   if(!$resulMateriais->EOF)
                   {
                          while(!$resultMateriais->EOF)
                          {
                             $dataMateriais = $resultMateriais->fields;
                             
                             $populateMaterirais[$dataMateriais['COD']][$dataMateriais['MID_ALMOXARIFADO']]['ESTOQUE_MINIMO'] =  $dataMateriais['ESTOQUE_MINIMO'];
                             $populateMaterirais[$dataMateriais['COD']][$dataMateriais['MID_ALMOXARIFADO']]['CUSTO_UNITARIO'] =  $dataMateriais['CUSTO_UNITARIO'];
                             $populateMaterirais[$dataMateriais['COD']][$dataMateriais['MID_ALMOXARIFADO']]['ESTOQUE_ATUAL'] =  $dataMateriais['ESTOQUE_ATUAL'];
                             $populateMaterirais[$dataMateriais['COD']][$dataMateriais['MID_ALMOXARIFADO']]['CUSTO_UNITARIO_MATERIAL'] =  $dataMateriais['CUSTO_UNITARIO_MATERIAL'];
                             
                             $populateMaterirais[$dataMateriais['COD']]['UNIDADE'] = $dataMateriais['UNIDADE'];
                             $populateMaterirais[$dataMateriais['COD']]['DESCRICAO'] = $dataMateriais['DESCRICAO'];
                             
                             $resultMateriais->MoveNext();
                          }
                   }
                 $txt = ''  ;
                 
              foreach($populateMaterirais as $key => $data)
              {
                  $txt .= "<tr>
                           <td $tdstyle width=\"20px\">{$key}</td>
                           <td $tdstyle width=\"120px\">{$populateMaterirais[$key]['DESCRICAO']}</td>
                           <td $tdstyle width=\"20px\">{$populateMaterirais[$key]['UNIDADE']}</td>";
                  
                  $CustoTotal = 0;
                  $totalAlmoxarifado =  array();
                  foreach($dadosAlmoxarifado as $dataAlmoxarifado)
                  {
                     if(isset($populateMaterirais[$key][$dataAlmoxarifado['MID']]))
                     {
                        $valorUnitario = ($_GET['valor_almoxarifado'] == 1)?$populateMaterirais[$key][$dataAlmoxarifado['MID']]['CUSTO_UNITARIO']:$populateMaterirais[$key][$dataAlmoxarifado['MID']]['CUSTO_UNITARIO_MATERIAL'];
                       
                        $nec = $populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_MINIMO'] - $populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_ATUAL'];
			           if ($nec < 0 ) $nec = 0;
                        
                        $CustoTotal += round($nec * $valorUnitario, 2);
                        
                        $totalAlmoxarifado[$dataAlmoxarifado['MID']]['TOTAL_CUSTO_UNITARIO'] += $valorUnitario;
                        $totalAlmoxarifado[$dataAlmoxarifado['MID']]['TOTAL_ESTOQUE_MINIMO'] += $populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_MINIMO'];
                        $totalAlmoxarifado[$dataAlmoxarifado['MID']]['TOTAL_ESTOQUE_ATUAL'] += $populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_ATUAL'];
                        $totalAlmoxarifado[$dataAlmoxarifado['MID']]['TOTAL_NECESSIDADE'] += $nec;
                        
                        $txt .= "<td $tdstyle>{$valorUnitario}</td>
                                <td $tdstyle>{$populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_MINIMO']}</td>
                                <td $tdstyle>{$populateMaterirais[$key][$dataAlmoxarifado['MID']]['ESTOQUE_ATUAL']}</td>
                                <td $tdstyle>{$nec}</td>"; 
                     }
                     else
                     {
                     $txt .= "<td $tdstyle>0</td>
                                <td $tdstyle>0</td>
                                <td $tdstyle>0</td>
                                <td $tdstyle>0</td>"; 
                     }
                  }
                  $CustoTotalGeral += $CustoTotal;
                  
                  $txt .="<td $tdstyle width=\"120px\">". number_format($CustoTotal, 2, ',', '')."</td>
                  </tr>";
                  
                  $registrosEncontrados++;
              }
              
              $foot ="<tr>
    
    <td $tdstyle colspan=\"3\"><strong>{$ling['ord_total']}</strong></td>";
    
     foreach($dadosAlmoxarifado as $data){
      
        $foot .="<td $tdstyle >{$totalAlmoxarifado[$data['MID']]['TOTAL_CUSTO_UNITARIO']}</td>
          <td $tdstyle>{$totalAlmoxarifado[$data['MID']]['TOTAL_ESTOQUE_MINIMO']}</td>
          <td $tdstyle>{$totalAlmoxarifado[$data['MID']]['TOTAL_ESTOQUE_ATUAL']}</td>
          <td $tdstyle>{$totalAlmoxarifado[$data['MID']]['TOTAL_NECESSIDADE']}</td>";     
     }
      
   
    $foot .="<td $tdstyle><strong>".number_format($CustoTotalGeral, 2, ',', '')."</strong></td>
    </tr></table>";
              
              $CustoTotalGeral = 0;
              
              $relatorio .= $head.$txt.$foot;
                            
              $result->MoveNext();          
      }
      
      $tempoexec_final = utime();
	  $tempoexec = round($tempoexec_final - $tempoexec_inicial,4);
      
      if ($_GET['word']) exportar_word($ling['rel_ponto_compra_mat'],$filtro,$i,$relatorio,$tempoexec);
	  else relatorio_padrao($ling['rel_ponto_compra_mat'],$filtro,$registrosEncontrados,$relatorio,1,$tempoexec);    
      
   }
    exit;
    $foot = '';
 $tempoexec_inicia = utime();

	$head="<table id=\"dados_processados\" cellspacing=\"0\" cellpadding=\"0\"
	 style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">";
	$head .= "<thead>";
	
    $head .= "<tr>
    <th $tdstyle>{$ling['almoxarifado']}</th>
    <th $tdstyle>{$ling['cod2']}</th>
    <th $tdstyle>{$ling['descricao']} {$ling['item_peca']}</th>
    <th $tdstyle>{$ling['rel_desc_uni']}.</th>
    <th $tdstyle>{$ling['rel_desc_custo_uni']}</th>
    <th $tdstyle>{$ling['rel_desc_est_minimo']}</th>
    <th $tdstyle>{$ling['rel_desc_est_atual']}</th>
    <th $tdstyle>{$ling['necessidade']}</th>
    <th $tdstyle>{$ling['rel_desc_custo']}</th>
    </tr>";
	$head .= "</thead>";
    

	$i = 0;
	$tun = 0;
	$testm = 0;
	$testa = 0;
	$tnec = 0;
	$tcun = 0;
	$tcusto = 0;
	
	$where = 'WHERE A.MID = MA.MID_ALMOXARIFADO AND MA.MID_MATERIAL = M.MID AND MA.ESTOQUE_ATUAL <= M.ESTOQUE_MINIMO AND M.ESTOQUE_MINIMO > 0';

	$fam = (int)$_GET['fam'];
	$mat = (int)$_GET['mat'];
	if ($fam) $where .= " AND FAMILIA = '$fam'";
	if ($mat){
        
        $where .= " AND  LIKE ()M.MID = '$mat'";
    }

	$sql = "SELECT 
     M.COD, 
     M.DESCRICAO, 
     M.ESTOQUE_MINIMO, 
     M.UNIDADE, 
     MA.CUSTO_UNITARIO, 
     MA.ESTOQUE_ATUAL, 
     A.COD AS COD_ALMOX, 
     A.DESCRICAO AS DESC_ALMOX 
     FROM ".MATERIAIS." M, ".
     MATERIAIS_ALMOXARIFADO." MA, ".
     ALMOXARIFADO." A 
     $where 
     ORDER BY M.COD ASC";    
     
    
	//echo "$sql<br><br>\n\n";
	if (!$resultado= $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)){
		$err = $dba[$tdb[MATERIAIS]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	} else {
		while (!$resultado->EOF) {
			$campo = $resultado->fields;
			$i++;
			$estm = $campo['ESTOQUE_MINIMO'];
			$un_mid = $campo['UNIDADE'];
			$un = VoltaValor(MATERIAIS_UNIDADE,'COD','MID',$campo['UNIDADE'],$tdb[MATERIAIS_UNIDADE]['dba']);
			$esta = $campo['ESTOQUE_ATUAL'];
			$nec = $estm - $esta;
			if ($nec < 0 ) $nec = 0;
			$cun = $campo['CUSTO_UNITARIO'];
			$custo = round($cun * $nec,2);

			$tun += $un;
			$testm += $estm;
			$testa += $esta;
			$tnec += $nec;
			$tcun += $cun;
			$tcusto += $custo;

			if (!$nec) $nec = '-';
			if (!$custo) $custo = '-';

			$txt.="<tr>
            <td $tdstyle>".$campo['COD_ALMOX'] .'-'.$campo['DESC_ALMOX']."</td>
            <td $tdstyle>".$campo['COD']."</td>
            <td $tdstyle>".$campo['DESCRICAO']."</td>
            <td $tdstyle>$un&nbsp;</td>
            <td $tdstyle>".number_format($cun, 2, ',', '')."&nbsp;</td>
            <td $tdstyle>$estm&nbsp;</td>
            <td $tdstyle>$esta&nbsp;</td>
            <td $tdstyle>$nec&nbsp;</td>
            <td $tdstyle>".number_format($custo, 2, ',', '')."&nbsp;</td>
            </tr>";

			$resultado->MoveNext();
		}
	}
	
	$foot.="<tr>
    
    <td $tdstyle colspan=\"5\"><strong>{$ling['ord_total']}</strong></td>
    
    <td $tdstyle><strong>$testm</strong></td>
    <td $tdstyle><strong>$testa</strong></td>
    <td $tdstyle><strong>$tnec</strong></td>
    <td $tdstyle><strong>".number_format($tcusto, 2, ',', '')."</strong></td>
    </tr></table>";

	if ($i) $txt = $head.$txt.$foot;
	else $txt = $ling['rel_desc_texto_ponto_compra_mat'];

	if ($fam) $filtro = $tdb[MATERIAIS_FAMILIA]['DESC'].' '.VoltaValor(MATERIAIS_FAMILIA,'DESCRICAO','MID',$fam,0);
	elseif ($mat) $filtro = $tdb[MATERIAIS]['DESC'].' '.VoltaValor(MATERIAIS,'DESCRICAO','MID',$mat,0);
	else $filtro = $ling['nenhum'];

	$tempoexec_final = utime();
	
	$tempoexec = round($tempoexec_final - $tempoexec_inicial,4);
	

	if ($_GET['word']) exportar_word($ling['rel_ponto_compra_mat'],$filtro,$i,$txt,1);
	else relatorio_padrao($ling['rel_ponto_compra_mat'],$filtro,$i,$txt,1,$tempoexec);

} // fim OK

# primeira visualização
elseif (!$alvo) {
	print("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
	<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
	<head>
	 <meta http-equiv=\"pragma\" content=\"no-cache\" />
	<title>{$ling['manusis']}</title>
	<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
	<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n");
		if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
		print("</head>
	<body>

	<div id=\"central_relatorio\">
	<div id=\"cab_relatorio\">
	<h1 />{$ling['rel_ponto_compra_mat']}
	</div><div id=\"corpo_relatorio\">
	<fieldset>
	<legend>".$ling['filtros']."</legend>
	<div id=\"escolha\">");
	$primeirocampo=strtoupper($ling['todos']);
	echo "<form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatorio\" id=\"form_relatorio\" method=\"GET\">
	<input type=\"hidden\" name=\"alvo\" value=\"ok\">
	<label class=\"campo_label\" for=\"fam\">".$tdb[MATERIAIS_FAMILIA]['DESC'].":</label><br />";
	FormSelectD('COD', 'DESCRICAO',MATERIAIS_FAMILIA,$_GET['fam'],"fam","fam","MID",0);
	echo "<div id=\"mat\">
    <br />
    <label class=\"campo_label\" for=\"mat\">".$tdb[MATERIAIS]['DESC'].":</label>";
	echo "<input class=\"campo_text\" type=\"text\" name=\"mat\" id=\"mat\">";
	echo "</div>
	<br />
    <label class=\"campo_label\" for=\"mid_empresas\">".$tdb[EMPRESAS]['DESC']."</label>
    <br />\n";
      FormSelectMult("COD", "NOME", EMPRESAS, array(), "mid_empresas", "mid_empresas", "MID" );
	echo "</div>
    <label class=\"campo_label\" for=\"mid_almoxarifado\">".$tdb[ALMOXARIFADO]['DESC']."</label>
    <br />\n";
      FormSelectMult("COD", "DESCRICAO", ALMOXARIFADO, array(), "mid_almoxarifado", "mid_almoxarifado", "MID" );
	echo "  <br />   
     <label class=\"campo_label\" for=\"valor_almoxarifado1\">".$ling['relatorio_materias_preco_almoxarifado1']."
     <input type=\"radio\" value=\"1\" id=\"valor_almoxarifado1\" name=\"valor_almoxarifado\" checked=\"checked\" /></label>
   
     <label class=\"campo_label\" for=\"valor_almoxarifado2\">".$ling['relatorio_materias_preco_almoxarifado2']."
     <input type=\"radio\" value=\"2\" id=\"valor_almoxarifado2\" name=\"valor_almoxarifado\" /></label>
    <br />\n    
	</fieldset>
	<input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
	<input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\"></form>
	</div>";
}
##################################
?>
