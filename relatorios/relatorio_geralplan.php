<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Autor: Fernando Cosentino
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$tb=LimpaTexto($_GET['tb']);
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=LimpaTexto($_GET['criv']);
$cric=LimpaTexto($_GET['cric']);
$cris2=(int)$_GET['cris2'];
$criv2=LimpaTexto($_GET['criv2']);
$cric2=LimpaTexto($_GET['cric2']);
$or=$_GET['or'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];

if (($relatorio != "") or ($exword != "")){
    $datai=explode("/",$_GET['datai']);
    $datai=$datai[2]."-".$datai[1]."-".$datai[0];
    $dataf=explode("/",$_GET['dataf']);
    $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
    $empf=(int)$_GET['filtro_emp'];
    $areaf=(int)$_GET['filtro_area'];
    $setorf=(int)$_GET['filtro_setor'];
    $maqf=(int)$_GET['filtro_maq'];
    $ccf=(int)$_GET['filtro_cc'];
    $tipo_prev=(int)$_GET['filtro_prev'];
    $tipo_rota=(int)$_GET['filtro_rota'];
    $tipo_sol=(int)$_GET['filtro_sol'];

    $opcao_atv=(int)$_GET['opcao_atv'];
    $opcao_mat=(int)$_GET['opcao_mat'];
    $opcao_mao=(int)$_GET['opcao_mao'];
    $opcao_osf=(int)$_GET['opcao_osf'];
    $opcao_osa=(int)$_GET['opcao_osa'];
    $maq_fam=(int)$_GET['filtro_maq_fam'];

    $tempoexec_inicial = utime();
    $tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";


    if ($maq_fam != 0) {
        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);
        $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

        $sql="SELECT MID FROM ".MAQUINAS." WHERE FAMILIA = '$maq_fam' $fil_emp";
        if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
            $err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
            erromsg("SQL ERROR .<br>$err<br><br>$sql");
            exit;
        }
        $mfil= "AND (";
        $ifmaq=0;
        while (!$resultado->EOF) {
            $campo=$resultado -> fields;
            if ($ifmaq == 0) $mfil .= "a.MID_MAQUINA = ".$campo['MID'];
            else $mfil .= " OR a.MID_MAQUINA = ".$campo['MID'];
            $ifmaq++;
            $resultado->MoveNext();
        }
        if ($ifmaq == 0)$mfil.="a.MID_MAQUINA = 'A'";
        $mfil.=")";
    }

    $mostrafiltro ='';
    if ($empf != 0) {
        $fil.=" AND a.MID_EMPRESA = '$empf'";
        $mostrafiltro .= "<li>".$tdb[EMPRESAS]['DESC'].': '.
        htmlentities(VoltaValor(EMPRESAS,'NOME','MID',$empf,0))."</li>";
    }
    if ($areaf != 0) {
        $fil.=" AND a.MID_AREA = '$areaf'";
        $mostrafiltro .= "<li>".$tdb[AREAS]['DESC'].': '.
        htmlentities(VoltaValor(AREAS,'DESCRICAO','MID',$areaf,0))."</li>";
    }
    if ($setorf != 0) {
        $fil.=" AND a.MID_SETOR = '$setorf'";
        $mostrafiltro .= "<li>".$tdb[SETORES]['DESC'].': '.
        htmlentities(VoltaValor(SETORES,'DESCRICAO','MID',$setorf,0))."</li>";
    }
    if ($maqf != 0) {
        $fil.=" AND a.MID_MAQUINA = '$maqf'";
        $mostrafiltro .= "<li>".$tdb[MAQUINAS]['DESC'].': '.
        htmlentities(VoltaValor(MAQUINAS,'DESCRICAO','MID',$maqf,0))."<!-- MAQ: $maqf --></li>";
    }
    if ($ccf != 0) {
        $fil.=" AND a.CENTRO_DE_CUSTO = '$ccf'";
        $mostrafiltro .= "<li>".$tdb[CENTRO_DE_CUSTO]['DESC'].': '.
        htmlentities(VoltaValor(CENTRO_DE_CUSTO,'DESCRICAO','MID',$ccf,0))."</li>";
    }
    $filtipo=null;
    if ($tipo_prev) $filtipo[]=$ling['preventiva'];
    if ($tipo_rota) $filtipo[]=$ling['def_rota'];
    if ($tipo_sol) $filtipo[]=$ling['sol_desc'];
    if ($filtipo) $mostrafiltro .= "<li>{$ling['origem']}: ".implode(', ',$filtipo)."</li>";

    if ($mfil) $fil.=" $mfil";
    if (($opcao_osa != 0) and ($opcao_osf != 0)) $fil2.=" (a.STATUS = 1 OR a.STATUS = 2)";
    elseif (($opcao_osa != 0) and ($opcao_osf == 0)) $fil2.=" a.STATUS = 1";
    elseif (($opcao_osa == 0) and ($opcao_osf != 0)) $fil2.=" a.STATUS = 2";
    if ($_GET['datai'] != "") $fil.=" AND a.DATA_PROG >= '$datai' AND a.DATA_PROG <= '$dataf'";

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(ORDEM_PLANEJADO, 2);
    if ($fil_emp != "") {
        // MAQUINAS VISIVEIS NAS ROTAS
        $filtro_lub = VoltaFiltroEmpresa(ORDEM_LUB, 2);
        $fil .= "AND (" . $fil_emp . " OR (TIPO = 2 AND (SELECT COUNT(MID) FROM " . ORDEM_LUB . " WHERE MID_ORDEM = a.MID AND $filtro_lub) > 0))";
    }


    $sql="SELECT a.* FROM ".ORDEM_PLANEJADO." as a WHERE $fil2 $fil ORDER BY a.DATA_PROG ASC";
    if (! $resultado = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        erromsg("{$ling['arquivo']}: " . __FILE__ . "<br />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    $iii=0;
    while (!$resultado->EOF) {
        $campo=$resultado -> fields;
        list($eano, $emes, $edia) = explode('-',$campo['DATA_PROG'],3);
        $esem = date('W',mktime(0,0,0,$emes,$edia,$eano));
        ################ - PREVENTIVA - ################
        if (($campo['TIPO'] == 1) and ($tipo_prev)) {
            if ($campo['STATUS'] == 2) $status=$ling['ORDEM_FECHADA'];
            elseif ($campo['STATUS'] == 1) $status="";
            $txt.="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
            <tr>
            <td align=\"left\"><font size=\"2\"><strong>{$ling['rel_desc_os_num']}: ".$campo['NUMERO']."</strong></font></td>
            <td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])." </td>
            <td align=\"left\"><strong>{$ling['plano']}</strong>: ".VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$campo['MID_PROGRAMACAO'],$tdb[PROGRAMACAO_TIPO]['dba']),0)." </td>
            <td align=\"left\"><strong>{$ling['rel_desc_dt_prog_min']}</strong>: ".$resultado -> UserDate($campo['DATA_PROG'],'d/m/Y').", semana $esem</td>
            </tr>
            <tr>
            <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc2_min']}</strong>: ".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."  </td>
            <td align=\"left\" colspan=\"2\"><strong>{$ling['sol_objeto']}</strong>: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])." </td>
            </tr>";
            if ($status != "") {
                $txt.= "
                <td align=\"left\" colspan=\"2\"><strong>{$ling['ORDEM_FECHADA']}</strong></td>
                <td align=\"left\"><strong>{$ling['data_inicio']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." (".$campo['HORA_INICIO'].")</td>
                <td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')."(".$campo['HORA_FINAL'].")</td>
                </tr>";
            }
            $txt.="
            </table><br>";
        }
        ################ - ROTA - ################
        elseif (($campo['TIPO'] == 2) and ($tipo_rota)) {
            if ($campo['STATUS'] == 2) $status=$ling['ORDEM_FECHADA'];
            elseif ($campo['STATUS'] == 1) $status="";
            $txt.="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
            <tr>
            <td align=\"left\"><font size=\"2\"><strong>{$ling['rel_desc_os_num']}: ".$campo['NUMERO']."</strong></font></td>
            <td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])." </td>
            <td align=\"left\"><strong>{$ling['plano']}</strong>: ".VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$campo['MID_PROGRAMACAO'],$tdb[PROGRAMACAO_TIPO]['dba']),0)." </td>
            <td align=\"left\"><strong>{$ling['rel_desc_dt_prog_min']}</strong>: ".$resultado -> UserDate($campo['DATA_PROG'],'d/m/Y').", semana $esem</td>
            </tr>";
            if ($status != "") {
                $txt.= "
                <td align=\"left\" colspan=\"2\"><strong>{$ling['ORDEM_FECHADA']}</strong></td>
                <td align=\"left\"><strong>{$ling['data_inicio']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." (".$campo['HORA_INICIO'].")</td>
                <td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')."(".$campo['HORA_FINAL'].")</td>
                </tr>";
            }
            $txt.="
            </table><br>";
        }
        ################ -  - ################
        if (($campo['TIPO'] == 4) and ($tipo_sol)) {
            if ($campo['STATUS'] == 2) $status=$ling['ORDEM_FECHADA'];
            elseif ($campo['STATUS'] == 1) $status="";
            $txt.="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
            <tr>
            <td align=\"left\"><font size=\"2\"><strong>{$ling['rel_desc_os_num']}: ".$campo['NUMERO']."</strong></font></td>
            <td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])." </td>
            <td align=\"left\"><strong>{$ling['assunto']}:</strong>: ".VoltaValor(SOLICITACOES,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$campo['MID_PROGRAMACAO'],$tdb[PROGRAMACAO_TIPO]['dba']),0)." </td>
            <td align=\"left\"><strong>{$ling['rel_desc_dt_prog_min']}</strong>: ".$resultado -> UserDate($campo['DATA_PROG'],'d/m/Y')." </td>
            </tr>
            <tr>
            <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc2_min']}</strong>: ".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."  </td>
            <td align=\"left\" colspan=\"2\"><strong>{$ling['prog_objeto']}</strong>: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])." </td>
            </tr>";
            if ($status != "") {
                $txt.= "
                <td align=\"left\" colspan=\"2\"><strong>{$ling['ORDEM_FECHADA']}</strong></td>
                <td align=\"left\"><strong>{$ling['data_inicio']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." (".$campo['HORA_INICIO'].")</td>
                <td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')."(".$campo['HORA_FINAL'].")</td>
                </tr>";
            }
            $txt.="
            </table><br>";
        }
        $obj=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$campo['MID']."'");
        $obj_campo = $obj -> fields;
        $objl=$i=count($obj -> getrows());
        if ($objl != 0) {
            $txt.= "<table id=\"dados_processados\" cellpadding=\"2\" width=\"100%\"><tr>
            <tr><td colspan=\"2\" aling=\"center\" style='border: 1px solid black'><strong>{$ling['REPROGRAMACOES']}</strong></td></tr>
                                <tr>
            <td style='border: 1px solid black'><strong>{$ling['data']}</strong></td>
            <td style='border: 1px solid black'><strong>{$ling['motivo']}</strong></td>
            </tr>";
            $tmp=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$campo['MID']."'");
            while (!$tmp->EOF) {
                $obj_campo=$tmp->fields;
                $txt.="<tr>
                <td style='border: 1px solid black'>".$tmp -> UserDate($obj_campo['DATA'],'d/m/Y')."</td>
                <td style='border: 1px solid black'>".$obj_campo['MOTIVO']."</td>
                </td></tr>";
                $tmp->MoveNext();
            }
            $txt.= "</table><br />";
        }
        $plano_mid=VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$campo['MID_PROGRAMACAO'],$tdb[PROGRAMACAO]['dba']);

        ################ - PREVENTIVA - ################
        if (($campo['TIPO'] == 1) and ($tipo_prev)) {
            $txt.= "<table id=\"dados_processados\"
             style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">";
            $plano_texto=VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano_mid,0);
            if ($plano_texto == "") $plano_texto= $ling['rel_desc_plano_removido'];
            if ($_GET['opcao_atv'] == 1) {
                $txt.= "
                <thead><tr>
                <th $tdstyle colspan=\"6\"><span style='font-size:7.5pt;font-family:Arial'><strong>{$ling['rel_desc_atividades']}</strong></span></th>
                </tr>
                <tr>
                <th $tdstyle><strong>{$ling['N']}</strong></span></th>
                <th $tdstyle><strong>{$ling['DESCRICAO_M']}</strong></span></th>
                <th $tdstyle><strong>{$ling['I_M']}</strong></span></th>
                <th $tdstyle><strong>{$ling['PARTE_POSICAO']}</strong></span></th>
                <th $tdstyle><strong>{$ling['ESP']}</strong></span></th>
                <th $tdstyle><strong>{$ling['TEMPO_PREVISTO_M']}</strong></span></th>
                </tr></thead>";
            }
            $obj=$dba[$tdb[ORDEM_PLANEJADO_PREV]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_PREV." WHERE MID_ORDEM = '".$campo['MID']."' ORDER BY TAREFA ASC");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                $tt=VoltaValor(ATIVIDADES,"TEMPO_PREVISTO","MID",$obj_campo['MID_ATV'],0);
                $espm=VoltaValor(ATIVIDADES,"ESPECIALIDADE","MID",$obj_campo['MID_ATV'],0);
                $esp=VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$espm,0);
                $te=explode(":",$tt);
                $tmp_mat[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat[$obj_campo['MID_MATERIAL']];
                $tmp_mat_total[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat_total[$obj_campo['MID_MATERIAL']];
                $tmp_mk=mktime($te[0],$te[1],$te[2],0,0,0);
                $tmp_esp[$espm][]=$tt;
                $tmp_esp_total[$espm][]=$tt;
                if ($_GET['opcao_atv'] == 1) {
                    $txt .= "<tr>";
                    $txt .= "<td $tdstyle>&nbsp;".VoltaValor(ATIVIDADES,"NUMERO","MID",$obj_campo['MID_ATV'],0)."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;".$obj_campo['TAREFA']."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;".$obj_campo['INSTRUCAO_DE_TRABALHO']."</span></td>";
                    if ($obj_campo['MID_CONJUNTO'] != 0) $txt .= "<td $tdstyle>&nbsp;".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0)."</span></td>";
                    else $txt .= "<td $tdstyle>&nbsp;".$obj_campo['PARTE']."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;$esp</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;$tt</span></td>";
                    $txt .= "</tr>";
                    if ($obj_campo['MID_MATERIAL'] != 0) {
                        $doc .= "<tr>\n";
                        $doc .= "<td $tdstyle colspan=5>&nbsp;".VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0)."</td>";
                        $doc .= "<td $tdstyle>&nbsp;".$obj_campo['QUANTIDADE']."</td>";
                        $doc .= "</tr>\n";
                    }
                }
                $obj->MoveNext();
            }
            $txt.= "</table>";
            $txt.="<div style=\"display: block; border: 1px solid black; padding: 4px;\" align=\"left\"><strong>{$ling['rel_desc_aloc_mo']}</strong><ul>";
            $obj=$dba[$tdb[ESPECIALIDADES]['dba']] -> Execute("SELECT * FROM ".ESPECIALIDADES."");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                if ($tmp_esp[$obj_campo['MID']] != "") {
                    $txt.="<li> ".$obj_campo['DESCRICAO'].": ".SomaHora($tmp_esp[$obj_campo['MID']])."</li>";
                }
                $obj->MoveNext();
            }
            $txt.="</ul><strong>{$ling['rel_desc_total_mat']}</strong><ul>";
            $obj=$dba[$tdb[MATERIAIS]['dba']] -> Execute("SELECT * FROM ".MATERIAIS."");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                if ($tmp_mat[$obj_campo['MID']] != "") {
                    $txt.="<li>".$obj_campo['COD']." - ".$obj_campo['DESCRICAO'].": ".$tmp_mat[$obj_campo['MID']]."</li>";
                }
                $obj->MoveNext();
            }
            $txt.="</ul></div><br />";

        }
        ################ - ROTA - ################
        if (($campo['TIPO'] == 2) and ($tipo_rota)) {
            $txt.= "<table id=\"dados_processados\"
             style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">";
            $plano_texto=VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano_mid,0);
            if ($plano_texto == "") $plano_texto="PLANO REMOVIDO";
            if ($_GET['opcao_atv'] == 1) {
                $txt.= "
                <tr>
                <th $tdstyle colspan=\"8\"><strong>{$ling['rel_desc_atividades']}</strong></span></th>
                </tr>";
            }
            $sql2 = "SELECT a.*,b.MID_PLANO,c.POSICAO FROM ".ORDEM_PLANEJADO_LUB." as a, ".LINK_ROTAS." as b, ".ROTEIRO_ROTAS." as c WHERE a.MID_MAQUINA = c.MID_MAQUINA and b.MID = a.MID_ATV and b.MID_PLANO = c.MID_PLANO and MID_ORDEM = '".$campo['MID']."' ORDER BY b.MID_PLANO,c.POSICAO ASC";
            //die($sql2);
            $ultimo_obj ='';
            $obj=$dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']] -> Execute($sql2);
            while (!$obj->EOF) {
                //die($campo['MID']);
                $obj_campo = $obj -> fields;
                $tt=VoltaValor(LINK_ROTAS,"TEMPO_PREVISTO","MID",$obj_campo['MID_ATV'],0);
                $espm=VoltaValor(LINK_ROTAS,"ESPECIALIDADE","MID",$obj_campo['MID_ATV'],0);
                $esp=VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$espm,0);
                $te=explode(":",$tt);
                $tmp_mat[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat[$obj_campo['MID_MATERIAL']];
                $tmp_mat_total[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat_total[$obj_campo['MID_MATERIAL']];
                //$tmp_mk=mktime($te[0],$te[1],$te[2],0,0,0);
                $tmp_esp[$espm][]=$tt;
                $tmp_esp_total[$espm][]=$tt;
                if ($_GET['opcao_atv'] == 1) {
                    $tiporota = VoltaValor(LINK_ROTAS,'TIPO','MID',$obj_campo['MID_ATV'],0);
                    if ($tiporota == 1) {
                        $tipoimg = 'lubrificacao.png';
                    }
                    if ($tiporota == 2) {
                        $tipoimg = 'inspecao.png';
                    }

                    // verifica se come�ou outra m�quina e caso positivo mostra novo header
                    if ($ultimo_obj != $obj_campo['MID_MAQUINA']) {
                        $objeto = htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],0));
                        $txt .= "<tr><td colspan=7 $tdstyle>"
                        .$tdb[MAQUINAS]['DESC'].": $objeto</td></tr>
                        <tr>
                        <td $tdstyle><strong>".$tdb[ORDEM_PLANEJADO]['TIPO']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['MID_PONTO'].'/'.$tdb[LINK_ROTAS]['MID_CONJUNTO']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['TAREFA']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['ESPECIALIDADE']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['TEMPO_PREVISTO']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['MID_MATERIAL']."</strong></span></td>
                        <td $tdstyle><strong>".$tdb[LINK_ROTAS]['QUANTIDADE']."</strong></span></td>
                        </tr>";
                    }

                    $txt .= "<tr>";
                    $txt .= "<td $tdstyle><img src=\"../imagens/icones/22x22/$tipoimg\" border=0></span></td>";
                    if ($obj_campo['MID_CONJUNTO']) $txt .= "<td $tdstyle>&nbsp;".htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0))."</span></td>";
                    else $txt .= "<td $tdstyle>&nbsp;".htmlentities(VoltaValor(PONTOS_LUBRIFICACAO,"PONTO","MID",$obj_campo['PONTO'],0))."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;".htmlentities($obj_campo['TAREFA'])."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;$esp</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;$tt</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;".htmlentities(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0))."</span></td>";
                    $txt .= "<td $tdstyle>&nbsp;".$obj_campo['QUANTIDADE']."</span></td>";
                    $txt .= "</tr>";
                }
                $ultimo_obj = $obj_campo['MID_MAQUINA'];
                $obj->MoveNext();
            }
            $txt.= "</table>";
            $txt.="<div style=\"display: block; border: 1px solid black; padding: 4px;\" align=\"left\"><strong>{$ling['rel_desc_aloc_mo']}</strong><ul>";
            $obj=$dba[$tdb[ESPECIALIDADES]['dba']] -> Execute("SELECT * FROM ".ESPECIALIDADES."");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                if ($tmp_esp[$obj_campo['MID']] != "") {
                    $txt.="<li> ".$obj_campo['DESCRICAO'].": ".SomaHora($tmp_esp[$obj_campo['MID']])."</li>";
                }
                $obj->MoveNext();
            }
            $txt.="</ul><strong>{$ling['rel_desc_total_mat']}</strong><ul>";
            $obj=$dba[$tdb[MATERIAIS]['dba']] -> Execute("SELECT * FROM ".MATERIAIS."");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                if ($tmp_mat[$obj_campo['MID']] != "") {
                    $txt.="<li>".$obj_campo['COD']."-".$obj_campo['DESCRICAO'].": ".$tmp_mat[$obj_campo['MID']]."</li>";
                }
                $obj->MoveNext();
            }
            $txt.="</ul></div><br />";

        }
        ################
        unset($tmp_esp);
        unset($tmp_mat);
        $resultado->MoveNext();
    }
    $txt.= "<br /><h2> {$ling['rel_desc_estatistica_geral']} </h2><br />
    <div style=\"border: 1px solid black; padding: 5px\" align=\"left\">";
    $txt.="<strong>{$ling['rel_desc_aloc_mo']}</strong><ul>";
    $obj=$dba[$tdb[ESPECIALIDADES]['dba']] -> Execute("SELECT * FROM ".ESPECIALIDADES."");
    while (!$obj->EOF) {
        $obj_campo = $obj -> fields;
        if ($tmp_esp_total[$obj_campo['MID']] != "") {
            $txt.="<li> ".$obj_campo['DESCRICAO'].":  ".SomaHora($tmp_esp_total[$obj_campo['MID']])."</li>";
        }
        $obj->MoveNext();
    }
    $txt.="</ul><br><strong>{$ling['rel_desc_total_mat']}</strong><ul>";
    $obj=$dba[$tdb[MATERIAIS]['dba']] -> Execute("SELECT * FROM ".MATERIAIS."");
    while (!$obj->EOF) {
        $obj_campo = $obj -> fields;
        if ($tmp_mat_total[$obj_campo['MID']] != "") {
            $txt.="<li>".$obj_campo['COD']."-".$obj_campo['DESCRICAO'].": ".$tmp_mat_total[$obj_campo['MID']]."</li>";
        }
        $obj->MoveNext();
    }
    $txt.="</ul></div>";
    $txt.= "</ul><br /> <br /><br /> <br /></div><br /> <br /></div>";
    $iii="$iii";

    if (($_GET['dataf']) or ($_GET['datai'])) $mostrafiltro = "<li>{$ling['rel_desc_periodo']}: ".$_GET['datai']." a ".$_GET['dataf']."</li>$mostrafiltro";
    if (!$mostrafiltro) $mostrafiltro = $ling['nenhum'];
    else $mostrafiltro = "<ul style=\"list-style-type:none;\">$mostrafiltro</ul>";

    $tempoexec_final = utime();
    $tempoexec = round($tempoexec_final - $tempoexec_inicial,4);

    if ($relatorio != "") relatorio_padrao($ling['rel_programacao'],$mostrafiltro,$iii,$txt,1,$tempoexec);
    else exportar_word($ling['rel_programacao'],$mostrafiltro,$iii,$txt,$_GET['papel_orientacao']);
}


else {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['rel_programacao']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"relatorio_geralplan.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>".$ling['localizacoes']."</legend>	
    <div id=\"filtro_relatorio\">";

    // FILTROS NOS RELAT�RIOS
    FiltrosRelatorio(1, 1, 1, 1);

    echo "</div>";

    echo "<label for=\"filtro_cc\">" . $tdb[CENTRO_DE_CUSTO]['DESC'] . "</label>";
    FormSelectD("COD", "DESCRICAO", CENTRO_DE_CUSTO, $_GET['filtro_cc'], "filtro_cc", "filtro_cc", "MID", 0, "", "","","","","TODOS");

    echo "</fieldset>";


    echo "<fieldset><legend>".$ling['origem_prog']."</legend>";
    echo "<input class=\"campo_check\" checked=\"checked\" type=\"checkbox\" name=\"filtro_prev\" id=\"t1\" value=\"1\" />
    <label for=\"t1\">".$ling['plan01']."</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"checkbox\" name=\"filtro_rota\" id=\"t2\" value=\"1\" />
    <label for=\"t2\">".$ling['def_rota']."</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"checkbox\" name=\"filtro_sol\" id=\"t3\" value=\"1\" />
    <label for=\"t3\">".$ling['plan03']."</label>
    <div id=\"serv\"></div>
    </fieldset>";

    echo "<fieldset><legend>{$ling['ord_visualizar']}</legend>
    <input checked=\"checked\" class=\"campo_check\" type=\"checkbox\" name=\"opcao_atv\" id=\"opcao_atv\" value=\"1\" />
    <label for=\"opcao_atv\">{$ling['atividades']}</label>
    <input checked=\"checked\" class=\"campo_check\" type=\"checkbox\" name=\"opcao_mat\" id=\"opcao_pen\" value=\"1\" />
    <label for=\"opcao_pen\">{$ling['rel_desc_prev_mat']}</label>
    <input checked=\"checked\" class=\"campo_check\" type=\"checkbox\" name=\"opcao_mao\" id=\"opcao_so_pen\" value=\"1\" />
    <label for=\"opcao_so_pen\">{$ling['rel_desc_prev_mo']}</label>
    <input checked=\"checked\" class=\"campo_check\" type=\"checkbox\" name=\"opcao_osa\" id=\"opcao_so_osa\" value=\"1\" />
    <label for=\"opcao_so_osa\">{$ling['ordens_abertas']}</label>
    <input checked=\"checked\" class=\"campo_check\" type=\"checkbox\" name=\"opcao_osf\" id=\"opcao_so_osf\" value=\"1\" />
    <label for=\"opcao_so_osf\">{$ling['ordens_fechadas']}</label>

    </fieldset>";

    echo "<fieldset><legend>".$ling['datas']."</legend>";
    FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
    echo "<br clear=\"all\" />";
    FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");

    echo "
    </fieldset>
    </fieldset>";
    echo "<fieldset>
<legend>".$ling['papel_orientacao']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
<label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
<label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
</fieldset>
<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";

}
?>
