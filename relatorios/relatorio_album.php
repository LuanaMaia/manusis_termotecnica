<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Gr�fico de Defeitos
*/
// Configura��es
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);

elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
$tb=$_GET['tb'];
$filtro_tipo=(int)$_GET['filtro_tipo'];
$tipo=(int)$_GET['tipo'];
$id=(int)$_GET['id'];
$oq=(int)$_GET['oq'];
$obj=(int)$_GET['obj'];


// MONTO O HTML COM FORMULARIO E FILTROS PARA O RELATORIO, CASO ELE NAO TENHA SIDO ENVIADO
if ($_GET['env'] == "") {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>".$ling['relatorio_album_img']."
</div>
<div id=\"corpo_relatorio\">
<form action=\"relatorio_album.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>".$ling['filtros']."</legend>";

	if ($tipo == 2) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"2\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"2\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t2\">".$tdb[SETORES]['DESC']."</label><br clear=\"all\" />";

	if ($tipo == 5) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"5\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"5\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t5\">".$tdb[MAQUINAS_FAMILIA]['DESC']."</label><br clear=\"all\" />";

	if ($tipo == 3) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"3\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"3\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t3\">".$tdb[MAQUINAS]['DESC']."</label><br clear=\"all\" />";

	if ($tipo == 4) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"4\"  checked=\"checked\" onclick=\"document.forms[0].submit();\"/>";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"4\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t4\">{$ling['rel_todas_acima']}</label><br clear=\"all\" />";

    // FILTROS POR EMPRESA
	if ($tipo == 2) FormSelectD("COD", "DESCRICAO", SETORES, $_GET['obj'], "obj", "obj", "MID", "");
	if ($tipo == 3) FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "");
	if ($tipo == 5) FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['obj'], "obj", "obj", "MID", "");

	echo "</fieldset>";
	echo "


<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"env\" value=\"{$ling['rel_gerar_grafco']}\" />
</form><br />
</div>
</div>
</body>
</html>";
}
// O USUARIO ENVIOU O FORMULARIO, INICIO O PROCESSO
else {
	// BASEADO NO TIPO ENVIADO, MONTO O FILTRO PARA A CONSULTA SQL
	if ($tipo == 2) {
		$fil="AND MID_SETOR = '$obj'";
		$filtro=$tdb[SETORES]['DESC']." -> ".VoltaValor(SETORES,"DESCRICAO","MID",$obj,0);
	}
	if ($tipo == 3) {
		$fil="AND MID = '$obj'";
		$filtro=$tdb[MAQUINAS]['DESC']." -> ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,0);
	}
	if ($tipo == 5) {
		$fil="AND FAMILIA = '$obj'";
		$filtro=$tdb[MAQUINAS_FAMILIA]['DESC']." -> ".VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$obj,0);
	}
	elseif ($tipo == 4) {
        $filtro=$ling['nenhum'];
    }

    // FILTRO POR EMPRESA
    $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);
    $fil .= ($fil_emp != "")? "AND $fil_emp" : "";

	// EXECUTO A CONSULTA NA TABELA E VERIFICO SE FOI BEM SUCEDIDA, CASO N�O EU MOSTRO E ERRO E DOU UM EXIT
	$sql="SELECT * FROM ".MAQUINAS." WHERE STATUS = 1 $fil ORDER BY COD ASC";
	//die($sql);
	if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
		$err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	}

	/*
	OK, CONSULTA FUNCIONOU, INICIO O LOOP COM AS INFORMA��ES RECOLHIDAS

	VOC� PODER� DEFINIR AQUI UMA TABELA OU UM DIV
	PARA INICIAR SEU HTML (SE DESEJAR OU CASO PRECISAR)
	*/

	$qtd_encontrada=0; // CONTADOR DE REGISTROS...
	while (!$resultado->EOF) { // ENQUANTO EXITIR INFORMA��ES NO OBJETO QUE RECEBEU A CONSULTA, O LOOP N�O PARA
		$campo=$resultado->fields; // PEGO OS CAMPOS DA CONSULTA
		
		// SO PARA FACILITAR A IMPRESS�O NOS ECHO OU PRINT
		$cod=$campo['COD'];
		$descricao=$campo['DESCRICAO'];
		$modelo=$campo['MODELO'];
		$mid=$campo['MID'];
		$localizacao2=VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],0);
		
		// PROCURO AS IMAGENS DESSA MAQUINA
		$dir="../".$manusis['dir']['maquinas']."/$mid"; // O MID DA MAQUINA � O DIRETORIO ONDE ELA AS IMAGENS S�O SALVAS LEMBRA?????
		$dir_i=$manusis['dir']['maquinas']."/$mid";
		//A LINHA ABAIXO O OTAVIO ANULOU PQ EXISTIAM DIRETORIOS VAZIOS QUE DAVAM ERROS
		// ABAIXO UM EXEMPLO PARA LISTAR IMAGENS... DAI PRA FRENTE VOC� MONTA COMO DESEJAR
		
		//$doc.= "<table>\n
		if (file_exists($dir)) {
			$od = opendir($dir);
			$od2 = opendir($dir);
			$cont_verarq = 0;
			while (false !== ($arq_v = readdir($od2))) {
				$cont_verarq++;
			}
			if ($cont_verarq > 2){
				$doc.= "<center><table width =\"100%\">\n
		<tr class=\"cor2\" bgcolor=\"#000000\">
<td align=\"center\" colspan=\"2\"><strong>$descricao</strong></td>
</tr>
<tr class=\"cor2\" align=\"center\"><td>{$ling['arq_nome']}</td><td>{$ling['tamanho2']}</td></tr>";
				$i=0;
				while (false !== ($arq = readdir($od))) { //mostra
					$dd=explode(".",$arq);
					if (($dd[1] == "jpg") or ($dd[1] == "png")) {
					// O ARQUIVO I.PHP REDIMENCIONA A IMAGEM
						$doc.= "<tr>
<td align=\"left\"><img src=\"../i.php?i=$dir_i/$arq&s=120\" border=1 hspace=5 align=\"middle\" \><a href=\"$dir/$arq\" target=\"_blank\">$arq</a></td>
<td align=\"left\" width=\"20%\">".de_bytes(filesize("$dir/$arq"))."</td>
</td></tr>\n";
						$i++;
						$qtd_encontrada++; // SOMO O CONTADOR
					}
				}
			$cont_verarq = 0;
			$doc .= "</table></center><br>";
			}
		}
 		 /*z
		 LEMBRE-SE SEMPRE DE N�O USAR ECHO (COMO NOS EXEMPLOS ACIMA)
		 POIS O MANUSIS TEM UMA FUN��O QUE GERA O LAYOUT DO RELATORIO AUTOMATICO...
		 ENT�O GRAVE TODAS SEUS TEXTOS EM UMA VARIAVEL, POR EXEMPLO $doc E DEPOIS 
		 USE A FUN��O LOGO ABAIXO PARA GERAR O RELATORIO PADRONIZADO. 
		 */
		
		$resultado -> MoveNext(); // AVAN�O PARA O PROXIMO REGISTRO
		//$qtd_encontrada++; // SOMO O CONTADOR
	}

	// GERO O RELATORIO.
	relatorio_padrao($ling['relatorio_album_img'],$filtro,$qtd_encontrada,$doc,1);
}
?>