<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: Relatorio
 */
$phpajax = "relatorio_contador.php";
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];

$ajax = $_GET['ajax'];

if ($ajax == 'area') {
	$area=(int)$_GET['area'];
	if ($area) $sqlcond = "WHERE MID_AREA = '$area'";
	else $sqlcond = '';

	echo "<label class=\"campo_label \" for=\"filtro_setor\">".$tdb[SETORES]['DESC'].":</label>";
	FormSelectD("COD", 'DESCRICAO', SETORES, $_GET['filtro_setor'], "filtro_setor", "filtro_setor", "MID", 0, "", "atualiza_area2('maq','$phpajax?ajax=setor&setor=' + this.options[this.selectedIndex].value)", $sqlcond);

	exit ();
}
elseif ($ajax == 'setor') {
	$setor=(int)$_GET['setor'];
	if ($setor) $sqlcond = "WHERE MID_SETOR = '$setor'";
	else $sqlcond = '';

	echo "<label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";
	FormSelectD("COD", 'DESCRICAO', MAQUINAS, $_GET['filtro_maq'], "filtro_maq", "filtro_maq", "MID", 0, "", "atualiza_area2('contador','$phpajax?ajax=maq&maq=' + this.options[this.selectedIndex].value)", $sqlcond);

	exit ();
}
elseif ($ajax == 'maq') {
	$maq=(int)$_GET['maq'];
	if ($maq) $sqlcond = "WHERE MID_MAQUINA = '$maq'";
	else $sqlcond = '';

	echo "<label class=\"campo_label \" for=\"filtro_contador\">".$tdb[MAQUINAS_CONTADOR]['DESC'].":</label>";
	FormSelectD('DESCRICAO', "", MAQUINAS_CONTADOR, $_GET['filtro_contador'], "filtro_contador", "filtro_contador", "MID", 0, "", "", $sqlcond);

	exit ();
}

elseif (($relatorio != "") or ($exword != "")){
	if ($_GET['datai']) {
		$datai=explode("/",$_GET['datai']);
		$datai=$datai[2]."-".$datai[1]."-".$datai[0];
	}
	if ($_GET['dataf']) {
		$dataf=explode("/",$_GET['dataf']);
		$dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
	}
	$areaf=(int)$_GET['filtro_area'];
	$setorf=(int)$_GET['filtro_setor'];
	$maqf=(int)$_GET['filtro_maq'];
	$contf=(int)$_GET['filtro_contador'];
	$maq_fam=(int)$_GET['filtro_maq_fam'];
	if ($areaf != 0) $fil.=" AND b.MID_AREA = '$areaf'";
	if ($setorf != 0) $fil.=" AND a.MID_SETOR = '$setorf'";
	if ($maqf != 0) $fil.=" AND a.MID = '$maqf'";
	if ($maq_fam != 0) $fil.=" AND a.FAMILIA = '$maq_fam'";
	if ($mfil) $fil.=" $mfil";
	if ($contf) $fil = "AND a.MID = '".(int)VoltaValor(MAQUINAS_CONTADOR,'MID_MAQUINA','MID',$contf,$tdb[MAQUINAS_CONTADOR]['dba'])."'";

	// Filtro por Empresa
	$fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);
	$fil .= ($fil_emp != "")? "AND " . $fil_emp : "";

	$sql="SELECT a.*,b.MID_AREA FROM ".MAQUINAS." as a, ".SETORES." as b WHERE a.MID_SETOR = b.MID $fil ORDER BY b.MID_AREA,a.MID_SETOR,a.FAMILIA,a.COD ASC";
	if (!$resultado_maq = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg()."<br><br>$sql");
	$iii=0;
	$tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";
	while (!$resultado_maq->EOF) {
		$campo_maq=$resultado_maq -> fields;
		$mid_maq = $campo_maq['MID'];
		$maq_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$mid_maq,0);
		$setor_desc = VoltaValor(SETORES,'COD','MID',$campo_maq['MID_SETOR'],0)." - ".VoltaValor(SETORES,'DESCRICAO','MID',$campo_maq['MID_SETOR'],0);
		$area_desc = VoltaValor(AREAS,'COD','MID',$campo_maq['MID_AREA'],0)." - ".VoltaValor(AREAS,'DESCRICAO','MID',$campo_maq['MID_AREA'],0);
		$fam_desc = VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$campo_maq['FAMILIA'],0);
		if ($contf) $cont_desc = VoltaValor(MAQUINAS_CONTADOR,'DESCRICAO','MID',$contf,0);

		$j = 0;

		$header = "<table id=\"dados_processados\" cellspacing=\"0\" cellpadding=\"0\"
	                style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">
                         <div align=\"left\" ><strong><font size=\"3\">".$campo_maq['COD'].'-'.$campo_maq['DESCRICAO']."</font></strong></div>";


		// inicio da tabela desta maquina/equipamento
		$txtj="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
				<thead>
				<tr>
				<th align=\"left\"><strong>{$tdb[AREAS]['DESC']}</strong>: $area_desc</th>
				<th align=\"left\"><strong>{$tdb[SETORES]['DESC']}</strong>: $setor_desc</th>
				</tr><tr>
				<th align=\"left\"><strong>{$tdb[MAQUINAS_FAMILIA]['DESC']}</strong>: $fam_desc</th>
				<th align=\"left\"><strong>{$tdb[MAQUINAS]['DESC']}</strong>: $maq_desc</th>
				</tr>";
		$txtj.="</thead>
				<tr>
				<td align=\"left\" colspan=\"2\">";


		if ($contf) $sql="SELECT * FROM ".MAQUINAS_CONTADOR." WHERE MID = '$contf' ORDER BY DESCRICAO ASC";
		else $sql="SELECT * FROM ".MAQUINAS_CONTADOR." WHERE MID_MAQUINA = '$mid_maq' ORDER BY DESCRICAO ASC";
		if (!$res_ponto = $dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> ErrorMsg()."<br><br>$sql");
		while (!$res_ponto->EOF) {
			$j++;
			$campo_ponto=$res_ponto -> fields;
			$ponto = $campo_ponto['MID'];
			$ponto_desc = VoltaValor(MAQUINAS_CONTADOR,'DESCRICAO','MID',$ponto,0);
			$ptipo = (int)VoltaValor(MAQUINAS_CONTADOR,'TIPO','MID',$ponto,0);
			if ($ptipo) $ptipo_desc = ' - '.VoltaValor(MAQUINAS_CONTADOR_TIPO,'DESCRICAO','MID',$ptipo,0);
			$zera = VoltaValor(MAQUINAS_CONTADOR,'ZERA','MID',$ponto,0);
				
			$txtj.="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
					<thead><tr>
					<th align=\"center\" colspan=2 $tdstyle><strong>$ponto_desc{$ptipo_desc}</strong></th>
					</tr><tr>
					<th $tdstyle>{$tdb[LANCA_CONTADOR]['DATA']}</th><th>{$tdb[LANCA_CONTADOR]['VALOR']}</th></tr>
					</thead>"; //  - {$tdb[MAQUINAS_CONTADOR]['ZERA']}: $zera
				
			$header .= "
			<thead>
			        <tr>
					  <th align=\"center\" colspan=\"4\" $tdstyle><strong>$ponto_desc{$ptipo_desc}</strong></th>
					</tr>
					<tr>
					  <th $tdstyle>{$tdb[LANCA_CONTADOR]['DATA']}</th>
					  <th $tdstyle>{$tdb[LANCA_CONTADOR]['MID_USUARIO']}</th>
					  <th $tdstyle>{$tdb[LANCA_CONTADOR]['VALOR']}</th>
					  <th $tdstyle>{$tdb[ORDEM]['NUMERO']} {$tdb[ORDEM]['DESC']}</th>
					</tr>
					</thead>";
				
				
			$fil2 = '';
			if ($datai) $fil2 .= " AND DATA >= '$datai'";
			if ($dataf) $fil2 .= " AND DATA <= '$dataf'";
			$sql="SELECT * FROM ".LANCA_CONTADOR." WHERE MID_CONTADOR = '$ponto' $fil2 ORDER BY DATA ASC";
			if (!$res_lanc = $dba[$tdb[LANCA_CONTADOR]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[LANCA_CONTADOR]['dba']] -> ErrorMsg()."<br><br>$sql");
			$k=$kv=0;
			while (!$res_lanc->EOF) {
				$campo_lanc=$res_lanc -> fields;
				if($campo_lanc['MID_USUARIO'] !=0)
				{
					$usuario = VoltaValor(USUARIOS, 'NOME', 'MID',$campo_lanc['MID_USUARIO'] );
				}
				else
				{
					$usuario = $manusis['admin']['nome'];
				}

				$status = $campo_lanc['STATUS'] == 0?'*':'';

				$sql = "SELECT *  FROM " . CONTROLE_CONTADOR . " WHERE MID_LANCAMENTO = ".$campo_lanc['MID'];

				$numeroOrdem = array();
				$result = $dba[$tdb[LANCA_CONTADOR]['dba']] -> Execute($sql);
				if(!$result->EOF)
				{
					while(!$result->EOF)
					{
						$campoControleContador = $result->fields;
						if($campoControleContador['MID_ORDEM'] !=0 ){

							$numeroOrdem[] = VoltaValor(ORDEM, 'NUMERO', 'MID', $campoControleContador['MID_ORDEM']);
						}
						$result->MoveNext();
					}
					
					if(count($numeroOrdem) > 0)					
					   $numeroOrdem = implode('-', $numeroOrdem);
					else
					   $numeroOrdem = '';   
						
				}
				else{
					$numeroOrdem = '';
				}


				$iii++;
				$k++;
				$valor = $campo_lanc['VALOR'];
				$kv += $valor;
				$edata = NossaData($campo_lanc['DATA']);
				$txtj .= "<tr><td $tdstyle>$edata</td><td $tdstyle>$valor</td></tr>";

				$header .="<tr>
				       <td $tdstyle>$edata</td>
				       <td $tdstyle>$usuario</td>
				       <td $tdstyle>$valor$status</td>
				       <td $tdstyle>$numeroOrdem</td>
				       </tr>";

				$res_lanc->MoveNext();
			}
			if (!$k){
				$txtj   .= "<tr><td $tdstyle colspan=\"2\" align=\"center\"><center>{$ling['rel_desc_apontamento_disp']}</center></td></tr>";
				$header .="<tr><td $tdstyle colspan=\"4\" align=\"center\"><center>{$ling['rel_desc_apontamento_disp']}</center></td></tr>";
			}
			else {
				$txtj .= "<tr><td $tdstyle colspan=\"2\" align=\"center\"><center>{$ling['rel_desc_valor_peri']}: $kv</center></td></tr>";
				$header .= "<tr><td $tdstyle colspan=\"4\" align=\"center\"><center>{$ling['rel_desc_valor_peri']}: $kv</center></td></tr>";
			}

				
			// fim da tabela ponto
			$txtj .= "</table><br clear=\"all\" />";
			$res_ponto->MoveNext();
		}

		// fim da tabela desta maquina/equipamento
		$txtj .= "</td></tr></table><br clear=\"all\" />";
		if ($j){
			$txt .= $txtj;
			$header .="</table><br clear=\"all\" />";
			$header2 .= $header;
		}
		$resultado_maq->MoveNext();
	}


	$header2 .= "<div style=\"text-align: left;\" id=\"lt_rodape\">
	<strong>{$ling['rel_desc_legendas']}:</strong>
	<br />
	<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\">
<tbody><tr>
<td valign=\"top\" style=\"border-left: 1px solid rgb(102, 102, 102);\"><span style=\"border: 1px solid rgb(204, 204, 204); float: left; height: 20px; width: 20px; margin: 2px;\">*</span>{$ling['setando_contador_inicial']}&nbsp;<br clear=\"all\"></td></tr></tbody></table></div>";
	

	$filtro='';
	if ($areaf) AddStr($filtro,', ',"{$tdb[AREAS]['DESC']}: ".VoltaValor(AREAS,'COD','MID',$areaf,0).' - '.VoltaValor(AREAS,'DESCRICAO','MID',$areaf,0));
	if ($setorf) AddStr($filtro,', ',"{$tdb[SETORES]['DESC']}: ".VoltaValor(SETORES,'COD','MID',$setorf,0).' - '.VoltaValor(SETORES,'DESCRICAO','MID',$setorf,0));
	if ($maqf) AddStr($filtro,', ',"{$tdb[MAQUINAS]['DESC']}: ".VoltaValor(MAQUINAS,'DESCRICAO','MID',$maqf,0));
	if ($maq_fam) AddStr($filtro,', ',"{$tdb[MAQUINAS_FAMILIA]['DESC']}: ".VoltaValor(MAQUINAS_FAMILIA,'COD','MID',$maq_fam,0).' - '.VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$maq_fam,0));
	if ($contf) AddStr($filtro,', ',"{$tdb[MAQUINAS_CONTADOR]['DESC']}: ".VoltaValor(MAQUINAS_CONTADOR,'DESCRICAO','MID',$contf,0));
	if ($datai) AddStr($filtro,', ',"{$ling['rel_desc_de']} ".$_GET['datai']);
	if ($dataf) AddStr($filtro,', ',"{$ling['rel_desc_ate']} ".$_GET['dataf']);

	if (!$filtro) $filtro = $ling['nenhum'];

	if ($relatorio != "") relatorio_padrao($ling['rel_lanca_contador'],$filtro,$iii,$header2,1);
	else exportar_word($ling['rel_lanca_contador'],$filtro,$iii,$header2,$_GET['papel_orientacao']);
}


else {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['rel_lanca_contador']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>{$ling['rel_desc_localizacoes']}</legend>	
	<label class=\"campo_label \" for=\"filtro_area\">".$tdb[AREAS]['DESC'].":</label>";

	FormSelectD("COD",'DESCRICAO',AREAS, $_GET['filtro_area'], "filtro_area", "filtro_area", "MID", 0, "", "atualiza_area2('setor','$phpajax?ajax=area&area=' + this.options[this.selectedIndex].value)");

	echo "<br clear=\"all\" />
	<div id=\"setor\">
    <label class=\"campo_label \" for=\"filtro_setor\">".$tdb[SETORES]['DESC'].":</label>";

	FormSelectD("COD", 'DESCRICAO', SETORES, $_GET['filtro_setor'], "filtro_setor", "filtro_setor", "MID", 0, "", "atualiza_area2('maq','$phpajax?ajax=setor&setor=' + this.options[this.selectedIndex].value)");

	echo "</div>
    <br clear=\"all\" />
    <label class=\"campo_label \" for=\"filtro_maq_fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";

	FormSelectD("COD", 'DESCRICAO', MAQUINAS_FAMILIA, $_GET['filtro_maq_fam'], "filtro_maq_fam", "filtro_maq_fam", "MID", 0);

	echo "<br clear=\"all\" />
    <div id=\"maq\">
    <label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";

	FormSelectD("COD", 'DESCRICAO', MAQUINAS, $_GET['filtro_maq'], "filtro_maq", "filtro_maq", "MID", 0, "", "atualiza_area2('contador','$phpajax?ajax=maq&maq=' + this.options[this.selectedIndex].value)");

	echo "</div>
    <br clear=\"all\">
    <div id=\"contador\">
	<label class=\"campo_label \" for=\"filtro_contador\">".$tdb[MAQUINAS_CONTADOR]['DESC'].":</label>";

	FormSelectD('DESCRICAO', "", MAQUINAS_CONTADOR, $_GET['filtro_contador'], "filtro_contador", "filtro_contador", "MID", 0);

	echo "</div>";
	echo "</fieldset>";
	echo "<fieldset><legend>{$ling['rel_desc_periodo_op']}</legend>";
	FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
	echo "<br clear=\"all\" />";
	FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
	echo "
	</fieldset>";

	echo "<fieldset>
<legend>".$ling['papel_orientacao']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
<label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
<label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
</fieldset>
<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";

}

?>