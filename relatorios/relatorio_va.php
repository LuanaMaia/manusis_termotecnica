<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: Relat�rio de Estrutura Geral
 */
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);

$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
#############################

$filtro_tipo=(int)$_GET['filtro_tipo'];
$tipo=(int)$_GET['tipo'];
$id=(int)$_GET['id'];
$oq=(int)$_GET['oq'];
$obj=(int)$_GET['obj'];
$enviar = $_GET['enviar'];
$where = '1';

if (!$enviar) {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
    <body><div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1>{$ling['relatorio_estrutural']}</h1>
    </div>
    <div id=\"corpo_relatorio\">
    <form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    <fieldset>
    <legend>".$ling['filtros']."</legend>";

    if ($tipo == 1) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"1\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
    else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"1\" onclick=\"document.forms[0].submit();\" />";
    echo "<label for=\"t5\">".$tdb[EMPRESAS]['DESC']."</label><br clear=\"all\" />";

    if ($tipo == 2) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t1\" value=\"2\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
    else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t1\" value=\"2\" onclick=\"document.forms[0].submit();\" />";
    echo "<label for=\"t1\">".$tdb[AREAS]['DESC']."</label><br clear=\"all\" />";

    if ($tipo == 3) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"3\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
    else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"3\" onclick=\"document.forms[0].submit();\" />";
    echo "<label for=\"t2\">".$tdb[SETORES]['DESC']."</label><br clear=\"all\" />";

    if ($tipo == 4) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"4\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
    else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"4\" onclick=\"document.forms[0].submit();\" />";
    echo "<label for=\"t3\">".$tdb[MAQUINAS]['DESC']."</label><br clear=\"all\" />";

    if (($tipo == 5) or ($tipo == 0)) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"5\"  checked=\"checked\" onclick=\"document.forms[0].submit();\"/>";
    else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"5\" onclick=\"document.forms[0].submit();\" />";
    echo "<label for=\"t4\">{$ling['rel_desc_vizu_tudo']}</label><br clear=\"all\" />";

    if ($tipo == 1) FormSelectD("COD", "NOME",      EMPRESAS, $_GET['obj'], "obj", "obj", "MID", "");
    if ($tipo == 2) FormSelectD("COD", "DESCRICAO", AREAS,    $_GET['obj'], "obj", "obj", "MID", "");
    if ($tipo == 3) FormSelectD("COD", "DESCRICAO", SETORES,  $_GET['obj'], "obj", "obj", "MID", "");
    if ($tipo == 4) FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "");

    echo "</fieldset>
    <br />
    <input class=\"botao\" type=\"submit\" name=\"enviar\" value=\"{$ling['navegador']}\" />
    <input class=\"botao\" type=\"submit\" name=\"enviar\" value=\"{$ling['word']}\" />
    </form><br />
    </div>
    </div>
    </body>
    </html>";
}
else {
    // FILTROS
    $filtro_emp = 0;
    $filtro_area = 0;
    $filtro_setor = 0;
    $filtro_maq = 0;

    // BASEADO NO TIPO ENVIADO, MONTO O FILTRO PARA A CONSULTA SQL
    if ($tipo == 1) {
        $filtro=$tdb[EMPRESAS]['DESC']." -> ".VoltaValor(EMPRESAS, "NOME", "MID", $obj, 0);
        // FILTROS
        $filtro_emp = $obj;
    }
    if ($tipo == 2) {
        $filtro=$tdb[AREAS]['DESC']." -> ".VoltaValor(AREAS,"DESCRICAO","MID",$obj,0);
        // FILTROS
        $filtro_area = $obj;
        $filtro_emp = VoltaValor(AREAS, "MID_EMPRESA", "MID", $obj,0);
    }
    if ($tipo == 3) {
        $filtro=$tdb[SETORES]['DESC']." -> ".VoltaValor(SETORES,"DESCRICAO","MID",$obj,0);
        // FILTROS
        $filtro_setor = $obj;
        $filtro_area = VoltaValor(SETORES, "MID_AREA", "MID", $filtro_setor,0);
        $filtro_emp = VoltaValor(AREAS, "MID_EMPRESA", "MID", $filtro_area,0);
    }
    if ($tipo == 4) {
        $filtro=$tdb[MAQUINAS]['DESC']." -> ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,0);
        //FILTROS
        $filtro_maq = $obj;
        $filtro_setor = VoltaValor(MAQUINAS, "MID_SETOR", "MID", $filtro_maq,0);
        $filtro_area = VoltaValor(SETORES, "MID_AREA", "MID", $filtro_setor,0);
        $filtro_emp = VoltaValor(AREAS, "MID_EMPRESA", "MID", $filtro_area,0);
    }

    if ($tipo == 5){
        $filtro=$ling['nenhum'];
    }

    // EXECUTO A CONSULTA NA TABELA E VERIFICO SE FOI BEM SUCEDIDA, CASO N�O EU MOSTRO E ERRO E DOU UM EXIT
    $tree = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\" width=\"100%\" id=\"dados_processados\" style=\"border:0px;\"><tr><td style=\"border:1px solid black;\">";

    // Contador
    $iim = 0;

    $padding = "20px";

    $tree .= "<ul style=\"padding-left:$padding;\">";

    // Empresas
    $lista_emp = ListaMatriz($filtro_emp);
    foreach ($lista_emp as $emp) {
        // Contando
        $iim++;

        $tree .= "<li style=\"padding-bottom:10px;\"><strong>" . $emp['cod'] . "-" . $emp['nome'] . "</strong>";

        // Areas
        $lista_area = ListaArea($emp['mid'], $filtro_area);
        if($lista_area) {
            $tree .= "<ul style=\"padding-left:$padding;\">";
            foreach ($lista_area as $area) {
                // Contando
                $iim++;

                $tree .= "<li>" . $area['cod'] . "-" . $area['nome']. "";
                 
                // Setores
                $lista_setor = ListaSetor($area['mid'], $filtro_setor);
                if($lista_setor) {
                    $tree .= "<ul style=\"padding-left:$padding;\">";
                    foreach ($lista_setor as $setor) {
                        // Contando
                        $iim++;

                        $tree .= "<li>" . $setor['cod'] . "-" . $setor['nome'];
                         
                        // Maquinas
                        $lista_maq = ListaMaquina($setor['mid'], $filtro_maq);
                        if ($lista_maq) {
                            $tree .= "<ul style=\"padding-left:$padding;\">";
                            foreach ($lista_maq as $maq) {
                                // Contando
                                $iim++;

                                $tree .= "<li>" . $maq['cod'] . "-" . $maq['nome'];

                                // Conjuntos
                                $lista_conj = ListaConjunto($maq['mid']);
                                if($lista_conj) {
                                    $tree .= "<ul style=\"padding-left:$padding;\">";
                                    foreach ($lista_conj as $conj) {
                                        // Contando
                                        $iim++;

                                        $tree .= "<li>" . $conj['tag'] . "-" . $conj['nome'];

                                        // Componentes
                                        $lista_equip = ListaEquipamento($conj['mid']);
                                        if($lista_equip) {
                                            $tree .= "<ul style=\"padding-left:$padding;\">";
                                            foreach ($lista_equip as $equip) {
                                                // Contando
                                                $iim++;

                                                $tree .= "<li>" . $equip['cod'] . "-" . $equip['nome'] . "</li>";
                                            }
                                            $tree .= "</ul>";
                                        }
                                        // Sub-Conjutos
                                        $lista_subconj = ListaSubConjunto($conj['mid']);
                                        if($lista_subconj) {
                                            $tree .= "<ul style=\"padding-left:$padding;\">";
                                            foreach ($lista_subconj as $subconj) {
                                                // Contando
                                                $iim++;

                                                $tree .= "<li>" . $subconj['tag'] . "-" . $subconj['nome'];

                                                $lista_equip2 = ListaEquipamento($subconj['mid']);
                                                if($lista_equip2) {
                                                    $tree .= "<ul style=\"padding-left:$padding;\">";
                                                    foreach ($lista_equip2 as $equip2) {
                                                        // Contando
                                                        $iim++;

                                                        $tree .= "<li>" . $equip2['cod'] . "-" . $equip2['nome'] . "</li>";
                                                    }
                                                    $tree .= "</ul>";
                                                }
                                                $tree .= "</li>";
                                            }
                                            $tree .= "</ul>";
                                        }
                                        $tree .= "</li>";
                                    }
                                    $tree .= "</ul>";
                                }
                                $tree .= "</li>";
                            }
                            $tree .= "</ul>";
                        }
                        $tree .= "</li>";
                    }
                    $tree .= "</ul>";
                }
                $tree .= "</li>";
            }
            $tree .= "</ul>";
        }
        $tree .= "</li>";
    }

    $tree .= "</ul>";
    $tree .= "</td></tr></table>";

    //echo $tree;
    //exit;
    if ($_GET['enviar'] == "Word") exportar_word($ling['relatorio_estrutural'],$filtro,$iim,$tree,1);
    else relatorio_padrao($ling['relatorio_estrutural'],$filtro,$iim,$tree,1);
}


?>