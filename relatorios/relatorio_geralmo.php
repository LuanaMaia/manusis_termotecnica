<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$tb=(int)$_GET['tb'];
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=LimpaTexto($_GET['criv']);
$cric=LimpaTexto($_GET['cric']);
$cris2=(int)$_GET['cris2'];
$criv2=LimpaTexto($_GET['criv2']);
$cric2=LimpaTexto($_GET['cric2']);
$or=$_GET['or'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];
if ($tb != 0){
    $datai=explode("/",$_GET['datai']);
    $datai=$datai[2]."-".$datai[1]."-".$datai[0];
    $dataf=explode("/",$_GET['dataf']);
    $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];

    $txt.= "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
    $txt.= "<tr><td colspan=\"4\" aling=\"center\"><strong>{$ling['rel_desc_mo']}</strong></td></tr>
    <tr>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['FUNCIONARIO_M']."</strong></span></td>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['EQUIPE_M']."</strong></span></td>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_INICIO_M']."</strong></span></td>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_FINAL_M']."</strong></span></td>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['total_horas']."</strong></span></td>
    <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['OS']."</strong></span></td>
    </tr>";

    $obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_FUNCIONARIO = '$tb' AND DATA_INICIO >= '$datai' AND DATA_INICIO <= '$dataf' ORDER BY DATA_INICIO");
    while (!$obj->EOF) {
        $obj_campo = $obj -> fields;
        $tmp_esp=VoltaValor(FUNCIONARIOS,"ESPECIALIDADE","MID",$obj_campo['MID_FUNCIONARIO'],0);

        $hi=explode(":",$obj_campo['HORA_INICIO']);
        $di=explode("-",$obj_campo['DATA_INICIO']);
        $df=explode("-",$obj_campo['DATA_FINAL']);
        $hf=explode(":",$obj_campo['HORA_FINAL']);
        $di=mktime($hi[0],$hi[1],$hi[2],$di[1],$di[2],$di[0]);
        $df=mktime($hf[0],$hf[1],$hf[2],$df[1],$df[2],$df[0]);

        $dt=$df-$di;
        $ts=round($dt /(60*60),8);
        $h2=round($dt /(60*60),2);
        $tempo_total_servico=$tempo_total_servico + $ts;
        $mfunc[$obj_campo['MID_FUNCIONARIO']]=$mfunc[$obj_campo['MID_FUNCIONARIO']] + $ts;
        $mfunci[$obj_campo['MID_FUNCIONARIO']]++;

        $txt .= "<tr>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(FUNCIONARIOS,"NOME","MID",$obj_campo['MID_FUNCIONARIO'],$tdb[FUNCIONARIOS]['dba'])."</span></td>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(EQUIPES,"DESCRICAO","MID",$obj_campo['EQUIPE'],$tdb[EQUIPES]['dba'])."</span></td>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_INICIO'],'d/m/Y')." ".$obj_campo['HORA_INICIO']."</span></td>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_FINAL'],'d/m/Y')." ".$obj_campo['HORA_FINAL']."</span></td>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$h2</span></td>";
        $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(ORDEM_PLANEJADO,"NUMERO","MID",$obj_campo['MID_ORDEM'],$tdb[EQUIPES]['dba'])."</span></td>";
        $txt .= "</tr>\n";

        $obj->MoveNext();
    }
    $txt.= "</table> ".round($mfunc[$tb],2)."";
    echo $txt;
}
elseif (($relatorio != "") or ($exword != "")){
    $datai=explode("/",$_GET['datai']);
    $datai=$datai[2]."-".$datai[1]."-".$datai[0];
    $dataf=explode("/",$_GET['dataf']);
    $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
    $empf=(int)$_GET['filtro_emp'];
    $areaf=(int)$_GET['filtro_area'];
    $setorf=(int)$_GET['filtro_setor'];
    $maqf=(int)$_GET['filtro_maq'];
    $ccf=(int)$_GET['filtro_cc'];
    $defeitof=(int)$_GET['filtro_defeito'];
    $natuf=(int)$_GET['filtro_natureza'];
    $equipf=(int)$_GET['filtro_equipe'];
    $espf=(int)$_GET['filtro_esp'];
    $funcf=(int)$_GET['filtro_func'];
    $tipo_serv=(int)$_GET['filtro_tipo_serv'];
    $filtro_servico=(int)$_GET['filtro_servico'];

    if ($empf != 0) $fil.=" AND MID_EMPRESA = '$empf'";
    if ($areaf != 0) $fil.=" AND MID_AREA = '$areaf'";
    if ($setorf != 0) $fil.=" AND MID_SETOR = '$setorf'";
    if ($maqf != 0) $fil.=" AND MID_MAQUINA = '$maqf'";
    if ($ccf != 0) $fil.=" AND CENTRO_DE_CUSTO = '$ccf'";
    if ($defeitof != 0) $fil.=" AND DEFEITO = '$defeitof'";
    if ($natuf != 0) $fil.=" AND NATUREZA = '$natuf'";
    if ($tipo_serv == 1) {
        if ($filtro_servico == 0) $fil.=" AND (TIPO != '0' OR TIPO != '4' OR TIPO != NULL)";
        else $fil.=" AND TIPO = '$filtro_servico'";
    }
    if ($tipo_serv == 2) {
        if ($filtro_servico == 0) $fil.=" AND (TIPO_SERVICO != '0' OR TIPO_SERVICO != NULL)";
        else $fil.=" AND TIPO_SERVICO = '$filtro_servico'";
    }
    if ($_GET['datai'] != "") $fil.=" AND DATA_INICIO >= '$datai' AND DATA_INICIO <= '$dataf'";

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(ORDEM_PLANEJADO, 2);
    $fil .= ($fil_emp != "")? " AND " . $fil_emp : "";

    $sql="SELECT * FROM ".ORDEM_PLANEJADO." WHERE STATUS = 2 $fil ORDER BY DATA_INICIO ASC";
    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    $iii=0;
    while (!$resultado->EOF) {
        $campo=$resultado -> fields;
        if ($equipf != 0) $fil2.=" AND EQUIPE = '$equipf'";
        if ($funcf != 0) $fil2.=" AND MID_FUNCIONARIO = '$funcf'";

        $tmp=$dba[0] -> Execute("SELECT MID,MID_FUNCIONARIO FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $fil2");
        while (!$tmp->EOF) {
            $obj_campo = $tmp -> fields;
            $tmp_esp=VoltaValor(FUNCIONARIOS,"ESPECIALIDADE","MID",$obj_campo['MID_FUNCIONARIO'],0);
            if (($espf != 0) and ($tmp_esp == $espf)) $mostra=1;
            elseif ($espf == 0) $mostra=1;
            $tmp -> MoveNext();
        }

        if ($mostra == 1) {
            if (($campo['TIPO'] == "") or ($campo['TIPO'] == 0) or ($campo['TIPO'] == 4))  {
                $txt.="
                <table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\"><tr>
                <td align=\"left\"><font size=\"2\"><strong>{$ling['ordem_n']}: ".$campo['NUMERO']."</strong></font></td>
                <td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba'])." </td>
                <td align=\"left\"><strong>{$ling['responsavel']}</strong>: ".VoltaValor(FUNCIONARIOS,"NOME","MID",$campo['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba'])." </td>
                <td colspan=\"3\" align=\"left\"><strong>{$ling['data_abre']}</strong>: ".$resultado -> UserDate($campo['DATA_ABRE'],'d/m/Y')."  ".$campo['HORA_ABRE']."
                </tr>
                <tr>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc1_min']}</strong>: ".VoltaValor(AREAS,"DESCRICAO","MID",$campo['MID_AREA'],$tdb[AREAS]['dba'])." </td>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc2_min']}</strong>: ".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."  </td>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_cc2']}</strong>: ".VoltaValor(CENTRO_DE_CUSTO,"DESCRICAO","MID",$campo['CENTRO_DE_CUSTO'],$tdb[CENTRO_DE_CUSTO]['dba'])." </td>
                </tr><tr>
                <td align=\"left\" colspan=\"4\"><strong>{$ling['sol_objeto']}</strong>: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])."
                <strong>{$ling['sol_posicao']}</strong>:".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])."
                </td>
                <td align=\"left\"><strong>{$ling['data_inicio']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." (".$campo['HORA_INICIO'].")</td>
                <td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')."(".$campo['HORA_FINAL'].")</td>
                </tr>
                </table><br>";
            }
            else {
                $txt.="
                <table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\"><tr>
                <td align=\"left\"><font size=\"2\"><strong>{$ling['ordem_n']}: ".$campo['NUMERO']."</strong></font> </td>
                <td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])."</td>

                <td align=\"left\"><strong>{$ling['responsavel']}</strong>: ".VoltaValor(FUNCIONARIOS,"NOME","MID",$campo['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba'])." </td>
                <td colspan=\"3\" align=\"left\"><strong>{$ling['data_abre']}</strong>: ".$resultado -> UserDate($campo['DATA_ABRE'],'d/m/Y')."
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                </tr>
                <tr>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc1_min']}</strong>: ".VoltaValor(AREAS,"DESCRICAO","MID",$campo['MID_AREA'],$tdb[AREAS]['dba'])." </td>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc2_min']}</strong>: ".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."  </td>
                <td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_cc2']}</strong>: ".VoltaValor(CENTRO_DE_CUSTO,"DESCRICAO","MID",$campo['CENTRO_DE_CUSTO'],$tdb[CENTRO_DE_CUSTO]['dba'])." </td>
                </tr><tr>
                <td align=\"left\" colspan=\"4\"><strong>{$ling['sol_objeto']}</strong>: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])."
                <strong>{$ling['sol_posicao']}</strong>:".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])."
                </td>
                <td align=\"left\"><strong>{$ling['data_inicio']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." (".$campo['HORA_INICIO'].")</td>
                <td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')."(".$campo['HORA_FINAL'].") </td>
                </table><br>";
            }

            $txt.= "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
            $txt.= "<tr><td colspan=\"4\" aling=\"center\"><strong>{$ling['rel_desc_mo']}</strong></td></tr>
            <tr>
            <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['FUNCIONARIO_M']."</strong></span></td>
            <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['EQUIPE_M']."</strong></span></td>
            <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_INICIO_M']."</strong></span></td>
            <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['DATA_FINAL_M']."</strong></span></td>
            <td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['total_horas']."</strong></span></td>
            </tr>";

            $obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."' $fil2");
            while (!$obj->EOF) {
                $obj_campo = $obj -> fields;
                $tmp_esp=VoltaValor(FUNCIONARIOS,"ESPECIALIDADE","MID",$obj_campo['MID_FUNCIONARIO'],0);
                if (($espf != 0) and ($espf != $tmp_esp)) {
                }
                else {
                    $hi=explode(":",$obj_campo['HORA_INICIO']);
                    $di=explode("-",$obj_campo['DATA_INICIO']);
                    $df=explode("-",$obj_campo['DATA_FINAL']);
                    $hf=explode(":",$obj_campo['HORA_FINAL']);
                    $di=mktime($hi[0],$hi[1],$hi[2],$di[1],$di[2],$di[0]);
                    $df=mktime($hf[0],$hf[1],$hf[2],$df[1],$df[2],$df[0]);
                    if ($di <= $df) {
                        $dt=$df-$di;
                        $ts=round($dt /(60*60),8);
                        $h2=round($dt /(60*60),2);
                        $tempo_total_servico=$tempo_total_servico + $ts;
                        $mfunc[$obj_campo['MID_FUNCIONARIO']]=$mfunc[$obj_campo['MID_FUNCIONARIO']] + $ts;
                        $mfunci[$obj_campo['MID_FUNCIONARIO']]++;
                    }
                    $txt .= "<tr>";
                    $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(FUNCIONARIOS,"NOME","MID",$obj_campo['MID_FUNCIONARIO'],$tdb[FUNCIONARIOS]['dba'])."</span></td>";
                    $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(EQUIPES,"DESCRICAO","MID",$obj_campo['EQUIPE'],$tdb[EQUIPES]['dba'])."</span></td>";
                    $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_INICIO'],'d/m/Y')." ".$obj_campo['HORA_INICIO']."</span></td>";
                    $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj -> UserDate($obj_campo['DATA_FINAL'],'d/m/Y')." ".$obj_campo['HORA_FINAL']."</span></td>";
                    $txt .= "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$h2</span></td>";
                    $txt .= "</tr>\n";
                }
                $obj->MoveNext();
            }
            $txt.= "</table>";
            $iii++;
            unset($total_horas);
            unset($total_horasp);
            unset($tempo_parada_maq);
        }
        unset($mostra);
        $resultado->MoveNext();
    }

    $horas_servico=round($tempo_total_servico,2);
    $horas_parada=round($tempo_total_parada,2);

    $txt.= "<h2> {$ling['rel_desc_estatistica_geral']} </h2><br />
    <div style=\"border: 1px solid black; padding: 5px\" align=\"left\">
    <h3>{$ling['tempo_total_serv']}:  ".round($tempo_total_servico,2)." horas</h3>
    <br clear=\"all\" /><h3 style=\"font-size:12px\">{$ling['funcionarios']}</h3><br clear=\"all\" />
    <ul>";
    $re1=$dba[$tdb[FUNCIONARIOS]['dba']] -> Execute("SELECT MID,NOME FROM ".FUNCIONARIOS." ORDER BY NOME ASC");
    while (!$re1->EOF) {
        $ca=$re1 -> fields;
        if ($mfunc[$ca['MID']] != "") {
            $txt.= "<li>".$ca['NOME']."<br>";
            $tmp_por_ts=round($mfunc[$ca['MID']],2);
            $tmp_por=round($tmp_por_ts * 100 / ($tempo_total_servico),1);
            $tmp_td=100 - $tmp_por;
            $txt.= "<table style=\"font-size:11px; margin:1px; border: 1px solid black; padding: 1px;\" width=\"400\" border=\"0\">
            <tr><td colspan=\"2\"> {$ling['rel_desc_tempo_serv_h']}: $tmp_por_ts ($tmp_por%) {$ling['qtd']}: ".$mfunci[$ca['MID']]."</td></tr>
            <tr>
            <td bgcolor=\"#281A71\" align=\"right\" width=\"$tmp_por%\">&nbsp;</font></td>
            <td bgcolor=\"white\" width=\"$tmp_td%\">&nbsp;</td></tr></table><br clear=\"all\" />";
            $txt.= "</li>";
        }
        $re1 -> MoveNext();
    }

    $txt.= "</ul><br /></div></div>";
    $iii="$iii";
    if ($empf != 0) $filtro.=$tdb[EMPRESAS]['DESC']." (".VoltaValor(EMPRESAS,"NOME","MID",$empf,0).") - ";
    if ($areaf != 0) $filtro.=$tdb[AREAS]['DESC']." (".VoltaValor(AREAS,"DESCRICAO","MID",$areaf,0).") - ";
    if ($setorf != 0) $filtro.=$tdb[SETORES]['DESC']." (".VoltaValor(SETORES,"DESCRICAO","MID",$setorf,0).") - ";
    if ($maqf != 0) $filtro.=$tdb[MAQUINAS]['DESC']." (".VoltaValor(MAQUINAS,"DESCRICAO","MID",$maqf,0).") - ";
    if ($equipf != 0) $filtro.=$tdb[EQUIPES]['DESC']." (".VoltaValor(EQUIPES,"DESCRICAO","MID",$equipf,0).") - ";
    if ($funcf != 0) $filtro.=$tdb[FUNCIONARIOS]['DESC']." (".VoltaValor(FUNCIONARIOS,"NOME","MID",$funcf,0).") - ";
    if ($_GET['datai'] != "")	$filtro.="Periodo (".$_GET['datai']." a ".$_GET['dataf'].")";

    if ($relatorio != "") relatorio_padrao($ling['rel_mao_de_obra'],$filtro,$iii,$txt,1);
    else exportar_word($ling['rel_mao_de_obra'],$filtro,$iii,$txt,$_GET['papel_orientacao']);
}


else {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n
    </head>
    <body><div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1>{$ling['rel_mao_de_obra']}
    </div>
    <div id=\"corpo_relatorio\">
    <form action=\"relatorio_geralmo.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    <fieldset><legend>{{$ling['rel_desc_mo']}}</legend>
    <label class=\"campo_label \" for=\"filtro_equipe\">".$tdb[EQUIPES]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'',EQUIPES,$_GET['filtro_equipe'],"filtro_equipe","filtro_equipe","MID",0, "", "atualiza_area2('func','../parametros.php?id=12&equip=' + this.options[this.selectedIndex].value + '&esp=' + document.getElementById('filtro_esp').options[document.getElementById('filtro_esp').selectedIndex].value)", "", "", "", $ling['rel_desc_todas2']);

    echo "<br clear=\"all\" />
    <label class=\"campo_label \" for=\"filtro_esp\">".$tdb[ESPECIALIDADES]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'',ESPECIALIDADES,$_GET['filtro_esp'],"filtro_esp","filtro_esp","MID",0, "", "atualiza_area2('func','../parametros.php?id=12&esp=' + this.options[this.selectedIndex].value + '&equip=' + document.getElementById('filtro_equipe').options[document.getElementById('filtro_equipe').selectedIndex].value)", "", "", "", $ling['rel_desc_todas2']);

    echo "<br clear=\"all\" />
    <div id=\"func\">";
    echo "<label class=\"campo_label \" for=\"filtro_func\">{$ling['funcionario']}:</label>";
    FormSelectD("NOME",'',FUNCIONARIOS,$_GET['filtro_func'],"filtro_func","filtro_func","MID",0, "", "", "", "", "", $ling['todos2']);

    echo "</div>
    </fieldset>
    <fieldset>
    <legend>{$ling['rel_desc_localizacoes']}</legend>
    <div id=\"filtro_relatorio\">";
    
    // Filtros
    FiltrosRelatorio(1, 1, 1, 1);

    echo "</div>
    
    <label class=\"campo_label \" for=\"filtro_cc\">".$tdb[CENTRO_DE_CUSTO]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'',CENTRO_DE_CUSTO,$_GET['filtro_cc'],"filtro_cc","filtro_cc","MID",0, "", "", "", "", "", $ling['todos2']);

    echo "<br clear=\"all\" />
    <label class=\"campo_label \" for=\"filtro_defeito\">".$tdb[DEFEITO]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'', DEFEITO, $_GET['filtro_defeito'],"filtro_defeito","filtro_defeito","MID",0, "", "", "", "", "", $ling['todos2']);

    echo "<br clear=\"all\" />
    <label class=\"campo_label \" for=\"filtro_natureza\">".$tdb[NATUREZA_SERVICOS]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'', NATUREZA_SERVICOS, $_GET['filtro_natureza'], "filtro_natureza", "filtro_natureza", "MID", 0, "", "", "", "", "", $ling['todos2']);

    echo "</fieldset>";
    echo "<fieldset><legend>".$ling['tipo_servicos']."</legend>";
    echo "<input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t1\" value=\"1\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=1')\" />
    <label for=\"t1\">".$ling['somente_sistematico']."</label>
    <input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t2\" value=\"2\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=2')\" />
    <label for=\"t2\">".$ling['somente_nao_sistematico']."</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t3\" value=\"3\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=3')\" />
    <label for=\"t3\">".$ling['todos']."</label>
    <div id=\"serv\"></div>
    </fieldset>";

    echo "<fieldset><legend>".$ling['datas']."</legend>";
    FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
    echo "<br clear=\"all\" />";
    FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
    echo "
    </fieldset>

<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";

}


?>
