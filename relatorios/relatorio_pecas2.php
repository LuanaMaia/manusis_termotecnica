<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
 * Nota: Relat�rio material por localizacao
 */
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
#############################
$ajaxdestino = "relatorio_pecas2.php";
$ajax = $_GET['ajax'];
$setor = $_GET['setor'];
$emp = $_GET['emp'];
$area = $_GET['area'];
$fam = $_GET['fam'];
$obj = $_GET['obj'];

$tempoexec_inicial = utime();

$and = '';

if ($ajax or $_GET['env']) {
	$condmaq = '';
	$condset = '';
	if ($obj)
	$condmaq = " MID = '$obj'";
	else {
		if ($fam) {
			if ($condmaq) $condmaq .= ' AND ';
			$condmaq .= " FAMILIA = '$fam'";
			$and = ' AND ';
		}
		if ($setor) {
			$condmaq .= $and ." MID_SETOR = '$setor'";
			$and = ' AND ';
		}
		elseif ($area) {
			$condset = $and . " MID_AREA = '$area'";
			// gera sequencia de or mid_setor=tal etc
			$setfil = '';
			// Filtro por Empresa
			$fil_emp = VoltaFiltroEmpresa(SETORES, 2);
			$fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

			$sql="SELECT MID FROM ".SETORES." WHERE MID_AREA = '$area' $fil_emp";
			if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
				$err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
				erromsg("SQL ERROR .<br>$err<br><br>$sql");
				exit;
			}
			while (!$resultado->EOF) {
				$campo=$resultado -> fields;
				if ($setfil) $setfil .= ' OR ';
				$setfil .= " MID_SETOR = ".$campo['MID'];
				$resultado->MoveNext();
			}
			if ($setfil) $setfil = "($setfil)";
			else $setfil = $and . 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

			if ($condmaq) $condmaq .= ' AND ';
			$condmaq .= $setfil;

		}
		elseif ($emp) {
			$condarea = $and. " MID_EMPRESA = '$emp'";
			// gera sequencia de or mid_setor=tal etc
			$setfil = '';

			// Filtro por Empresa
			$fil_emp = VoltaFiltroEmpresa(AREAS, 2);
			$fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

			$sql="SELECT S.MID FROM ".SETORES." S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = '$emp' $fil_emp";
			if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
				$err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
				erromsg("SQL ERROR .<br>$err<br><br>$sql");
				exit;
			}
			while (!$resultado->EOF) {
				$campo=$resultado -> fields;
				if ($setfil) $setfil .= ' OR ';
				$setfil .= "MID_SETOR = ".$campo['MID'];
				$resultado->MoveNext();
			}
			if ($setfil) $setfil = "($setfil)";
			else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

			if ($condmaq) $condmaq .= ' AND ';
			$condmaq .= $setfil;
		}
	}
}

if (!$_GET['env']) { // n�o exibindo relatorio

	if ($ajax) {
		// Filtros
		echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
		FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
		$where_a = "";
		if($emp) {
			$where_a = "WHERE MID_EMPRESA = $emp";
		}
		FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&setor=$setor&obj=$obj&area=' + this.value)", $where_a);
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
		$where_s = "";
		if($area) {
			$where_s = "WHERE MID_AREA = $area";
		}
		elseif ($emp) {
			$where_s = "WHERE MID_AREA IN (SELECT MID FROM " . AREAS . " WHERE MID_EMPRESA = $emp)";
		}
		FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&obj=$obj&setor=' + this.value)", $where_s);
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
		FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&emp=$emp&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
		$where_m = "";
		if ($setor) {
			$where_m = "WHERE MID_SETOR = $setor";
		}
		elseif ($area) {
			$where_m = "WHERE MID_SETOR IN (SELECT MID FROM " . SETORES . " WHERE MID_AREA = $area)";
		}
		elseif ($emp) {
			$list_m = array();
			// Buscando
			$sql = "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S, " . AREAS . " A WHERE M.MID_SETOR = S.MID AND S.MID_AREA = A.MID AND A.MID_EMPRESA = $emp";
			if (!$resultado = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
				$err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
				erromsg("SQL ERROR .<br>$err<br><br>$sql");
				exit;
			}
			while (! $resultado->EOF) {
				$list_m[] = $resultado->fields['MID'];
				$resultado->MoveNext();
			}
			if(count($list_m) > 0) {
				$where_m = "WHERE MID IN (" . implode(', ', $list_m) . ")";
			}
		}
		if($fam) {
			$where_m .= ($where_m == "")? "WHERE " : " AND ";
			$where_m .= "FAMILIA = $fam";
		}
		FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&setor=$setor&obj=' + this.value)", $where_m);
	}
	else {
		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
        <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
        <head>
         <meta http-equiv=\"pragma\" content=\"no-cache\" />
        <title>{$ling['manusis']}</title>
        <link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
        <script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
		if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../lib/movediv.js\"> </script>\n";
		echo "</head>
        <body><div id=\"central_relatorio\">
        <div id=\"cab_relatorio\">
        <h1>{$ling['rel_localizacao_pecas']}</h1>
        </div>
        <div id=\"corpo_relatorio\">
        <form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
        <fieldset>
        <legend>".$ling['filtros']."</legend>
        <div id=\"fil\">";

		// Filtros
		echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
		FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
		FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&setor=$setor&obj=$obj&area=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
		FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&obj=$obj&setor=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
		FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
		echo "<br clear=\"all\" />";

		echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
		FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=' + this.value)");


		echo "</div>";
		echo "<br /><font size=1>{$ling['rel_desc_loc_mat_texto']}</font>
        </fieldset>
        <input type=\"hidden\" name=\"env\" value=\"1\" />
        <input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
        <input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\">
        </form><br />
        </div>
        </div>
        </body>
        </html>";
	}
}
else {
	$tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";

	

	$txt = '';
	$ipp=0;

	// Filtro por Empresa
	$fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);

	// Filtros
	if ($condmaq) {
		$condmaq = " WHERE $condmaq";
		if ($fil_emp != "") {
			$condmaq .= " AND " . $fil_emp;
		}
	}
	elseif ($fil_emp != "") {
		$condmaq = " WHERE " . $fil_emp;
	}

	$relatorio = '';

	$sql="SELECT * FROM ".MAQUINAS." $condmaq ORDER BY DESCRICAO ASC";

	if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
		$err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	}

	while (!$resultado->EOF) {

		$campoMaquina =$resultado -> fields;
			
		$head="<table id=\"dados_processados\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">";

		$head .= "<div align=\"left\" ><strong><font size=\"3\">".$campoMaquina['COD'].'-'.$campoMaquina['DESCRICAO']."</font></strong></div>";

		$head .= "<tr>
		<th $tdstyle width=\"10px\">{$ling['tipo']}</th>
		<th $tdstyle width=\"120px\">{$ling['local']}</th>
		<th $tdstyle >{$ling['rel_desc_material']}</th>
		<th $tdstyle width=\"20px\" >{$ling['unidade']}</th>
		<th $tdstyle width=\"20px\">{$ling['qtd']}</th>
		</tr>";

		$sql = "SELECT
		  ".MAQUINAS_CONJUNTO_MATERIAL.".*,".
		MATERIAIS_UNIDADE . ".COD as COD_UNIDADE, ".
		MAQUINAS_CONJUNTO .".TAG,".
		MATERIAIS. ".DESCRICAO AS MATERIAL_DESCRICAO
		FROM ". 
		MAQUINAS_CONJUNTO_MATERIAL.
		" INNER JOIN ". MATERIAIS_UNIDADE ." 
		 ON ". MAQUINAS_CONJUNTO_MATERIAL. ".MID_MATERIAL = ".MATERIAIS_UNIDADE.".MID 
		 INNER JOIN " .MAQUINAS_CONJUNTO . " 
		 ON ". MAQUINAS_CONJUNTO_MATERIAL .".MID_CONJUNTO = ". MAQUINAS_CONJUNTO .".MID 
		 INNER JOIN ".MATERIAIS ."
		 ON ". MAQUINAS_CONJUNTO_MATERIAL .".MID_MATERIAL = ".MATERIAIS .".MID  	 
		 WHERE ". MAQUINAS_CONJUNTO_MATERIAL .".MID_MAQUINA = '".$campoMaquina['MID']."' ORDER BY MID_CONJUNTO ASC";
			
		$rpos=$dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]-> Execute($sql);
			
		$estetxta = '';
		if(!$rpos->EOF){

			while (!$rpos->EOF) {

				$campoConjunto = $rpos->fields;

			$estetxta .="<tr>
			<td $tdstyle>".substr($tdb[MAQUINAS_CONJUNTO]['DESC'], 0, 1)."</td>
			<td $tdstyle>{$campoConjunto['TAG']}</td>
			<td $tdstyle>{$campoConjunto['MATERIAL_DESCRICAO']}</td>
			<td $tdstyle >{$campoConjunto['COD_UNIDADE']}</td>
			<td $tdstyle>{$campoConjunto['QUANTIDADE']}</td>
			</tr>";
				$rpos->MoveNext();
				$ipp++;
			}
		}


		$sql = "SELECT * FROM ".EQUIPAMENTOS." WHERE MID_MAQUINA = '".$campoMaquina['MID']."' ORDER BY DESCRICAO ASC";
		
		$rpos=$dba[$tdb[EQUIPAMENTOS]['dba']]-> Execute($sql);

		if(!$rpos->EOF){
				
			while (!$rpos->EOF) {
				$listapeca=ListaPecaEquipamento($rpos->fields['MID']);
				if ($listapeca != 0) {
					for ($ip=0; $listapeca[$ip]['nome'] != ""; $ip++) {
						$peca=$listapeca[$ip]['nome'];
						$qto=$listapeca[$ip]['qto'];
						$pmid=$listapeca[$ip]['mid'];
						$pun = VoltaValor(MATERIAIS,"UNIDADE","MID",$rpos ->fields['MID_MATERIAL'],$tdb[MATERIAIS]['dba']);
						$pun = VoltaValor(MATERIAIS_UNIDADE,"COD","MID",$pun,$tdb[MATERIAIS_UNIDADE]['dba']);
						$pcod = VoltaValor(MATERIAIS,"COD","MID",$pmid,$tdb[MATERIAIS]['dba']);
						$estetxta.="<tr>
					    <td $tdstyle>".substr($tdb[EQUIPAMENTOS]['DESC'], 0, 1)."</td>
					    <td $tdstyle>".$rpos ->fields['COD']."-".$rpos ->fields['DESCRICAO']."</td>
					    <td $tdstyle>$peca</td>
					    <td $tdstyle>$pun</td>
					    <td $tdstyle >$qto</td>
					    </tr>\n";
						$ipp++;
					}
				}
				$rpos->MoveNext();
			}
		}
		
		if($estetxta == '')
		{
			$resultado->MoveNext();
			continue;
		}


		$foot = "</table>";
		$relatorio .= $head .$estetxta. $foot;

		$resultado->MoveNext();
	}

	$filtro = '';
	
	if ($emp) {
		$filtro .= $tdb[EMPRESAS]['DESC'].': '.VoltaValor(EMPRESAS,"NOME","MID",$emp,$tdb[EMPRESAS]['dba']);
	}
	if ($area) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[AREAS]['DESC'].': '.VoltaValor(AREAS,"DESCRICAO","MID",$area,$tdb[AREAS]['dba']);
	}
	if ($setor) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[SETORES]['DESC'].': '.VoltaValor(SETORES,"DESCRICAO","MID",$setor,$tdb[SETORES]['dba']);
	}
	if ($fam) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS_FAMILIA]['DESC'].': '.VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$fam,$tdb[MAQUINAS_FAMILIA]['dba']);
	}
	if ($obj) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS]['DESC'].': '.VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,$tdb[MAQUINAS]['dba']);
	}
	
	$tempoexec_final = utime();
	$tempoexec = round($tempoexec_final - $tempoexec_inicial,4);	
	
	$relatorio .= "<div style=\"text-align: left;\" id=\"lt_rodape\">
	<br />
	<strong>{$ling['rel_desc_legendas']}:</strong>
	<br />
	<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\">
<tbody><tr>
<td valign=\"top\" style=\"border-left: 1px solid rgb(102, 102, 102);\">Tipo:<br><span style=\"border: 1px solid rgb(204, 204, 204); float: left; height: 20px; width: 20px; margin: 2px;\">".substr($tdb[MAQUINAS_CONJUNTO]['DESC'], 0, 1)."</span>{$tdb[MAQUINAS_CONJUNTO]['DESC']}&nbsp;<br clear=\"all\"><span style=\"border: 1px solid rgb(204, 204, 204); float: left; height: 20px; width: 20px; margin: 2px;\">".substr($tdb[EQUIPAMENTOS]['DESC'], 0, 1)."</span> {$tdb[EQUIPAMENTOS]['DESC']}&nbsp;</td></tr></tbody></table></div>";
	
	
	 if ($_GET['word']) exportar_word($ling['rel_desc_loc_mat_texto'],$filtro,$ipp,$relatorio,$tempoexec);
	 else relatorio_padrao($ling['rel_desc_loc_mat_texto'],$filtro,$ipp,$relatorio,1,$tempoexec);

} // fim OK

?>
