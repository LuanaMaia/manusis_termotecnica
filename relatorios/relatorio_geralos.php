<?
/**
 * Manusis SQM
 * Autor: Felipe Matos Malinoski <felipe@manusistem.com.br>
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";


// GERANDO RELAT�RIO
if (($_GET['relatorio'] != "") or ($_GET['exword'] != "")){
    // MOSTRAR CAMPOS
    // OS
    $mostrar_os = $_GET['opcao']['os'];

    // FILTROS
    $filtro_sql_os  = "";
    $ordem_sql_os   = "";
    $group_sql_os   = "";
    $filtro_desc    = "";
    $filtro_rel_tabela = "";

    // FILTROS
    // Localiza��es
    $filtro_emp  = (int) $_GET['filtro_emp'];
    if($filtro_emp != 0) {
        $filtro_sql_os  .= " AND O.MID_EMPRESA = $filtro_emp";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_EMPRESA'] . ": " . htmlentities(VoltaValor(EMPRESAS, 'NOME', 'MID', $filtro_emp)) . "</li>\n";
    }

    $filtro_area  = (int) $_GET['filtro_area'];
    if($filtro_area != 0) {
        $filtro_sql_os  .= " AND O.MID_AREA = $filtro_area";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_AREA'] . ": " . htmlentities(VoltaValor(AREAS, 'DESCRICAO', 'MID', $filtro_area)) . "</li>\n";
    }

    $filtro_setor  = (int) $_GET['filtro_setor'];
    if($filtro_setor != 0) {
        $filtro_sql_os  .= " AND O.MID_SETOR = $filtro_setor";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_SETOR'] . ": " . htmlentities(VoltaValor(SETORES, 'DESCRICAO', 'MID', $filtro_setor)) . "</li>\n";
    }

    $filtro_maq_fam  = (int) $_GET['filtro_maq_fam'];
    if($filtro_maq_fam != 0) {
        $filtro_sql_os  .= " AND O.MID_MAQUINA_FAMILIA = $filtro_maq_fam";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_MAQUINA_FAMILIA'] . ": " . htmlentities(VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', $filtro_maq_fam)) . "</li>\n";
    }

    $filtro_maq  = $_GET['filtro_maq'];
    if (count($filtro_maq)>0 and ! in_array(0, array_values($filtro_maq))){
        $filtro_sql_os  .= " AND MID_MAQUINA IN (".implode(',', array_values($filtro_maq)).")" ;	
                foreach($filtro_maq as $k => $mid_maq){			
				$filtro_desc .= "<li>" . $tdb[ORDEM]['MID_MAQUINA'] . ": " . htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq)) . "</li>\n";
				
			}
    }

    $filtro_conj  = (int) $_GET['filtro_conj'];
    if($filtro_conj != 0) {
        $filtro_sql_os  .= " AND O.MID_CONJUNTO = $filtro_conj";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_CONJUNTO'] . ": " . htmlentities(VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $filtro_conj)) . "</li>\n";
    }

    $filtro_equip  = (int) $_GET['filtro_equip'];
    if($filtro_equip != 0) {
        $filtro_sql_os  .= " AND O.MID_EQUIPAMENTO = $filtro_equip";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['MID_EQUIPAMENTO'] . ": " . htmlentities(VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $filtro_equip)) . "</li>\n";
    }
    
    
    // Filtros
    $filtro_numos    = LimpaTexto($_GET['filtro_numos']);
    if($filtro_numos != "") {
        $filtro_sql_os  .= " AND O.NUMERO LIKE '$filtro_numos'";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['NUMERO'] . ": $filtro_numos</li>\n";
    }
    
    $filtro_natureza = (int) $_GET['filtro_natureza'];
    if($filtro_natureza != 0) {
        $filtro_sql_os  .= " AND O.NATUREZA = $filtro_natureza";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['NATUREZA'] . ": " . htmlentities(VoltaValor(NATUREZA_SERVICOS, 'DESCRICAO', 'MID', $filtro_natureza)) . "</li>\n";
    }

    $filtro_defeito  = (int) $_GET['filtro_defeito'];
    if($filtro_defeito != 0) {
        $filtro_sql_os  .= " AND O.DEFEITO = $filtro_defeito";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['DEFEITO'] . ": " . htmlentities(VoltaValor(DEFEITO, 'DESCRICAO', 'MID', $filtro_defeito)) . "</li>\n";
    }

    $filtro_causa  = (int) $_GET['filtro_causa'];
    if($filtro_causa != 0) {
        $filtro_sql_os  .= " AND O.CAUSA = $filtro_causa";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['CAUSA'] . ": " . htmlentities(VoltaValor(CAUSA, 'DESCRICAO', 'MID', $filtro_causa)) . "</li>\n";
    }

    $filtro_solucao  = (int) $_GET['filtro_solucao'];
    if($filtro_solucao != 0) {
        $filtro_sql_os  .= " AND O.SOLUCAO = $filtro_solucao";
        $filtro_desc .= "<li>" . $tdb[ORDEM]['SOLUCAO'] . ": " . htmlentities(VoltaValor(SOLUCAO, 'DESCRICAO', 'MID', $filtro_solucao)) . "</li>\n";
    }

    $filtro_material = (int) $_GET['filtro_material'];
    if($filtro_material != 0) {
        $filtro_sql_os  .= " AND (SELECT COUNT(MID) FROM " . ORDEM_MATERIAL . " WHERE MID_ORDEM = O.MID AND MID_MATERIAL = $filtro_material) > 0";
        $filtro_desc .= "<li>" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . ": " . htmlentities(VoltaValor(MATERIAIS, 'COD', 'MID', $filtro_material) . '-' . VoltaValor(MATERIAIS, 'DESCRICAO', 'MID', $filtro_material, 1, 0)) . "</li>\n";
    }

    $filtro_parada = (int) $_GET['filtro_parada'];
    if($filtro_parada == 1) {
        $filtro_sql_os  .= " AND (SELECT COUNT(MID) FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = O.MID) > 0";
        $filtro_desc .= "<li>" . $tdb[ORDEM_MAQ_PARADA]['DESC'] . ": Somente com parada</li>\n";
    }
    elseif($filtro_parada == 2) {
        $filtro_sql_os  .= " AND (SELECT COUNT(MID) FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = O.MID) = 0";
        $filtro_desc .= "<li>" . $tdb[ORDEM_MAQ_PARADA]['DESC'] . ": {$ling['rel_desc_sem_parada']}</li>\n";
    }
    
    // Tipo de servi�o
    $filtro_mostrar_serv  = (int) $_GET['filtro_mostrar_serv'];
    if ($filtro_mostrar_serv == 1) {
        // Selecionou um tipo de servico
        $filtro_servico  = (int) $_GET['filtro_servico'];
        if($filtro_servico != 0) {
            $filtro_sql_os  .= " AND O.TIPO_SERVICO = $filtro_servico";
            $filtro_desc .= "<li>" . $tdb[ORDEM]['TIPO_SERVICO'] . ": " . htmlentities(VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $filtro_servico)) . "</li>\n";
        }
        // Todos os n�o sistematicos
        else {
            $filtro_sql_os .= " AND (O.TIPO = 0 OR O.TIPO = 4)";
            $filtro_desc   .= "<li>{$ling['servicos']}: {$ling['rel_desc_serv_nsist']}</li>\n";
        }
    }
    elseif ($filtro_mostrar_serv == 2) {
        // Selecionou um tipo de sistematica
        $filtro_tipo_prog  = (int) $_GET['filtro_tipo_prog'];
        if($filtro_tipo_prog != 0) {
            $filtro_sql_os  .= " AND O.TIPO = $filtro_tipo_prog";
            $filtro_desc .= "<li>" . $tdb[PROGRAMACAO_TIPO]['DESC'] . ": " . htmlentities(VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $filtro_tipo_prog)) . "</li>\n";
            
            // Selecionou um plano
            $filtro_plano  = (int) $_GET['filtro_plano'];
            if($filtro_plano != 0) {
                $filtro_sql_os  .= " AND O.MID_PROGRAMACAO IN (SELECT MID FROM " . PROGRAMACAO . " WHERE MID_PLANO = $filtro_plano AND TIPO = $filtro_tipo_prog)";
                
                if ($filtro_tipo_prog == 1) {
                    $filtro_desc .= "<li>" . $tdb[PLANO_PADRAO]['DESC'] . ": " . htmlentities(VoltaValor(PLANO_PADRAO, 'DESCRICAO', 'MID', $filtro_plano)) . "</li>\n";
                }
                elseif ($filtro_tipo_prog == 2) {
                    $filtro_desc .= "<li>" . $tdb[PLANO_ROTAS]['DESC'] . ": " . htmlentities(VoltaValor(PLANO_ROTAS, 'DESCRICAO', 'MID', $filtro_plano)) . "</li>\n";
                }
            }
        }
        // Todos os sistematicos
        else {
            $filtro_sql_os .= " AND (O.TIPO = 1 OR O.TIPO = 2)";
            $filtro_desc   .= "<li>{$ling['servicos']}: {$ling['rel_desc_serv_sist']}</li>\n";
        }
    }
    
    // M�o de Obra
    $filtro_func   = (int) $_GET['filtro_func'];
    if($filtro_func != 0) {
        $filtro_sql_os  .= " AND ((SELECT COUNT(MID) FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO = $filtro_func) > 0 OR (SELECT COUNT(MID) FROM " . ORDEM_MADODEOBRA . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO = $filtro_func) > 0)";
        $filtro_desc .= "<li>" . $tdb[ORDEM_MADODEOBRA]['MID_FUNCIONARIO'] . ": " . htmlentities(VoltaValor(FUNCIONARIOS, 'NOME', 'MID', $filtro_func)) . "</li>\n";
    }

    $filtro_equipe = (int) $_GET['filtro_equipe'];
    if($filtro_equipe != 0) {
        // Buscando funcionarios dessa equipe
        $sql_e = "SELECT MID FROM " . FUNCIONARIOS . " WHERE EQUIPE = $filtro_equipe";
        if(! $rs_e = $dba[$tdb[FUNCIONARIOS]['dba']] -> Execute($sql_e)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . "<br />" . $sql_e);
        }
        $id_tmp = array();
        while(! $rs_e->EOF) {
            $id_tmp[] = $rs_e->fields['MID'];
            $rs_e->MoveNext();
        }

        $filtro_sql_os  .= " AND ((SELECT COUNT(MID) FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO IN (" . implode(", ", $id_tmp) . ")) > 0 OR (SELECT COUNT(MID) FROM " . ORDEM_MADODEOBRA . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO IN (" . implode(", ", $id_tmp) . ")) > 0)";
        $filtro_desc .= "<li>" . $tdb[FUNCIONARIOS]['EQUIPE'] . ": " . htmlentities(VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $filtro_equipe)) . "</li>\n";
    }

    $filtro_esp  = (int) $_GET['filtro_esp'];
    if($filtro_esp != 0) {
        // Buscando funcionarios dessa equipe
        $sql_e = "SELECT MID FROM " . FUNCIONARIOS . " WHERE ESPECIALIDADE = $filtro_esp";
        if(! $rs_e = $dba[$tdb[FUNCIONARIOS]['dba']] -> Execute($sql_e)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . "<br />" . $sql_e);
        }
        $id_tmp = array();
        while(! $rs_e->EOF) {
            $id_tmp[] = $rs_e->fields['MID'];
            $rs_e->MoveNext();
        }

        $filtro_sql_os  .= " AND ((SELECT COUNT(MID) FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO IN (" . implode(", ", $id_tmp) . ")) > 0 OR (SELECT COUNT(MID) FROM " . ORDEM_MADODEOBRA . " WHERE MID_ORDEM = O.MID AND MID_FUNCIONARIO IN (" . implode(", ", $id_tmp) . ")) > 0)";
        $filtro_desc .= "<li>" . $tdb[FUNCIONARIOS]['ESPECIALIDADE'] . ": " . htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $filtro_esp)) . "</li>\n";
    }

    $filtro_reprog = (int) $_GET['filtro_reprog'];
    if($filtro_reprog == 1) {
        $filtro_sql_os .= " AND (SELECT COUNT(MID) FROM " . ORDEM_REPROGRAMA . " WHERE MID_ORDEM = O.MID) > 0";
        $filtro_desc .= "<li>{$ling['rel_desc_os_repro']}</li>\n";
    }


    $filtro_status_os = (int) $_GET['filtro_status_os'];
    if($filtro_status_os != 0) {
        if($filtro_status_os == 11) {
            $filtro_sql_os .= " AND O.STATUS != 3";
            $filtro_desc .= "<li>" . $tdb[ORDEM]['STATUS'] . ": {$ling['todos_menos_cancel']}</li>\n";
        }
        else {
            $filtro_sql_os .= " AND O.STATUS = $filtro_status_os";
            $filtro_desc .= "<li>" . $tdb[ORDEM]['STATUS'] . ": " . htmlentities(VoltaValor(ORDEM_STATUS, 'DESCRICAO', 'MID', $filtro_status_os)) . "</li>\n";
        }
    }

    $filtro_ordem = $_GET['filtro_ordem'];
    if($filtro_ordem == 1){
        $ordem_sql_os  = "O.NUMERO";
    }
    elseif($filtro_ordem == 2){
        $ordem_sql_os  = "O.MID_MAQUINA";
    }
    elseif($filtro_ordem == 3){
        $ordem_sql_os  = "O.DATA_ABRE";
    }
    elseif($filtro_ordem == 4){
        $ordem_sql_os  = "O.DATA_PROG";
    }
    elseif($filtro_ordem == 5){
        $ordem_sql_os  = "O.DATA_INICIO";
    }
    elseif($filtro_ordem == 6){
        $ordem_sql_os  = "O.DATA_FINAL";
    }

    $filtro_periodo = $_GET['filtro_periodo'];
    if($filtro_periodo == 1){
        $filtro_periodo_os_ini  = "O.DATA_ABRE";
        $filtro_periodo_os_fim  = "O.DATA_ABRE";
        
        $filtro_periodo_desc = $tdb[ORDEM]['DATA_ABRE'];
    }
    elseif($filtro_periodo == 2){
        $filtro_periodo_os_ini  = "O.DATA_PROG";
        $filtro_periodo_os_fim  = "O.DATA_PROG";
        
        $filtro_periodo_desc = $tdb[ORDEM]['DATA_PROG'];
    }
    elseif($filtro_periodo == 3){  
        $filtro_periodo_os_ini  = "O.DATA_INICIO";
        $filtro_periodo_os_fim  = "O.DATA_INICIO";
        
        $filtro_periodo_desc = $tdb[ORDEM]['DATA_INICIO'];
    }
    elseif($filtro_periodo == 4){
        $filtro_periodo_os_ini  = "O.DATA_FINAL";
        $filtro_periodo_os_fim  = "O.DATA_FINAL";
        
        $filtro_periodo_desc = $tdb[ORDEM]['DATA_FINAL'];
    }
    elseif($filtro_periodo == 5){  
        $filtro_periodo_os_ini  = "MO.DATA_INICIO";
        $filtro_periodo_os_fim  = "MO.DATA_FINAL";
        
        $filtro_periodo_desc = $ling['data_aponta'];
        
        // RELACAO DAS TABELAS
        $group_sql_os = " GROUP BY O.MID ";
        $filtro_sql_os .= " AND MO.MID_ORDEM = O.MID";
        $filtro_rel_tabela .= ", " . ORDEM_MAODEOBRA . " MO";
    }

    $filtro_datai   = $_GET['datai'];
    if($filtro_datai != "") {
        $filtro_sql_os  .= " AND $filtro_periodo_os_ini >= '" . DataSQL($filtro_datai) . "'";
    }

    $filtro_dataf   = $_GET['dataf'];
    if($filtro_dataf != "") {
        $filtro_sql_os  .= " AND $filtro_periodo_os_fim <= '" . DataSQL($filtro_dataf) . "'";
    }

    if(($filtro_datai != "") and ($filtro_dataf != "")) {
        $filtro_desc .= "<li>" . $filtro_periodo_desc . " {$ling['de']} $filtro_datai {$ling['rel_desc_ate']} $filtro_dataf</li>\n";
    }
    elseif($filtro_datai != "") {
        $filtro_desc .= "<li>" . $filtro_periodo_desc . " {$ling['de']} $filtro_datai</li>\n";
    }
    elseif($filtro_dataf != "") {
        $filtro_desc .= "<li>" . $filtro_periodo_desc . " {$ling['rel_desc_ate']} $filtro_datai</li>\n";
    }


    // ESTAT�STICAS
    $estatisticas[EMPRESAS]       = array();
    $estatisticas[EMPRESAS]['DESC_TAB'] = 'NOME';
    
    $estatisticas[AREAS]          = array();
    $estatisticas[AREAS]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[SETORES]        = array();
    $estatisticas[SETORES]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[MAQUINAS]       = array();
    $estatisticas[MAQUINAS]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[MAQUINAS_CONJUNTO]       = array();
    $estatisticas[MAQUINAS_CONJUNTO]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[EQUIPAMENTOS]       = array();
    $estatisticas[EQUIPAMENTOS]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[FUNCIONARIOS]   = array();
    $estatisticas[FUNCIONARIOS]['DESC_TAB'] = 'NOME';
    
    $estatisticas[ESPECIALIDADES] = array();
    $estatisticas[ESPECIALIDADES]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[TIPOS_SERVICOS] = array();
    $estatisticas[TIPOS_SERVICOS]['DESC_TAB'] = 'DESCRICAO';
    
    // PARA SISTEMATICAS
    $estatisticas[PLANO_PADRAO] = array();
    $estatisticas[PLANO_PADRAO]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[PLANO_ROTAS] = array();
    $estatisticas[PLANO_ROTAS]['DESC_TAB'] = 'DESCRICAO';
    
    $estatisticas[MATERIAIS] = array();
    $estatisticas[MATERIAIS]['DESC_TAB'] = 'DESCRICAO';


    // Contador
    $i = 0;

    $sql_os = "SELECT O.* FROM " . ORDEM . " O $filtro_rel_tabela WHERE 1 = 1 $filtro_sql_os $group_sql_os ORDER BY $ordem_sql_os DESC";
    if (! $rs_os = $dba[$tdb[ORDEM]['dba']]->Execute($sql_os)) {
        erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']]->ErrorMsg() . "<br />" . $sql_os);
    }
    while(! $rs_os->EOF) {
        $cc_os = $rs_os->fields;
        

        // TIRANDO CARCTERES ESPECIAIS
        foreach ($cc_os as $key => $val) {
            $cc_os[$key] = htmlentities($val);
            if($cc_os[$key] === "") {
                $cc_os[$key] = "&nbsp;";
            }
        }
        
        // SE FOR SITEMATICA RECUPERA O PLANO
        if ($cc_os['MID_PROGRAMACAO'] != 0) {
            $plano_mid = VoltaValor(PROGRAMACAO, 'MID_PLANO', 'MID', $cc_os['MID_PROGRAMACAO']);
            
            if ($cc_os['TIPO'] == 1) {
                $cc_os['MID_PLANO'] = array (
                 'MID' => $plano_mid,
                 'DESCRICAO' => VoltaValor(PLANO_PADRAO, 'DESCRICAO', 'MID', $plano_mid)
                );
            }
            elseif ($cc_os['TIPO'] == 2) {
                $cc_os['MID_PLANO'] = array (
                 'MID' => $plano_mid,
                 'DESCRICAO' => VoltaValor(PLANO_ROTAS, 'DESCRICAO', 'MID', $plano_mid)
                );
            }
        }

        // TRATANDO OS CAMPOS
        $cc_os['MID_EMPRESA'] = array (
         'MID' => $cc_os['MID_EMPRESA'],
         'COD' => VoltaValor(EMPRESAS, 'COD', 'MID', $cc_os['MID_EMPRESA']),
         'DESCRICAO' => VoltaValor(EMPRESAS, 'NOME', 'MID', $cc_os['MID_EMPRESA'])
        );
        
        $cc_os['MID_AREA'] = array (
         'MID' => $cc_os['MID_AREA'],
         'DESCRICAO' => VoltaValor(AREAS, 'DESCRICAO', 'MID', $cc_os['MID_AREA'])
        );
        
        $cc_os['MID_SETOR'] = array (
         'MID' => $cc_os['MID_SETOR'],
         'DESCRICAO' => VoltaValor(SETORES, 'DESCRICAO', 'MID', $cc_os['MID_SETOR'])
        );
        
        $cc_os['MID_MAQUINA'] = array (
            'MID' => $cc_os['MID_MAQUINA'],
            'DESCRICAO' => htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $cc_os['MID_MAQUINA']))
        );
        
        $cc_os['MID_CONJUNTO'] = array (
            'MID' => $cc_os['MID_CONJUNTO'],
            'DESCRICAO' => htmlentities(VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $cc_os['MID_CONJUNTO']))
        );

        $cc_os['STATUS'] =  VoltaValor(ORDEM_STATUS, 'DESCRICAO', 'MID', $cc_os['STATUS']);
        
       $cc_os['MID_EQUIPAMENTO'] = array (
         'MID' => $cc_os['MID_EQUIPAMENTO'],
         'COD' => VoltaValor(EQUIPAMENTOS, 'COD', 'MID', $cc_os['MID_EQUIPAMENTO']),
         'DESCRICAO' => VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $cc_os['MID_EQUIPAMENTO'])
        );
        
        $cc_os['NATUREZA'] = array (
         'MID' => $cc_os['NATUREZA'],
         'DESCRICAO' => VoltaValor(NATUREZA_SERVICOS, 'DESCRICAO', 'MID', $cc_os['NATUREZA'])
        );
        
        $cc_os['RESPONSAVEL'] = array (
         'MID' => $cc_os['RESPONSAVEL'],
         'DESCRICAO' => VoltaValor(FUNCIONARIOS, 'NOME', 'MID', $cc_os['RESPONSAVEL'])
        );
        
        if ($cc_os['TIPO'] == 0) {
            $cc_os['TIPO_SERVICO'] = array (
             'MID' => $cc_os['TIPO_SERVICO'],
             'DESCRICAO' => VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $cc_os['TIPO_SERVICO'])
            );
        }
        elseif($cc_os['TIPO'] == 4) {
            $cc_os['TIPO_SERVICO'] = array (
             'MID' => $cc_os['TIPO_SERVICO'],
             'DESCRICAO' => VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $cc_os['TIPO']) . " (" . VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $cc_os['TIPO_SERVICO']) . ")",
             'DESCRICAO2' => VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $cc_os['TIPO_SERVICO'])
            );
        }
        else {
            $cc_os['TIPO_SERVICO'] = array (
             'MID' => $cc_os['TIPO'],
             'DESCRICAO' => VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $cc_os['TIPO']) . "<br />" . $cc_os['MID_PLANO']['DESCRICAO']
            );
        }

        $cc_os['DATA_PROG'] = NossaData($cc_os['DATA_PROG']);
        $cc_os['DATA_ABRE'] = NossaData($cc_os['DATA_ABRE']);

        $cc_os['DEFEITO'] = array (
         'MID' => $cc_os['DEFEITO'],
         'DESCRICAO' => VoltaValor(DEFEITO, 'DESCRICAO', 'MID', $cc_os['DEFEITO'])
        );

        $cc_os['CAUSA'] = array (
         'MID' => $cc_os['CAUSA'],
         'DESCRICAO' => VoltaValor(CAUSA, 'DESCRICAO', 'MID', $cc_os['CAUSA'])
        );

        $cc_os['SOLUCAO'] = array (
         'MID' => $cc_os['SOLUCAO'],
         'DESCRICAO' => VoltaValor(SOLUCAO, 'DESCRICAO', 'MID', $cc_os['SOLUCAO'])
        );
        
        
        
        // ESTATISTICAS
        // EMPRESA
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['DESC'] = $cc_os['MID_EMPRESA']['DESCRICAO'];
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[EMPRESAS][$cc_os['MID_EMPRESA']['MID']]['CONT'] ++;
        
        // LOCAL 1
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['DESC'] = $cc_os['MID_AREA']['DESCRICAO'];
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[AREAS][$cc_os['MID_AREA']['MID']]['CONT'] ++;
        
        // LOCAL 2
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['DESC'] = $cc_os['MID_SETOR']['DESCRICAO'];
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[SETORES][$cc_os['MID_SETOR']['MID']]['CONT'] ++;
        
        // Maquina
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['DESC'] = $cc_os['MID_MAQUINA']['DESCRICAO'];
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[MAQUINAS][$cc_os['MID_MAQUINA']['MID']]['CONT'] ++;
        
        // Conjunto
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['DESC'] = $cc_os['MID_MAQUINA_CONJUNTO']['DESCRICAO'];
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[MAQUINAS_CONJUNTO][$cc_os['MID_MAQUINA_CONJUNTO']['MID']]['CONT'] ++;
        
        // Componente
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['DESC'] = $cc_os['MID_EQUIPAMENTO']['DESCRICAO'];
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
        $estatisticas[EQUIPAMENTOS][$cc_os['MID_EQUIPAMENTO']['MID']]['CONT'] ++;
        
        // SERVI�OS N�O SISTEMATICOS E SISTEMATICOS
        if (($cc_os['TIPO'] == 0) or ($cc_os['TIPO'] == 4)) {
            // Tipo de Servi�o
            if ($cc_os['TIPO'] == 4) {
                $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['DESC'] = $cc_os['TIPO_SERVICO']['DESCRICAO2'];
            }
            else {
                $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['DESC'] = $cc_os['TIPO_SERVICO']['DESCRICAO'];
            }
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
            $estatisticas[TIPOS_SERVICOS][$cc_os['TIPO_SERVICO']['MID']]['CONT'] ++;
        }
        elseif ($cc_os['TIPO'] == 1) {
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['DESC'] = $cc_os['MID_PLANO']['DESCRICAO'];
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
            $estatisticas[PLANO_PADRAO][$cc_os['MID_PLANO']['MID']]['CONT'] ++;
        }
        elseif ($cc_os['TIPO'] == 2) {
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['DESC'] = $cc_os['MID_PLANO']['DESCRICAO'];
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['PREV'] += TempoPrevOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['TS']   += $cc_os['TEMPO_TOTAL'];
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['HH']   += TempoHHOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['CMO']  += CustoMoOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['PARD'] += TempoParadaOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['CMAT'] += CustoMatOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['CUST'] += CustoOutrosOS($cc_os['MID']);
            $estatisticas[PLANO_ROTAS][$cc_os['MID_PLANO']['MID']]['CONT'] ++;
        }

       
    
        if (count($mostrar_os) > 0) {
            $txt .= <<<GERALOS
<table width="100%" class="tab_geralos" border="0" cellpadding="5" cellspacing="0">
<thead>            
<tr>
    <th width="35%" colspan="2" align="left" nowrap="nowrap">{$ling['rel_desc_os_num']}: {$cc_os['NUMERO']} ({$cc_os['STATUS']})</th>
    <th width="35%" colspan="2">{$cc_os['TIPO_SERVICO']['DESCRICAO']}</th>
    <th colspan="2" align="right">{$tdb[ORDEM]['DATA_ABRE']}: {$cc_os['DATA_ABRE']} {$cc_os['HORA_ABRE']}<br />{$tdb[ORDEM]['DATA_PROG']}: {$cc_os['DATA_PROG']}</th>
</tr>
</thead>
<tbody>
GERALOS;
        }
        
        if($mostrar_os['maq'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['MID_MAQUINA']}:</td>
    <td colspan="5">{$cc_os['MID_MAQUINA']['DESCRICAO']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['conj'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['MID_CONJUNTO']}:</td>
    <td colspan="5">{$cc_os['MID_CONJUNTO']['DESCRICAO']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['equip'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['MID_EQUIPAMENTO']}:</td>
    <td colspan="5">{$cc_os['MID_EQUIPAMENTO']['COD']}-{$cc_os['MID_EQUIPAMENTO']['DESCRICAO']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['nat'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['NATUREZA']}:</td>
    <td colspan="5">{$cc_os['NATUREZA']['DESCRICAO']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['sol'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['SOLICITANTE']}:</td>
    <td colspan="5">{$cc_os['SOLICITANTE']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['resp'] == 1){
            $txt .= <<<GERALOS
<tr>
    <td>{$tdb[ORDEM]['RESPONSAVEL']}:</td>
    <td colspan="5">{$cc_os['RESPONSAVEL']['DESCRICAO']}</td>
</tr>
GERALOS;
        }

        if($mostrar_os['texto'] == 1){
            $txt .= "
<tr>
    <td>{$tdb[ORDEM]['TEXTO']}:</td>
    <td colspan=\"5\">" . nl2br($cc_os['TEXTO']) . "</td>
</tr>";
        }

        if($mostrar_os['defeito'] == 1){
            $txt .= "
<tr>
    <td>{$tdb[ORDEM]['DEFEITO']}:</td>
    <td colspan=\"5\">
    {$cc_os['DEFEITO']['DESCRICAO']}
    " . (($cc_os['DEFEITO_TEXTO'] != "")? "<br />" . nl2br($cc_os['DEFEITO_TEXTO']) : "") . "
    </td>
</tr>";
        }

        if($mostrar_os['causa'] == 1){
            $txt .= "
<tr>
    <td>{$tdb[ORDEM]['CAUSA']}:</td>
    <td colspan=\"5\">
    {$cc_os['CAUSA']['DESCRICAO']}
    " . (($cc_os['CAUSA_TEXTO'] != "")? "<br />" . nl2br($cc_os['CAUSA_TEXTO']) : "") . "
    </td>
</tr>";
        }

        if($mostrar_os['solucao'] == 1){
            $txt .= "
<tr>
    <td>{$tdb[ORDEM]['SOLUCAO']}:</td>
    <td colspan=\"5\">
    {$cc_os['SOLUCAO']['DESCRICAO']}
    " . (($cc_os['SOLUCAO_TEXTO'] != "")? "<br />" . nl2br($cc_os['SOLUCAO_TEXTO']) : "") . "
    </td>
</tr>";
        }
		
       
        // Checa se o tipo de servi�o � por especialidade
       
        $speedcheck = VoltaValor(TIPOS_SERVICOS, "SPEED_CHECK", "MID", $cc_os['TIPO_SERVICO']['MID'], $tdb[TIPOS_SERVICOS]['dba']); 
		
        if($speedcheck == 1){
        	
        	$txt .= "
        	 <tr>
                        <td colspan=\"6\"><strong>" . $tdb[SPEED_CHECK]['DESC'] . ": </strong>".VoltaValor(SPEED_CHECK, "DESCRICAO", "MID",$cc_os["MID_SPEED_CHECK"], $tdb[SPEED_CHECK]['dba'])."<br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
        	
        	
        		
        	$sqlSpeed = "SELECT * FROM ".ORDEM_SPEED_CHECK." WHERE MID_ORDEM = {$cc_os['MID']} ORDER BY ETAPA, NUMERO ASC";
        	if(!$rsSpeed = $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sqlSpeed)){
        		erromsg("Erro ao localizar Atividades programadas em:<br />
        				Arquivo: ".__FILE__."<br />
        				Linha: ".__LINE__."<br />
        				Erro: ".$dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg()."<br />
        				SQL: $sqlSpeed
        				");
        	}
        	elseif(!$rsSpeed->EOF){
        	// Listando as atividades agrupadas pela etapa e est�gio, ordenando pelo n�mero da atividade
        		$etapa = '';
        	
        		while(!$rsSpeed->EOF){
        	$rowSpeed = $rsSpeed->fields;
        	
        	if($etapa != $rowSpeed['ETAPA']){
        	$txt .= " <thead><tr>\n";
        	$txt .= "<td colspan='6' $tdstyle><strong>".VoltaValor(ETAPAS_PLANO, 'DESCRICAO', 'MID', $rowSpeed['ETAPA'], 0)."</strong></td>";
        	$txt .= "</tr>\n";
        	$txt .= "<tr>\n";
        	$txt .= "<td $tdstyle width='5%'><strong>".(html_entity_decode($tdb[SPEED_CHECK_ATIVIDADES]['NUMERO']))."</strong></td>";
        	$txt .= "<td $tdstyle width='50%'><strong>".(html_entity_decode($ling['descricao']))."</strong></td>";
        	$txt .= "<td $tdstyle width='1%'><strong>OK</strong></td>";
        	$txt .= "<td $tdstyle width='1%'><strong>NOK</strong></td>";
        	$txt .= "<td $tdstyle width='1%'><strong>NA</strong></td>";
        	$txt .= "<td $tdstyle width='27%'><strong>".(html_entity_decode($tdb[ORDEM_SPEED_CHECK]['COMENTARIO']))."</strong></td>";
        	$txt .= "</tr> </thead>\n";
        	}
        	
        	$txt .= "<tr>\n";
        	$txt .= "<td $tdstyle>{$rowSpeed['NUMERO']}</td>\n";
        	$txt .= "<td $tdstyle>".htmlentities($rowSpeed['DESCRICAO'])."</td>\n";
        	$txt .= "<td $tdstyle>".($rowSpeed['DIAGNOSTICO'] == 1 ? "<center>X</center>" : "&nbsp;" )."</td>\n";
        	$txt .= "<td $tdstyle>".($rowSpeed['DIAGNOSTICO'] == 2 ? "<center>X</center>" : "&nbsp;" )."</td>\n";
        	$txt .= "<td $tdstyle>".($rowSpeed['DIAGNOSTICO'] == 0 ? "<center>X</center>" : "&nbsp;" )."</td>\n";
        	$txt .= "<td $tdstyle>".htmlentities($rowSpeed['COMENTARIO'])."</td>\n";
        			$txt .= "</tr>\n";
        	
        	$etapa = $rowSpeed['ETAPA'];
        	
        	$rsSpeed->MoveNext();
        		
        	}
        	}
        	else{
        	// Nenhuma atividade encontrada
        	$txt .= "<tr>\n";
        	$txt .= "<td colspan='6' $tdstyle>{$ling['atividades_nao_encontradas']}</td>";
        	$txt .= "</tr>\n";
        	}
        	$txt .= "</table></td></tr>\n";
        	
        	
        }
        
        

        // BUSCANDO DADOS INTERNOS A OS
        // Especialidade prevista
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = " . $cc_os['MID'];
        if (! $rs_tmp = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql_tmp)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
        }
        $ii = 0;
        $total = 0;
        while (! $rs_tmp->EOF) {
            $cc_tmp = $rs_tmp->fields;

            // TIRANDO CARCTERES ESPECIAIS
            foreach ($cc_tmp as $key => $val) {
                $cc_tmp[$key] = htmlentities($val);
                if($cc_tmp[$key] === "") {
                    $cc_tmp[$key] = "&nbsp;";
                }
            }

            // Cabe�alho
            if($mostrar_os['moprev'] == 1) {
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_MO_PREVISTO]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_MO_PREVISTO]['MID_ESPECIALIDADE'] . "</td>
                                <td>" . $tdb[ORDEM_MO_PREVISTO]['QUANTIDADE'] . "</td>
                                <td width=\"1\" nowrap=\"nowrap\">" . $tdb[ORDEM_MO_PREVISTO]['TEMPO'] . " (h)</td>
                            </tr>
                            </thead>
                            <tbody>";
                }
            }

            $esp = VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $cc_tmp['MID_ESPECIALIDADE']);

            if($mostrar_os['moprev'] == 1) {
                $txt .= "<tr>
                <td>" . htmlentities($esp) . "</td>
                <td>" . $cc_tmp['QUANTIDADE'] . "</td>
                <td>" . $cc_tmp['TEMPO'] . "</td>
                </tr>";
            }

            $total += $cc_tmp['TEMPO'];

            
            // Especialidade
            $estatisticas[ESPECIALIDADES][$cc_tmp['MID_ESPECIALIDADE']]['DESC']  = $esp;
            $estatisticas[ESPECIALIDADES][$cc_tmp['MID_ESPECIALIDADE']]['PREV'] += $cc_tmp['TEMPO'];
            
            if(! isset($estatisticas[ESPECIALIDADES][$cc_tmp['MID_ESPECIALIDADE']]['MIDOS'][$cc_os['MID']])) {
                $estatisticas[ESPECIALIDADES][$cc_tmp['MID_ESPECIALIDADE']]['CONT'] ++;
                $estatisticas[ESPECIALIDADES][$cc_tmp['MID_ESPECIALIDADE']]['MIDOS'][$cc_os['MID']] = 1;
            }
                           

            $rs_tmp->MoveNext();
            $ii ++;
        }
        
        if($mostrar_os['moprev'] == 1) {
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"2\">{$ling['ord_total']}</td><td>" . round($total, 2)  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }
        
        // MO prevista
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = " . $cc_os['MID'];
        if (! $rs_tmp = $dba[$tdb[ORDEM_MO_ALOC]['dba']]->Execute($sql_tmp)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
        }
        $ii = 0;
        $total = 0;
        while (! $rs_tmp->EOF) {
            $cc_tmp = $rs_tmp->fields;

            // TIRANDO CARCTERES ESPECIAIS
            foreach ($cc_tmp as $key => $val) {
                $cc_tmp[$key] = htmlentities($val);
                if($cc_tmp[$key] === "") {
                    $cc_tmp[$key] = "&nbsp;";
                }
            }

            // Cabe�alho
            if($mostrar_os['moprev'] == 1) {
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_MO_ALOC]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_MO_ALOC]['MID_FUNCIONARIO'] . "</td>
                                <td>" . $tdb[ORDEM_MO_ALOC]['MID_ESPECIALIDADE'] . "</td>
                                <td width=\"1\" nowrap=\"nowrap\">" . $tdb[ORDEM_MO_ALOC]['TEMPO'] . " (h)</td>
                            </tr>
                            </thead>
                            <tbody>";
                }
            }

            $esp = VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $cc_tmp['MID_ESPECIALIDADE']);
            $func = VoltaValor(FUNCIONARIOS, "NOME", "MID", $cc_tmp['MID_FUNCIONARIO']);

            if($mostrar_os['moprev'] == 1) {
                $txt .= "<tr>
                <td>" . htmlentities($func) . "</td>
                <td>" . htmlentities($esp) . "</td>
                <td>" . $cc_tmp['TEMPO'] . "</td>
                </tr>";
            }

            $total += $cc_tmp['TEMPO'];
            
            // Funcionario
            $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['DESC']  = $func;
            $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['PREV'] += $cc_tmp['TEMPO'];
            
            if(! isset($estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['MIDOS'][$cc_os['MID']])) {
                $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['CONT'] ++;
                $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['MIDOS'][$cc_os['MID']] = 1;
            }

            $rs_tmp->MoveNext();
            $ii ++;
        }
        
        if($mostrar_os['moprev'] == 1) {
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"2\">{$ling['ord_total']}</td><td>" . round($total, 2)  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }
        
        // Material previsto
        $sql_tmp = "SELECT * FROM " . ORDEM_MAT_PREVISTO . " WHERE MID_ORDEM = " . $cc_os['MID'];
        if (! $rs_tmp = $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->Execute($sql_tmp)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
        }
        $ii = 0;
        $total = 0;
        while (! $rs_tmp->EOF) {
            $cc_tmp = $rs_tmp->fields;

            // TIRANDO CARCTERES ESPECIAIS
            foreach ($cc_tmp as $key => $val) {
                $cc_tmp[$key] = htmlentities($val);
                if($cc_tmp[$key] === "") {
                    $cc_tmp[$key] = "&nbsp;";
                }
            }

            // Cabe�alho
            if($mostrar_os['mtprev'] == 1) {
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_MAT_PREVISTO]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL'] . "</td>
                                <td>" . $tdb[MATERIAIS]['UNIDADE'] . "</td>
                                <td width=\"1\" nowrap=\"nowrap\">" . $tdb[ORDEM_MAT_PREVISTO]['QUANTIDADE'] . " (h)</td>
                            </tr>
                            </thead>
                            <tbody>";
                }
            }
            
            if($mostrar_os['mtprev'] == 1) {
                $mat = VoltaValor(MATERIAIS, 'DESCRICAO', 'MID', $cc_tmp['MID_MATERIAL']);
                $uni = VoltaValor(MATERIAIS, 'UNIDADE', 'MID', $cc_tmp['MID_MATERIAL']);
                
                
                $txt .= "<tr>
                <td>" . htmlentities($mat) . "</td>
                <td>" . htmlentities(VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', $uni)) . "</td>
                <td>" . $cc_tmp['QUANTIDADE'] . "</td>
                </tr>";
            }

            $total += $cc_tmp['QUANTIDADE'];

            $rs_tmp->MoveNext();
            $ii ++;
        }
        if($mostrar_os['mtprev'] == 1) {
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"2\">{$ling['ord_total']}</td><td>" . round($total, 2)  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }
        
        // PREVENTIVA
        if (($cc_os['TIPO'] == 1) and ($mostrar_os['atividade'] == 1)) {
            // Cabe�alho
            $txt .= "
            <tr>
            <td colspan=\"6\"><strong>" . $tdb[ORDEM_PREV]['DESC'] . ":</strong><br />
            <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
            <thead>
            <tr>
            <td>{$tdb[ORDEM_PREV]['NUMERO']}</td>
            <td>{$tdb[ORDEM_PREV]['TAREFA']}</td>
            <td>{$tdb[ORDEM_PREV]['INSTRUCAO_DE_TRABALHO']}</td>
            <td>{$tdb[ORDEM_PREV]['ESPECIALIDADE']}</td>
            <td>{$tdb[ORDEM_PREV]['QUANTIDADE_MO']}</td>
            <td>{$tdb[ORDEM_PREV]['TEMPO_PREVISTO']}</td>
            <td>{$tdb[ORDEM_PREV]['DIAGNOSTICO']}</td>
            </tr>
            </thead>
            <tbody>";

            
            // ATIVIDADES
            $sql_tmp = "SELECT * FROM " . ORDEM_PREV . " WHERE MID_ORDEM = " . $cc_os['MID'] . " ORDER BY NUMERO ASC";
            if (! $rs_tmp = $dba[$tdb[ORDEM_PREV]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_PREV]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $conj_ant = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }
                
                if ($conj_ant != $cc_tmp['MID_CONJUNTO']) {
                    $txt .= "<tr>
                    <td colspan=\"5\">" . htmlentities(VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $cc_tmp['MID_CONJUNTO'])) . "</td>
                    </tr>";
                    $conj_ant = $cc_tmp['MID_CONJUNTO'];
                }

                $txt .= "<tr>
                <td>" . $cc_tmp['NUMERO'] . "</td>
                <td>" . $cc_tmp['TAREFA'] . "</td>
                <td>" . $cc_tmp['INSTRUCAO_DE_TRABALHO'] . "</td>
                <td>" . htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $cc_tmp['ESPECIALIDADE'])) . "</td>
                <td>" . $cc_tmp['QUANTIDADE_MO'] . "</td>
                <td>" . $cc_tmp['TEMPO_PREVISTO'] . "</td>
                <td>" . (($cc_tmp['DIAGNOSTICO'] == 1)? $ling['ok'] : $ling['nao_ok']) . "</td>
                </tr>";

                $rs_tmp->MoveNext();
                $ii ++;
            }

            $txt .= "
            </tbody>
            </table>
            </td>
            </tr>";
        }
        
        // ROTA
        if ($cc_os['TIPO'] == 2) {
            if ($mostrar_os['atividade'] == 1) {
                // Cabe�alho
                $txt .= "
                <tr>
                <td colspan=\"6\"><strong>" . $tdb[ORDEM_LUB]['DESC'] . ":</strong><br />
                <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                <thead>
                <tr>
                <td>".$tdb[LINK_ROTAS]['TIPO']."</td>
                <td>".$tdb[LINK_ROTAS]['NUMERO']."</td>
                <td>".$tdb[LINK_ROTAS]['MID_PONTO']."</td>
                <td>".$tdb[PONTOS_LUBRIFICACAO]['NPONTO']."</td>
                <td>".$tdb[LINK_ROTAS]['MID_CONJUNTO']."</td>
                <td>".$tdb[LINK_ROTAS]['MID_MATERIAL']."</td>
                <td>".$tdb[LINK_ROTAS]['QUANTIDADE']."</td>
                <td>".$tdb[LINK_ROTAS]['TAREFA']."</td>
                <td>".$tdb[LINK_ROTAS]['DIAGNOSTICO']."</td>
                </tr>
                </thead>
                <tbody>";
            }
            
            // ATIVIDADES
            $sql_tmp = "SELECT L.* FROM " . ORDEM_LUB . " L, " . ROTEIRO_ROTAS . " R WHERE L.MID_MAQUINA = R.MID_MAQUINA AND L.MID_PLANO = R.MID_PLANO AND L.MID_ORDEM = " . $cc_os['MID'] . " ORDER BY R.POSICAO, L.NUMERO ASC";
            if (! $rs_tmp = $dba[$tdb[ORDEM_PREV]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_PREV]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $maq_ant = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }
                
                if ($maq_ant != $cc_tmp['MID_MAQUINA']) {
                    $txt .= "<tr>
                    <td colspan=\"9\">" . htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $cc_tmp['MID_MAQUINA'])) . "</td>
                    </tr>";
                    $maq_ant = $cc_tmp['MID_MAQUINA'];
                }
                
                //UNIDADE
                $uni = VoltaValor(MATERIAIS, 'UNIDADE', 'MID', $cc_tmp['MID_MATERIAL']);
                $uni = VoltaValor(MATERIAIS_UNIDADE, 'DESCRICAO', 'MID', $uni);
                $uni_mostra = ($uni != '')? "($uni)" : "";
                
                if ($mostrar_os['atividade'] == 1) {
                    $txt .= "<tr>
                    <td width=\"1\">" . (($cc_tmp['TIPO'] == 1)? "<img src=\"../imagens/icones/22x22/lubrificacao.png\" title=\"{$ling['rel_desc_lub']}\" />" : "<img src=\"../imagens/icones/22x22/inspecao.png\" title=\"{$ling['rel_desc_insp']}\" />") . "</td>
                    <td>" . $cc_tmp['NUMERO'] . "</td>
                    <td>" . htmlentities(VoltaValor(PONTOS_LUBRIFICACAO, 'PONTO', 'MID', $cc_tmp['MID_PONTO'])) . "</td>
                    <td>" . htmlentities(VoltaValor(PONTOS_LUBRIFICACAO, 'NPONTO', 'MID', $cc_tmp['MID_PONTO'])) . "</td>
                    <td>" . htmlentities(VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $cc_tmp['MID_CONJUNTO'])) . "</td>
                    <td>" . htmlentities(VoltaValor(MATERIAIS, 'DESCRICAO', 'MID', $cc_tmp['MID_MATERIAL'])) . "</td>
                    <td>" . $cc_tmp['QUANTIDADE'] . " $uni_mostra</td>
                    <td>" . $cc_tmp['TAREFA'] . "</td>
                    <td>" . (($cc_tmp['DIAGNOSTICO'] == 1)? "OK" : "&Ntilde; OK") . "</td>
                    </tr>";
                }
                
                // ESTATISTICA POR MATERIAIS
                if ((int)$cc_tmp['MID_MATERIAL'] != 0) {
                    // Custo unitario
                    $custo_uni = (float)VoltaValor(MATERIAIS, 'CUSTO_UNITARIO', 'MID', $cc_tmp['MID_MATERIAL']);
                    
                    $estatisticas[MATERIAIS][$cc_tmp['MID_MATERIAL']]['DESC']   = VoltaValor(MATERIAIS, 'DESCRICAO', 'MID', $cc_tmp['MID_MATERIAL']);
                    $estatisticas[MATERIAIS][$cc_tmp['MID_MATERIAL']]['UNI']    = $uni;
                    $estatisticas[MATERIAIS][$cc_tmp['MID_MATERIAL']]['QUANT'] += $cc_tmp['QUANTIDADE'];
                    $estatisticas[MATERIAIS][$cc_tmp['MID_MATERIAL']]['CUSTO'] += $cc_tmp['QUANTIDADE'] * $custo_uni;
                }
                
                $rs_tmp->MoveNext();
                $ii ++;
            }
            
            if ($mostrar_os['atividade'] == 1) {
                $txt .= "
                </tbody>
                </table>
                </td>
                </tr>";
            }
        }
        
        // PARADA DE OBJETO DE MANUTEN��O        
        if($mostrar_os['parada'] == 1) {                        
            // PARADA DE OBJ DE MANUTEN��O
            if ($filtro_periodo != 5) {
                $sql_tmp = "SELECT * FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = " . $cc_os['MID'];
            }
            else {
                if($filtro_datai != "") {
                    $filtro_tmp  .= " AND DATA_INICIO >= '" . DataSQL($filtro_datai) . "'";
                }

                if($filtro_dataf != "") {
                    $filtro_tmp  .= " AND DATA_FINAL <= '" . DataSQL($filtro_dataf) . "'";
                }
                
                $sql_tmp = "SELECT * FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = " . $cc_os['MID'] . "" . $filtro_tmp;
            }
            
            if (! $rs_tmp = $dba[$tdb[ORDEM_MAQ_PARADA]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAQ_PARADA]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $total = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }
                
                // Cabe�alho
                if ($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_MAQ_PARADA]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_MAQ_PARADA]['DATA_INICIO'] . "</td>
                                <td>" . $tdb[ORDEM_MAQ_PARADA]['HORA_INICIO'] . "</td>
                                <td>" . $tdb[ORDEM_MAQ_PARADA]['DATA_FINAL'] . "</td>
                                <td>" . $tdb[ORDEM_MAQ_PARADA]['HORA_FINAL'] . "</td>
                                <td width=\"1\" nowrap=\"nowrap\">{$ling['tempo_h']}</td>
                            </tr>
                            </thead>
                            <tbody>";
                }

                $datai = NossaData($cc_tmp['DATA_INICIO']);
                $dataf = NossaData($cc_tmp['DATA_FINAL']);

                $mki = VoltaTime($cc_tmp['HORA_INICIO'], $data);
                $mkf = VoltaTime($cc_tmp['HORA_FINAL'], $data);

                $tempo = round(($mkf - $mki) / 60 / 60, 2);

                $txt .= "<tr>
                <td>" . $datai . "</td>
                <td>" . $cc_tmp['HORA_INICIO'] . "</td>
                <td>" . $dataf . "</td>
                <td>" . $cc_tmp['HORA_FINAL'] . "</td>
                <td>" . $tempo . "</td>
                </tr>";
            
                

                $total += $tempo;

                $rs_tmp->MoveNext();
                $ii ++;
            }
            
            if ($ii > 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"4\">{$ling['total_h']}</td><td>" . round($total, 2)  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }
        

        // MO apontada
        if ($filtro_periodo != 5) {
            $sql_tmp = "SELECT * FROM " . ORDEM_MADODEOBRA . " WHERE MID_ORDEM = " . $cc_os['MID'];
        }
        else {
            if($filtro_datai != "") {
                $filtro_tmp  .= " AND DATA_INICIO >= '" . DataSQL($filtro_datai) . "'";
            }

            if($filtro_dataf != "") {
                $filtro_tmp  .= " AND DATA_FINAL <= '" . DataSQL($filtro_dataf) . "'";
            }
            
            $sql_tmp = "SELECT * FROM " . ORDEM_MADODEOBRA . " WHERE MID_ORDEM = " . $cc_os['MID'] . "" . $filtro_tmp;
        }
        if (! $rs_tmp = $dba[$tdb[ORDEM_MADODEOBRA]['dba']]->Execute($sql_tmp)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MADODEOBRA]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
        }
        $ii = 0;
        $total = 0;
        while (! $rs_tmp->EOF) {
            $cc_tmp = $rs_tmp->fields;

            // TIRANDO CARCTERES ESPECIAIS
            foreach ($cc_tmp as $key => $val) {
                $cc_tmp[$key] = htmlentities($val);
                if($cc_tmp[$key] === "") {
                    $cc_tmp[$key] = "&nbsp;";
                }
            }

            // Cabe�alho
            if($mostrar_os['maodeobra'] == 1) {
                if($ii == 0) {
                    $txt .= "
                <tr>
                    <td colspan=\"6\"><strong>" . $tdb[ORDEM_MADODEOBRA]['DESC'] . ":</strong><br />
                    <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                        <thead>
                        <tr>
                            <td>" . $tdb[ORDEM_MADODEOBRA]['MID_FUNCIONARIO'] . "</td>
                            <td>" . $tdb[FUNCIONARIOS]['ESPECIALIDADE'] . "</td>
                            <td>" . $tdb[ORDEM_MADODEOBRA]['DATA_INICIO'] . "</td>
                            <td>" . $tdb[ORDEM_MADODEOBRA]['HORA_INICIO'] . "</td>
                            <td>" . $tdb[ORDEM_MADODEOBRA]['DATA_FINAL'] . "</td>
                            <td>" . $tdb[ORDEM_MADODEOBRA]['HORA_FINAL'] . "</td>
                            <td width=\"1\" nowrap=\"nowrap\">{$ling['tempo_h']}</td>
                        </tr>
                        </thead>
                        <tbody>";
                }
            }

            $esp = VoltaValor(FUNCIONARIOS, "ESPECIALIDADE", "MID", $cc_tmp['MID_FUNCIONARIO']);
            $func = VoltaValor(FUNCIONARIOS, "NOME", "MID", $cc_tmp['MID_FUNCIONARIO']);

            $datai = NossaData($cc_tmp['DATA_INICIO']);
            $dataf = NossaData($cc_tmp['DATA_FINAL']);

            $mki = VoltaTime($cc_tmp['HORA_INICIO'], $datai);
            $mkf = VoltaTime($cc_tmp['HORA_FINAL'], $dataf);

            $tempo = round(($mkf - $mki) / 60 / 60, 2);

            if($mostrar_os['maodeobra'] == 1) {
                $txt .= "<tr>
                <td>" . htmlentities($func) . "</td>
                <td>" . htmlentities(VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $esp)) . "</td>
                <td>" . $datai . "</td>
                <td>" . $cc_tmp['HORA_INICIO'] . "</td>
                <td>" . $dataf . "</td>
                <td>" . $cc_tmp['HORA_FINAL'] . "</td>
                <td>" . $tempo . "</td>
                </tr>";
            }
            
         
            // Funcion�rio
            $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['DESC'] = $func;
            $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['HH']  += $tempo;
            $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['CMO'] += round($tempo * $valor_hora, 2);
            
            if(! isset($estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['MIDOS'][$cc_os['MID']])) {
                $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['CONT'] ++;
                $estatisticas[FUNCIONARIOS][$cc_tmp['MID_FUNCIONARIO']]['MIDOS'][$cc_os['MID']] = 1;
            }

            // Especialidade
            $estatisticas[ESPECIALIDADES][$esp]['DESC'] = VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $esp);
            $estatisticas[ESPECIALIDADES][$esp]['HH']  += $tempo;
            $estatisticas[ESPECIALIDADES][$esp]['CMO'] += round($tempo * $valor_hora, 2);
            
            if(! isset($estatisticas[ESPECIALIDADES][$esp]['MIDOS'][$cc_os['MID']])) {
                $estatisticas[ESPECIALIDADES][$esp]['CONT'] ++;
                $estatisticas[ESPECIALIDADES][$esp]['MIDOS'][$cc_os['MID']] = 1;
            }

            $total += $tempo;

            $rs_tmp->MoveNext();
            $ii ++;
        }
        if($mostrar_os['maodeobra'] == 1) {
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"6\">{$ling['total_h']}</td><td>" . round($total, 2)  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }

        // Materiais
        if($mostrar_os['material'] == 1) {
            $sql_tmp = "SELECT * FROM " . ORDEM_MATERIAL . " WHERE MID_ORDEM = " . $cc_os['MID'];
            if (! $rs_tmp = $dba[$tdb[ORDEM_MATERIAL]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MATERIAL]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $total = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }

                // Cabe�alho
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_MATERIAL]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_MATERIAL]['MID_ALMOXARIFADO'] . "</td>
                                <td>" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . "</td>
                                <td>" . $tdb[ORDEM_MATERIAL]['QUANTIDADE'] . "</td>
                                <td>" . $tdb[ORDEM_MATERIAL]['CUSTO_UNITARIO'] . " (R$)</td>
                                <td>" . $tdb[ORDEM_MATERIAL]['CUSTO_TOTAL'] . " (R$)</td>
                            </tr>
                            </thead>
                            <tbody>";
                }
                
                $mat = VoltaValor(MATERIAIS, "DESCRICAO", "MID", $cc_tmp['MID_MATERIAL']);


                $txt .= "<tr>
                <td>" . htmlentities(VoltaValor(ALMOXARIFADO, "DESCRICAO", "MID", $cc_tmp['MID_ALMOXARIFADO'])) . "</td>
                <td>" . htmlentities($mat) . "</td>
                <td>" . $cc_tmp['QUANTIDADE'] . "</td>
                <td>" . number_format($cc_tmp['CUSTO_UNITARIO'], 2, ',', '.') . "</td>
                <td>" . number_format($cc_tmp['CUSTO_TOTAL'], 2, ',', '.') . "</td>
                </tr>";

                $total += $cc_tmp['CUSTO_TOTAL'];

                $rs_tmp->MoveNext();
                $ii ++;
            }
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td colspan=\"4\">{$ling['ord_total']} ({$ling['rel_desc_sifrao']})</td><td>" . number_format(round($total, 2), 2, ',', '.')  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }

        // Outros custos
        if($mostrar_os['custo'] == 1) {
            $sql_tmp = "SELECT * FROM " . ORDEM_CUSTOS . " WHERE MID_ORDEM = " . $cc_os['MID'];
            if (! $rs_tmp = $dba[$tdb[ORDEM_CUSTOS]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_CUSTOS]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $total = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }

                // Cabe�alho
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_CUSTOS]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_CUSTOS]['DESCRICAO'] . "</td>
                                <td>" . $tdb[ORDEM_CUSTOS]['CUSTO'] . " ({$ling['rel_desc_sifrao']})</td>
                            </tr>
                            </thead>
                            <tbody>";
                }


                $txt .= "<tr>
                <td>" . $cc_tmp['DESCRICAO'] . "</td>
                <td>" . number_format($cc_tmp['CUSTO'], 2, ',', '.') . "</td>
                </tr>";

                $total += $cc_tmp['CUSTO'];

                $rs_tmp->MoveNext();
                $ii ++;
            }
            if($ii != 0) {
                $txt .= "
                            </tbody>
                            <tfoot>
                                <tr><td>{$ling['ord_total']} ({$ling['rel_desc_sifrao']})</td><td>" . number_format(round($total, 2), 2, ',', '.')  . "</td></tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>";
            }
        }

        // REPROGRAMA��ES
        if($mostrar_os['reprog'] == 1) {
            $sql_tmp = "SELECT * FROM " . ORDEM_REPROGRAMA . " WHERE MID_ORDEM = " . $cc_os['MID'];
            if (! $rs_tmp = $dba[$tdb[ORDEM_REPROGRAMA]['dba']]->Execute($sql_tmp)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . "<br  />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_REPROGRAMA]['dba']]->ErrorMsg() . "<br />" . $sql_tmp);
            }
            $ii = 0;
            $total = 0;
            while (! $rs_tmp->EOF) {
                $cc_tmp = $rs_tmp->fields;

                // TIRANDO CARCTERES ESPECIAIS
                foreach ($cc_tmp as $key => $val) {
                    $cc_tmp[$key] = htmlentities($val);
                    if($cc_tmp[$key] === "") {
                        $cc_tmp[$key] = "&nbsp;";
                    }
                }

                // Cabe�alho
                if($ii == 0) {
                    $txt .= "
                    <tr>
                        <td colspan=\"6\"><strong>" . $tdb[ORDEM_REPROGRAMA]['DESC'] . ":</strong><br />
                        <table width=\"100%\" class=\"tab_geralos_sub\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n
                            <thead>
                            <tr>
                                <td>" . $tdb[ORDEM_REPROGRAMA]['MOTIVO'] . "</td>
                                <td>" . $tdb[ORDEM_REPROGRAMA]['DATA'] . "</td>
                            </tr>
                            </thead>
                            <tbody>";
                }


                $txt .= "<tr>
                <td>" . $cc_tmp['MOTIVO'] . "</td>
                <td>" . NossaData($cc_tmp['DATA']) . "</td>
                </tr>";

                $rs_tmp->MoveNext();
                $ii ++;
            }
            if($ii != 0) {
                $txt .= "
                            </tbody>
                        </table>
                    </td>
                </tr>";
            }
        }

       
        if (count($mostrar_os) > 0) {
            $txt .= "</table>
            <br />";
        }
        
        $rs_os->MoveNext();
        $i ++;
    }



    // MOSTRANDO AS ESTATISTICAS
    $txt .= "<h3 class=\"geralos\">{$ling['rel_desc_estatisticas']}</h3>";
    $txt .= "<br />
    <div style=\"padding-left:2px;\">";

    foreach ($estatisticas as $tab => $def) {
        // Consumo em lubrifica��o
        if ($tab == MATERIAIS) {
            $txt .= <<<GERALOS
        <table width="100%" class="tab_geralos_sub" border="0" cellpadding="5" cellspacing="0">
        <thead>
        <tr>
            <th colspan="4" align="left">{$ling['rel_desc_consumo_mat_lub']}</th>
        </tr>
        <tr>
            <td width="50%">{$tdb[$tab][$def['DESC_TAB']]}</td>
            <td>{$ling['rel_desc_uni']}.</td>
            <td>{$ling['qtd']}</td>
            <td>{$ling['rel_desc_custo']}</td>
        </tr>
        </thead>
        <tbody>
GERALOS;
            
            // Tirando esse
            unset($def['DESC_TAB']);
            
            // Totais
            $quant = 0;
            $custo = 0;
            
            // Listando
            foreach ($def as $est) {
                $txt .= "<tr>
                    <td>" . $est['DESC'] . "</td>
                    <td>" . $est['UNI'] . "</td>
                    <td>" . $est['QUANT'] . "</td>
                    <td>" . number_format($est['CUSTO'], 2, ',', '.') . "</td>
                </tr>";
                
                // Totais
                $quant += $est['QUANT'];
                $custo += $est['CUSTO'];
            }
            
            $txt .= "<tr>
                    <td colspan=\"2\">TOTAL</td>
                    <td>" . $quant . "</td>
                    <td>" . number_format($custo, 2, ',', '.') . "</td>
                </tr>";

            $txt .= <<<GERALOS
        </tbody>
        </table>
        <br />
GERALOS;
        }
        else {
            $txt .= <<<GERALOS
        <table width="100%" class="tab_geralos_sub" border="0" cellpadding="5" cellspacing="0">
        <thead>
        <tr>
            <th colspan="9" align="left">POR {$tdb[$tab]['DESC']}</th>
        </tr>
        <tr>
            <td width="50%">{$tdb[$tab][$def['DESC_TAB']]}</td>
            <td>{$ling['rel_desc_qtd_os']}</td>
            <td>{$ling['rel_desc_temp_prev']}</td>
            <td>{$ling['rel_desc_temp_hh']}</td>
            <td>{$ling['rel_desc_temp_serv']}</td>
            <td>{$ling['rel_desc_temp_parada']}</td>
            <td>{$ling['rel_desc_custo_mo']}</td>
            <td>{$ling['rel_desc_custo_mat']}</td>
            <td>{$ling['rel_desc_custo_outros']}</td>
        </tr>
        </thead>
        <tbody>
GERALOS;

            // Tirando esse
            unset($def['DESC_TAB']);

            // Obter uma lista de colunas
            $desc  = array();
            $cont  = array();
            $prev  = array();
            $hh    = array();
            $ts    = array();
            $tp    = array();
            $mcusto = array();
            $mtcusto = array();
            $ocusto = array();
            foreach ($def as $key => $row) {
                $desc[$key]  = $row['DESC'];
                $cont[$key] = $row['CONT'];
                $prev[$key] = $row['PREV'];
                $hh[$key] = $row['HH'];
                $ts[$key] = $row['TS'];
                $tp[$key] = $row['PARD'];
                $mcusto[$key] = $row['CMO'];
                $mtcusto[$key] = $row['CMAT'];
                $ocusto[$key] = $row['CUST'];
            }

            array_multisort($hh, SORT_NUMERIC, SORT_DESC, $def);

            // Totais
            $cont_tot  = 0;
            $prev_tot  = 0;
            $hh_tot    = 0;
            $ts_tot    = 0;
            $tp_tot    = 0;
            $mcusto_tot = 0;
            $mtcusto_tot = 0;
            $ocusto_tot = 0;
            
            // Listando
            foreach ($def as $est) {
                if($est['DESC'] == "") {
                    continue;
                }

                $txt .= "<tr>
                    <td>" . $est['DESC'] . "</td>
                    <td>" . $est['CONT'] . "</td>
                    <td>" . $est['PREV'] . "</td>
                    <td>" . $est['HH'] . "</td>
                    <td>" . $est['TS'] . "</td>
                    <td>" . $est['PARD'] . "</td>
                    <td>" . number_format($est['CMO'], 2, ',', '.') . "</td>
                    <td>" . number_format($est['CMAT'], 2, ',', '.') . "</td>
                    <td>" . number_format($est['CUST'], 2, ',', '.') . "</td>
                </tr>";
                
                // Totais
                $cont_tot  += $est['CONT'];
                $prev_tot  += $est['PREV'];
                $hh_tot    += $est['HH'];
                $ts_tot    += $est['TS'];
                $tp_tot    += $est['PARD'];
                $mcusto_tot += $est['CMO'];
                $mtcusto_tot += $est['CMAT'];
                $ocusto_tot += $est['CUST'];
            }
            
            $txt .= "<tr>
                    <td>{$ling['ord_total']}</td>
                    <td>" . $cont_tot . "</td>
                    <td>" . $prev_tot . "</td>
                    <td>" . $hh_tot . "</td>
                    <td>" . $ts_tot . "</td>
                    <td>" . $tp_tot . "</td>
                    <td>" . number_format($mcusto_tot, 2, ',', '.') . "</td>
                    <td>" . number_format($mtcusto_tot, 2, ',', '.') . "</td>
                    <td>" . number_format($ocusto_tot, 2, ',', '.') . "</td>
                </tr>";

            $txt .= <<<GERALOS
        </tbody>
        </table>
        <br />
GERALOS;
        }
    }

    $txt .= "</div>";


    $txt .= "
    <style>
    h3.geralos {
        padding:3px;
        font-size:16px;
        text-align:left;
        border-bottom:2px dotted #333;
        border-right:2px dotted #333;
    }

    .tab_geralos {
        border: 1px solid black;
        border-bottom: 0px;
    }

    .tab_geralos td,
    .tab_geralos th {
        border-bottom: 1px solid black;
        vertical-align:top;
    }

    .tab_geralos thead th {
        background-color: #999;
        font-size:10px;
    }

    .tab_geralos tbody th {
        background-color: lightgray;
    }

    .tab_geralos tbody td {
        text-align:left;
    }

    .tab_geralos_sub {
        border: 1px solid black;
        border-bottom: 0px;
        border-collapse:collapse;
    }

    .tab_geralos_sub thead th {
        border: 1px solid black;
        background-color: #999;
        font-size:10px;
    }

    .tab_geralos_sub thead td {
        border: 1px solid black;
        background-color: lightgray;
        text-align:left;
    }

    .tab_geralos_sub td {
        border: 1px solid black;
        vertical-align:top;
        text-align:left;
    }
    </style>";

    // Tratando a exibi��o dos filtros
    if ($filtro_desc == "") $filtro_desc = $ling['nenhum'];
    else $filtro_desc = "<ul style=\"list-style-type:none;\">$filtro_desc</ul>";

    if ($_GET['relatorio'] != "") relatorio_padrao($ling['rel_geral_os'], $filtro_desc, $i, $txt, 1);
    else exportar_word($ling['rel_geral_os'], $filtro_desc, $i, $txt, $_GET['papel_orientacao']);
}
// FILTROS
else {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
    <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n
    </head>
    <body><div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1>{$ling['rel_geral_os']}
    </div>
    <div id=\"corpo_relatorio\">
    <form action=\"relatorio_geralos.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    <fieldset>
    <legend>{$ling['rel_desc_localizacoes']}</legend>
    
    <div id=\"filtro_relatorio\">";
    
    // Filtros b�sicos
    FiltrosRelatorio(1, 1, 1, 1, 1, 1, 1, 0, 0, 1);

    echo "</div>
    
    </fieldset>";

    echo "<fieldset><legend>{$ling['filtros']}</legend>
    
    <label for=\"filtro_numos\">{$ling['rel_desc_num_os2']}:</label>
    <input  type=\"text\" id=\"filtro_numos\" class=\"campo_text\" value=\"".$_GET['filtro_numos']."\" name=\"filtro_numos\" size=\"9\" maxlength=\"9\" /> 
    <br clear=\"all\" />";
    
    echo "<label class=\"campo_label\" for=\"filtro_natureza\">".$tdb[ORDEM_PLANEJADO]['NATUREZA'].":</label>";
    FormSelectD('DESCRICAO','',NATUREZA_SERVICOS,$_GET['filtro_natureza'],'filtro_natureza','filtro_natureza','MID',0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />";

    echo "<label class=\"campo_label \" for=\"filtro_defeito\">".$tdb[ORDEM_PLANEJADO]['DEFEITO'].":</label>";
    FormSelectD('DESCRICAO', '',DEFEITO,$_GET['filtro_defeito'],'filtro_defeito','filtro_defeito','MID',0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />";

    echo "<label class=\"campo_label \" for=\"filtro_causa\">".$tdb[ORDEM_PLANEJADO]['CAUSA'].":</label>";
    FormSelectD('DESCRICAO', '', CAUSA, $_GET['filtro_causa'], 'filtro_causa', 'filtro_causa', 'MID', 0, '', '', '', 'A', '', strtoupper($ling['todos']));
    echo "<br clear=\"all\" />";

    echo "<label class=\"campo_label \" for=\"filtro_solucao\">".$tdb[ORDEM_PLANEJADO]['SOLUCAO'].":</label>";
    FormSelectD('DESCRICAO', '',SOLUCAO,$_GET['filtro_solucao'],'filtro_solucao','filtro_solucao','MID',0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />";

    echo "<label class=\"campo_label \" for=\"filtro_material\">".$tdb[ORDEM_MATERIAL]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'',MATERIAIS,$_GET['filtro_material'],"filtro_material","filtro_material","MID",0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />";
    
    echo "<label for=\"filtro_parada\">".$tdb[ORDEM_MAQ_PARADA]['DESC'].":</label>";
    echo "<select id=\"filtro_parada\" name=\"filtro_parada\" class=\"campo_select\">";
    echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
    echo "<option value=\"1\">{$ling['rel_desc_com_parada']}</option>";
    echo "<option value=\"2\">{$ling['rel_desc_sem_parada']}</option>";
    echo "</select>";
    echo "<br clear=\"all\" />";
        
    echo "</fieldset>";

    echo "<fieldset><legend>{$ling['servicos']}</legend>";
    
    echo "<label class=\"campo_label\" for=\"filtro_mostrar_serv\">{$ling['mostrar']}:</label>";
    echo "<select id=\"filtro_mostrar_serv\" name=\"filtro_mostrar_serv\" class=\"campo_select\" onchange=\"atualiza_area2('servico_sel', '../parametros.php?id=mostra_serv&opcao=' + this.value)\">";
    echo "<option value=\"\">{$ling['todos2']}</option>";
    echo "<option value=\"1\">{$ling['rel_desc_serv_nsist']}</option>";
    echo "<option value=\"2\">{$ling['rel_desc_serv_sist']}</option>";
    echo "</select>";
    echo "<br clear=\"all\" />";
    
    echo "<div id=\"servico_sel\"></div>";
    
    echo "</fieldset>";
    
    echo "<fieldset><legend>{$ling['maodeobra']}</legend>";
    
    echo "<label class=\"campo_label \" for=\"filtro_func\">" . $tdb[FUNCIONARIOS]['DESC'] . ":</label>";
    FormSelectD("NOME",'',FUNCIONARIOS,$_GET['filtro_func'],"filtro_func","filtro_func","MID",0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />
    
    <label class=\"campo_label \" for=\"filtro_equipe\">" . $tdb[EQUIPES]['DESC'] . ":</label>";
    FormSelectD("DESCRICAO",'',EQUIPES,$_GET['filtro_equipe'],"filtro_equipe","filtro_equipe","MID",0,'','','','A','',strtoupper($ling['todos']));
    echo "<br clear=\"all\" />
    
    <label class=\"campo_label \" for=\"filtro_esp\">" . $tdb[ESPECIALIDADES]['DESC'] . ":</label>";
    FormSelectD("DESCRICAO",'',ESPECIALIDADES, $_GET['filtro_esp'], "filtro_esp", "filtro_esp","MID",0,'','','','A','',strtoupper($ling['todos']));
    
    echo "</fieldset>";

    echo "<fieldset><legend>{$ling['ord_visualizar']}</legend>
    
    <label for=\"filtro_areprog\">{$ling['rel_desc_os_repro']}:</label>
    <input class=\"campo_check\" type=\"checkbox\" name=\"filtro_reprog\" id=\"filtro_areprog\" value=\"1\" />
    <br clear=\"all\" />";


    echo "<label for=\"filtro_status_os\">{$ling['ord_status_os']}:</label>";
    echo "<select id=\"filtro_status_os\" name=\"filtro_status_os\" class=\"campo_select\">";
    echo "<option value=\"\">{$ling['rel_desc_todas']}</option>";
    echo "<option value=\"11\">{$ling['rel_desc_todas_cancel']}</option>";
    echo "<option value=\"1\">{$ling['rel_desc_abertas']}</option>";
    echo "<option value=\"2\">{$ling['rel_desc_fechadas']}</option>";
    echo "<option value=\"3\">{$ling['rel_desc_canceladas']}</option>";
    echo "</select>";
    echo "<br clear=\"all\" />";


    echo "</fieldset>
    
    <fieldset><legend>{$ling['rel_desc_mostrar_campos']}</legend>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][maq]\" id=\"opcao_os_maq\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_maq\">{$tdb[ORDEM]['MID_MAQUINA']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][conj]\" id=\"opcao_os_conj\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_conj\">{$tdb[ORDEM]['MID_CONJUNTO']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][equip]\" id=\"opcao_os_equip\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_equip\">{$tdb[ORDEM]['MID_EQUIPAMENTO']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][nat]\" id=\"opcao_os_nat\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_nat\">{$tdb[ORDEM]['NATUREZA']}</label>
    
    <br clear=\"all\" />

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][sol]\" id=\"opcao_os_sol\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_sol\">{$tdb[ORDEM]['SOLICITANTE']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][resp]\" id=\"opcao_os_resp\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_resp\">{$tdb[ORDEM]['RESPONSAVEL']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][texto]\" id=\"opcao_os_texto\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_texto\">{$tdb[ORDEM]['TEXTO']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][moprev]\" id=\"opcao_os_moprev\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_moprev\">{$tdb[ORDEM_MO_ALOC]['DESC']}</label>
    
    <br clear=\"all\" />
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][mtprev]\" id=\"opcao_os_mtprev\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_mtprev\">{$tdb[ORDEM_MAT_PREVISTO]['DESC']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][atividade]\" id=\"opcao_os_atividade\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_atividade\">{$tdb[ORDEM_PREV]['DESC']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][defeito]\" id=\"opcao_os_defeito\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_defeito\">{$tdb[ORDEM]['DEFEITO']}</label>

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][causa]\" id=\"opcao_os_causa\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_causa\">{$tdb[ORDEM]['CAUSA']}</label>
    
    <br clear=\"all\" />

    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][solucao]\" id=\"opcao_os_solucao\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_solucao\">{$tdb[ORDEM]['SOLUCAO']}</label>
        
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][parada]\" id=\"opcao_os_parada\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_parada\">{$tdb[ORDEM_MAQ_PARADA]['DESC']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][maodeobra]\" id=\"opcao_os_maodeobra\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_maodeobra\">{$tdb[ORDEM_MADODEOBRA]['DESC']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][material]\" id=\"opcao_os_material\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_material\">{$tdb[ORDEM_MATERIAL]['DESC']}</label>
    
    <br clear=\"all\" />
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][custo]\" id=\"opcao_os_custo\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_custo\">{$tdb[ORDEM_CUSTOS]['DESC']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][pend]\" id=\"opcao_os_pend\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_pend\">{$tdb[PENDENCIAS]['DESC']}</label>
    
    <input class=\"campo_check\" type=\"checkbox\" name=\"opcao[os][reprog]\" id=\"opcao_os_reprog\" value=\"1\" checked=\"checked\" />
    <label for=\"opcao_os_reprog\">{$tdb[ORDEM_REPROGRAMA]['DESC']}</label>
    
    </fieldset>
    
    <fieldset><legend>{$ling['ordernar']}</legend>
    <label for=\"filtro_ordem\">{$ling['rel_desc_campo_considerar']}:</label>
    <select name=\"filtro_ordem\" id=\"filtro_ordem\" class=\"campo_select\">
    <option value=\"1\">{$tdb[ORDEM]['NUMERO']}</option>
    <option value=\"2\">{$tdb[ORDEM]['MID_MAQUINA']}</option>
    <option value=\"3\" selected=\"selected\">{$tdb[ORDEM]['DATA_ABRE']}</option>
    <option value=\"4\">{$tdb[ORDEM]['DATA_PROG']}</option>
    <option value=\"5\">{$tdb[ORDEM]['DATA_INICIO']}</option>
    <option value=\"6\">{$tdb[ORDEM]['DATA_FINAL']}</option>
    </select>
    </fieldset>";

    echo "<fieldset><legend>{$ling['rel_desc_periodo']}</legend>
    <label for=\"filtro_periodo\">{$ling['rel_desc_campo_considerar']}:</label>
    <select name=\"filtro_periodo\" id=\"filtro_periodo\" class=\"campo_select\">
    <option value=\"1\" selected=\"selected\">{$tdb[ORDEM]['DATA_ABRE']}</option>
    <option value=\"2\">{$tdb[ORDEM]['DATA_PROG']}</option>
    <option value=\"3\">{$tdb[ORDEM]['DATA_INICIO']}</option>
    <option value=\"4\">{$tdb[ORDEM]['DATA_FINAL']}</option>
    <option value=\"5\">{$ling['data_aponta']}</option>
    </select>
    <br clear=\"all\" />
    ";
    FormData("{$ling['data_inicio']}:","datai",$_GET['datai'],"campo_label");
    echo "<br clear=\"all\" />";
    FormData("{$ling['data_fim']}:","dataf",$_GET['dataf'],"campo_label");
    echo "
    </fieldset>";
    echo "<fieldset>
    <legend>".$ling['papel_orientacao']."</legend>
    <input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
    <label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
    <br clear=\"all\" />
    <input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
    <label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
    </fieldset>
    <br />
    <input type=\"hidden\" name=\"tb\" value=\"$tb\" />
    <input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
    <input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
    </form><br />
    </div>
    </div>
    </body>
    </html>";
}



?>
