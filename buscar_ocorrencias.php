<?
/**
* Buscar Ocorr�ncias
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package ordens
*/
// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
	die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
	die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
	die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
	die ($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
	die ($ling['bd01']);
}
// Autentifica��o
elseif (!require("lib/autent.php")) {
	die ($ling['autent01']);
}
// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
	$manusis['tema']="padrao";
}

// Variaveis de direcionamento
$texto=LimpaTexto($_GET['texto']);

$por="%";
$cond="LIKE";
$texto=str_replace(" ",$por,$texto);



$filtro=(int)$_GET['filtro'];
$filtrostatus=$_GET['filtrostatus'];
if ($_GET['filtrostatus'] != "") {
	if ($filtrostatus == 0) {
		$filtro_status =" AND ordem.STATUS = '2'";
	}
	elseif ($filtrostatus != 0) {
		$filtro_status =" AND ordem.STATUS = '$filtrostatus'";
	}
}
$filtromaq=(int)$_GET['filtromaq'];
if ($filtromaq != 0) {
	$filtro_status =" AND ordem.MID_MAQUINA = '$filtromaq'";
}
$qtdpor=(int)$_GET['qtdpor'];
if ($qtdpor == 0) {
	$qtdpor=30;
}


$datai=$_GET['datai'];
$dataf=$_GET['dataf'];

$di=explode("/",$datai);
$df=explode("/",$dataf);
if ((int)$di[0] == 0) {
	$datai="01/01/".date("Y");
}
if ((int)$df[0] == 0) {
	$dataf="31/12/".date("Y");
}
if ($datai != "") {
	$di=explode("/",$datai);
	$df=explode("/",$dataf);
	if (!checkdate ((int)$di[1], (int)$di[0], (int)$di[2])) {
		$erromsg.="<strong><font color=\"red\">".$ling['data_inicial_invalida']." ($datai)</font></strong><br>";
	}
	if (!checkdate ((int)$df[1], (int)$df[0], (int)$df[2])) {
		$erromsg.="<strong><font color=\"red\">".$ling['data_final_invalida']." ($dataf)</font></strong><br>";
	}
	$data_sqli=$di[2]."-".$di[1]."-".$di[0];
	$data_sqlf=$df[2]."-".$df[1]."-".$df[0];
}


echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']} - {$ling['busca_histo_servi']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>
<script type=\"text/javascript\">
function mostrafiltro(){
	filtro=document.getElementById('dfiltro');
	if (filtro.style.visibility == 'visible') {
		filtro.style.visibility = 'hidden';
		filtro.style.display = 'none';
	}
	else {
		filtro.style.visibility='visible';
		filtro.style.display='block';
	}
}
</script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>".$ling['busca_histo_servi']."
</div>
<div id=\"corpo_relatorio\" align=\"center\">
<form action=\"buscar_ocorrencias.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<br /> 
<div align=\"center\" style=\"text-align:center;font-size:13px;\" />
".$ling['sistema_busca_somente_ordens_realizada_prog']."<br /> <br />
</div>
<div align=\"center\" style=\"text-align:center;font-size:13px; width:500px;border:1px solid gray;padding:5px\" />
<input style=\"font-size:12px;\" size=\"50\" id=\"texto\" name=\"texto\" value=\"".stripslashes($_GET['texto'])."\" class=\"campo_text\" /> <a style=\"font-size:10px; font-color:gray; text-decoration:none;	cursor: hand;\" href=\"javascript:return\" onclick=\"mostrafiltro()\">{$ling['filtros']} >> </a><br />
<div align=\"center\">";
if (($filtro == 0) and ($_GET['datai'] == "")){
	echo "<div id=\"dfiltro\" align=\"left\" style=\"padding:3px;margin-top:3px;width:370px;border-top:1px solid gray;visibility:hidden; display:none\">";
}
else {
	echo "<div id=\"dfiltro\" align=\"left\" style=\"padding:3px;margin-top:3px;width:370px;border-top:1px solid gray;visibility=visible\">";
}
echo "
	<label for=\"filtro\" class=\"campo_label\"> {$ling['usar_somente_este_campo']}: &nbsp;&nbsp;</label>
	<select class=\"campo_select\" name=\"filtro\" id=\"filtro\">
	<option value=\"0\">".$ling['todos']."</option>
	<option value=\"1\">".$ling['ativi_preventiva']."</option>
	<option value=\"2\">".$ling['ativi_de_rota']."</option>
	<option value=\"3\">".$ling['plano_preventiva']."</option>
	<option value=\"4\">".$ling['plano_rotas']."</option>
	<option value=\"5\">{$tdb[ORDEM]['MID_MAQUINA']}</option>
	<option value=\"6\">{$tdb[ORDEM]['MID_CONJUNTO']}</option>
	<option value=\"7\">{$tdb[ORDEM]['DEFEITO']}</option>
	<option value=\"8\">{$tdb[ORDEM]['CAUSA']}</option>
	<option value=\"9\">{$tdb[ORDEM]['SOLUCAO']}</option>
	<option value=\"10\">".$ling['prog_material']."</option>
	</select><br clear=\"all\" /><script>
	filtro=document.getElementById('filtro');
	filtro.options[$filtro].selected='selected';
	</script>";

echo "<label for=\"filtrostatus\" class=\"campo_label\"> {$ling['usar_somente_este_campo']}: &nbsp;&nbsp;</label>
	<select class=\"campo_select\" name=\"filtrostatus\" id=\"filtrostatus\">
	<option value=\"0\">".$ling['TODAS']."</option>
	<option value=\"1\">".$ling['abertas']."</option>
	<option value=\"2\">".$ling['fechadas']."</option>
	<option value=\"3\">".$ling['canceladas']."</option>


	</select><script>
	filtro=document.getElementById('filtrostatus');
	filtro.options[$filtrostatus].selected='selected';
	</script><br clear=\"all\" />";
echo "<label for=\"qtdpor\" class=\"campo_label\">{$ling['qtd_reg']}: &nbsp;&nbsp;</label>
	<input type=\"text\" size=\"2\" class=\"campo_text\" name=\"qtdpor\" id=\"qtdpor\" value=\"$qtdpor\">
<br clear=\"all\" />";
FormData("{$ling['data_inicio']}:","datai",$datai,"campo_label2");
echo "<br clear=\"all\" />";
FormData("{$ling['data_fim']}:","dataf",$dataf,"campo_label2");
echo "
	</div></div>


</div><br />
<input style=\"font-size:12px;\" class=\"botao\" type=\"submit\" name=\"botao\" value=\" {$ling['busca_histo_servi']} \" />
</form><br /><br />
";

// ATIVIDADE PREVENTIVA
if ($filtro == 1) {
	$sql="select ordem.NUMERO,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.MID_MAQUINA,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem,ordem_prev
WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
ordem_prev.MID_ORDEM = ordem.MID AND ordem_prev.TAREFA $cond '$por{$texto}$por' 
$filtro_status
ORDER BY ordem.DATA_PROG DESC";

}
// ATIVIDADE DE ROTA
elseif ($filtro == 2) {
	$sql="select ordem.NUMERO,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.MID_MAQUINA,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem,ordem_prev
WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
ordem_lub.MID_ORDEM = ordem.MID AND ordem_lub.TAREFA $cond '$por{$texto}$por' 
$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}
// PLANO PADRAO
elseif ($filtro == 3) {
	$sql="select ordem.NUMERO,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.MID_MAQUINA,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem,programacao INNER JOIN plano_padrao ON (programacao.MID_PLANO = plano_padrao.MID)
WHERE programacao.MID = ordem.MID_PROGRAMACAO AND programacao.TIPO = 1 AND ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
plano_padrao.DESCRICAO $cond '$por{$texto}$por' 
$filtro_status
ORDER BY ordem.DATA_PROG DESC";

}

// PLANO ROTAS
elseif ($filtro == 4) {
	$sql="select ordem.NUMERO,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.MID_MAQUINA,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem,programacao INNER JOIN plano_rotas ON (programacao.MID_PLANO = plano_rotas.MID)
WHERE programacao.MID = ordem.MID_PROGRAMACAO AND programacao.TIPO = 2 AND ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
plano_rotas.DESCRICAO $cond '$por{$texto}$por' 
$filtro_status
ORDER BY ordem.DATA_PROG DESC";

}

// MAQUINAS
elseif ($filtro == 5) {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN maquinas ON (maquinas.MID = ordem.MID_MAQUINA)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
(maquinas.COD $cond '$por{$texto}$por' OR maquinas.DESCRICAO $cond '$por{$texto}$por')

$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}
// CONTJUNO
elseif ($filtro == 6) {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN maquinas_conjunto ON (maquinas_conjunto.MID = ordem.MID_CONJUNTO)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
(maquinas_conjunto.TAG $cond '$por{$texto}$por' OR maquinas_conjunto.DESCRICAO $cond '$por{$texto}$por')

$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}

elseif ($filtro == 7) {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN defeito ON (defeito.MID = ordem.DEFEITO)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
defeito.DESCRICAO $cond '$por{$texto}$por'

$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}

elseif ($filtro == 8) {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN causa ON (causa.MID = ordem.DEFEITO)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
causa.DESCRICAO $cond '$por{$texto}$por'

$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}

elseif ($filtro == 9) {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN solucao ON (solucao.MID = ordem.SOLUCAO)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
solucao.DESCRICAO $cond '$por{$texto}$por'

$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}
// MATERIAL
elseif ($filtro == 10) {
	$sql="select ordem.NUMERO,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.MID_MAQUINA,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem,ordem_material INNER JOIN materiais ON (materiais.MID = ordem_material.MID_MATERIAL)
WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND 
ordem_material.MID_ORDEM = ordem.MID AND (materiais.DESCRICAO $cond '$por{$texto}$por' OR materiais.COD $cond '$por{$texto}$por')
$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}
else {
	$sql="select ordem.NUMERO,
ordem.MID_MAQUINA,
ordem.TIPO,
ordem.TIPO_SERVICO,
ordem.NUMERO,
ordem.DATA_PROG,
ordem.DATA_INICIO,
ordem.DATA_FINAL,
ordem.STATUS,
ordem.MID
FROM ordem INNER JOIN maquinas ON (maquinas.MID = ordem.MID_MAQUINA)

WHERE ordem.DATA_PROG >= '$data_sqli' AND ordem.DATA_PROG <= '$data_sqlf' AND (
(maquinas.COD $cond '$por{$texto}$por' OR maquinas.DESCRICAO $cond '$por{$texto}$por') OR

ordem.DEFEITO_TEXTO $cond '$por{$texto}$por' OR
ordem.CAUSA_TEXTO $cond '$por{$texto}$por' OR
ordem.SOLUCAO_TEXTO $cond '$por{$texto}$por' OR
ordem.NUMERO $cond '$por{$texto}$por' OR
ordem.TEXTO $cond '$por{$texto}$por'
)
$filtro_status
ORDER BY ordem.DATA_PROG DESC";
}

$resultado=$dba[0] -> SelectLimit($sql,$qtdpor,0);
if (!$resultado) {
	erromsg($dba[0] -> ErrorMsg());
}
else {
	echo "<p align=\"center\" style=\"font-size:12px\">{$ling['clic_filtrar_obj']}</p>";

	echo "<div id=\"lt_tabela\"><table width=\"100%\">
	<tr>
	<th></th>
	<th>{$ling['numero']}</th>
	<th>{$ling['tipo']}</th>
	<th>{$ling['rel_desc_obj2_min']}</th>
	<th>{$ling['rel_desc_dt_prog_min']}</th>
	<th>{$ling['data_inicio']}</th>
	<th>{$ling['data_inicio']}</th>
	<th>{$ling['ver']}</th>
	</tr>
	";
	$i=0;
	while (!$resultado->EOF) {
		$campo=$resultado->fields;
		if ((int)$campo['TIPO'] == 0) {
			$stipo=VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],0);
		}
		else {
			$stipo=VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],0);
		}
		if ((int)$campo['STATUS'] == 1) {
			$smig="osabertas.png";
		}
		if ((int)$campo['STATUS'] == 2) {
			$smig="osfechadas.png";
		}
		if ((int)$campo['STATUS'] == 3) {
			$smig="oscancel.png";
		}
		$dataprog=NossaData($campo['DATA_PROG']);
		if ($campo['DATA_INICIO'] != "") {
			$datainicial=NossaData($campo['DATA_INICIO']);
		}
		if ($campo['DATA_FINAL'] != "") {
			$datafinal=NossaData($campo['DATA_FINAL']);
		}
		echo "<tr>
		<td><img src=\"imagens/icones/22x22/$smig\" alt=\"$smig\" /></td>
		<td>{$campo['NUMERO']}</td>
		<td>$stipo</td>
		<td><a href=\"buscar_ocorrencias.php?qtdpor=$qtdpor&texto=$texto&filtro=$filtro&filtrostatus=$filtrostatus&datai=$datai&dataf=$dataf&botao=1&filtromaq={$campo['MID_MAQUINA']}\">".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],0)."</a></td>
		<td>$dataprog</td>
		<td>$datainicial</td>
		<td>$datafinal</td>
		<td><a href=\"javascript:janela('detalha_ord.php?busca=".$campo['MID']."', 'parm', 500,400)\"><img src=\"imagens/icones/22x22/visualizar.png\" border=\"0\" title=\" {$ling['ord_visualizar_os']} \" alt=\" \" /></a></td>
		</tr>";
		unset($dataprog);
		unset($datainicial);
		unset($datafinal);
		unset($stipo);
		$resultado->MoveNext();
		$i++;
	}

	echo "</table></div>";
	if ($i == 0){
		echo "<p align=\"center\" style=\"font-size:12px\"><strong>".$ling['nenhum_reg_encontrado']."</strong></p>";
	}
}
echo "


</div>

</div>
</body>
</html>";


?>
